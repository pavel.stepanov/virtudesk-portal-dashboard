<?php

return [

    'timezone' => [
        'EST' => 'America/New_York',
        'PST' => 'America/Los_Angeles',
        'CST' => 'America/Chicago',
        'MST' => 'America/Denver',
        'MST-Phoenix' => 'America/Phoenix',
        'MNL' => 'Asia/Manila'
    ],

    'shift' => [
        '1st_shift' => '1st Shift',
        '2nd_shift' => '2nd Shift',
        '3rd_shift' => '3rd Shift',
        '4th_shift' => '4th Shift',
        '5th_shift' => '5th Shift',
        '6th_shift' => '6th Shift',
    ],

    'request_type' => [
        'vacation_leave' => 'Vacation Leave',
        'overtime' => 'Overtime',
        'shift_change' => 'Shift Change',
        'sick_leave' => 'Sick Leave',
    ],

    'request_status' => [
        'approved' => 'Approved',
        'pending' => 'Pending',
        'rejected' => 'Rejected',
    ],

    'keyboard_activity' => [
        'Low' => 50,
        'Medium' => 1000,
        'High' => 5000,
    ],

    'mouse_activity' => [
        'Low' => 500,
        'Medium' => 3000,
        'High' => 10000,
    ],

    'noreply_email' => 'noreply@virtudesk.com',

    'siteinfo' => [
        'email' => 'worforce@myvirtudesk.com',
        'phone' => '+6312345687',
    ],

    'tracker_info' => [
        'download_url' => 'http://google.com',
        'guide_url' => 'http://google.com',
    ],

    'tasks_color' => [
        'Prospecting' => '#f56954',
        'Marketing' => '#00a65a',
        'Coach' => '#f39c12',
        'Client Services' => '#00c0ef',
        'Training' => '#3c8dbc',
        'Recruitment' => '#d2d6de',
    ],

    'terms_and_condition' => '/docs/Terms-and-Conditionsupon-logging-in_Version-3_05052018.pdf',
];