<?php

	return [
		'super_administrator' => [
			[
				'label'  => 'Request & Concerns',
				'icon'   => 'fa-ticket',
				'path'   => 'dashboard/requests',
				'selected_parent_path' => 'dashboard/requests',
				'selected_nav_path' => 'dashboard/requests/take-action',
				'child'  => FALSE,
			],
			[
				'label'  => 'Messages',
				'icon'   => 'fa-ticket',
				'path'   => 'dashboard/messages',
				'selected_parent_path' => 'dashboard/messages',
				'selected_nav_path' => 'dashboard/messages/take-action-messages',
				'child'  => FALSE,
			],
			[
				'label'  => 'All Screenshots',
				'icon'   => 'fa-desktop',
				'path'   => 'dashboard/screenshots-all',
				'child'  => FALSE,
			],
			[
				'label'  => 'All Users',
				'icon'   => 'fa-user',
				'path'   => '#',
				'selected_parent_path' => 'dashboard/users',
				'child'  => [
					'dashboard/users' => [
						'label'  => 'List Users',
						'icon'   => 'fa-list',
						'selected_nav_path' => 'dashboard/users/edit',
					],
					'dashboard/users/create' => [
						'label'  => 'Create User',
						'icon'   => 'fa-plus',
					]
				],
			],
			[
				'label'  => 'Clients',
				'icon'   => 'fa-address-book-o',
				'path'   => '#',
				'selected_parent_path' => 'dashboard/clients',
				'child'  => [
					'dashboard/clients?active=1' => [
						'label'  => 'Active Clients',
						'icon'   => 'fa-list',
						'selected_parent_path' => 'dashboard/clients',
						'selected_nav_path' => 'dashboard/clients/edit'
					],	
					'dashboard/clients?active=0' => [
						'label'  => 'Inactive Clients',
						'icon'   => 'fa-list',
						'selected_parent_path' => 'dashboard/clients',
						'selected_nav_path' => 'dashboard/clients/edit'
					],
					'dashboard/clients/create' => [
						'label'  => 'Add Client',
						'icon'   => 'fa-plus',
					],
				],
			],
			[
				'label'  => 'Virtual Assistants',
				'icon'   => 'fa-user-circle-o',
				'path'   => '#',
				'selected_parent_path' => 'dashboard/va',
				'child'  => [
					'dashboard/va' => [
						'label'  => 'List VA',
						'icon'   => 'fa-list',
						'selected_parent_path' => 'dashboard/va',
						'selected_nav_path' => 'dashboard/va/edit',
					],
					'dashboard/va/create' => [
						'label'  => 'Add VA',
						'icon'   => 'fa-plus',
					],
					'dashboard/online-va' => [
						'label'  => 'Online VA',
						'icon'   => 'fa-list-ul',
					],
					'dashboard/va-work-duration' => [
						'label'  => 'Work Duration',
						'icon'   => 'fa-history',
					],
					'dashboard/schedules' => [
						'label'  => 'Schedules',
						'icon'   => 'fa-calendar',
					],
					'dashboard/attendance' => [
						'label'  => 'Attendance',
						'icon'   => 'fa-check-square',
					],
					'dashboard/screenshots' => [
						'label'  => 'Screenshots',
						'icon'   => 'fa-desktop',
					],
					'dashboard/timeline' => [
						'label'  => 'Timeline',
						'icon'   => 'fa-area-chart',
					],
					'dashboard/notifications' => [
						'label'  => 'Notifications',
						'icon'   => 'fa-exclamation',
					],
					'dashboard/idle' => [
						'label'  => 'Idle',
						'icon'   => 'fa-question',
					],
					'dashboard/scheduled-va' => [
						'label'  => 'Scheduled VA',
						'icon'   => 'fa-bell',
					],
				],
			],
			[
				'label'  => 'Projects',
				'icon'   => 'fa-folder',
				'path'   => '#',
				'selected_parent_path' => 'dashboard/projects',
				'child'  => [
					'dashboard/projects' => [
						'label'  => 'List projects',
						'icon'   => 'fa-list',
						'selected_nav_path' => 'dashboard/projects/edit',
					],
					'dashboard/projects/create' => [
						'label'  => 'Create Project',
						'icon'   => 'fa-plus',
					],
				],
			],
			[
				'label'  => 'Tasks',
				'icon'   => 'fa-pencil',
				'path'   => '#',
				'selected_parent_path' => 'dashboard/client-tasks',
				'child'  => [
					'dashboard/taskboard' => [
						'label'  => 'Taskboard',
						'icon'   => 'fa-clone',
						'selected_nav_path' => 'dashboard/taskboard',
					],
					'dashboard/client-tasks' => [
						'label'  => 'Open Tasks',
						'icon'   => 'fa-list',
						'selected_nav_path' => 'dashboard/client-tasks/edit',
					],
					'dashboard/client-tasks/archive' => [
						'label'  => 'Archived Tasks',
						'icon'   => 'fa-list',
					],
					'dashboard/client-tasks/deleted' => [
						'label'  => 'Deleted Tasks',
						'icon'   => 'fa-list',
					],
					'dashboard/client-tasks/create' => [
						'label'  => 'Create Client Tasks',
						'icon'   => 'fa-plus',
					],
				],
			],

			[
				'label'  => 'Admin Tasks',
				'icon'   => 'fa-pencil',
				'path'   => '#',
				'selected_parent_path' => 'dashboard/tasks',
				'child'  => [
					'dashboard/tasks' => [
						'label'  => 'List Admin Tasks',
						'icon'   => 'fa-list',
						'selected_nav_path' => 'dashboard/tasks/edit',
					],
					'dashboard/tasks/create' => [
						'label'  => 'Create Admin Tasks',
						'icon'   => 'fa-plus',
					],
				],
			],

			[
				'label'  => 'Teams',
				'icon'   => 'fa-users',
				'path'   => '#',
				'selected_parent_path' => 'dashboard/teams',
				'child'  => [
					'dashboard/teams' => [
						'label'  => 'List Teams',
						'icon'   => 'fa-list',
						'selected_parent_path' => 'dashboard/teams',
						'selected_nav_path' => 'dashboard/teams/edit',
					],
					'dashboard/teams/create' => [
						'label'  => 'Add Team',
						'icon'   => 'fa-plus',
					],
				],
			],	
			[
				'label'  => 'Settings',
				'icon'   => 'fa-cog',
				'path'   => '#',
				'selected_parent_path' => 'dashboard/settings',
				'child'  => [
					'dashboard/settings' => [
						'label'  => 'List Settings',
						'icon'   => 'fa-list',
						'selected_nav_path' => 'dashboard/settings/edit',
					],
					'dashboard/settings/create' => [
						'label'  => 'Create Setting',
						'icon'   => 'fa-plus',
					],
				],
			],
			[
				'label'  => 'Tools',
				'icon'   => 'fa-wrench',
				'path'   => '#',
				'selected_parent_path' => 'Tools',
				'child'  => [
					'dashboard/roles' => [
						'label'  => 'Roles',
						'icon'   => 'fa-user-secret',
						'selected_nav_path' => 'dashboard/role-permissions',
					],
					'dashboard/permissions' => [
						'label'  => 'Permissions',
						'icon'   => 'fa-lock'
					],
					'dashboard/daily-reports' => [
						'label'  => 'Daily Reports',
						'icon'   => 'fa-print',
					],
					'dashboard/summary-reports' => [
						'label'  => 'Summary Reports',
						'icon'   => 'fa-file',
					],
					'dashboard/attendance-reports' => [
						'label'  => 'Attendance Reports',
						'icon'   => 'fa-check-square',
					],
					'dashboard/timesheets' => [
						'label'  => 'Timesheets',
						'icon'   => 'fa-arrow-circle-o-down',
					],
					'dashboard/billing' => [
						'label'  => 'Billing',
						'icon'   => 'fa-calculator',
					],
					'dashboard/activity-log' => [
						'label'  => 'Activity Logs',
						'icon'   => 'fa-eye',
					],
				],
			],
		],

		'administrator' => [
			[
				'label'  => 'Request & Concerns',
				'icon'   => 'fa-ticket',
				'path'   => 'dashboard/requests',
				'selected_parent_path' => 'dashboard/requests',
				'selected_nav_path' => 'dashboard/requests/take-action',
				'child'  => FALSE,
			],
			[
				'label'  => 'All Screenshots',
				'icon'   => 'fa-desktop',
				'path'   => 'dashboard/screenshots-all',
				'child'  => FALSE,
			],
			[
				'label'  => 'All Users',
				'icon'   => 'fa-user',
				'path'   => '#',
				'selected_parent_path' => 'dashboard/users',
				'child'  => [
					'dashboard/users' => [
						'label'  => 'List Users',
						'icon'   => 'fa-list',
						'selected_nav_path' => 'dashboard/users/edit',
					],
					'dashboard/users/create' => [
						'label'  => 'Create User',
						'icon'   => 'fa-plus',
					]
				],
			],
			[
				'label'  => 'Clients',
				'icon'   => 'fa-address-book-o',
				'path'   => '#',
				'selected_parent_path' => 'dashboard/clients',
				'child'  => [
					'dashboard/clients' => [
						'label'  => 'List Clients',
						'icon'   => 'fa-list',
						'selected_nav_path' => 'dashboard/clients/edit'
					],
					'dashboard/clients/create' => [
						'label'  => 'Add Client',
						'icon'   => 'fa-plus',
					],
				],
			],
			[
				'label'  => 'Virtual Assistants',
				'icon'   => 'fa-user-circle-o',
				'path'   => '#',
				'selected_parent_path' => 'dashboard/va',
				'child'  => [
					'dashboard/va' => [
						'label'  => 'List VA',
						'icon'   => 'fa-list',
						'selected_parent_path' => 'dashboard/va',
						'selected_nav_path' => 'dashboard/va/edit',
					],
					'dashboard/va/create' => [
						'label'  => 'Add VA',
						'icon'   => 'fa-plus',
					],
					'dashboard/online-va' => [
						'label'  => 'Online VA',
						'icon'   => 'fa-list-ul',
					],
					'dashboard/va-work-duration' => [
						'label'  => 'Work Duration',
						'icon'   => 'fa-history',
					],
					'dashboard/schedules' => [
						'label'  => 'Schedules',
						'icon'   => 'fa-calendar',
					],
					'dashboard/attendance' => [
						'label'  => 'Attendance',
						'icon'   => 'fa-check-square',
					],
					'dashboard/screenshots' => [
						'label'  => 'Screenshots',
						'icon'   => 'fa-desktop',
					],
					'dashboard/timeline' => [
						'label'  => 'Timeline',
						'icon'   => 'fa-area-chart',
					],
					'dashboard/notifications' => [
						'label'  => 'Notifications',
						'icon'   => 'fa-exclamation',
					],
					'dashboard/idle' => [
						'label'  => 'Idle',
						'icon'   => 'fa-question',
					],
					'dashboard/scheduled-va' => [
						'label'  => 'Scheduled VA',
						'icon'   => 'fa-bell',
					],
				],
			],
			[
				'label'  => 'Teams',
				'icon'   => 'fa-users',
				'path'   => '#',
				'selected_parent_path' => 'dashboard/teams',
				'child'  => [
					'dashboard/teams' => [
						'label'  => 'List Teams',
						'icon'   => 'fa-list',
						'selected_parent_path' => 'dashboard/teams',
						'selected_nav_path' => 'dashboard/teams/edit',
					],
					'dashboard/teams/create' => [
						'label'  => 'Add Team',
						'icon'   => 'fa-plus',
					],
				],
			],		
			[
				'label'  => 'Tools',
				'icon'   => 'fa-wrench',
				'path'   => '#',
				'selected_parent_path' => 'Tools',
				'child'  => [
					'dashboard/settings' => [
						'label'  => 'Settings',
						'icon'   => 'fa-cog',
					],
					'dashboard/daily-reports' => [
						'label'  => 'Daily Reports',
						'icon'   => 'fa-print',
					],
					'dashboard/summary-reports' => [
						'label'  => 'Summary Reports',
						'icon'   => 'fa-file',
					],
					'dashboard/attendance-reports' => [
						'label'  => 'Attendance Reports',
						'icon'   => 'fa-check-square',
					],
					'dashboard/timesheets' => [
						'label'  => 'Timesheet',
						'icon'   => 'fa-arrow-circle-o-down',
					],
					'dashboard/activity-log' => [
						'label'  => 'Activity Logs',
						'icon'   => 'fa-eye',
					],
				],
			],
		],

		'client' => [
			[
				'label'  => 'Virtual Assistants',
				'icon'   => 'fa-user-circle-o',
				'path'   => '#',
				'child'  => [
					'dashboard/va' => [
						'label'  => 'List VA',
						'icon'   => 'fa-address-book-o',
					],
					'dashboard/online-va' => [
						'label'  => 'Who\'s Online',
						'icon'   => 'fa-list-ul',
					]
				],
			],
			[
				'label'  => 'Tasks',
				'icon'   => 'fa-pencil',
				'path'   => '#',
				'selected_parent_path' => 'dashboard/client-tasks',
				'child'  => [
					'dashboard/client-tasks' => [
						'label'  => 'List Tasks',
						'icon'   => 'fa-list',
						'selected_nav_path' => 'dashboard/client-tasks/edit',
					],
					'dashboard/client-tasks/create' => [
						'label'  => 'Create Tasks',
						'icon'   => 'fa-plus',
					],
				],
			],
			[
				'label'  => 'Schedules',
				'icon'   => 'fa-calendar',
				'path'   => 'dashboard/schedules',
				'child'  => FALSE,
			],
			[
				'label'  => 'Attendance',
				'icon'   => 'fa-check-square',
				'path'   => 'dashboard/attendance',
				'child'  => FALSE,
			],
			[
				'label'  => 'Screenshots',
				'icon'   => 'fa-desktop',
				'path'   => 'dashboard/screenshots',
				'child'  => FALSE,
			],
			[
				'label'  => 'Bills',
				'icon'   => 'fa-calculator',
				'path'   => 'client/bills',
				'child'  => FALSE,
			],
		],

		'va' => [
			[
				'label'  => 'Schedules',
				'icon'   => 'fa-calendar',
				'path'   => 'dashboard/schedules',
				'child'  => FALSE,
			],
			[
				'label'  => 'Attendance',
				'icon'   => 'fa-check-square',
				'path'   => 'dashboard/attendance',
				'child'  => FALSE,
			],

			[
				'label'  => 'Reports',
				'icon'   => 'fa-file-text-o',
				'path'   => 'va-dashboard/reports',
				'child'  => FALSE,
			],
			[
				'label'  => 'Request & Concerns',
				'icon'   => 'fa-edit',
				'path'   => '#',
				'selected_parent_path' => 'dashboard/requests',
				'child'  => [
					'dashboard/requests' => [
						'label'  => 'Status',
						'icon'   => 'fa-circle-o',
						'selected_nav_path' => 'dashboard/requests/info'
					],					
					'dashboard/requests/leave' => [
						'label'  => 'Vacation Leaves',
						'icon'   => 'fa-circle-o',
					],
					'dashboard/requests/overtime' => [
						'label'  => 'Overtime Request',
						'icon'   => 'fa-circle-o',
					],
					'dashboard/requests/sick-leave' => [
						'label'  => 'Sick Leave Approval',
						'icon'   => 'fa-circle-o',
					],
					'dashboard/requests/shift' => [
						'label'  => 'Shift Change Requests',
						'icon'   => 'fa-circle-o',
					],
				],
			],
		],

		'manager' => [
			[
				'label'  => 'All Screenshots',
				'icon'   => 'fa-desktop',
				'path'   => 'dashboard/screenshots-all',
				'child'  => FALSE,
			],
			[
				'label'  => 'Projects',
				'icon'   => 'fa-folder',
				'path'   => '#',
				'selected_parent_path' => 'dashboard/projects',
				'child'  => [
					'dashboard/projects' => [
						'label'  => 'List projects',
						'icon'   => 'fa-list',
						'selected_nav_path' => 'dashboard/projects/edit',
					],
					'dashboard/projects/create' => [
						'label'  => 'Create Project',
						'icon'   => 'fa-plus',
					],
				],
			],
			[
				'label'  => 'Clients',
				'icon'   => 'fa-address-book-o',
				'path'   => '#',
				'selected_parent_path' => 'dashboard/clients',
				'child'  => [
					'dashboard/clients' => [
						'label'  => 'List Clients',
						'icon'   => 'fa-list',
						'selected_parent_path' => 'dashboard/clients',
						'selected_nav_path' => 'dashboard/clients/edit'
					],
					'dashboard/clients/create' => [
						'label'  => 'Add Client',
						'icon'   => 'fa-plus',
					],
				],
			],
			[
				'label'  => 'Tasks',
				'icon'   => 'fa-pencil',
				'path'   => '#',
				'selected_parent_path' => 'dashboard/client-tasks',
				'child'  => [
					'dashboard/taskboard' => [
						'label'  => 'Taskboard',
						'icon'   => 'fa-clone',
					],
					'dashboard/client-tasks' => [
						'label'  => 'Open Tasks',
						'icon'   => 'fa-list',
						'selected_nav_path' => 'dashboard/client-tasks/edit',
					],
					'dashboard/client-tasks/archive' => [
						'label'  => 'Archived Tasks',
						'icon'   => 'fa-list',
					],
					'dashboard/client-tasks/deleted' => [
						'label'  => 'Deleted Tasks',
						'icon'   => 'fa-list',
					],
					'dashboard/client-tasks/create' => [
						'label'  => 'Create Tasks',
						'icon'   => 'fa-plus',
					],
				],
			],
			[
				'label'  => 'Virtual Assistants',
				'icon'   => 'fa-user-circle-o',
				'path'   => '#',
				'child'  => [
					'dashboard/va' => [
						'label'  => 'List VA',
						'icon'   => 'fa-address-book-o',
					],
					'dashboard/online-va' => [
						'label'  => 'Who\'s Online',
						'icon'   => 'fa-list-ul',
					],
					'dashboard/screenshots' => [
						'label'  => 'Screenshots',
						'icon'   => 'fa-desktop',
					],
				],
			],
			[
				'label'  => 'Schedules',
				'icon'   => 'fa-calendar',
				'path'   => 'dashboard/schedules',
				'child'  => FALSE,
			],
			[
				'label'  => 'Attendance',
				'icon'   => 'fa-check-square',
				'path'   => 'dashboard/attendance',
				'child'  => FALSE,
			],
			[
				'label'  => 'Reports',
				'icon'   => 'fa-wrench',
				'path'   => '#',
				'selected_parent_path' => 'Reports',
				'child'  => [
					'dashboard/daily-reports' => [
						'label'  => 'Daily Reports',
						'icon'   => 'fa-print',
					],
					'dashboard/summary-reports' => [
						'label'  => 'Summary Reports',
						'icon'   => 'fa-print',
					],
				],
			],
			[
				'label'  => 'Request & Concerns',
				'icon'   => 'fa-edit',
				'path'   => '#',
				'selected_parent_path' => 'dashboard/requests',
				'child'  => [
					'dashboard/requests' => [
						'label'  => 'Status',
						'icon'   => 'fa-circle-o',
						'selected_nav_path' => 'dashboard/requests/info'
					],					
					'dashboard/requests/leave' => [
						'label'  => 'Vacation Leaves',
						'icon'   => 'fa-circle-o',
					],
					'dashboard/requests/overtime' => [
						'label'  => 'Overtime Request',
						'icon'   => 'fa-circle-o',
					],
					'dashboard/requests/sick-leave' => [
						'label'  => 'Sick Leave Approval',
						'icon'   => 'fa-circle-o',
					],
					'dashboard/requests/shift' => [
						'label'  => 'Shift Change Requests',
						'icon'   => 'fa-circle-o',
					],
				],
			],
		],

		'billing' => [
			[
				'label'  => 'Bulk Generate',
				'icon'   => 'fa-cogs',
				'path'   => 'dashboard/billing/bulk',
				'child'  => FALSE,
			],
			[
				'label'  => 'Billing',
				'icon'   => 'fa-calculator',
				'path'   => 'dashboard/billing',
				'child'  => FALSE,
			],
			[
				'label'  => 'Invoice',
				'icon'   => 'fa-money',
				'path'   => 'dashboard/invoice',
				'child'  => FALSE,
			],
			[
				'label'  => 'Clients',
				'icon'   => 'fa-address-book-o',
				'path'   => '#',
				'selected_parent_path' => 'dashboard/clients',
				'child'  => [
					'dashboard/clients' => [
						'label'  => 'List Clients',
						'icon'   => 'fa-list',
						'selected_parent_path' => 'dashboard/clients',
						'selected_nav_path' => 'dashboard/clients/edit'
					]
				],
			],
			[
				'label'  => 'Virtual Assistants',
				'icon'   => 'fa-user-circle-o',
				'path'   => '#',
				'selected_parent_path' => 'dashboard/va',
				'child'  => [
					'dashboard/va' => [
						'label'  => 'List VA',
						'icon'   => 'fa-list',
						'selected_parent_path' => 'dashboard/va',
						'selected_nav_path' => 'dashboard/va/edit',
					],
					'dashboard/summary-reports' => [
						'label'  => 'Summary Reports',
						'icon'   => 'fa-file',
						'selected_parent_path' => 'dashboard/va',
					],
				],
			],
		],
	];