let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

 //Only used in Dev server
mix.options({
    processCssUrls: true,
    imgLoaderOptions: {
      enabled: false,
    }
  }); 

mix.js('resources/assets/js/app.js', 'public/js')
    .sass('resources/assets/sass/app.scss', 'public/css')
    .sass('resources/assets/sass/client.scss', 'public/css')
    .copy('resources/assets/js/jqclock.min.js', 'public/js')
    .copy('resources/assets/js/jquery.timer.js', 'public/js')
    .copy('resources/assets/js/jquery.are-you-sure.js', 'public/js')
    .copy('resources/assets/js/jquery.sparkline.min.js', 'public/js')
    .copy('resources/assets/js/notify.min.js', 'public/js')
    .copy('resources/assets/sass/dataTables.checkboxes.css', 'public/css')
    .copy('resources/assets/js/dataTables.checkboxes.min.js', 'public/js')
    .copy('resources/assets/sass/bootstrap-clockpicker.min.css', 'public/css')
    .copy('resources/assets/sass/pdf.css', 'public/css')
    .copy('resources/assets/js/bootstrap-clockpicker.min.js', 'public/js')

    //  Timedly scripts
    .copy('resources/assets/timedly/vendor/jquery/dist/jquery.min.js', 'public/js')
    .scripts([
      // core
      'resources/assets/timedly/vendor/jquery/dist/jquery.min.js',
      'resources/assets/timedly/vendor/bootstrap/dist/js/bootstrap.bundle.min.js',
      'resources/assets/timedly/vendor/js-cookie/js.cookie.js',
      'resources/assets/timedly/vendor/jquery.scrollbar/jquery.scrollbar.min.js',
      'resources/assets/timedly/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js',
      'resources/assets/timedly/vendor/lavalamp/js/jquery.lavalamp.min.js',
      'resources/assets/timedly/js/components/vendor/bootstrap-multiselect.js',

      // optionals
      'resources/assets/timedly/vendor/chart.js/dist/Chart.min.js',
      'resources/assets/timedly/vendor/chart.js/dist/Chart.extension.js',
      'resources/assets/timedly/vendor/jvectormap-next/jquery-jvectormap.min.js',
      'resources/assets/timedly/js/vendor/jvectormap/jquery-jvectormap-world-mill.js',
      'resources/assets/timedly/vendor/datatables.net/js/jquery.dataTables.min.js',
      'resources/assets/timedly/vendor/datatables.net-bs4/js/dataTables.bootstrap4.min.js',
      'resources/assets/timedly/vendor/datatables.net-buttons/js/dataTables.buttons.min.js',
      'resources/assets/timedly/vendor/datatables.net-buttons/js/buttons.html5.min.js',
      'resources/assets/timedly/vendor/datatables.net-buttons/js/buttons.flash.min.js',
      'resources/assets/timedly/vendor/datatables.net-buttons/js/buttons.print.min.js',
      'resources/assets/timedly/vendor/datatables.net-select/js/dataTables.select.min.js',
      'resources/assets/timedly/vendor/jvectormap-next/jquery-jvectormap.min.js',
      'resources/assets/timedly/js/vendor/jvectormap/jquery-jvectormap-world-mill.js',
      'resources/assets/timedly/vendor/sweetalert2/dist/sweetalert2.min.js',
      'resources/assets/timedly/vendor/bootstrap-notify/bootstrap-notify.min.js',
      'resources/assets/timedly/vendor/select2/dist/js/select2.min.js',
      'resources/assets/timedly/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
      'resources/assets/timedly/vendor/nouislider/distribute/nouislider.min.js',
      'resources/assets/timedly/vendor/quill/dist/quill.min.js',
      'resources/assets/timedly/vendor/dropzone/dist/min/dropzone.min.js',
      'resources/assets/timedly/vendor/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js',
      'resources/assets/timedly/js/argon.js',
      'resources/assets/timedly/js/main.js',
      'resources/assets/js/bootstrap-clockpicker.min.js'
  ], 'public/js/timedly.js')

  .sass('resources/assets/timedly/scss/argon.scss', 'public/css/timedly')
  .sass('resources/assets/timedly/scss/custom.scss', 'public/css/timedly')

  .styles([
    'resources/assets/timedly/vendor/nucleo/css/nucleo.css',
    'resources/assets/timedly/css/argon.css',
    'resources/assets/timedly/css/bootstrap-multiselect.css',
    'resources/assets/timedly/css/flaticons.css',
    'resources/assets/timedly/vendor/datatables.net-bs4/css/dataTables.bootstrap4.min.css',
    'resources/assets/timedly/vendor/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css',
    'resources/assets/timedly/vendor/datatables.net-select-bs4/css/select.bootstrap4.min.css',
    'resources/assets/timedly/vendor/animate.css/animate.min.css',
    'resources/assets/timedly/vendor/sweetalert2/dist/sweetalert2.min.css',
    'resources/assets/sass/bootstrap-clockpicker.min.css',
  ], 'public/css/timedly/main.css')
  
  .copy('resources/assets/timedly/vendor/nucleo/fonts/nucleo-icons.eot', 'public/fonts')
  .copy('resources/assets/timedly/vendor/nucleo/fonts/nucleo-icons.svg', 'public/fonts')
  .copy('resources/assets/timedly/vendor/nucleo/fonts/nucleo-icons.ttf', 'public/fonts')
  .copy('resources/assets/timedly/vendor/nucleo/fonts/nucleo-icons.woff', 'public/fonts')
  .copy('resources/assets/timedly/vendor/nucleo/fonts/nucleo-icons.woff2', 'public/fonts')

  .copy('resources/assets/timedly/fonts/flaticon/Flaticon.eot', 'public/fonts')
  .copy('resources/assets/timedly/fonts/flaticon/Flaticon.ttf', 'public/fonts')
  .copy('resources/assets/timedly/fonts/flaticon/Flaticon.woff', 'public/fonts')
  .copy('resources/assets/timedly/fonts/flaticon/Flaticon.woff2', 'public/fonts')
  .copyDirectory('resources/assets/timedly/img', 'public/images/timedly');

/*
 |--------------------------------------------------------------------------
 | Browsersync
 |--------------------------------------------------------------------------
 |
 | An automation tool that makes web development faster.
 |
 */

mix.browserSync({
  proxy: 'vatimetracker.test'
});