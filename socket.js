var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var Redis = require('ioredis');
var sub = new Redis();
var pub = new Redis(); // IMPORTANT! ALWAYS JSON stringify when sending data packages.

sub.subscribe('broadcast', function (err, count) { });
sub.on('message', function (channel, message) {
    message = JSON.parse(message);
    console.log('channel', message.data.channel);
    if (typeof message.data.data.socket_id !== 'undefined' && message.data.data.socket_id !== null) {
        // emit to one
        if (message.data.channel.startsWith('user-ping-')) {
            let socketList = io.sockets.server.eio.clients;
            if (socketList[message.data.data.socket_id] === undefined) {
                pub.publish('user-auto-timeout', JSON.stringify({
                    'socket_id': message.data.data.socket_id,
                }));
            }
        } else {
            io.to(message.data.data.socket_id).emit(message.data.channel, message.data.data);
        }
    } else {
        // emit to all
        io.emit(message.data.channel, message.data.data);
    }
});

io.on('connect', function (socket) {
    console.log('new socket connected', socket.id);

    socket.on('user-connect', function (accessToken) {
        pub.publish('user-connect', JSON.stringify({
            'access_token': accessToken,
            'socket_id': socket.id,
        }));
    });

    socket.on('disconnect', function () {
        pub.publish('user-disconnect', JSON.stringify({
            'socket_id': socket.id,
        }));
    });
});

http.listen(3000, function () {
    console.log('Listening on Port 3000');
});