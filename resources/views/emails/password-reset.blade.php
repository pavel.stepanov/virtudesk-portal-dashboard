Greetings!
<br />
You have requested a password reset. If you did not request this, please ignore this email.
<br />
<br />
To reset your password, click on this link <a href="{{ $url }}?token={{ $token }}">{{ $url }}?token={{ $token }}</a>

