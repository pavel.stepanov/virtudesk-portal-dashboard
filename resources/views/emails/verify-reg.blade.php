Welcome to VIRTUDESK!
<br />
<br />
Please verify your registration by using going to <a href="{{ $verify_url }}">{{ $verify_url }}</a> <br />
Activation Code: {{ $verify_token }}
<br />
<br />
Or click this link <a href="{{ $verify_url }}?verify_token={{ $verify_token }}">{{ $verify_url }}?verify_token={{ $verify_token }}</a>

