<!DOCTYPE html>
<html>
    <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="shortcut icon" type="image/png" href="{{ asset('fe') }}/img/favicon.png" />
    <title>VIRTUDESK | @yield('page-title')</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">

    <!-- Google Font -->
    <link rel="stylesheet"
            href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

    @yield('meta')
    @stack('view-styles')
    @php
        $q = \Auth::user();
        if ($q->avatar != null) {
            $avatar = "/uploads/avatars/" . $q->avatar;
        } else {
            $avatar = "//www.gravatar.com/avatar/" . md5($q->email) . "?s=128&d=mm";
        }    
    @endphp
    </head>
    @if ($q->is('client'))
    @php
        $skin = "skin-green";
        $role = "Client";
    @endphp
    @endif
    @if ($q->is('super_administrator')||$q->is('administrator'))
        @php
            $skin = "skin-black-light";
            $role =  "Admin"
        @endphp
    @endif
    @if ($q->is('va'))
        @php
        $skin = "skin-blue";
        $role = "Portal";
        @endphp
    @endif
    @if ($q->is('manager')||$q->is('billing'))
        @php
        $skin = "skin-blue-light";
        $role = "Portal";
        @endphp
    @endif

    @if (Cookie::get('virtudesk-sidebar', '0') == '0')
    <body class="hold-transition {{$skin}} sidebar-mini" data-cookie="{{Cookie::get('virtudesk-sidebar','0')}}">
    @else
    <body class="hold-transition {{$skin}} sidebar-mini sidebar-collapse" data-cookie="{{Cookie::get('virtudesk-sidebar','0')}}">
    @endif
        <!-- Site wrapper -->
        <div class="wrapper">

            <header class="main-header">
                <!-- Logo -->
                <a href="/dashboard" class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini"><b>VD</b></span>
                <!-- logo for regular state and mobile devices -->
                <span class="logo-lg"><b>VIRTUDESK</b> {{$role}}</span>
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>

                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="{{$avatar}}" class="user-image" alt="User Image">
                        @php
                            $fullname = $q->first_name . " " . $q->last_name;
                        @endphp
                        <span class="hidden-xs">{{ $fullname }}</span>
                        </a>
                        <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="{{$avatar}}" class="img-circle" alt="User Image">

                            <p>
                            {{ $fullname }}
                            <small>Member since {{ date('M Y', strtotime($q->created_at)) }}</small>
                            </p>
                        </li>

                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                            <a href="/dashboard/users/profile" class="btn btn-default btn-flat">Profile</a>
                            </div>
                            <div class="pull-right">
                            <a href="/auth/logout" class="btn btn-default btn-flat">Sign out</a>
                            </div>
                        </li>
                        </ul>
                    </li>

                    </ul>
                </div>
                </nav>
            </header>

            <!-- =============================================== -->

            <!-- Left side column. contains the sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                        <img src="{{$avatar}}" class="img-circle" alt="User Image">
                        </div>
                        <div class="pull-left info">
                        <p>{{ $fullname }}</p>
                        <a href="#"><i class="fa fa-circle text-success"></i> Online - {{$q->timezone}}</a>
                        </div>
                    </div>

                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu" data-widget="tree">
                        <li class="header">MAIN NAVIGATION</li>
                        @php
                            if ( $q->is( 'super_administrator' ) ) {
                                $navigation = config( 'navigation.super_administrator' );
                            } elseif ( $q->is( 'administrator' ) ) {
                                $navigation = config( 'navigation.administrator' );
                            } elseif ( $q->is( 'client' ) ) {
                                $navigation = config( 'navigation.client' );
                            } elseif ( $q->is( 'va' ) ) {
                                $navigation = config( 'navigation.va' );
                            } elseif ( $q->is( 'manager' ) ) {
                                $navigation = config( 'navigation.manager' );
                            } elseif ( $q->is( 'billing' ) ) {
                                $navigation = config( 'navigation.billing' );
                            }

                            $current_path = Route::getFacadeRoot()->current()->uri();
                            $action = Route::getFacadeRoot()->current()->getAction();                       
                        @endphp
                        @if ($current_path == 'dashboard')
                        <li class='active'>
                            <a href="/dashboard">
                                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                            </a>
                        </li>
                        @else
                        <li>
                            <a href="/dashboard">
                                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                            </a>
                        </li>                
                        @endif

                        @foreach ( $navigation as $nav )
                            @if ( ( $nav['child'] && array_key_exists( $current_path, $nav['child'] ) )  
                                || ( isset( $action['selected_parent_path']) && 
                                isset( $nav['selected_parent_path']) &&
                                $nav['child'] &&
                                ($nav['selected_parent_path'] == $action['selected_parent_path'])  ))
                                <li class="treeview active menu-open">
                            @else 
                                @if ($nav['child']==false)
                                    @if (($current_path == $nav['path']) || ( ( isset($nav['selected_nav_path']) && isset($action['selected_nav_path']) )  && $action['selected_nav_path'] == $nav['selected_nav_path']) 
                                    || ( ( isset($nav['selected_parent_path']) && isset($action['selected_parent_path']) ) && $action['selected_parent_path'] == $nav['selected_parent_path'] )
                                    
                                    )
                                    <li class='active'>
                                    @else
                                    <li>
                                    @endif
                                @else
                                    @if (isset($set_menu) && isset($nav['selected_parent_path']) && ($set_menu['active_parent'] == $nav['selected_parent_path']))
                                    <li class="treeview active menu-open">
                                    @else
                                    <li class="treeview">
                                    @endif
                                @endif
                            @endif
                            <a href="/{{ $nav['path'] }}">
                                <i class="fa {{ $nav['icon'] }}"></i> <span>{{ $nav['label'] }}</span>
                                @if( $nav['child'] )
                                <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                                </span>
                                @endif
                            </a>
                            @if( $nav['child'] )
                                @if ( array_key_exists( $current_path, $nav['child'] ) )
                                    <ul class="treeview-menu" style="display:block">
                                @else 
                                    <ul class="treeview-menu">
                                @endif
                                
                                @foreach ( $nav['child'] as $key => $child ) 
                                    @if ( ($key == $current_path)  || ( ( isset($child['selected_nav_path']) && isset($action['selected_nav_path']) )  && $action['selected_nav_path'] == $child['selected_nav_path']) )
                                    <li class="active">
                                    @else
                                        @if (isset($set_menu) && ($key == $set_menu['active_menu']) && ($current_path == $set_menu['active_path']))
                                            <li class="active">
                                        @else
                                            <li>
                                        @endif
                                    @endif
                                    
                                        <a href="/{{ $key }}"><i class="fa {{ $child['icon'] }}"></i> {{ $child['label'] }}</a>
                                    </li>
                                @endforeach
                                </ul>
                            @endif
                        </li>
                        @endforeach

                    
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- =============================================== -->

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Main content -->
                <section class="content">
                @if ( $q->is( 'client' ) && !$q->read_terms)
                <div class="alert alert-warning alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    Please see our updated <a href="{{ config('vatimetracker.terms_and_condition') }}" target="_blank" class="toc-link">Terms and conditions</a>
                </div>
                @endif  
              
                @yield('content')
                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->

            <footer class="main-footer">
                <!-- To the right -->
                <div class="pull-right hidden-xs"><b>Version</b> 2.0.0</div>
                <!-- Default to the left -->
                <strong>VIRTUDESK Copyright © {{date('Y', strtotime(now()))}}.</strong> All rights reserved.
            </footer>


        </div>
        <!-- ./wrapper -->

        <script src="{{ mix('js/app.js') }}"></script>
        @stack('view-scripts') 
        <script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>
        <script>
            $(".sidebar-toggle").click(function(){
                if ($("body").hasClass("sidebar-collapse")) {
                    Cookies.remove('virtudesk-sidebar');
                    Cookies.set('virtudesk-sidebar', '0', { expires: 7 });
                } else {
                    Cookies.remove('virtudesk-sidebar');
                    Cookies.set('virtudesk-sidebar', '1', { expires: 7 });
                }
            });

            $('.toc-link').click(function() {
                $.ajax({
                    url: "/dashboard/users/read-terms",
                    dataType: "json"
                }).done(function(data) {
                    
                }); 
            });
        </script>
    </body>
</html>
