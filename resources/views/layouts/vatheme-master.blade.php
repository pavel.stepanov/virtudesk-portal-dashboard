<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>VIRTUDESK | @yield('page-title')</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <link rel="shortcut icon" type="image/png" href="{{ asset('fe') }}/img/favicon.png" />

    <!-- Bootstrap core CSS     -->
    <link href="{{asset('fe/css/bootstrap.min.css')}}" rel="stylesheet" />
    <link href="{{ mix('css/client.css') }}" rel="stylesheet">

    <!--  Material Dashboard CSS    -->
    <link href="{{ asset('fe') }}/css/material-dashboard.css?v=1.2.1" rel="stylesheet" />


    <!--     Fonts and icons     -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- custom css -->
    <link rel="stylesheet" href="{{ asset('fe') }}/css/myStyle.css">
    <!-- fontawesome 5 -->
    <link href="https://use.fontawesome.com/releases/v5.0.4/css/all.css" rel="stylesheet">
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    @yield('meta')
    @stack('view-styles')
</head>

@php
    $q = \Auth::user();
    if ($q->avatar != null) {
        $avatar = "/uploads/avatars/" . $q->avatar;
    } else {
        $avatar = "//www.gravatar.com/avatar/" . md5($q->email) . "?s=128&d=mm";
    }    
@endphp
<body>
    <div class="wrapper">
        
	 @include('layouts.fe.includes.sidebar', ['q' => $q] )

        <div class="main-panel">

            @include('layouts.fe.includes.topnav')
 
            @yield('content')
            
	    @include('layouts.fe.includes.footer')
        </div>
    </div>
</body>
<!--   Core JS Files   -->
<script src="{{ asset('fe') }}/js/jquery-3.2.1.min.js" type="text/javascript"></script>
<script src="{{ asset('fe') }}/js/bootstrap.min.js" type="text/javascript"></script>
<script src="{{ asset('fe') }}/js/material.min.js" type="text/javascript"></script>
<script src="{{ asset('fe') }}/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
<!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
<!-- Library for adding dinamically elements -->
<script src="{{ asset('fe') }}/js/arrive.min.js" type="text/javascript"></script>
<!-- Forms Validations Plugin -->
<script src="{{ asset('fe') }}/js/jquery.validate.min.js"></script>
<!--  Plugin for Date Time Picker and Full Calendar Plugin-->
<script src="{{ asset('fe') }}/js/moment.min.js"></script>
<script src="{{ asset('fe') }}/js/jquery.tagsinput.js"></script>

<!-- Material Dashboard javascript methods -->
<script src="{{ asset('fe') }}/js/material-dashboard.js?v=1.2.1"></script>

@stack('view-scripts') 
<script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>
<script>
    $(".sidebar-toggle").click(function(){
        if ($("body").hasClass("sidebar-collapse")) {
            Cookies.remove('virtudesk-sidebar');
            Cookies.set('virtudesk-sidebar', '0', { expires: 7 });
        } else {
            Cookies.remove('virtudesk-sidebar');
            Cookies.set('virtudesk-sidebar', '1', { expires: 7 });
        }
    });

    $('.toc-link').click(function() {
        $.ajax({
            url: "/dashboard/users/read-terms",
            dataType: "json"
        }).done(function(data) {
            
        }); 
    });
</script>

</html>