<!DOCTYPE html>
<html lang="en" class="@yield('htmlclass')">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>VA Time Tracker | @yield('page-title')</title>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta description="VA Time Tracker Portal">

        <link href="{{ mix('css/app.css') }}" rel="stylesheet">

	    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

        <link rel="shortcut icon" type="image/png" href="{{ asset('fe') }}/img/favicon.png" />
  
        <meta name="theme-color" content="#ff6600" />

        <!--Custom Font-->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
        <!--[if lt IE 9]>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->        
        @yield('meta')
        @stack('view-styles')      

    </head>
    <body>
    @yield('content')
    <script src="{{ mix('js/app.js') }}"></script>
    @stack('view-scripts') 
    </body>
</html>