@php
    $q = \Auth::user();
    if ($q->avatar != null) {
        $avatar = "/uploads/avatars/" . $q->avatar;
    } else {
        $avatar = "//www.gravatar.com/avatar/" . md5($q->email) . "?s=128&d=mm";
    }    
@endphp
{{-- Head --}}
@include('includes.timedly.header')

{{-- Sidebar Navigation --}}
@include('includes.timedly.sidebar')

{{-- Main Content --}}
<div class="main-content" id="panel">
    
    {{-- Top Navigation --}}
    @include('includes.timedly.topbar', ['q' => $q] )

    @yield('content')

@include('includes.timedly.footer')
