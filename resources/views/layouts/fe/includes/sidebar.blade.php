<div class="sidebar" data-active-color="rose" data-background-color="black" data-image="{{ asset('fe') }}/img/sidebar-4.jpg">
    <!--
    Tip 1: You can change the color of active element of the sidebar using: data-active-color="purple | blue | green | orange | red | rose"
    Tip 2: you can also add an image using data-image tag
    Tip 3: you can change the color of the sidebar with data-background-color="white | black"
    -->
    <div class="logo">
        <a href="/dashboard">
            <img src="{{ asset('fe') }}/img/virtudesk-logo.png" alt="Virtudesk Logo" class="center-block">
        </a>
        <a href="/dashboard" class="simple-text logo-normal text-center">
            Client
        </a>
    </div>
    @php
    $fullname = $q->first_name . " " . $q->last_name;
    @endphp
    <div class="sidebar-wrapper">
        <div class="user">
            <div class="photo">
                <img src="{{$avatar}}" />
            </div>
            <div class="info">
                <a href="/dashboard/users/profile"><span>{{$fullname}}</a></span>
                <div class="clearfix"></div>                        
            </div>
        </div>
        <ul class="nav">
            
                        @php
                            $current_path = Route::getFacadeRoot()->current()->uri();
                            $action = Route::getFacadeRoot()->current()->getAction();                       
                        @endphp

                        <li class="@if ($currentPage =='dashboard') {{'active'}} @endif">
                            <a href="/dashboard">
                            <i class="material-icons">dashboard</i>
                            <p> Dashboard </p>
                            </a>
                        </li>

                        <li class="@if ($currentPage == 'vaList'|| $currentPage == 'online') {{ 'active' }} @endif">
                            <a data-toggle="collapse" href="#vAssistants" @if ($currentPage == 'vaList'|| $currentPage == 'online') {{ 'aria-expanded="true"' }} @endif>
                                <i class="fas fa-users"></i>
                                <p> Virtual Assistants
                                    <b class="caret"></b>
                                </p>
                            </a>
                            <div  class="collapse @if ($currentPage == 'vaList'|| $currentPage == 'online') {{ 'in' }} @endif" id="vAssistants">
                                <ul class="nav">
                                    <li class="@if ($currentPage =='vaList') {{'active'}} @endif">
                                        <a href="{{ url('/dashboard/va') }}">
                                            <span class="sidebar-mini"> - </span>
                                            <span class="sidebar-normal"> List </span>
                                        </a>
                                    </li>
                                    <li class="@if ($currentPage =='online') {{'active'}} @endif">
                                        <a href="{{ url('/dashboard/online-va') }}">
                                            <span class="sidebar-mini"> - </span>
                                            <span class="sidebar-normal"> Who's Online </span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>

                        <li class="@if ($currentPage == 'projects'|| $currentPage == 'createProject') {{ 'active' }} @endif">
                            <a data-toggle="collapse" href="#vProjects" @if ($currentPage == 'projects'|| $currentPage == 'createProject') {{ 'aria-expanded="true"' }} @endif>
                                <i class="fas fa-folder"></i>
                                <p> Projects
                                    <b class="caret"></b>
                                </p>
                            </a>
                            <div  class="collapse @if ($currentPage == 'projects'|| $currentPage == 'createProject') {{ 'in' }} @endif" id="vProjects">
                                <ul class="nav">
                                    <li class="@if ($currentPage =='projects') {{'active'}} @endif">
                                        <a href="{{ url('/dashboard/projects') }}">
                                            <span class="sidebar-mini"> - </span>
                                            <span class="sidebar-normal"> List </span>
                                        </a>
                                    </li>
                                    <li class="@if ($currentPage =='createProject') {{'active'}} @endif">
                                        <a href="{{ url('/dashboard/projects/create') }}">
                                            <span class="sidebar-mini"> - </span>
                                            <span class="sidebar-normal"> Create Project</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>



                        <li class="@if ($currentPage == 'taskList'|| $currentPage == 'createTask' || $currentPage == 'taskBoard' || $currentPage == 'archiveTaskList' || $currentPage == 'deletedTaskList') {{ 'active' }} @endif">
                            <a data-toggle="collapse" href="#vTasks" @if ($currentPage == 'taskList'|| $currentPage == 'createTask') {{ 'aria-expanded="true"' }} @endif>
                                <i class="fas fa-star"></i>
                                <p> Tasks
                                    <b class="caret"></b>
                                </p>
                            </a>
                            <div  class="collapse @if ($currentPage == 'taskList'|| $currentPage == 'createTask'|| $currentPage == 'taskBoard' || $currentPage == 'archiveTaskList' || $currentPage == 'deletedTaskList') {{ 'in' }} @endif" id="vTasks">
                                <ul class="nav">
                                    <li class="@if ($currentPage =='taskBoard') {{'active'}} @endif">
                                        <a href="{{ url('/dashboard/taskboard') }}">
                                            <span class="sidebar-mini"> - </span>
                                            <span class="sidebar-normal"> Taskboard</span>
                                        </a>
                                    </li>
                                    <li class="@if ($currentPage =='taskList') {{'active'}} @endif">
                                        <a href="{{ url('/dashboard/client-tasks') }}">
                                            <span class="sidebar-mini"> - </span>
                                            <span class="sidebar-normal"> Open Tasks</span>
                                        </a>
                                    </li>
                                    <li class="@if ($currentPage =='createTask') {{'active'}} @endif">
                                        <a href="{{ url('/dashboard/client-tasks/create') }}">
                                            <span class="sidebar-mini"> - </span>
                                            <span class="sidebar-normal"> Create Task </span>
                                        </a>
                                    </li>
                                    <li class="@if ($currentPage =='archiveTaskList') {{'active'}} @endif">
                                        <a href="{{ url('/dashboard/client-tasks/archive') }}">
                                            <span class="sidebar-mini"> - </span>
                                            <span class="sidebar-normal"> Archived Tasks</span>
                                        </a>
                                    </li>
                                    <li class="@if ($currentPage =='deletedTaskList') {{'active'}} @endif">
                                        <a href="{{ url('/dashboard/client-tasks/deleted') }}">
                                            <span class="sidebar-mini"> - </span>
                                            <span class="sidebar-normal"> Deleted Tasks</span>
                                        </a>
                                    </li>

                                </ul>
                            </div>
                        </li>
                        <!--
                        <li class="@if ($currentPage == 'invoice') {{ 'active' }} @endif">
                        <a href="/dashboard/invoice">
                        <i class="material-icons">receipt</i></i> <p>Invoices</p>
                        </a>
                        </li> 
                        -->

                        <li class="@if ($currentPage == 'schedules') {{ 'active' }} @endif">
                        <a href="/dashboard/schedules">
                        <i class="fa fa-calendar"></i> <p>Schedules</p>
                        </a>
                        </li>

                        <li class="@if ($currentPage == 'attendance') {{ 'active' }} @endif">
                        <a href="/dashboard/attendance">
                        <i class="fa fa-check-square"></i> <p>Attendance</p>
                        </a>
                        </li>

                        <li class="@if ($currentPage == 'summaryReports'|| $currentPage == 'dailyReports') {{ 'active' }} @endif">
                            <a data-toggle="collapse" href="#vReports" @if ($currentPage == 'summaryReports'|| $currentPage == 'dailyReports') {{ 'aria-expanded="true"' }} @endif>
                                <i class="fas fa-folder"></i>
                                <p> Reports
                                    <b class="caret"></b>
                                </p>
                            </a>
                            <div  class="collapse @if ($currentPage == 'summaryReports'|| $currentPage == 'dailyReports') {{ 'in' }} @endif" id="vReports">
                                <ul class="nav">
                                    <li class="@if ($currentPage =='dailyReports') {{'active'}} @endif">
                                        <a href="{{ url('/dashboard/daily-reports') }}">
                                            <span class="sidebar-mini"> - </span>
                                            <span class="sidebar-normal"> Daily Reports </span>
                                        </a>
                                    </li>
                                    <li class="@if ($currentPage =='summaryReports') {{'active'}} @endif">
                                        <a href="{{ url('/dashboard/summary-reports') }}">
                                            <span class="sidebar-mini"> - </span>
                                            <span class="sidebar-normal"> All Task Reports</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>

                        <li class="@if ($currentPage == 'screenshots') {{ 'active' }} @endif">
                        <a href="/dashboard/screenshots">
                        <i class="fa fa-desktop"></i> <p>Screenshots</p>
                        </a>
                        </li>
            
                        <li>
                            <a href="/auth/logout">
                                <i class="fas fa-sign-out-alt"></i>
                                <p> Log Out </p>
                            </a>
                        </li>
        </ul>
    </div>
</div>
<!-- End of Sidebar -->