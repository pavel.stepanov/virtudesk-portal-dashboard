
<nav class="navbar navbar-transparent navbar-absolute">
    <div class="container-fluid">
        <div class="navbar-minimize">
            <button id="minimizeSidebar" class="btn btn-round btn-white btn-fill btn-just-icon">
            <i class="material-icons visible-on-sidebar-regular">more_vert</i>
            <i class="material-icons visible-on-sidebar-mini">view_list</i>
            </button>
        </div>
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"> @yield('page-title') </a>
        </div>
        <div class="collapse navbar-collapse">
        
            <ul class="nav navbar-nav navbar-right">
                @if ( $q->is( 'client' ) && !$q->read_terms)
                    @php
                        $notification_count = 1;
                    @endphp
                @else 
                    @php
                    $notification_count = 0;
                    @endphp
                @endif
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="material-icons">notifications</i>
                        <span class="notification">{{$notification_count}}</span>
                        <p class="hidden-lg hidden-md">
                            Notifications
                            <b class="caret"></b>
                        </p>
                    </a>
                    @if ($notification_count>0)
                    <ul class="dropdown-menu">
                    @if ( $q->is( 'client' ) && !$q->read_terms)
                        <li>
                            <a href="{{ config('vatimetracker.terms_and_condition') }}" target="_blank">Please see our updated Terms and Conditions.</a>
                        </li>
                    @endif
                    </ul>
                    @endif
                </li>
                
                <li class="separator hidden-lg hidden-md"></li>
            </ul>

        <!--
            <form class="navbar-form navbar-right" role="search">
                <div class="form-group form-search is-empty">
                    <input type="text" class="form-control" placeholder=" Search ">
                    <span class="material-input"></span>
                </div>
                <button type="submit" class="btn btn-white btn-round btn-just-icon">
                <i class="material-icons">search</i>
                <div class="ripple-container"></div>
                </button>
            </form>
        -->
        </div>
    </div>
</nav>