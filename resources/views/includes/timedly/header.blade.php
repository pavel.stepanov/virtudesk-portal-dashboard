<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Virtual Assistant Management Platform">
    <meta name="author" content="Creative Tim">
    <title>@yield('pageTitle') - timedly</title>
    <!-- Favicon -->
    <link rel="icon" href="{{ asset('images/timedly/brand/favicon.png') }}" type="image/png">
    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
    <!-- Icons -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
          integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

    <link rel="stylesheet" href="{{ asset('css/timedly/main.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('css/timedly/argon.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('css/timedly/custom.css') }}" type="text/css">
    
    <script src="{{ asset('js/jquery.min.js') }}"></script>

{{--    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>--}}

    {{-- @if($currentPage == 'allReport') --}}
        <!-- select date range -->
        <script>
            $(document).ready(function(){
                $('input[id="customRadioInline4"]').click(function(){
                    $(".date-range").show();
                });
                $('input[id="customRadioInline1"]').click(function(){
                    $(".date-range").hide();
                });
                $('').click(function(){
                    $(".date-range").hide();
                });
                $('input[id="customRadioInline3"]').click(function(){
                    $(".date-range").hide();
                });
            });
        </script>
    {{-- @endif --}}
</head>
