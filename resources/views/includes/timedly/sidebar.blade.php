<body>
<!-- Sidenav -->
<nav class="sidenav navbar navbar-vertical fixed-left navbar-expand-xs navbar-light bg-white" id="sidenav-main">
    <div class="scrollbar-inner">
        <!-- logo -->
        <div class="sidenav-header d-flex align-items-center">
            <a href="/" class="navbar-brand" href="pages/dashboards/dashboard.html">
                <img src="{{ asset('images/timedly/brand/blue.png') }}" class="navbar-brand-img" alt="...">
            </a>
            <div class="ml-auto">
                <!-- Sidenav toggler -->
                <div class="sidenav-toggler d-none d-xl-block" data-action="sidenav-unpin" data-target="#sidenav-main">
                    <div class="sidenav-toggler-inner">
                        <i class="sidenav-toggler-line"></i>
                        <i class="sidenav-toggler-line"></i>
                        <i class="sidenav-toggler-line"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="navbar-inner">
            <!-- Collapse -->
            <div class="collapse navbar-collapse" id="sidenav-collapse-main">
                <!-- Nav items -->

                <ul class="navbar-nav">

                    @php
                        $currentPage = Route::getFacadeRoot()->current()->uri();
                        $path = explode('/', $currentPage);
                        $action = Route::getFacadeRoot()->current()->getAction();
                        $main = count($path) > 1 ? $path[1] : $path[0];
                    @endphp

                    <li class="nav-item">
                        <a class="nav-link @if($main == 'dashboard'){{'active'}} @endif" href="{{url('/')}}">
                            <i class="fas fa-tachometer-alt"></i>
                        <span class="nav-link-text">Dashboard</span>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link @if($main == 'va'|| $main == 'online-va'){{'active'}}@endif"
                           href="#virtual-assistants" data-toggle="collapse" role="button"
                           aria-expanded="@if($main == 'va'|| $main == 'online-va'){{'true'}}@endif"
                           aria-controls="virtual-assistants">
                            <i class="ni ni-single-02"></i>
                            <span class="nav-link-text">Virtual Assistants</span>
                        </a>
                        <div class="collapse @if($main == 'va'|| $main == 'online-va'){{'show'}}@endif"
                             id="virtual-assistants">
                            <ul class="nav nav-sm flex-column">
                                <li class="nav-item @if($main == 'va') {{'active'}} @endif">
                                    <a href="{{url('dashboard/va')}}" class="nav-link">List</a>
                                </li>
                                <li class="nav-item @if($main == 'online-va'){{'active'}}@endif">
                                    <a href="{{url('dashboard/online-va')}}" class="nav-link">Who's Online</a>
                                </li>
                            </ul>
                        </div>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link @if($main == 'projects') {{'active'}}@endif" href="#my-projects" data-toggle="collapse" role="button"
                           aria-expanded="@if($main == 'allProjects'|| $main == 'createProject' || $main == 'archivedProject'){{'true'}}@endif"
                           aria-controls="virtual-assistants">
                            <i class="ni ni-briefcase-24"></i>
                            <span class="nav-link-text">Projects</span>
                        </a>
                        <div class="collapse @if($main == 'projects') {{'show'}} @endif" id="my-projects">
                            <ul class="nav nav-sm flex-column">
                                <li class="nav-item @if($main == 'projects' && !isset($path[2])) {{'active'}} @endif">
                                    <a href="{{url('dashboard/projects')}}" class="nav-link">All Projects</a>
                                </li>
                                <li class="nav-item @if($main == 'projects' && isset($path[2]) && $path[2] == 'create'){{'active'}}@endif">
                                    <a href="{{url('dashboard/projects/create')}}" class="nav-link">Create Project</a>
                                </li>
                            </ul>
                        </div>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link @if($main == 'client-tasks' || $main == 'taskboard'){{'active'}}@endif" href="#tasks" data-toggle="collapse" role="button" aria-expanded="@if($main == 'client-tasks' || $main == 'taskboard'){{'true'}}@endif"
                           aria-controls="tasks">
                            <i class="ni ni-bullet-list-67"></i>
                            <span class="nav-link-text">Tasks</span>
                        </a>
                        <div class="collapse @if($main == 'client-tasks' || $main == 'taskboard'){{'show'}}@endif" id="tasks">
                            <ul class="nav nav-sm flex-column">
                                <li class="nav-item @if($main == 'taskboard'){{'active'}}@endif">
                                    <a href="{{url('dashboard/taskboard')}}" class="nav-link">Taskboard</a>
                                </li>
                                <li class="nav-item @if($main == 'client-tasks' && !isset($path[2])){{'active'}}@endif">
                                    <a href="{{url('dashboard/client-tasks')}}" class="nav-link">All Open Tasks</a>
                                </li>
                                <li class="nav-item @if($main == 'client-tasks' && isset($path[2]) && $path[2] == 'create'){{'active'}}@endif">
                                    <a href="{{url('dashboard/client-tasks/create')}}" class="nav-link">Create Task</a>
                                </li>
                                <li class="nav-item @if($main == 'client-tasks' && isset($path[2]) && $path[2] == 'archive'){{'active'}}@endif">
                                    <a href="{{url('dashboard/client-tasks/archive')}}" class="nav-link">Archived Tasks</a>
                                </li>
                                <li class="nav-item @if($main == 'client-tasks' && isset($path[2]) && $path[2] == 'deleted'){{'active'}}@endif">
                                    <a href="{{url('dashboard/client-tasks/deleted')}}" class="nav-link">Deleted Tasks</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <!--
                    <li class="nav-item">
                        <a class="nav-link @if($main == 'invoice'){{'active'}}@endif" href="{{url('dashboard/invoice')}}">
                            <i class="ni ni-single-copy-04"></i>
                            <span class="nav-link-text">Invoices</span>
                        </a>
                    </li>
                    -->

                    <li class="nav-item">
                        <a class="nav-link @if($main == 'schedules'){{'active'}}@endif" href="{{url('dashboard/schedules')}}">
                            <i class="ni ni-calendar-grid-58"></i>
                            <span class="nav-calendar-grid-58">Schedules</span>
                        </a>
                    </li>

                    <li class="nav-item">
                            <a class="nav-link @if($main == 'attendance'){{'active'}}@endif" href="{{url('dashboard/attendance')}}">
                                <i class="ni ni-watch-time"></i>
                                <span class="nav-calendar-grid-58">Attendance</span>
                            </a>
                        </li>

                    <li class="nav-item">
                        <a class="nav-link @if($main == 'dailyReport' || $main == 'allReport'){{'active'}}@endif" href="#reports" data-toggle="collapse" role="button" aria-expanded="@if($main == 'dailyReport' || $main == 'allReport'){{'true'}}@endif"
                           aria-controls="tasks">
                            <i class="fas fa-clipboard-list"></i>
                            <span class="nav-link-text">Reports</span>
                        </a>
                        <div class="collapse @if($main == 'daily-reports' || $main == 'summary-reports'){{'show'}}@endif" id="reports">
                            <ul class="nav nav-sm flex-column">
                                <li class="nav-item @if($main == 'daily-reports'){{'active'}}@endif">
                                    <a href="{{url('dashboard/daily-reports')}}" class="nav-link">Daily Task Report</a>
                                </li>
                                <li class="nav-item @if($main == 'summary-reports'){{'active'}}@endif">
                                    <a href="{{url('dashboard/summary-reports')}}" class="nav-link">All Tasks Reports</a>
                                </li>
                            </ul>
                        </div>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link @if($main == 'screenshots'){{'active'}}@endif" href="{{url('dashboard/screenshots')}}">
                            <i class="ni ni-camera-compact"></i>
                            <span class="nav-link-text">Screenshots</span>
                        </a>
                    </li>

                    <li class="nav-item">
                    <a class="nav-link" href="{{ url('auth/logout') }}">
                            <i class="ni ni-user-run"></i>
                            <span class="nav-link-text">Logout</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>
