@extends('layouts.timedly.app')

@section('pageTitle')
    Attendance
@endsection

@section('pageScripts')
<script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
<script>
    var _table;
    var temp_delete_id = 0;

    function fetchData(target_date, type_pick) {
        $('#attendance-table').DataTable().destroy();

        $('#attendance-table').DataTable({
            keys: !0,
            language: {
                paginate: {
                    previous: "<i class='fas fa-angle-left'>",
                    next: "<i class='fas fa-angle-right'>"
                }
            },
            "autoWidth": false,
            "bAutoWidth": false,
            processing: true,
            serverSide: false,
            ajax: { 
                url:'/dashboard/attendance/datatables',
                data: {
                    target_date : target_date,
                    type_pick: type_pick,
                    type : 'GET'
                }
            },
            "columns": [
                    { "data": "user_id" },
                    { "data": "schedule"},
                    { "data": "date_in"},
                    { "data": "time_in"},
                    { "data": "remark", width: "150px"},
            ]
        });

    }

    $(document).ready(function() {
        fetchData();

        $('#datepicker').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true
        }).on('changeDate', function(e) {
            target_date = $("#target_date").val();
            type_pick = $('#typepick').find(":selected").val();
            fetchData(target_date, type_pick);
        });

        $("#typepick").change(function(){
            target_date = $("#target_date").val();
            type_pick = $('#typepick').find(":selected").val();
            fetchData(target_date, type_pick);
        });

        //this shows the modal
        $(document).on("click", ".button-delete", function(){
            var id = $(this).attr('data-id');

            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                buttonsStyling: false,
                confirmButtonClass: 'btn btn-danger',
                confirmButtonText: 'Yes, delete it!',
                cancelButtonClass: 'btn btn-secondary'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        url: "/dashboard/client-tasks/delete/"+id,
                        dataType: "json"
                    }).done(function(data) {
                        // Show confirmation
                        swal({
                            title: 'Deleted!',
                            text: 'Task has been deleted.',
                            type: 'success',
                            buttonsStyling: false,
                            confirmButtonClass: 'btn btn-primary'
                        });
                        _table.ajax.reload( null, false );
                    });
                }
            });
        });
    })
</script>
@endsection

@section('content')
    <div class="header bg-gradient-info pb-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <div class="col-lg-6 col-7">
                        <h6 class="h2 text-white d-inline-block mb-0">Attendance</h6>

                        <div class="row">
                            <div class="col-lg-6 col-12">
                                <label>Select Date</label>
                                <div class="input-group date" id="datepicker">
                                    <input type="text" id='target_date' name='target_date' class="form-control" value="{{date('Y-m-d', strtotime(now()))}}">
                                    <div class="input-group-addon">
                                    <span class="glyphicon glyphicon-th"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-12">
                                @if (\Auth::user()->is('client'))
                                <label>Type</label>
                                <select class="form-control" id="typepick" name="typepick">
                                    <option value="present">Present</option>
                                    <option value="absent">Absent</option>
                                    <option value="break">On Break</option>
                                </select>
                                @endif

                                @if (\Auth::user()->is('administrator') || Auth::user()->is('super_administrator'))
                                <label>Type</label>
                                <select class="form-control" id="typepick" name="typepick">
                                    <option value="present">Present</option>
                                    <option value="absent">Absent</option>
                                    <option value="break">On Break</option>
                                    <option value="late">Late</option>
                                    <option value="overbreak">Over Break</option>
                                </select>
                                @endif
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Page content -->

    <div class="container-fluid mt--6">
        <div class="row">

            <div class="col-12">
                <div class="card">
                    <!-- Card header -->
                    <div class="table-responsive py-4">
                        <table class="table table-flush" id="attendance-table">
                            <thead class="thead-light">
                            <tr>
                                <th>Name</th>
                                <th>Schedule</th>
                                <th>Date In</th>
                                <th>Time In</th>
                                <th>Remark</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>


        </div>
@endsection
