@extends('layouts.timedly.app')

@section('pageTitle')
    Summary Reports
@endsection

@section('pageScripts')
<script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
@endsection

@section('specialScripts')
<script>
    var _table;
    var temp_delete_id = 0;

    // use this transport for "binary" data type
    $.ajaxTransport("+binary", function(options, originalOptions, jqXHR){
        // check for conditions and support for blob / arraybuffer response type
        if (window.FormData && ((options.dataType && (options.dataType == 'binary')) || (options.data && ((window.ArrayBuffer && options.data instanceof ArrayBuffer) || (window.Blob && options.data instanceof Blob)))))
        {
            return {
                // create new XMLHttpRequest
                send: function(_, callback){
            // setup all variables
                    var xhr = new XMLHttpRequest(),
                        url = options.url,
                        type = options.type,
            // blob or arraybuffer. Default is blob
                        dataType = options.responseType || "blob",
                        data = options.data || null;
                    
                    xhr.addEventListener('load', function(){
                        var data = {};
                        data[options.dataType] = xhr.response;
            // make callback and send data
                        callback(xhr.status, xhr.statusText, data, xhr.getAllResponseHeaders());
                    });

                    xhr.open(type, url, true);
                    xhr.responseType = dataType;
                    xhr.send(data);
                },
                abort: function(){
                    jqXHR.abort();
                }
            };
        }
    });

    $(document).ready(function() {
        $("#generate-pdf").hide();

        $('.datedatedate').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true
        }).on('changeDate', function(e) {
            target_date = $("#target_date").val();
            $("#generate-pdf").hide();
        });

        $('#va').multiselect({
            maxHeight: 200,
            buttonWidth: '150px',
            numberDisplayed	: 1,
            buttonClass: 'btn btn-secondary',
            includeSelectAllOption: true,
            onChange: function(option, checked, select) {
                $("#generate-pdf").hide();
            }
        });

        $("#generate-report").click(function(){

            start_date = $("#start_date").val();
            end_date = $("#end_date").val();
            va = $("#va").val();

            if (va==0) {
                alert('Select a VA first');
                return;
            }

            $('.daily-report-content').html('<i class="fa fa-spin fa-refresh"></i>');
            $.ajax({
                url: "/dashboard/summary-reports/generate",
                dataType: "json",
                data: {
                    "va_id" : va,
                    "start_date" :  start_date,
                    "end_date" :  end_date
                },
            }).done(function(data) {
                if (data.html!="<p>No timesheet records found.</p>") $("#generate-pdf").show();
                $('.daily-report-content').html(data.html);
            });

        });


        $("#generate-pdf").click(function(){

            start_date = $("#start_date").val();
            end_date = $("#end_date").val();
            va = $("#va").val();

            if (va==0) {
                alert('Select a VA first');
                return;
            }


            $.ajax({
                    url: "/dashboard/summary-reports/generate/pdf",
                    dataType: 'binary',
                    data: {
                        "va_id" : va,
                        "start_date" :  start_date,
                    "end_date" :  end_date
                    },
            }).done(function(data) {

                var url = window.URL.createObjectURL(data);
                var $a = $('<a />', {
                'href': url,
                'download': 'summary-report.pdf',
                'text': "click"
                }).hide().appendTo("body")[0].click();
                setTimeout(function() {
                window.URL.revokeObjectURL(url);
                }, 10000);

                });

            });
        
    })
</script>
@endsection

@section('content')
    <div class="header bg-gradient-info pb-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <div class="col-lg-6 col-7">
                        <h6 class="h2 text-white d-inline-block mb-0">Sumarry Reports</h6>
                    </div>
                    <div class="col-12">
                        <div class="row">
                            <div class='col-md-4'>
                                <label>Start Date</label>
                                <div class="input-group date datedatedate">
                                <input type="text" id='start_date' name='start_date' class="form-control" value="{{date('Y-m-d', strtotime(now()))}}">
                                <div class="input-group-addon">
                                <span class="glyphicon glyphicon-th"></span>
                                </div>
                                </div>
    
                                <label>End Date</label>
                                <div class="input-group date datedatedate">
                                <input type="text" id='end_date' name='end_date' class="form-control" value="{{date('Y-m-d', strtotime(now()))}}">
                                <div class="input-group-addon">
                                <span class="glyphicon glyphicon-th"></span>
                                </div>
                                </div>
                            </div>

                            <div class='col-md-3'>
                                <label style="display: block;">VA</label>
                                <select class="form-control btn-secondary" id="va" name="va" multiple="multiple" required>
                                    @foreach ($vas as $va)
                                    <option value="{{$va->id}}">{{$va->fullname}}</option>
                                    @endforeach 
                                </select>
                            </div>

                            <div class='col-md-5'>
                                <label>Actions</label>
                                <div class="input-group btn-toolbar">
                                    <button class="btn btn-secondary" id='generate-report'><i class="fa fa-cog"></i> Generate Report</button>
                                    <button class="btn btn-secondary" id='generate-pdf'><i class="fa fa-file"></i> Generate PDF</button>
                                </div>                            
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Page content -->

    <div class="container-fluid mt--6">
        <div class="row">

            <div class="col-12">
                <div class="card">
                    <div class="table-responsive py-4">
                        <div class='col-sm-12 daily-report-content'>
                            <div class="alert alert-primary" role="alert">
                                No selected data. Please use the filter above.
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
@endsection
