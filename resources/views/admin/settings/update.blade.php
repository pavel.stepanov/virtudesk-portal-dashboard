@extends('layouts.adminlte-master')
@section('page-title', "Settings")

@section('content')
<div class="col-lg-12">
    @if ($errors->any())
        <div class="box box-warning box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">Errors</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
                <!-- /.box-tools -->
            </div>

            <!-- /.box-header -->
            <div class="box-body">
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </div>
            <!-- /.box-body -->
        </div>
    @endif

    @if (session()->has('notification_message'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-info"></i> Success!</h4>
            {{ session()->get('notification_message') }}
        </div>
    @endif

    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-edit"></i> Update Setting
        </div>
        <div class="panel-body">
            <div class="col-md-12">
                <form enctype="multipart/form-data" method="POST" action="{{url('/dashboard/settings/save')}}">
                {{ csrf_field() }}
                <div class="row">
                    <div class="box box-info">
                        <div class="box-body">
                        @foreach ($settings as $setting)
                            <div class="form-group">
                                <label>{{$setting->name}}</label>
                                <input type="text" class="form-control input-lg" name="{{$setting->slug}}" value="{{ $setting->value }}"/>
                            </div>
                        @endforeach
                        </div>
                    </div>

                    <div class="box box-info">
                        <div class="box-footer clearfix">
                        <button type="submit" class="pull-right btn btn-success btn-lg">Update</button>
                        </div>
                    </div>

                </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection