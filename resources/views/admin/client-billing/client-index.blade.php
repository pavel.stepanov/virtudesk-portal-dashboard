@extends('layouts.vatheme-master')
@section('page-title', "Client Invoice")
@php
    $currentPage = 'invoice'
@endphp
@section('content')
<div class="content">
    <div class="container-fluid">
	<div class="row">
	    <div class="col-md-12">
		<div class="card">
		    <div class="card-header card-header-icon" data-background-color="rose">
            <i class="material-icons">receipt</i>
            </div>
		    <div class="card-content">
			<h4 class="card-title">Invoices</h4>

                            <table class="table table-bordered table-hover dataTable" id="cb-table" role='grid' width='100%'>
                                <thead>
                                    <tr>
                                        <th>Invoice Number</th>
                                        <th>Date Prepared</th>
                                        <th>Billing Period</th>
                                        <th>Total Amount Due</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                            </table>
        </div> <!-- end of .card-content -->
    </div>
</div>

</div>
</div>
</div>
@endsection

@push('view-styles')
<link href="//cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css" rel="stylesheet">
@endpush

@push('view-scripts')
<script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>

<script>
var _table;
$(function() {
    _table = $('#cb-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '/dashboard/client-billing/datatables',
        "columns": [
                { "data": "invoice_number"},
                { "data": "date_generated"},
                { "data": "billing_period"},
                { "data": "total_amount_due" },
                { "data": "status" },
                { "data": "actions", width: "150px", orderable: false, searchable: false},
        ]
    });
});

     var temp_delete_id = 0;

    //this shows the modal
    $(document).on("click", ".button-delete", function(){
        var id = $(this).attr('data-id');
        temp_delete_id = id;
        $('.modal-body .content').html('<i class="fa fa-spin fa-refresh"></i>');
        $.ajax({
            url: "/dashboard/client-billing/show/"+id,
            dataType: "json"
        }).done(function(data) {
            $('.modal-body .content').html(data.html);
            $('#modal-warning').modal('show');
        }); 
    });

    //this does the actual delete
    $("#button-delete").click(function(){
        $(this).html('<i class="fa fa-spin fa-refresh"></i> Delete');
        $.ajax({
            url: "/dashboard/client-billing/delete/"+temp_delete_id,
            dataType: "json"
        }).done(function(data) {
            $('#modal-warning').modal('hide');
            $("#button-delete").html('Delete');
            _table.ajax.reload( null, false );
        }); 
    });

</script>

@endpush