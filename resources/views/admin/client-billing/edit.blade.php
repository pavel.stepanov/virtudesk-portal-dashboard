@extends('layouts.adminlte-master')
@section('page-title', "Invoice")

@section('content')
<div class="col-lg-12">

    @if ($errors->any())
    <div class="box box-warning box-solid">
        <div class="box-header with-border">
            <h3 class="box-title">Errors</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
            <!-- /.box-tools -->
        </div>

        <!-- /.box-header -->
        <div class="box-body">
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </div>
        <!-- /.box-body -->
    </div>
    @endif

    @if (session()->has('notification_message'))
    <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-info"></i> Success!</h4>
        {{ session()->get('notification_message') }}
    </div>
    @endif  
    
    <div class="panel panel-default">

        <div class="panel-heading">
            <i class="fa fa-edit"></i> Edit Invoice
        </div>

        <div class="panel-body">
            <div class="col-md-12">

                <form enctype="multipart/form-data" method="POST" action="{{url('/dashboard/invoice/update')}}">

                {{ csrf_field() }}

                        @if (isset($q))
                        <input type='hidden' value='{{$q->id}}' name='id'/>
                        @endif

                    <div class="row">
                        <div class="col-lg-12">

                            <div class="col-md-12 col-lg-12">

                                <div class="box box-info">
                                    <div class="box-header">
                                        <h3 class="box-title"><i class="fa fa-pencil"></i> Invoice Info</h3>
                                    </div>
                                    <div class="box-body">

                                            <div class="form-group">
                                                <label>Status</label>
                                                <select name="status" class="form-control input-lg">
                                                    @if ($q->status == "paid")
                                                    <option value="paid" selected>Paid</option>
                                                    @else
                                                    <option value="paid">Paid</option>
                                                    @endif

                                                    @if ($q->status== "unpaid")
                                                    <option value="unpaid" selected>Unpaid</option>
                                                    @else
                                                    <option value="unpaid">Unpaid</option>
                                                    @endif

                                                    @if ($q->status == "advance")
                                                    <option value="advance" selected>Advance</option>
                                                    @else
                                                    <option value="advance">Advance</option>
                                                    @endif
                                                </select>
                                            </div>

                                    </div>

                            </div>

                        </div>


                        <div class="col-xs-12">
                            <div class="box box-info">
                                <div class="box-footer clearfix">
                                <button type="submit" class="pull-right btn btn-success btn-lg">Update</button>
                                </div>
                            </div>
                        </div>

                    </div>
                    
                    </div>
                </form>
            </div>
        </div>
    </div>


</div>

@endsection
@push('view-scripts')
<script src="{{ asset('js/jquery.are-you-sure.js') }}"></script>
<script>
  $(function() {
    $('form').areYouSure(
      {
        message: 'It looks like you have been editing something. '
               + 'If you leave before saving, your changes will be lost.'
      }
    );
  });
</script>
@endpush