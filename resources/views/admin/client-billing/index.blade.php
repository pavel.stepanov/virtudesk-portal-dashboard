@extends('layouts.adminlte-master')
@section('page-title', "Client Invoice")

@section('content')

@include('admin.delete-modal')

<section class='content'>
    <div class='row'>
        <div class='col-xs-12'>
            <div class='box'>
                <div class='box-header'>
                    <h3 class='box-title'>Client Invoice</h3>
                    <span class="pull-right"><button id="btn-delete-selected" class="btn btn-primary"><i class="fa fa-trash"></i> Delete Selected</button></span>
                </div>
                <div class='box-body'>
                    <div class='row'>
                        <div class='col-sm-12'>
                            <table class="table table-bordered table-hover dataTable" id="cb-table" role='grid' width='100%'>
                                <thead>
                                    <tr>
                                        <th>Check</th>
                                        <th>Invoice Number</th>
                                        <th>Client</th>
                                        <th>Billing Period</th>
                                        <th>Total Amount Due</th>
                                        <th>Status</th>
                                        <th>Email Status</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@push('view-styles')
<link href="//cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('css/dataTables.checkboxes.css') }}" />
@endpush

@push('view-scripts')
<script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
<script src="{{ asset('js/dataTables.checkboxes.min.js') }}"></script>

<script>
var _table;
$(function() {
    _table = $('#cb-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '/dashboard/client-billing/datatables',
        "columns": [
                { "data": "check", 'targets': 0,'checkboxes': true, orderable: false, searchable: false},
                { "data": "id" },
                { "data": "client_id"},
                { "data": "billing_period"},
                { "data": "total_amount_due" },
                { "data": "status" },
                { "data": "email_status" },
                { "data": "actions", width: "150px", orderable: false, searchable: false},
        ]
    });
});

     var temp_delete_id = 0;

    //this shows the modal
    $(document).on("click", ".button-delete", function(){
        var id = $(this).attr('data-id');
        temp_delete_id = id;
        $('.modal-body .content').html('<i class="fa fa-spin fa-refresh"></i>');
        $.ajax({
            url: "/dashboard/client-billing/show/"+id,
            dataType: "json"
        }).done(function(data) {
            $('.modal-body .content').html(data.html);
            $('#modal-warning').modal('show');
        }); 
    });

    //this does the actual delete
    $("#button-delete").click(function(){
        $(this).html('<i class="fa fa-spin fa-refresh"></i> Delete');
        $.ajax({
            url: "/dashboard/client-billing/delete/"+temp_delete_id,
            dataType: "json"
        }).done(function(data) {
            $('#modal-warning').modal('hide');
            $("#button-delete").html('Delete');
            _table.ajax.reload( null, false );
        }); 
    });

    $("#btn-delete-selected").click(function() {

        var rows_selected = _table.column(0).checkboxes.selected();
        console.log(rows_selected);
        if (rows_selected.count() > 0) {

            var r = confirm("Are you sure you want to delete all selected invoices?");

            if (r == true ) {
                var multiple_delete_id = [];
                // Iterate over all selected checkboxes
                $.each(rows_selected, function(index, rowId){
                    multiple_delete_id.push(rowId);
                    //console.log(rowId);
                });

                $.ajax({
                    url: "/dashboard/client-billing/delete-multiple",
                    dataType: "json",
                    data: {
                        "sample" : 1,
                        "multiple_delete_id" : multiple_delete_id
                    },
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                }).done(function(data) {
                    _table.ajax.reload( null, false );
                    _table.column(0).checkboxes.deselectAll();
                }); 
            }
        }
    });

</script>

@endpush