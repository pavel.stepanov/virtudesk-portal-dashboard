@extends('layouts.timedly.app')

@section('pageScripts')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<!-- drag and drop script -->
<script>
    $(document).ready(function($){
        $("#project-selector").on("change", function(){
            var project_id = $(this).val();
            if (project_id!=0) {
                window.location.href="/dashboard/taskboard/"+project_id;
            } else {
                window.location.href="/dashboard/taskboard/";
            }
        });
        
        $(".sortable").sortable({
            connectWith:'.connectedSortable',
            receive: function( event, ui ) {
                var status = $(this).attr('id');
                var task_id= ui.item[0].attributes['task-id'].value;

                $.ajax({
                    url: "/dashboard/taskboard/change-status",
                    dataType: "json",
                    data: {
                        "task_id" : task_id,
                        "status" :  status
                    },
                }).done(function(data) {
                    console.log('done');
                });
            }
        });

        // Delete task
        $('.main-content').on('click', '.delete-task', function(e){
            e.preventDefault();
            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                buttonsStyling: false,
                confirmButtonClass: 'btn btn-danger',
                confirmButtonText: 'Yes, delete it!',
                cancelButtonClass: 'btn btn-secondary'
            }).then((result) => {
                if (result.value) {
                    var id = $(this).data('id');
                    var $card = $(this).parents('.task-board-item').first();
                    $.ajax({
                        url: "/dashboard/client-tasks/delete/"+id,
                        dataType: "json"
                    }).done(function(data) {
                        // Show confirmation
                        swal({
                            title: 'Deleted!',
                            text: 'Task has been deleted.',
                            type: 'success',
                            buttonsStyling: false,
                            confirmButtonClass: 'btn btn-primary'
                        });
                        $card.remove();
                    });
                }
            })
        });
    });
</script>
@endsection

@section('pageTitle')
    Task Board
@endsection

@section('content')
    <div class="header bg-gradient-info pb-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <div class="col-lg-6 col-7">
                        <h2 class="h2 text-white d-inline-block mb-0">Task Board</h2>
                        <select class="form-control" id="project-selector">
                            <option value="">All Projects</option>
                            @foreach ($projects as $project)
                            @if (($project_id != null) && ($project_id == $project->id))
                                <option value='{{$project->id}}' selected>{{$project->name}}</option>
                            @else
                                <option value='{{$project->id}}'>{{$project->name}}</option>
                            @endif
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col-sm-4">
                <div class="card">
                    <div class="card-header">
                        <!-- Title -->
                        <h5 class="h3 mb-0 text-warning">To do</h5>
                    </div>
                    <div class="card-body sortable connectedSortable" id="todo">

                        @foreach ($todos as $task)

                            @php
                                if ($task->va->avatar != null) {
                                    $avatar = "/uploads/avatars/" . $task->va->avatar;
                                } else {
                                    $avatar = "//www.gravatar.com/avatar/" . md5($task->va->email) . "?s=128&d=mm";
                                }
                            @endphp

                            <div class="task-board-item mb-3" task-id='{{$task->id}}'>
                                <div class="row">
                                    <div class="col-sm-8">
                                        <div class="task-board-item-title mb-2">
                                            <a href="{{url('dashboard/client-tasks/edit', $task->id)}}">{{strlen($task->name) > 35 ? substr($task->name,0,35)."..." : $task->name}}</a>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        @if ($task->priority==1)
                                        <span class="badge badge-pill badge-danger float-right">High</span>
                                        @endif
                                        @if ($task->priority==2)
                                        <span class="badge badge-pill badge-info float-right">Medium</span>
                                        @endif
                                        @if ($task->priority==3)
                                        <span class="badge badge-pill badge-primary float-right">Low</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-12 mt-3">
                                        <div class="small">{{strlen($task->description) > 150 ? substr($task->description,0,150)."..." : $task->description}}</div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6 text-left mt-3">
                                        <a href="{{url('dashboard/client-tasks/edit', $task->id)}}"><i class="fas fa-edit text-success tiny-icon" data-toggle="tooltip" data-placement="top" title="Edit task"></i></a>
                                        <a href="{{url('dashboard/client-tasks/view', $task->id)}}"><i class="fas fa-eye text-primary tiny-icon" data-toggle="tooltip" data-placement="top" title="View task"></i></a>
                                        <a href="#" class="delete-task" data-sweet-alert="confirm" data-placement="top" data-id="{{ $task->id }}"><i class="fas fa-trash text-danger tiny-icon" data-toggle="tooltip" data-placement="top" title="Delete task"></i></a>
                                    </div>
                                    <div class="col-sm-6 mt-3 text-right">
                                        <i class="ni ni-circle-08" data-toggle="tooltip" data-placement="top" title="Assigned to: {{$task->va->first_name}}"></i>
                                    </div>
                                </div>
                            </div>

                        @endforeach
                    </div>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="card">
                    <div class="card-header">
                        <!-- Title -->
                        <h5 class="h3 mb-0 text-info">In Progress</h5>
                    </div>
                    <div class="card-body sortable connectedSortable" id="in-progress">

                        @foreach ($in_progress as $task)

                            @php
                            if ($task->va->avatar != null) {
                                $avatar = "/uploads/avatars/" . $task->va->avatar;
                            } else {
                                $avatar = "//www.gravatar.com/avatar/" . md5($task->va->email) . "?s=128&d=mm";
                            }
                            @endphp

                            <div class="task-board-item mb-3" task-id="{{$task->id}}">
                                <div class="row">
                                    <div class="col-sm-8">
                                        <div class="task-board-item-title mb-2">
                                            <a href="{{url('dashboard/client-tasks/edit', $task->id)}}">{{strlen($task->name) > 35 ? substr($task->name,0,35)."..." : $task->name}}</a>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        @if ($task->priority==1)
                                        <span class="badge badge-pill badge-danger float-right">High</span>
                                        @endif
                                        @if ($task->priority==2)
                                        <span class="badge badge-pill badge-info float-right">Medium</span>
                                        @endif
                                        @if ($task->priority==3)
                                        <span class="badge badge-pill badge-primary float-right">Low</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-12 mt-3">
                                        <div class="small">{{strlen($task->description) > 150 ? substr($task->description,0,150)."..." : $task->description}}</div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6 text-left mt-3">
                                        <a href="{{url('dashboard/client-tasks/edit', $task->id)}}"><i class="fas fa-edit text-success tiny-icon" data-toggle="tooltip" data-placement="top" title="Edit task"></i></a>
                                        <a href="{{url('dashboard/client-tasks/view', $task->id)}}"><i class="fas fa-eye text-primary tiny-icon" data-toggle="tooltip" data-placement="top" title="View task"></i></a>
                                        <a href="#" class="delete-task" data-sweet-alert="confirm" data-placement="top" data-id="{{ $task->id }}"><i class="fas fa-trash text-danger tiny-icon" data-toggle="tooltip" data-placement="top" title="Delete task"></i></a>
                                    </div>
                                    <div class="col-sm-6 mt-3 text-right">
                                        <i class="ni ni-circle-08" data-toggle="tooltip" data-placement="top" title="Assigned to: {{$task->va->first_name}}"></i>
                                    </div>
                                </div>
                            </div>

                        @endforeach 
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="card">
                    <div class="card-header">
                        <!-- Title -->
                        <h5 class="h3 mb-0 text-success">Done</h5>
                    </div>
                    <div class="card-body sortable connectedSortable" id="done">

                        @foreach ($done as $task)

                            @php
                            if ($task->va->avatar != null) {
                                $avatar = "/uploads/avatars/" . $task->va->avatar;
                            } else {
                                $avatar = "//www.gravatar.com/avatar/" . md5($task->va->email) . "?s=128&d=mm";
                            }
                            @endphp

                            <div class="task-board-item mb-3" task-id="{{$task->id}}"">
                                <div class="row">
                                    <div class="col-sm-8">
                                        <div class="task-board-item-title mb-2">
                                            <a href="{{url('dashboard/client-tasks/edit', $task->id)}}">{{strlen($task->name) > 35 ? substr($task->name,0,35)."..." : $task->name}}</a>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        @if ($task->priority==1)
                                        <span class="badge badge-pill badge-danger float-right">High</span>
                                        @endif
                                        @if ($task->priority==2)
                                        <span class="badge badge-pill badge-info float-right">Medium</span>
                                        @endif
                                        @if ($task->priority==3)
                                        <span class="badge badge-pill badge-primary float-right">Low</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-12 mt-3">
                                        <div class="small">{{strlen($task->description) > 150 ? substr($task->description,0,150)."..." : $task->description}}</div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6 text-left mt-3">
                                        <a href="{{url('dashboard/client-tasks/edit', $task->id)}}"><i class="fas fa-edit text-success tiny-icon" data-toggle="tooltip" data-placement="top" title="Edit task"></i></a>
                                        <a href="{{url('dashboard/client-tasks/view', $task->id)}}"><i class="fas fa-eye text-primary tiny-icon" data-toggle="tooltip" data-placement="top" title="View task"></i></a>
                                        <a href="#" class="delete-task" data-sweet-alert="confirm" data-placement="top" data-id="{{ $task->id }}"><i class="fas fa-trash text-danger tiny-icon" data-toggle="tooltip" data-placement="top" title="Delete task"></i></a>
                                    </div>
                                    <div class="col-sm-6 mt-3 text-right">
                                        <i class="ni ni-circle-08" data-toggle="tooltip" data-placement="top" title="Assigned to: {{$task->va->first_name}}"></i>
                                    </div>
                                </div>
                            </div>

                        @endforeach
                    </div>
                </div>
            </div>
        </div>
@endsection
