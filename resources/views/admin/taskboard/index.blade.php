@extends('layouts.adminlte-master')
@section('page-title', "Task Board")

@section('content')

@include('admin.delete-modal')

@include('admin.taskboard.partial-index')

@endsection

@push('view-styles')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style>
  .sortable { list-style-type: none; margin:0; padding:2px; min-height:400px; border-radius: 5px;}
  .sortable li {
    padding:5px;
    margin:5px;
    height:150px;
    position:relative;
  }
  .card-header {
    font-weight:bold;
  }
  .card-title {
    float:left;
  }
  .card-delete {
    float:right;
    cursor:pointer;
  }
  .card-content {
    clear:both;
  }
  .card-footer {
    position:absolute;
    bottom:0;
    clear:both;
    width:100%;
    left: 0;
    padding: 5px;
  }
  .card-assign {
    float:right;
    text-align:right;
    margin-right: 5px;
  }
  .card-status {
    float:left;
  }
  .task-user-image {
    width: 25px;
    height: 25px;
    border-radius: 50%;
    float: right;
    margin-top: -8px;
  }
</style>
@endpush

@push('view-scripts')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
$( function() {
    $( ".sortable" ).sortable({
      connectWith: ".connectedSortable",
      placeholder: "ui-state-highlight",
      receive: function( event, ui ) {
        //$(this).css({"background-color":"blue"});
        var status = $(this).attr('id');
        var task_id= ui.item[0].attributes['task-id'].value;

        $.ajax({
            url: "/dashboard/taskboard/change-status",
            dataType: "json",
            data: {
                "task_id" : task_id,
                "status" :  status
            },
        }).done(function(data) {
            console.log('done');
        });
      }
    });
    $( ".sortable" ).disableSelection();
});

var temp_delete_id = 0;
var temp_element = null;

//this shows the modal
$(document).on("click", ".card-delete", function(){
    var id = $(this).attr('task-id');
    temp_delete_id = id;
    temp_element = $(this).parent().parent();
    $('.modal-body .content').html('<i class="fa fa-spin fa-refresh"></i>');
    console.log(id);
    $.ajax({
        url: "/dashboard/client-tasks/show/"+id,
        dataType: "json"
    }).done(function(data) {
        $('.modal-body .content').html(data.html);
        $('#modal-warning').modal('show');
    }); 
});

//this does the actual delete
$("#button-delete").click(function(){
    $(this).html('<i class="fa fa-spin fa-refresh"></i> Delete');
    $.ajax({
        url: "/dashboard/client-tasks/delete/"+temp_delete_id,
        dataType: "json"
    }).done(function(data) {
        $('#modal-warning').modal('hide');
        $("#button-delete").html('Delete');
        temp_element.remove();
    }); 
});
</script>
@endpush