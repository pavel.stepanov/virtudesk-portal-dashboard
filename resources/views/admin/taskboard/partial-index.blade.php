<section class='content'>
    <div class='row'>
        <div class='col-xs-12'>
            <div class='box'>
                <div class='box-header'>
                    <h3 class='box-title'>Task Board</h3>
                    <span class="pull-right">
                        <a href="/dashboard/client-tasks/create" class="btn btn-primary btn-add-task" id="btn-add-task"><i class="fa fa-plus"></i> Create Task</a>
                    </span>
                </div>
                <div class='box-body'>
                <div class="row">
                    <div class="col-md-6">
                        <label>Projects</label>
                        <select class="form-control input-lg" id='project-selector'>
                            <option value='0'>All Projects</option>
                            @foreach ($projects as $project)
                            @if (($project_id != null) && ($project_id == $project->id))
                                <option value='{{$project->id}}' selected>{{$project->name}}</option>
                            @else
                                <option value='{{$project->id}}'>{{$project->name}}</option>
                            @endif
                            @endforeach
                        </select>    
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-sm-4">
                        <h4>TODO</h4>
                        <ul class="sortable connectedSortable" id='todo'>
                            @foreach ($todos as $task)

                                @php
                                if ($task->va->avatar != null) {
                                    $avatar = "/uploads/avatars/" . $task->va->avatar;
                                } else {
                                    $avatar = "//www.gravatar.com/avatar/" . md5($task->va->email) . "?s=128&d=mm";
                                }
                                @endphp

                            <li class="ui-state-default" task-id='{{$task->id}}'>
                                <div class='card-header'>
                                    <div class='card-title'>{{strlen($task->name) > 35 ? substr($task->name,0,35)."..." : $task->name}}</div>
                                    <div class='card-delete' task-id='{{$task->id}}'>
                                    <i class="fa fa-trash"></i>
                                    </div>
                                    
                                </div>
                                <div class='card-content'>{{strlen($task->description) > 150 ? substr($task->description,0,150)."..." : $task->description}}</div>
                                <div class='card-footer'>
                                    @if ($task->priority==1)
                                    <div class='card-status'>High</div>
                                    @endif
                                    @if ($task->priority==2)
                                    <div class='card-status'>Medium</div>
                                    @endif
                                    @if ($task->priority==3)
                                    <div class='card-status'>Low</div>
                                    @endif
                                    <img src="{{$avatar}}" class="task-user-image" alt="{{$task->va->first_name}}">
                                    <div class='card-assign'>{{$task->va->first_name}}</div>
                                </div>
                            </li>
                            @endforeach 
                        </ul>
                    </div>
                    <div class="col-sm-4">
                        <h4>IN PROGRESS</h4>
                        <ul class="sortable connectedSortable" id='in-progress'>
                            @foreach ($in_progress as $task)

                                @php
                                if ($task->va->avatar != null) {
                                    $avatar = "/uploads/avatars/" . $task->va->avatar;
                                } else {
                                    $avatar = "//www.gravatar.com/avatar/" . md5($task->va->email) . "?s=128&d=mm";
                                }
                                @endphp

                            <li class="ui-state-default" task-id='{{$task->id}}'>
                                <div class='card-header'>
                                    <div class='card-title'>{{strlen($task->name) > 35 ? substr($task->name,0,35)."..." : $task->name}}</div>
                                    <div class='card-delete' task-id='{{$task->id}}'>
                                    <i class="fa fa-trash"></i>
                                    </div>
                                </div>
                                <div class='card-content'>{{strlen($task->description) > 150 ? substr($task->description,0,150)."..." : $task->description}}</div>
                                <div class='card-footer'>
                                    @if ($task->priority==1)
                                    <div class='card-status'>High</div>
                                    @endif
                                    @if ($task->priority==2)
                                    <div class='card-status'>Medium</div>
                                    @endif
                                    @if ($task->priority==3)
                                    <div class='card-status'>Low</div>
                                    @endif
                                    <img src="{{$avatar}}" class="task-user-image" alt="{{$task->va->first_name}}">
                                    <div class='card-assign'>{{$task->va->first_name}}</div>
                                </div>
                            </li>
                            @endforeach 
                        </ul>
                    </div>
                    <div class="col-sm-4">
                        <h4>DONE</h4>
                        <ul class="sortable connectedSortable" id='done'>
                            @foreach ($done as $task)

                                    @php
                                    if ($task->va->avatar != null) {
                                        $avatar = "/uploads/avatars/" . $task->va->avatar;
                                    } else {
                                        $avatar = "//www.gravatar.com/avatar/" . md5($task->va->email) . "?s=128&d=mm";
                                    }
                                    @endphp

                                <li class="ui-state-default" task-id='{{$task->id}}'>
                                    <div class='card-header'>
                                        <div class='card-title'>{{strlen($task->name) > 35 ? substr($task->name,0,35)."..." : $task->name}}</div>
                                        <div class='card-delete' task-id='{{$task->id}}'>
                                        <i class="fa fa-trash"></i>
                                        </div>
                                    </div>
                                    <div class='card-content'>{{strlen($task->description) > 150 ? substr($task->description,0,150)."..." : $task->description}}</div>
                                    <div class='card-footer'>
                                        @if ($task->priority==1)
                                        <div class='card-status'>High</div>
                                        @endif
                                        @if ($task->priority==2)
                                        <div class='card-status'>Medium</div>
                                        @endif
                                        @if ($task->priority==3)
                                        <div class='card-status'>Low</div>
                                        @endif
                                        <img src="{{$avatar}}" class="task-user-image" alt="{{$task->va->first_name}}">
                                        <div class='card-assign'>{{$task->va->first_name}}</div>
                                    </div>
                                </li>
                            @endforeach 
                        </ul>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</section>

@push('view-scripts')
    <script>
        $("#project-selector").on("change", function(){
            var project_id = $(this).val();
            if (project_id!=0) {
                window.location.href="/dashboard/taskboard/"+project_id;
            } else {
                window.location.href="/dashboard/taskboard/";
            }
        });
    </script>
@endpush