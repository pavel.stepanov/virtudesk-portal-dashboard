@extends('layouts.adminlte-master')
@section('page-title', "Dashboard")
@include('admin.preview-modal')
@push('view-styles')
<link href="//cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css" rel="stylesheet">
<style>
.products-list li{
    float: left !important;
    margin-right: 20px;
    margin-bottom: 20px;
    width: 200px !important;
}

.product-img{
    float:left;
    width: 200px !important;
}

.product-img img{
    width: 200px !important;
    height: 200px !important;
}

.product-info{
    float:left;
    width: 200px !important;
    margin-left: 0px !important;
}

.product-description{
    font-size: 12px;
}
</style>
@endpush
@section('content')

<div class="row">
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-blue">
            <div class="inner">
              <h3>{{$va_count}}</h3>

              <p>My Virtual Assistants</p>
               @if (session()->has('notification_message'))
    <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-info"></i> Success!</h4>
        {{ session()->get('notification_message') }}
    </div>
    @endif  
            </div>
            <div class="icon">
              <i class="ion ion-person-stalker"></i>
            </div>
            <a href="/dashboard/va" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-blue">
            <div class="inner">
              <h3>{{$total_time}}</h3>

              <p>Total Hours Today</p>
            </div>
            <div class="icon">
              <i class="ion ion-clock"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->

        <!-- ./col -->
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-blue">
            <div class="inner">
              <h3 id="sc_count">{{$screenshot_count}}</h3>

              <p>Screenshots Today</p>
            </div>
            <div class="icon">
              <i class="ion ion-android-desktop"></i>
            </div>
            <a href="/dashboard/screenshots" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
</div>

<div class="row">
    <div class="col-md-8">

        <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Latest Virtual Assistant Attendance <small class="text-primary" id="attendance_online"></small></h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div style="height: 500px !important; overflow-x:auto;">
                <table class="table no-margin">
                  <thead>
                  <tr>
                    <th>Date</th>
                    <th>Name</th>
                    <th>Time In</th>
                    <th>Status</th>
                  </tr>
                  </thead>
                  <tbody id="attendance_latest">
                    <span id="data-loading">loading please wait...</span>

                  </tbody>
                </table>
              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
              <a href="/dashboard/attendance" class="btn btn-sm btn-default btn-flat pull-right">View All</a>
            </div>
            <!-- /.box-footer -->
        </div>

        <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Latest Virtual Assistant Task Logs</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                <table class="table no-margin">
                  <thead>
                  <tr>
                    <th>Date</th>
                    <th>VA</th>
                    <th>Task Name</th>
                    <th>Time In</th>
                    <th>Time End</th>
                  </tr>
                  </thead>
                  <tbody id="vatasks">
                  </tbody>
                </table>
              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
        </div>
    </div>

    <div class="col-md-4">
        <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Latest Screenshots</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <ul class="products-list product-list-in-box" id="all-sc">
                 <span id="data-loading">loading please wait...</span>
              </ul>
            </div>
            <!-- /.box-body -->
            <div class="box-footer text-center">
              <a href="/dashboard/screenshots" class="uppercase">View All Screenshots</a>
            </div>
            <!-- /.box-footer -->
          </div>
    </div>
</div>
@endsection


<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
<script src="//momentjs.com/downloads/moment.min.js"></script>
<script src="{{ asset('js/notify.min.js') }}"></script>

<script>


$(document).ready(function() {
getManagerData();

window.setInterval(function(){
    getManagerData();
},150000);


        //this shows the preview modal
        $(document).on("click", ".button-preview", function(){
            var id = $(this).attr('data-id');
            $('.modal-body .content').html('<i class="fa fa-spin fa-refresh"></i>');
            $.ajax({
                url: "/dashboard/screenshots/preview/"+id,
                dataType: "json"
            }).done(function(data) {
                $('.modal-body .content').html(data.html);
                //$('#modal-preview').modal('show');
            });
        });
    });
    
    function getManagerData(){
         $.ajax({
                url: "/dashboard/get-manager-data",
                dataType: "json"
            }).done(function(data) {
                console.log(data.vatask_latest);
                //alert(data.vatask_latest);
                //$('#modal-preview').modal('show');
                $('#data-loading').hide();
                
                var screenshots_latest = '';
                var attendance_latest = '';
                var vatask_latest = '';
                var bucket = '{{ env('AWS_BUCKET') }}';
                var thumb_url = '';
                
                if(data.screenshot_latest.length >= 1){
                    $('#all-sc').html('');
                    $.each(data.screenshot_latest, function(i, item) {
                        
                        if(item.storage === "s3") {
                            $bucket = env('AWS_BUCKET');
                            thumb_url =  '//' + bucket + '.s3.amazonaws.com/'  + item.path + 'thumb_' + item.filename;
                        } else {
                            thumb_url = '/' + item.path + 'thumb_' + item.filename;
                        }
                        
                        screenshots_latest += '<li class="item"><div class="product-img"><img class="button-preview" width="200px" data-id="'+ item.id +'" src="'+ thumb_url +'" alt="Screenshot Image"></div><div class="product-info"><span class="product-title">'+ item.user.first_name + '  ' + item.user.last_name +'</span><span class="product-description">Taken on: '+ moment(item.created_at, "YYYY-MM-DD HH:mm:ss").format("MMM DD, YYYY h:mm A")  +'</span></div></li>';
                    });
                }
                
                if(data.attendance_latest.length >= 1){
                     $('#attendance_latest').html('');
                     
                     $.each(data.attendance_latest, function(i, item) {
                         //moment(moment().format('YYYY-MM-DD h:mm:ss A'), v.user_tz);
                         var status = '';
                         if(item.status == 'in'){
                             status = '<span class="status"><span style="color: green">&#9679;</span> in</span>';
                        }else if(item.status == 'out'){
                            status = '<span class="status"><span style="color: red">&#9679;</span> out</span>';
                        }else if(item.status == 'break'){
                            status = '<span class="status"><span style="color: orange">&#9679;</span> break</span>';
                        }else if(item.status == 'idle'){
                            status = '<span class="status"><span style="color: gray">&#9679;</span> idle</span>';
                        }
                        
                        
                        attendance_latest += '<tr><td>'+ item.schedule_date +'</td><td>'+ item.user.first_name + ' ' + item.user.last_name  +'</td><td>' + moment(item.time_in, "HH:mm:ss").format("h:mm A") + '</td><td>'  + status +  '</td></tr>';
                    });  
                }else{
                    $('#attendance_latest').html('<span>no latest attendance found</span>');
                }
                
                if(data.vatask_latest.length >= 1){
                    $('#vatasks').html('');
                    var date_start = '';
                    var date_end = '';
                    
                     $.each(data.vatask_latest, function(i, item) {
                        date_start = moment(item.time_start, "hh:mm:ss").format("h:mm A");
                        date_end = moment(item.time_end, "hh:mm:ss").format("h:mm A");
                        vatask_latest += '<tr><td>'+ item.date_start +'</td><td>'+ item.user.first_name + ' ' + item.user.last_name +'</td><td>'+ item.name +'</td><td>'+ date_start +'</td><td>'+ date_end +'</td></tr>';
                    });
                }
                
                //console.log(data.screenshot_count);
                
                $('#all-sc').html(screenshots_latest);
                $('#attendance_latest').html(attendance_latest);
                $('#vatasks').html(vatask_latest);
                $('#attendance_online').html('  (' +data.attendance_count + ' online)');
                $('#sc_count').html(data.screenshot_count);
            });
    }
</script>


