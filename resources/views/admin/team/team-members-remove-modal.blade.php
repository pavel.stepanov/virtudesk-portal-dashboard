<div class="modal modal-remove-warning fade" id="modal-remove-warning">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">&times;</span></button>
<h4 class="modal-title">Are you sure you want to remove?</h4>
</div>
<div class="modal-body">
<div class="content">
    <i class="fa fa-spin fa-refresh"></i>
</div>
</div>
<div class="modal-footer">
<button id='button-cancel' type="button" class="btn btn-outline btn-warning pull-left" data-dismiss="modal">Cancel</button>
<button id='button-delete' type="button" class="btn btn-outline btn-danger button-remove-member-confirm">Remove</button>
</div>
</div>
<!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</div>
<!-- /.modal -->