<div class="modal modal-team-members fade" id="modal-team-members">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">&times;</span></button>
<h4 class="modal-title">Team Members</h4>
</div>
<div class="modal-body" style="overflow-y:auto;height:350px">
<div class="content">
    <i class="fa fa-spin fa-refresh"></i>
</div>
</div>
<div class="modal-footer">
<button id='button-cancel' type="button" class="btn btn-warning" data-dismiss="modal">Close</button>

</div>
</div>
<!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</div>
<!-- /.modal -->