@if( count($q) ) 
<select name="vas" id="selected-team-member" data-team-id="{{ $id }}" class="form-control multi-select" multiple="multiple" required>
	@foreach($q as $user)
		<option value="{{ $user->id }}">{{ $user->name }}</option>
	@endforeach
</select>
@else
<div> No available Virtual Assitants</div>
@endif

<script>
    $(document).ready(function() {
        setTimeout(() => {
            $('.multi-select').multiselect({maxHeight: 300});
        }, 10);
    });
</script>
