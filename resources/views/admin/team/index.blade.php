@extends('layouts.adminlte-master')
@section('page-title', "Teams")

@section('content')

@include('admin.delete-modal')
@include('admin.team.team-members-modal')
@include('admin.team.team-members-add-modal')
@include('admin.team.team-members-remove-modal')

<section class='content'>
    <div class='row'>
        <div class='col-xs-12'>
            <div class='box'>
                <div class='box-header'>
                    <h3 class='box-title'>Teams</h3>
                    <span class="pull-right"><a href="/dashboard/teams/create" class="btn btn-primary btn-add-team" id="btn-add-team"><i class="fa fa-plus"></i> Create Team</a></span>
                </div>
                <div class='box-body'>
                    <div class='row'>
                        <div class='col-sm-12'>
                            <table class="table table-bordered table-hover dataTable" id="teams-table" role='grid' width='100%'>
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Leader</th>
                                        <th>Total Members</th>
                                        <th>Total Clients</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@push('view-styles')
<link href="//cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/css/bootstrap-multiselect.css" rel="stylesheet">
@endpush

@push('view-scripts')
<script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/js/bootstrap-multiselect.js"></script>

<script>
var _table;
    $(function() {
        _table = $('#teams-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '/dashboard/teams/datatables',
            "columns": [
                    { "data": "id" },
                    { "data": "name" },
                    { "data": "lead_user" },
                    { "data": "team_count" },
                    { "data": "client_count" },
                    { "data": "actions", width: "180px", orderable: false, searchable: false},
            ]
        });
    });

     var temp_delete_id = 0;
     var temp_va_id = 0;
     var temp_team_id = 0;

    //this shows the modal
    $(document).on("click", ".button-delete", function(){
        var id = $(this).attr('data-id');
        temp_delete_id = id;
        $('.modal-body .content').html('<i class="fa fa-spin fa-refresh"></i>');
        $.ajax({
            url: "/dashboard/teams/show/"+id,
            dataType: "json"
        }).done(function(data) {
            $('.modal-body .content').html(data.html);
            $('#modal-warning').modal('show');
        }); 
    });


    $(document).on("click", ".button-team-members", function(){
        var id = $(this).attr('data-id');
        teamMembersModal(id);
    });

    function teamMembersModal(id) {
        $('.modal-body .content').html('<i class="fa fa-spin fa-refresh"></i>');
        $.ajax({
            url: "/dashboard/teams/show-members/"+id,
            dataType: "json"
        }).done(function(data) {
            $('.modal-body .content').html(data.html);
            $('#modal-team-members').modal('show');
            $('#modal-team-members .modal-title').text("Team Members: " + data.team_name);
        }); 
    }

    $(document).on("click", ".button-team-add-members-modal", function(){
        var id = $(this).data('id'),
            modal_team_member_add = $('#modal-team-members-add');

        $('.modal-body .content').html('<i class="fa fa-spin fa-refresh"></i>');
        $.ajax({
            url: "/dashboard/teams/show-available-va/"+id,
            dataType: "json"
        }).done(function(data) {
            $('.modal-body .content', modal_team_member_add).html(data.html);
            modal_team_member_add.modal('show');
            if(data.va_count) {
                $('.button-team-member-add').removeAttr('disabled');
            } else {
                $('.button-team-member-add').attr('disabled','disabled');
            }
        }); 
    });

    $(document).on("click", ".button-team-member-add", function(){
        var va_id = $('#selected-team-member').val(),
            team_id = $('#selected-team-member').data('teamId');

            if (va_id.length == 0) {
                alert('Please select VAs');
                return;
            }
        
        $.ajax({
            url: "/dashboard/teams/team-member-add",
            dataType: "json",
            data: {
                "va_id" : va_id,
                "team_id" :  team_id,
            },
        }).done(function(data) {
            $('#modal-team-members-add').modal('hide');
            teamMembersModal(team_id);
        }); 
    });

    // remove member
    $(document).on("click", ".button-remove-member", function(){
        var modal_warning = $('#modal-remove-warning'),
            va_id = $(this).data('userId'),
            team_id = $(this).data('teamId'),
            name = $(this).data('name'),
            team_name = $(this).data('team');

        temp_va_id = va_id;
        temp_team_id = team_id;

        $('.modal-body .content', modal_warning).html('Are you sure to remove <strong>' + name + '</strong> from <strong>' + team_name + '</strong>');
        
        $('#modal-team-members').modal('hide');
        modal_warning.modal('show');
    });

    // remove member confirmed
    $(document).on("click", ".button-remove-member-confirm", function(){
        $('#modal-remove-warning').modal('hide');
        
        $.ajax({
            url: "/dashboard/teams/team-member-remove",
            dataType: "json",
            data: {
                "va_id" : temp_va_id,
                "team_id" :  temp_team_id,
            },
        }).done(function(data) {
            teamMembersModal(temp_team_id);
        }); 
    });


    // this does the actual delete
    // delete team
    $("#button-delete").click(function(){
        $(this).html('<i class="fa fa-spin fa-refresh"></i> Delete');
        $.ajax({
            url: "/dashboard/teams/delete/"+temp_delete_id,
            dataType: "json"
        }).done(function(data) {
            $('#modal-warning').modal('hide');
            $("#button-delete").html('Delete');
            _table.ajax.reload( null, false );
        }); 
    });

    $("#btn-add-team").click( function() {
        $.ajax({
            url: "/dashboard/teams/create",
            dataType: "json"
        }).done(function(data) {
            $('.modal-body .content').html(data.html);
            $('#modal-warning').modal('show');
        }); 
    });



</script>

@endpush