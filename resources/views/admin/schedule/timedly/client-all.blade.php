@extends('layouts.timedly.app')

@section('pageTitle')
    VA Schedules
@endsection

@section('pageScripts')
<script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
<script>

function fetchData(target_date) {
    $('#schedule-table').DataTable().destroy();

    $('#schedule-table').DataTable({
        keys: !0,
            language: {
                paginate: {
                    previous: "<i class='fas fa-angle-left'>",
                    next: "<i class='fas fa-angle-right'>"
                }
            },
            "autoWidth": false,
            "bAutoWidth": false,
        processing: true,
        serverSide: true,
        ajax: { 
            url:'/dashboard/schedules/datatables_all',
            data: {
                target_date : target_date,
                type : 'GET'
            }
        },
        "columns": [
                { "data": "first_name" },
                { "data": "last_name" },
                { "data": "schedule", "sortable": false, "width": "30%" },
                { "data": "va_schedule", "sortable": false, "width": "30%" },
                { "data": "actions" , width: "100px", "sortable": false},
        ]
    });

}

$(function() {

fetchData();

$('#datepicker').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true,
}).on('changeDate', function(e) {
    target_date = $("#target_date").val();
    fetchData(target_date);
});


});

</script>
@endsection

@section('content')
    <div class="header bg-gradient-info pb-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <div class="col-lg-6 col-7">
                        <h6 class="h2 text-white d-inline-block mb-0">VA Schedules</h6>

                        <div class='row'>
                        <div class="col-lg-6 col-12">
                                <label>Select Date</label>
                                <div class="input-group date" id="datepicker">
                                    <input type="text" id='target_date' name='target_date' class="form-control" value="{{date('Y-m-d', strtotime(now()))}}">
                                    <div class="input-group-addon">
                                    <span class="glyphicon glyphicon-th"></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Page content -->

    <div class="container-fluid mt--6">
        <div class="row">

            <div class="col-12">
                <div class="card">
                    <!-- Card header -->
                    <div class="table-responsive py-4">
                        <table class="table table-flush" id="schedule-table">
                            <thead class="thead-light">
                            <tr>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Client Schedule</th>
                                <th>VA Schedule</th>
                                <th>Options</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>


        </div>
@endsection
