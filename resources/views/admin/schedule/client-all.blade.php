@extends('layouts.vatheme-master')
@section('page-title', "VA Schedules")
@php
    $currentPage = 'schedules'
@endphp
@section('content')

<div class="content">
    <div class="container-fluid">
	<div class="row">
	    <div class="col-md-12">
		<div class="card">
		    <div class="card-header card-header-icon" data-background-color="purple">
			<i class="fa fa-user"></i>
		    </div>
		    <div class="card-content">
			<h4 class="card-title">VA Schedules</h4>

                    <div class='row'>
                        <div class='col-md-6'>
                            <label>Select Date</label>
                            <div class="input-group date" id="datepicker">
                            <input type="text" id='target_date' name='target_date' class="form-control input-lg" value="{{date('Y-m-d', strtotime(now()))}}">
                            <div class="input-group-addon">
                            <span class="glyphicon glyphicon-th"></span>
                            </div>
                            </div>
                        </div>
                        <div class='col-md-6'>
                        </div>
                    </div>
                    <div class='row'><hr></div>

			<div class="table-responsive">

                            <table class="table table-hover dataTable" id="schedule-table" role='grid' width='100%'>
                                <thead>
                                    <tr>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Client Schedule</th>
                                        <th>VA Schedule</th>
                                        <th>Options</th>
                                    </tr>
                                </thead>
                            </table>


            </div>
        </div> <!-- end of .card-content -->
    </div>
</div>

</div>
</div>
</div>

@endsection

@push('view-styles')
<link href="//cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css" />
@endpush

@push('view-scripts')
<script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>

<script>
function fetchData(target_date) {
    $('#schedule-table').DataTable().destroy();

    $('#schedule-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: { 
            url:'/dashboard/schedules/datatables_all',
            data: {
                target_date : target_date,
                type : 'GET'
            }
        },
        "columns": [
                { "data": "first_name" },
                { "data": "last_name" },
                { "data": "schedule", "sortable": false, "width": "30%" },
                { "data": "va_schedule", "sortable": false, "width": "30%" },
                { "data": "actions" , width: "100px", "sortable": false},
        ]
    });

}

$(function() {

fetchData();

$('#datepicker').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true,
}).on('changeDate', function(e) {
    target_date = $("#target_date").val();
    fetchData(target_date);
});


});
</script>

@endpush