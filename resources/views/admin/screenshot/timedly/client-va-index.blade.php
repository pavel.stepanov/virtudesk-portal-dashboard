@extends('layouts.timedly.app')

@section('pageTitle')
    Screenshots of {{$user->first_name}} {{$user->last_name}}
@endsection

@section('pageScripts')
<script>
var _table;

function fetchData(target_date, start_time, end_time) {
    $('#va-screenshots-table').DataTable().destroy();

    _table = $('#va-screenshots-table').DataTable({
        keys: !0,
        language: {
            paginate: {
                previous: "<i class='fas fa-angle-left'>",
                next: "<i class='fas fa-angle-right'>"
            }
        },
        processing: true,
        serverSide: true,
        searching: false,
        ajax: { 
            url:'/dashboard/screenshots/{{$user->id}}/datatables',
            data: {
                target_date : target_date,
                start_time: start_time,
                end_time: end_time,
                type : 'GET'
            }
        },
        "columns": [
                { "data": "path" },
                { "data": "time_taken" },
                { "data": "mouse_activity" },
                { "data": "keyboard_activity" },
                { "data": "created_at", visible: false },
                { "data": "actions", width: "150px", orderable: false, searchable: false},
        ],
        "order": [[ 4, "desc" ]],
        'drawCallback': function(settings) {
            if ($('.va-screenshots-table tr').length==2 && $('.va-screenshots-table tr').find('td').hasClass('dataTables_empty')) {
                $('.va-screenshots-table tr').css("height", "auto");
                $('.va-screenshots-table tr').css("width", "100%");
                $('.va-screenshots-table tr').css("margin", "0");
            } else {
                $('.va-screenshots-table tr').css("height", "220px");
                $('.va-screenshots-table tr').css("width", "220px");
                $('.va-screenshots-table tr').css("margin", "5px");
            }
            $('.va-screenshots-table thead tr').css("height", "auto");
        }
    });

}


$(function() {
    fetchData();

    $('#datepicker').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true,
    }).on('changeDate', function(e) {
        start_time = $("#start_time").val();
        end_time = $("#end_time").val();
        target_date = $("#target_date").val();
        fetchData(target_date, start_time, end_time);
    });

    $('.clockpicker').clockpicker({
        placement: 'bottom',
        align: 'left',
        donetext: 'Set',
        twelvehour:true,
        afterDone: function() {
            start_time = $("#start_time").val();
            end_time = $("#end_time").val();
            target_date = $("#target_date").val();
            fetchData(target_date, start_time, end_time);
        }
    });



    $("#mouse-sort").click(function(){
        _sort = $(this).data('sort');
        _table
            .order( [ 2, _sort ] )
            .draw();
        if (_sort=='asc') {
            $(this).data('sort', 'desc');
        } else {
            $(this).data('sort', 'asc');
        }
    });

    $("#keyboard-sort").click(function(){
        _sort = $(this).data('sort');
        _table
            .order( [ 3, _sort ] )
            .draw();
        if (_sort=='asc') {
            $(this).data('sort', 'desc');
        } else {
            $(this).data('sort', 'asc');
        }
    });
});

var temp_delete_id = 0;

//this shows the preview modal
$(document).on("click", ".button-preview", function(){
    var id = $(this).attr('data-id');
    $('.modal-body .content').html('<i class="fa fa-spin fa-refresh"></i>');
    $.ajax({
        url: "/dashboard/screenshots/preview/"+id,
        dataType: "json"
    }).done(function(data) {
        $('.modal-body .content').html(data.html);
        $('#modal-preview').modal('show');
    }); 
});

//this shows the modal
$(document).on("click", ".button-delete", function(){
    var id = $(this).attr('data-id');
    temp_delete_id = id;
    $('.modal-body .content').html('<i class="fa fa-spin fa-refresh"></i>');
    $.ajax({
        url: "/dashboard/screenshots/show/"+id,
        dataType: "json"
    }).done(function(data) {
        $('.modal-body .content').html(data.html);
        $('#modal-warning').modal('show');
    }); 
});

//this does the actual delete
$("#button-delete").click(function(){
    $(this).html('<i class="fa fa-spin fa-refresh"></i> Delete');
    $.ajax({
        url: "/dashboard/screenshots/delete/"+temp_delete_id,
        dataType: "json"
    }).done(function(data) {
        $('#modal-warning').modal('hide');
        $("#button-delete").html('Delete');
        _table.ajax.reload( null, false );
    }); 
});
</script>
@endsection

@section('content')
@include('includes.timedly.preview-modal')

    <div class="header bg-gradient-info pb-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <div class="col-lg-6 col-7">
                        <h6 class="h2 text-white d-inline-block mb-0">Screenshots of {{$user->first_name}} {{$user->last_name}}</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Page content -->

    <div class="container-fluid mt--6">
        <div class="row">

            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-3">
                                <label>Select Date</label>
                                <div class="input-group date" id="datepicker">
                                    <input type="text" id='target_date' name='target_date' class="form-control input-lg" value="{{date('Y-m-d', strtotime(now()))}}">
                                    <div class="input-group-addon">
                                        <span class="glyphicon glyphicon-th"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label>Sort by Activity</label>
                                <div class="btn-toolbar">
                                    <button style='height:54px; width:120px; margin-bottom: 10px;' class='btn btn-info btn-sort' id='mouse-sort' data-sort='asc'>Sort by <img src='/images/custom/mouse.png' style='width:32px'/></button>
                                    <button style='height:54px; width:120px'class='btn btn-info btn-sort' id='keyboard-sort' data-sort='asc'>Sort by <img src='/images/custom/keyboard.png' style='width:32px'/></button>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label>Start Time</label>
                                <div class="input-group clockpicker">
                                    <input id="start_time" name="start_time" type="text" class="form-control input-lg" value="12:00AM">
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-time"></span>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label>End Time</label>
                                <div class="input-group clockpicker">
                                    <input id="end_time" name="end_time" type="text" class="form-control input-lg" value="11:59PM">
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-time"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="table-responsive">
                                <table class="va-screenshots-table table table-hover dataTable" id="va-screenshots-table" role='grid' width='100%'>
                                    <thead>
                                        <tr>
                                            <th>Thumb</th>
                                            <th>Time Taken</th>
                                            <th>Mouse Activity</th>
                                            <th>Keyboard Activity</th>
                                            <th>Created</th>  
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
@endsection
