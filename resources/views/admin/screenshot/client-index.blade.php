@extends('layouts.vatheme-master')
@section('page-title', "Screenshots")
@php
    $currentPage = 'screenshots'
@endphp
@section('content')

<div class="content">
    <div class="container-fluid">
	<div class="row">
	    <div class="col-md-12">
		<div class="card">
		    <div class="card-header card-header-icon" data-background-color="purple">
			<i class="fa fa-user"></i>
		    </div>
		    <div class="card-content">
			<h4 class="card-title">Screenshots</h4>

                    <div class='row'>
                        <div class='col-md-6'>
                            <label>Select Date</label>
                            <div class="input-group date" id="datepicker">
                            <input type="text" id='target_date' name='target_date' class="form-control input-lg" value="{{date('Y-m-d', strtotime(now()))}}">
                            <div class="input-group-addon">
                            <span class="glyphicon glyphicon-th"></span>
                            </div>
                            </div>
                        </div>
                        <div class='col-md-6'>
                        </div>
                    </div>
                    <div class='row'><hr></div>

			<div class="table-responsive">

                <table class="table table-hover dataTable" id="screenshots-table" role='grid' width='100%'>
                    <thead>
                        <tr>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Screenshots</th>                                      
                            <th>Actions</th>
                        </tr>
                    </thead>
                </table>

            </div>
        </div> <!-- end of .card-content -->
    </div>
</div>

</div>
</div>
</div>


@endsection

@push('view-styles')
<link href="//cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css" />
<link rel="stylesheet" href="{{ asset('css/bootstrap-clockpicker.min.css') }}" />
@endpush

@push('view-scripts')
<script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
<script src="{{ asset('js/bootstrap-clockpicker.min.js') }}"></script>

<script>

function fetchData(target_date) {
    $('#screenshots-table').DataTable().destroy();
    
    $('#screenshots-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: { 
            url:'/dashboard/screenshots/datatables',
            data: {
                target_date : target_date,
                type : 'GET'
            }
        },
        "columns": [
                { "data": "first_name" },
                { "data": "last_name" },
                { "data": "screenshots" },
                { "data": "actions", width: "150px", orderable: false, searchable: false},
        ]
    });    
}





$(function() {
    fetchData();

    $('#datepicker').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true,
    }).on('changeDate', function(e) {
        target_date = $("#target_date").val();
        fetchData(target_date);
    });
});


</script>

@endpush