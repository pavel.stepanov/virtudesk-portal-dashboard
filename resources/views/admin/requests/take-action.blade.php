@extends('layouts.adminlte-master')
@section('page-title', "Requests - Take Action")

@section('content')
<div class="col-lg-12">

    <div class="panel panel-default">

        <div class="panel-heading">
            <i class="fa fa-user-circle-o fa-minus"></i> Take Action
        </div>

        <div class="panel-body">
            <div class="col-md-12">
                <form enctype="multipart/form-data" method="POST" action="{{url('/dashboard/requests/take-action')}}">

                    {{ csrf_field() }}
                    <input type="hidden" name='id' value="{{$r->id}}" />

                    <div class="box box-info">

                        <div class="box-header">
                            <h3 class="box-title"><i class="fa fa-ticket"></i> Request</h3>
                        </div>

                        <div class="box-body">

                            <div class="form-group">
                                <label>Request Type: </label>
                                <span>{{$r->showType()}}</span>
                            </div>
 
                            <div class="form-group">
                                <label>Requested by: </label>
                                <span>{{$r->user->first_name}} {{$r->user->last_name}}</span>
                            </div>

                            <div class="form-group">
                                <label>Details: </label>
                                <p>{{$r->details}}</p>
                            </div>

                            @if ($r->uploaded_file!='')
                            <div class="form-group">
                                <label>Document Support: </label>
                                <span><a href='/uploads/files/{{$r->uploaded_file}}'>Click here to download medical certificate</a></span>
                            </div>                            
                            @endif

                            <div class="form-group">
                                <label>Notes</label>
                                <textarea autofocus class="form-control input-lg" name="notes">{{ old('nodes') }}</textarea>
                            </div>

                        </div>
                    </div>


                    <div class="box box-info">
                        <div class="box-footer clearfix">
                            <div class="btn-toolbar">
                            <button type="submit" name="action" value="rejected" class="pull-right btn btn-warning btn-lg">Reject</button>
                            <button type="submit" name="action" value="approved" class="pull-right btn btn-success btn-lg">Approve</button>                            
                            </div>
                        </div>
                    </div>



                </form>
            </div>
        </div>

    </div>

</div>
@endsection