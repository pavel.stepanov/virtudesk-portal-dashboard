@extends('layouts.adminlte-master')
@section('page-title', "Shift Change")

@section('content')
<div class="col-lg-12">

    @if ($errors->any())
    <div class="box box-warning box-solid">
        <div class="box-header with-border">
            <h3 class="box-title">Errors</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
            <!-- /.box-tools -->
        </div>

        <!-- /.box-header -->
        <div class="box-body">
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </div>
        <!-- /.box-body -->
    </div>
    @endif

    @if (session()->has('notification_message'))
    <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-info"></i> Success!</h4>
        {{ session()->get('notification_message') }}
    </div>
    @endif

    <div class="panel panel-default">
        
        <div class="panel-heading">
            <i class="fa fa-user-circle-o fa-minus"></i> Request Shift Change
        </div>

        <div class="panel-body">
            <div class="col-md-12">
                <form enctype="multipart/form-data" method="POST" action="{{url('/dashboard/requests/save')}}">
                    {{ csrf_field() }}
                    <input type='hidden' name='type' value='shift_change'>
                    <div class="box box-info">
                        <div class="box-header">
                            <h3 class="box-title"><i class="fa fa-calendar"></i> Shift Change</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label>Details</label>
                                <textarea class="form-control input-lg" name="details"></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="box box-info">
                        <div class="box-footer clearfix">
                            <button type="submit" class="pull-right btn btn-success btn-lg">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>
@endsection
@push('view-scripts')
<script src="{{ asset('js/jquery.are-you-sure.js') }}"></script>
<script>
  $(function() {
    $('form').areYouSure(
      {
        message: 'It looks like you have been editing something. '
               + 'If you leave before saving, your changes will be lost.'
      }
    );
  });
</script>
@endpush