@extends('layouts.adminlte-master')
@section('page-title', "Request Information")

@section('content')
<div class="col-lg-12">

    <div class="panel panel-default">
        
        <div class="panel-heading">
            <i class="fa fa-info-circle"></i> Request Information
        </div>

        <div class="panel-body">
            <div class="col-md-12">

                    <div class="box box-info">
                        <div class="box-header">
                            <h3 class="box-title"><i class="fa fa-calendar"></i> Request {{$q->showType()}}</h3>
                        </div>
                        <div class="box-body">
                            <div class='row'>
                                <label>Status:</label>
                                <p>{{$q->showStatus()}}</p>
                            </div>                        
                            <div class='row'>
                                <label>Notes:</label>
                                <p>{{$q->notes}}</p>
                            </div>

                        </div>
                    </div>

            </div> 
        </div>
    </div>

</div>
@endsection