@extends('layouts.adminlte-master')
@section('page-title', "Information")

@section('content')
<div class="alert alert-warning" role="alert">{{$info_message}}</div>
@endsection