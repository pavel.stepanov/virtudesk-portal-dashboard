@extends('layouts.adminlte-master')
@section('page-title', "Clients")

@section('content')
<div class="col-lg-12">

    @if ($errors->any())
    <div class="box box-warning box-solid">
        <div class="box-header with-border">
            <h3 class="box-title">Errors</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
            <!-- /.box-tools -->
        </div>

        <!-- /.box-header -->
        <div class="box-body">
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </div>
        <!-- /.box-body -->
    </div>
    @endif

    @if (session()->has('notification_message'))
    <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-info"></i> Success!</h4>
        {{ session()->get('notification_message') }}
    </div>
    @endif  
    
    <div class="panel panel-default">

        <div class="panel-heading">
        @if (isset($q))
            <i class="fa fa-user-circle-o fa-minus"></i> Edit Client
        @else
            <i class="fa fa-user-circle-o fa-minus"></i> Add Client
        @endif
        </div>

        <div class="panel-body">
            <div class="col-md-12">
                @if (isset($q))
                <form enctype="multipart/form-data" method="POST" action="{{url('/dashboard/clients/update')}}">
                @else
                <form enctype="multipart/form-data" method="POST" action="{{url('/dashboard/clients/create')}}">
                @endif
                {{ csrf_field() }}

                        @if (isset($q))
                        <input type='hidden' value='{{$q->id}}' name='id'/>
                        @endif

                    <div class="row">
                        <div class="col-lg-12">

                            <div class="col-md-12 col-lg-12">

                                <div class="box box-info">
                                    <div class="box-header">
                                        <h3 class="box-title"><i class="fa fa-lock"></i> Account Info</h3>
                                    </div>
                                    <div class="box-body">

                                        @if (isset($q))
                                        
                                            <div class="form-group">
                                                <label>Status</label>
                                                <select name="is_active" class="form-control">
                                                    <option value="1" {{ $q->is_active ==  '1'  ? 'selected' : '' }}>Active</option>
                                                    <option value="0" {{ $q->is_active != '1'  ? 'selected' : '' }}>Inactive</option>
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <label>Email</label>
                                                <input type="text" class="form-control input-lg" name="email" value="{{ $q->email }}" placeholder="Email">
                                            </div>

                                            <div class="form-group">
                                                <label>Password</label>
                                                <input type="password" class="form-control input-lg" name="password" value="" placeholder="Password">
                                            </div>

                                            <div class="form-group">
                                                <label>Confirm Password</label>
                                                <input type="password" class="form-control input-lg" name="confirm_password" value="" placeholder="Confirm Password">
                                            </div>
                                        @else
                                        
                                            <div class="form-group">
                                                <label>Email</label>
                                                <input type="text" class="form-control input-lg" name="email" value="{{ old('email') }}" placeholder="Email">
                                            </div>

                                            <div class="form-group">
                                                <label>Password</label>
                                                <input type="password" class="form-control input-lg" name="password" value="" placeholder="Password">
                                            </div>

                                            <div class="form-group">
                                                <label>Confirm Password</label>
                                                <input type="password" class="form-control input-lg" name="confirm_password" value="" placeholder="Confirm Password">
                                            </div>
                                        @endif

                                    </div>
                                
                            

                                    <div class="box-header">
                                        <h3 class="box-title"><i class="fa fa-user"></i> User Info</h3>
                                    </div>
                                    <div class="box-body">

                                        @if (isset($q))

                                            <div class="form-group">
                                                <label>First Name</label>
                                                <input type="text" class="form-control input-lg" name="first_name" value="{{ $q->first_name }}" placeholder="First Name">
                                            </div>

                                            <div class="form-group">
                                                <label>Last Name</label>
                                                <input type="text" class="form-control input-lg" name="last_name" value="{{ $q->last_name }}" placeholder="Last Name">
                                            </div>

                                            <div class="form-group">
                                                <label>Address 1</label>
                                                <input type="text" class="form-control input-lg" name="address1" value="{{ $q->address1 }}" placeholder="Address 1">
                                            </div>

                                            <div class="form-group">
                                                <label>Address 2</label>
                                                <input type="text" class="form-control input-lg" name="address2" value="{{ $q->address2 }}" placeholder="Address 2">
                                            </div>

                                            <div class="form-group">
                                                <label>City</label>
                                                <input type="text" class="form-control input-lg" name="city" value="{{ $q->city }}" placeholder="City">
                                            </div>

                                            <div class="form-group">
                                                <label>Mobile Number</label>
                                                <input type="text" class="form-control input-lg" name="mobile_number" value="{{ $q->mobile_number }}" placeholder="Mobile Number">
                                            </div>

                                            <div class="form-group">
                                                <label>Alternate Contact Number</label>
                                                <input type="text" class="form-control input-lg" name="alternate_contact_number" value="{{ $q->alternate_contact_number }}" placeholder="Alternate Contact Number">
                                            </div>

                                            <div class="form-group">
                                                <label>Timezone</label>
                                                <select name="timezone" class="form-control input-lg">
                                                    @foreach ($timezone as $key => $value)
                                                    @if ($q->timezone == $key)
                                                    <option value="{{$key}}" selected>{{$key}} - {{$value}}</option>
                                                    @else
                                                    <option value="{{$key}}">{{$key}} - {{$value}}</option>
                                                    @endif
                                                    @endforeach
                                                </select>
                                                <span class="help-block has-warning">Take note that by updating the timezone, you need to edit affected schedules again.</span>
                                            </div>

                                        @else


                                            <div class="form-group">
                                                <label>First Name</label>
                                                <input type="text" class="form-control input-lg" name="first_name" value="{{ old('first_name') }}" placeholder="First Name">
                                            </div>

                                            <div class="form-group">
                                                <label>Last Name</label>
                                                <input type="text" class="form-control input-lg" name="last_name" value="{{ old('last_name') }}" placeholder="Last Name">
                                            </div>

                                            <div class="form-group">
                                                <label>Address 1</label>
                                                <input type="text" class="form-control input-lg" name="address1" value="{{ old('address1') }}" placeholder="Address 1">
                                            </div>

                                            <div class="form-group">
                                                <label>Address 2</label>
                                                <input type="text" class="form-control input-lg" name="address2" value="{{ old('address2') }}" placeholder="Address 2">
                                            </div>

                                            <div class="form-group">
                                                <label>City</label>
                                                <input type="text" class="form-control input-lg" name="city" value="{{ old('city') }}" placeholder="City">
                                            </div>

                                            <div class="form-group">
                                                <label>Mobile Number</label>
                                                <input type="text" class="form-control input-lg" name="mobile_number" value="{{ old('mobile_number') }}" placeholder="Mobile Number">
                                            </div>

                                            <div class="form-group">
                                                <label>Alternate Contact Number</label>
                                                <input type="text" class="form-control input-lg" name="alternate_contact_number" value="{{ old('alternate_contact_number') }}" placeholder="Alternate Contact Number">
                                            </div>

                                            <div class="form-group">
                                                <label>Timezone</label>
                                                <select name="timezone" class="form-control input-lg">
                                                    @foreach ($timezone as $key => $value)
                                                    <option value="{{$key}}">{{$key}} - {{$value}}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                        @endif

                                    </div>
                                    <div class="box-header">
                                <h3 class="box-title"><i class="fa fa-money"></i> Rate Per Hour</h3>
                                <div class="box-body">

                                    @if (isset($q))

                                    @if (isset($q->client_rates->rate_per_hour))
                                    <div class="form-group">
                                        <label>Rate Per Hour</label>
                                        <input type="text" class="form-control input-lg" name="rate_per_hour" value="{{ number_format( $q->client_rates->rate_per_hour, 2, '.', '' )  }}" placeholder="0.00">
                                        <span class="help-block">Generic rates used for all VAs</span>
                                    </div>
                                    @else
                                    <div class="form-group">
                                        <label>Rate Per Hour</label>
                                        <input type="text" class="form-control input-lg" name="rate_per_hour" value="{{ old('rate_per_hour') }}" placeholder="0.00">
                                        <span class="help-block">Generic rates used for all VAs</span>
                                    </div>                                    
                                    @endif

                                    <div class="form-group">
                                        <a class="btn btn-info btn-lg" href="/dashboard/client-va-rates/{{$q->id}}">Set Rate per VA</a>
                                        <span class="help-block">Rates specific per VA</span>
                                    </div>
                                    
                                    @else

                                    <div class="form-group">
                                        <label>Rate Per Hour</label>
                                        <input type="text" class="form-control input-lg" name="rate_per_hour" value="{{ old('rate_per_hour') }}" placeholder="0.00">
                                        <span class="help-block">Generic rates used for all VAs</span>
                                    </div>

                                    @endif

                                </div>
                            
                                <div class="box-body">
                                    @if (isset($q))
                                    <div class="form-group">
                                        <label>Managers</label>
                                        <select name='manager_id[]' class="form-control" id="manager_id" multiple="multiple" required>
                                            @foreach ($managers as $manager)
                                                @if ( in_array($manager->id,$current_manager_id) )
                                                <option value='{{$manager->id}}' selected>{{$manager->full_name}}</option>
                                                @else
                                                <option value='{{$manager->id}}'>{{$manager->full_name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    @else
                                    <div class="form-group">
                                        <label>Managers</label>
                                        <select name='manager_id[]' class="form-control" id="manager_id" multiple="multiple" required>
                                            @foreach ($managers as $manager)
                                                <option value='{{$manager->id}}'>{{$manager->full_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @endif
                                </div>
                            </div>

                            </div>

                        </div>


                        <div class="col-xs-12">
                            <div class="box box-info">
                                <div class="box-footer clearfix">
                                @if (isset($q))
                                <button type="submit" class="pull-right btn btn-success btn-lg">Update</button>
                                @else
                                    <button type="submit" class="pull-right btn btn-success btn-lg">Add</button>
                                @endif     
                                </div>
                            </div>
                        </div>

                    </div>
                    
                    </div>
                </form>
            </div>
        </div>
    </div>


</div>
<div class='clearfix'></div>
@endsection

@push('view-styles')
<link href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/css/bootstrap-multiselect.css" rel="stylesheet">
<style>
.multiselect-native-select {
    display:block;
}
</style>
@endpush
@push('view-scripts')
<script src="{{ asset('js/jquery.are-you-sure.js') }}"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/js/bootstrap-multiselect.js"></script>

<script>
  $(function() {
    $('form').areYouSure(
      {
        message: 'It looks like you have been editing something. '
               + 'If you leave before saving, your changes will be lost.'
      }
    );

    $('#manager_id').multiselect({
        maxHeight: 200,
        buttonWidth: '250px',
        numberDisplayed	: 1,
        includeSelectAllOption: true,
        onChange: function(option, checked, select) {

        }
    });
    
  });
</script>
@endpush