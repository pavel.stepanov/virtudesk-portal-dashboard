<div class="modal fade" id="modal-client-va-rate-add">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title"></h4>
			</div>
			<div class="modal-body">
				<div class="alert modal-notification"></div>
				<form name="client-va-add-rate"> 
					<div class="form-group">
	                    <label>Select VA</label>

	            		<div class="vas-html">
	            		</div> 
	            
	                </div>

					<div class="form-group">
	                    <label>Rate per Hour</label>
                        @if (isset($q->client_rates))
	                    <input type="text" class="form-control input-lg client-rate-per-hour" name="rate_per_hour" placeholder="{{ $q->client_rates->rate_per_hour_formatted }}">
                        @else
                        <input type="text" class="form-control input-lg client-rate-per-hour" name="rate_per_hour" placeholder="0">
                        @endif
                    </div>
	            </form>
			</div>
			<div class="modal-footer">
				<button id='button-cancel' type="button" class="btn btn-warning pull-left" data-dismiss="modal">Cancel</button>
				<button id='button-add-va-client-rate' type="button" class="btn btn-info">Add VA</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->