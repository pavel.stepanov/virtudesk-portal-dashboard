@extends('layouts.vatheme-master')
@section('page-title', "Dashboard")
@php
    $currentPage = 'dashboard'
@endphp
@section('content')

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-header" data-background-color="orange">
                        <i class="fas fa-users"></i>
                    </div>
                    <div class="card-content">
                        <p class="category">My Virtual Assistants</p>
                        <h3 class="card-title">{{$va_count}}</h3>
                    </div>
                    <div class="card-footer">
                        <div class="stats">
                            <i class="fas fa-list"></i>
                            <a href="/dashboard/va"> View List</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-header" data-background-color="rose">
                        <i class="material-icons">access_time</i>
                    </div>
                    <div class="card-content">
                        <p class="category">Total Working Hours</p>
                        <h3 class="card-title">{{$total_time}}</h3>
                    </div>
                    <div class="card-footer">
                        <div class="stats">
                        <!--
                          <a href="#"> View List</a>
                        -->
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-header" data-background-color="blue">
                        <i class="material-icons">camera_alt</i>
                    </div>
                    <div class="card-content">
                        <p class="category">Total Screenshots</p>
                        <h3 class="card-title">{{$screenshot_count}}</h3>
                    </div>
                    <div class="card-footer">
                        <div class="stats">
                            <i class="material-icons">collections</i> <a href="/dashboard/screenshots">View</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="green">
                        <i class="material-icons">assignment</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">Latest Virtual Assistant Attendance</h4>
                        <div class="table-responsive">
                            <table class="va-online-table table table-hover dataTable" id="online-table" role='grid'>
                            <thead>
                                <tr>
                                    <th>Date In</th>
                                    <th>Time In</th>
                                    <th>Name</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                        </table>                        </div>
                    </div> <!-- end of .card-content -->
                </div>

                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="green">
                        <i class="material-icons">assignment</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">Virtual Assistants Logs</h4>
                        <div class="table-responsive">
                            <table class="table">
                                <thead class="text-primary">
                                  <th>Date</th>
                                  <th>VA</th>
                                  <th>Task Name</th>
                                  <th>Time In</th>
                                  <th>Time End</th>
                                </thead>
                                <tbody>
                                @if (count($vatask_latest))
                                    @foreach ($vatask_latest as $vlist)

                                        @php
                                            $time_start = $user->convertToUserTimezone($vlist->date_start . ' ' . $vlist->time_start);
                                            $time_end = $user->convertToUserTimezone($vlist->date_end . ' ' . $vlist->time_end);
                                            $date_start = $user->convertToUserTimezone($vlist->date_start);
                                        @endphp

                                        <tr>
                                            <td>{{$date_start->format('Y-m-d')}}</td>
                                            <td>{{$vlist->user->first_name}} {{$vlist->user->last_name}}</td>
                                            <td>{{$vlist->name}}</td>
                                            <td>{{$time_start->format('h:i:s A')}}</td>
                                            <td>{{$time_end->format('h:i:s A')}}</td>
                                        </tr>
                                    @endforeach
                                @else 
                                    <tr>
                                        <td colspan="5">No items to display</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div> <!-- end of .card-content -->
                </div>
            </div>

            <div class="col-md-6">
            <div class="card">
              <div class="card-header card-header-icon" data-background-color="purple">
              <i class="material-icons">history</i>
              </div>

              <div class="card-content">
                <h4 class="card-title">Tracker Activities</h4>
              </div>

                    @if (count($screenshot_latest))
                    @foreach ($screenshot_latest as $slist)
                    <div class="single-scr">
                      <div class="col-md-6 screenshot-space">
                        <img src="{{$slist->thumbnail_url}}" alt="" class="img-responsive img-thumbnail" data-toggle="modal" data-target="#screenshot" style='cursor:pointer;'>
                        <button type="button" class="btn btn-warning btn-sm btn-round pull-right" data-toggle="modal" data-target="#screenshot">View</button>
                        <div class="tracker-summary">
                        @php
                            $created_at = $user->convertToUserTimezone($slist->created_at);
                        @endphp
                        <div><strong>{{$slist->user->first_name}} {{$slist->user->last_name}}</strong></div>
                        <div><small>Taken on: {{$created_at->format('M d, Y')}} at {{$created_at->format('h:i:s A')}}</small></div>

                        </div>
                      </div>
                      <div class="modal fade" id="screenshot">
                        <div class="modal-dialog modal-lg" role="dialog">
                          <div class="modal-content">
                            <div class="modal-body">
                            <img src="{{$slist->URL}}" alt="" class="img-responsive img-thumbnail">
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    @endforeach
                    @else
                        <div class="single-scr">
                            No items to display
                        </div>
                    @endif

            </div>
            </div>
        </div>

    </div>
</div>

@endsection