@extends('layouts.adminlte-master')
@section('page-title', "App & Web Usage")

@section('content')
@include('admin.appweb.preview')

<section class='content'>
    <div class='row'>
        <div class='col-xs-12'>
            <div class='box'>
                <div class='box-header'>
                    <h3 class='box-title'>App & Web Usage</h3>
                </div>
                <div class='box-body'>

                    <div class='row'>
                        <div class='col-md-6'>
                            <label>Select Date</label>
                            <div class="input-group date" id="datepicker">
                            <input type="text" id='target_date' name='target_date' class="form-control input-lg" value="{{date('Y-m-d', strtotime(now()))}}">
                            <div class="input-group-addon">
                            <span class="glyphicon glyphicon-th"></span>
                            </div>
                            </div>
                        </div>
                        <div class='col-md-6'>
                            
                        </div>
                    </div>

                    <div class='row'><hr></div>
                    <div class='row'>
                        <div class='col-sm-12'>
                            <table class="table table-bordered table-hover dataTable" id="attendance-table" role='grid' width='100%'>
                                <thead>
                                    <tr>
                                        <th>VA Name</th>
                                        <th>Process List</th>
                                        <th>Browser History</th>
                                        <th>Date</th>
                                        <th>Time</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

@push('view-styles')
<link href="//cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css" />
@endpush

@push('view-scripts')
<script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>

<script>
$(function() {
    fetchData();
});

$(function () {
    $('#datepicker').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true
    }).on('changeDate', function(e) {
        target_date = $("#target_date").val();
        fetchData(target_date);
    });

    $("#typepick").change(function(){
        target_date = $("#target_date").val();
        fetchData(target_date);
    });
    
});

function fetchData(target_date) {
    $('#attendance-table').DataTable().destroy();

    $('#attendance-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: { 
            url:'/dashboard/app-web/datatables',
            data: {
                target_date : target_date,
                type : 'GET'
            }
        },
        "columns": [
                { "data": "user_id" },
                { "data": "process_list_id"},
                { "data": "browser_history_id"},
                { "data": "date"},
                { "data": "time"},
        ]
    });

}

    //this shows the preview modal
    $(document).on("click", ".button-preview", function(){
        var id = $(this).attr('data-id');
        var type = $(this).attr('data-type');
        $('.modal-body .content').html('<i class="fa fa-spin fa-refresh"></i>');
        $.ajax({
            url: "/dashboard/app-web/preview/"+type+"/"+id,
            dataType: "json"
        }).done(function(data) {
            $('.modal-body .content').html(data.html);
            $('.modal-header .modal-title').html(data.title);
            $('#modal-preview').modal('show');
        }); 
    });
</script>
@endpush