<div class="modal modal-warning fade" id="modal-warning">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">&times;</span></button>
<h4 class="modal-title">Are you sure you want to delete?</h4>
</div>
<div class="modal-body">
<div class="content">
    <i class="fa fa-spin fa-refresh"></i>
</div>
</div>
<div class="modal-footer">
<button id='button-cancel' type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cancel</button>
<button id='button-delete' type="button" class="btn btn-outline">Delete</button>
</div>
</div>
<!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</div>
<!-- /.modal -->