<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        {{-- <meta charset="utf-8"> --}}
        <title>Attendance Report</title>
        <link rel="stylesheet" href="{{ public_path() }}/css/pdf.css">
    </head>
<body>
<h2>Attendance Reports</h2>
<div>{{$start_date}} - {{$end_date}}</div>
    <div class="section">
        <table class="table table-bordered table-hover" role='grid' width='100%'>
            <thead>
                <tr>
                    <th width="" style='text-align:center'>VA</th>
                    <th width="" style='text-align:center'>In</th>
                    <th width="" style='text-align:center'>Out</th>
                    <th width="" style='text-align:center'>Client</th>
                    <th width="" style='text-align:center'>Total Time</th>
                    <th width="" style='text-align:center'>Remarks</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($attendances as $att)
                <tr>
                    <td>{{$att->user->fullname}}</td>
                    <td>{{$att->date_in}} {{$att->time_in}}</td>
                    <td>{{$att->date_end}} {{$att->time_end}}</td>
                    <td>{{$att->client->fullname}}</td>
                    <td>{{$att->total_time}}</td>
                    <td>
                    @if ($typepick=='present')
                    <div>Present</div>
                    @endif
                    @if ( ( intval(strtotime($att->total_breaktime)) > intval(strtotime($att->max_breaktime)) && ($typepick=='overbreak') ) || ( ($typepick=='present') && intval(strtotime($att->total_breaktime)) > intval(strtotime($att->max_breaktime)) ) )
                        @php
                            $a = new DateTime($att->max_breaktime);
                            $b = new DateTime($att->total_breaktime);
                            $interval = $a->diff($b);
                        @endphp
                        <div>Max break: {{ $att->max_breaktime }}</div>
                        <div>Total break: {{ $att->total_breaktime }}</div>
                        <div>Overbreak: {{ $interval->format("%H:%I:%S") }}</div>
                    @endif
                    @if ( ( ($att->late_time != '00:00:00') && ($typepick=='late') ) || ( ($typepick=='present') &&  ($att->late_time != '00:00:00') ) )
                    <div>Late: {{$att->late_time}}</div>
                    @endif
                    @if ( ( ($att->total_idletime != '00:00:00') && ($typepick=='idle') ) || ( ($typepick=='present') && ($att->total_idletime != '00:00:00') ) )
                        <div>Idle Time: {{$att->total_idletime}}</div>
                    @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>

   </div>
</body>
</html>