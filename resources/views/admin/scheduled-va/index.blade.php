@extends('layouts.adminlte-master')
@section('page-title', "Scheduled VA")

@section('content')

<section class="content">
    <div class="row">
        <!-- Scheduled VA -->
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Scheduled VA</h3> 
                     <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                      </div>
                </div>
                <div class="box-body">
                    <div class="screenshots">
                        <table class="table table-bordered table-hover dataTable" id="schedule-table" role='grid' width='100%'>
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Client</th>
                                    <th>Schedule</th>
                                    <th>Status</th>
                                    <th>Start</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div><!-- box-body -->
            </div>
        </div>
        <!-- End Schedule VA -->

    </div>

</section>

@endsection
@push('view-styles')
<link href="//cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css" rel="stylesheet">
@endpush

@push('view-scripts')
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>

<script>

    function loadSchedules() {
        $('#schedule-table').DataTable({
            processing: true,
            serverSide: false,
            ajax: '/dashboard/schedules/today',
            "columns": [
                { "data": "name" },
                { "data": "client" },
                { "data": "schedule" },
                { "data": "status" },
                { "data": "user_start_time", visible: false },
            ],
            "order": [[ 4, "asc" ]],
        });
    }

    $(document).ready(function() {

        loadSchedules();

    });

</script>
@endpush