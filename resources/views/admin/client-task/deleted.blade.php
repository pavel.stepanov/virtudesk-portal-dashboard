@extends('layouts.adminlte-master')
@section('page-title', "Client Deleted Tasks")
@php
    $currentPage = 'deletedTaskList'
@endphp
@section('content')

@include('admin.delete-modal')
<section class='content'>
    <div class='row'>
        <div class='col-xs-12'>
            <div class='box'>
                <div class='box-header'>
                    <h3 class='box-title'>Deleted Tasks</h3>
                    <span class="pull-right"><a href="/dashboard/client-tasks/create" class="btn btn-primary"><i class="fa fa-plus"></i> Create Task</a></span>
                </div>
                <div class='box-body'>
                    <div class='row'>
                        <div class='col-sm-12'>
                            <table class="table table-bordered table-hover dataTable" id="task-table" role='grid' width='100%'>
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Priority</th>
                                        <th>Assigned to</th>
                                        <th>Name</th>
                                        <th>Description</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                            </table>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

@push('view-styles')
<link href="//cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css" rel="stylesheet">
@endpush

@push('view-scripts')
<script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>

<script>
var _table;
$(function() {
    _table = $('#task-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '/dashboard/client-tasks/deleted-datatables',
        "columns": [
                { "data": "id" },
                { "data": "priority"},
                { "data": "va_id"},
                { "data": "name" },
                { "data": "description" },
                { "data": "actions", width: "150px", orderable: false, searchable: false},
        ]
    });
});

     var temp_delete_id = 0;

    //this shows the modal
    $(document).on("click", ".button-delete", function(){
        var id = $(this).attr('data-id');
        temp_delete_id = id;
        $('.modal-body .content').html('<i class="fa fa-spin fa-refresh"></i>');
        $.ajax({
            url: "/dashboard/client-tasks/show/"+id,
            dataType: "json"
        }).done(function(data) {
            $('.modal-title').html('Are you sure you want to permanently delete?');
            $('.modal-body .content').html(data.html);
            $('#modal-warning').modal('show');
        }); 
    });

    //this does the actual delete
    $("#button-delete").click(function(){
        $(this).html('<i class="fa fa-spin fa-refresh"></i> Delete');
        $.ajax({
            url: "/dashboard/client-tasks/delete-permanent/"+temp_delete_id,
            dataType: "json"
        }).done(function(data) {
            $('#modal-warning').modal('hide');
            $("#button-delete").html('Delete');
            _table.ajax.reload( null, false );
        }); 
    });

</script>

@endpush