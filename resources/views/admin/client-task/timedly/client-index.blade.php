@extends('layouts.timedly.app')

@section('pageTitle')
    Client Tasks
@endsection

@section('pageScripts')
<script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
<script>
    var _table;
    var temp_delete_id = 0;

    $(document).ready(function() {
        var options = {
            keys: !0,
            language: {
                paginate: {
                    previous: "<i class='fas fa-angle-left'>",
                    next: "<i class='fas fa-angle-right'>"
                }
            },
            "autoWidth": false,
            "bAutoWidth": false,
            processing: true,
            serverSide: true,
            ajax: '/dashboard/client-tasks/datatables',
            "columns": [
                { "data": "project_id", "width": "15%" },
                { "data": "name", "width": "15%" },
                { "data": "description", "width": "25%" },
                { "data": "va_id" },
                { "data": "priority" },
                { "data": "status" },
                { "data": "actions", width: "150px", orderable: false, searchable: false}
            ]
        };

        _table = $('#va-data-table').DataTable(options);

        //this shows the modal
        $(document).on("click", ".button-delete", function(){
            var id = $(this).attr('data-id');

            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                buttonsStyling: false,
                confirmButtonClass: 'btn btn-danger',
                confirmButtonText: 'Yes, delete it!',
                cancelButtonClass: 'btn btn-secondary'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        url: "/dashboard/client-tasks/delete/"+id,
                        dataType: "json"
                    }).done(function(data) {
                        // Show confirmation
                        swal({
                            title: 'Deleted!',
                            text: 'Task has been deleted.',
                            type: 'success',
                            buttonsStyling: false,
                            confirmButtonClass: 'btn btn-primary'
                        });
                        _table.ajax.reload( null, false );
                    });
                }
            });
        });
    })
</script>
@endsection

@section('content')
    <div class="header bg-gradient-info pb-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <div class="col-lg-6 col-7">
                        <h6 class="h2 text-white d-inline-block mb-0">Client Tasks</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Page content -->

    <div class="container-fluid mt--6">
        <div class="row">

            <div class="col-12">
                <div class="card">
                    <!-- Card header -->
                    <div class="table-responsive py-4">
                        <table class="table table-flush" id="va-data-table">
                            <thead class="thead-light">
                            <tr>
                                <th>Project</th>
                                <th>Name</th>
                                <th>Description</th>
                                <th>Assigned to</th>
                                <th>Priority</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>


        </div>
@endsection
