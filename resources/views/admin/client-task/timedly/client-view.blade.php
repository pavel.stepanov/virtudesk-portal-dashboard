@extends('layouts.timedly.app')

@section('pageTitle', 'Task View')


@section('content')
<div class="header bg-gradient-info pb-6">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                <h2 class='h2 text-white d-inline-block mb-0'>View task Details</h2>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Page content -->

<div class="container-fluid mt--6">
    <div class="row justify-content-md-center">
        <div class="col-sm-8">
            <div class="card">
                <!-- Card header -->
                <div class="card-header">
                    <h3 class="mb-0 float-left">Task View</h3>
                    <a href='/dashboard/client-tasks/edit/{{$q->id}}' class="btn btn-icon btn-success float-right">
                    <span class="btn-inner--text">Edit</span>
                    </a>
                </div>

                <div class="card-body">

                    <div class="form-group">
                        <label class="form-control-label" for="date_due">Date Due</label>
                        @if ($q->date_due == null)
                        Not set
                        @else
                        {{$q->date_due}}
                        @endif
                    </div>

                    <div class="form-group">
                        <label class="form-control-label" for="date_due">Project</label>
                        @if ($q->project_id != 0)
                        {{$q->project->name}}
                        @else
                        No Project
                        @endif
                    </div>

                    <div class="form-group">
                        <label class="form-control-label" for="date_due">Assigned to</label>
                        {{$q->va->first_name}} 
                        {{$q->va->last_name}} 
                    </div>

                    <div class="form-group">
                        <label class="form-control-label" for="date_due">Description</label>
                        {{$q->description}}
                    </div>

                    <div class="form-group">
                        <label class="form-control-label" for="date_due">Priority</label>
                        @if ($q->priority==1) 
                        High
                        @endif
                        @if ($q->priority==2) 
                        Normal
                        @endif
                        @if ($q->priority==3) 
                        Low
                        @endif
                    </div>

                    <div class="form-group">
                        <label class="form-control-label" for="date_due">Status</label>
                        {{$q->status}}
                    </div>

                    <form enctype="multipart/form-data" method="POST" action="{{url('/dashboard/client-tasks/notes')}}" id="taskForm">
                        <input type='hidden' name='task_id' value='{{$q->id}}' />
                        {{ csrf_field() }}
                        <div class="form-group">
                                <label class="form-control-label" for="exampleFormControlTextarea1">Notes</label>
                                {{ Form::textarea('notes', null, ['class' => 'form-control', 'rows' => 3]) }}
                        </div>

                        <button class="btn btn-icon btn-success" type="submit">
                                <span class="btn-inner--text">Add Note</span>
                        </button>
                    </form>

                   

                    @if (!empty($notes))
                        @foreach ($notes as $note) 
                        <hr>
                        <div class="form-group">
                        <label class="form-control-label">{{$note->user->first_name}} {{$note->user->last_name}}</label>
                        <small class="form-text text-muted">{{$note->created_at}}</small>
                            <div>
                            {{$note->notes}}
                            </div>
                        </div>
                        @endforeach
                    @endif
                </div>
        </div>
    </div>
</div>

@endsection