@extends('layouts.timedly.app')

@section('pageTitle')
    @if (isset($q))
        Edit Task
    @else
        Add Task
    @endif
@endsection

@section('pageScripts')
<script>
    var updated = false;
    $(document).ready(function () {

        $('#datepicker').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        });

        // $('select, input, textarea').on('change input', function() {
        //     if( !updated ) {
        //         window.onbeforeunload = function(){
        //             return "Changes you've made may not be saved.";
        //         };

        //         updated = true;
        //     }
        // });

        @php
            if(session('notification_message') || $errors->any()):
                $message = '';
                $type = '';
                if( session('notification_message') ) {
                    $message = session('notification_message');
                    $type = 'success';
                    $title = 'Success!';
                } else {
                    $message = implode('<br/>', $errors->all());
                    $type = 'error';
                    $title = 'Something went wrong.';
                }
        @endphp

        swal({
            title: '{{$title}}',
            html: '{!!$message!!}',
            type: '{{$type}}',
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary'
        });

        @php
            endif;
        @endphp
    });
</script>
@endsection

@section('content')
    <div class="header bg-gradient-info pb-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <div class="col-lg-6 col-7">
                        @if (isset($q))
                            <h6 class="h2 text-white d-inline-block mb-0">Edit task</h6>
                        @else
                            <h6 class="h2 text-white d-inline-block mb-0">Create a new task</h6>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Page content -->

    <div class="container-fluid mt--6">
        <div class="row justify-content-md-center">
            <div class="col-sm-8">
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header">
                        <h3 class="mb-0 float-left">Task Info</h3>
                        @if (isset($q))
                        <a href='/dashboard/client-tasks/view/{{$q->id}}' class="btn btn-icon btn-success float-right">
                        <span class="btn-inner--text">View Notes</span>
                        </a>
                        @endif
                    </div>
                    <!-- Card body -->
                    <div class="card-body">
                        @if (isset($q))
                        <form enctype="multipart/form-data" method="POST" action="{{url('/dashboard/client-tasks/update')}}" id="taskForm">
                        @else
                        <form enctype="multipart/form-data" method="POST" action="{{url('/dashboard/client-tasks/create')}}" id="taskForm">
                        @endif
                            {{ csrf_field() }}

                            @if (isset($q))
                                {{ Form::hidden('id', $q->id) }}
                            @endif

                            <div class="form-group">
                                <label class="form-control-label" for="exampleFormControlSelect1">Date Due</label>
                                <small class="form-text text-muted">Can be left blank</small>
                                <div class="input-group date" id="datepicker">
                                @if (isset($q))
                                <input type="text" id='date_due' name='date_due' class="form-control input-lg" value="{{$q->date_due}}">
                                @else
                                <input type="text" id='date_due' name='date_due' class="form-control input-lg" value="">
                                @endif
                                    <div class="input-group-addon">
                                    <span class="glyphicon glyphicon-th"></span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="form-control-label" for="exampleFormControlSelect1">Priority</label>
                                @php
                                    if(isset($q) && $q->priority == 'High') {
                                        $val = 1;
                                    } elseif(isset($q) && $q->priority == 'Normal') {
                                        $val = 2;
                                    } elseif(isset($q) && $q->priority == 'Low') {
                                        $val = 3;
                                    } else {
                                        $val = null;
                                    }
                                @endphp

                                {{ Form::select('priority',[1 => 'High', 2 => 'Normal', 3 => 'Low'],$val,['class' => 'form-control']) }}
                            </div>

                            <div class="form-group">
                                <label class="form-control-label" for="name">Name</label>
                                {{ Form::text('name', isset($q) ? $q->name : null, ['class' => 'form-control', 'placeholder' => 'Name of the task']) }}
                            </div>

                            <div class="form-group">
                                <label class="form-control-label" for="exampleFormControlSelect1">Project Name</label>
                                <small class="form-text text-muted">Please select the project which you want to add to</small>
                                {{ Form::select('project_id', [0 => 'No Project'] + $projects->pluck('name', 'id')->toArray(), isset($q) ? $q->project_id : null, ['class' => 'form-control']) }}
                            </div>

                            <div class="form-group">
                                <div class="form-group va-names">
                                    <label class="form-control-label" for="va-names">Assign To</label>
                                    {{ Form::select('va_id', $va_users->pluck('first_name', 'id')->toArray(), isset($q) ? $q->va_id : null,['class' => 'form-control']) }}
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="form-control-label" for="exampleFormControlTextarea1">Description</label>
                                {{ Form::textarea('description', isset($q) ? $q->description : null, ['class' => 'form-control', 'placeholder' => 'Description of the task', 'rows' => 3]) }}
                            </div>

                            <button class="btn btn-icon btn-success float-right" type="submit">
                                <span class="btn-inner--text">{{ isset($q) ? 'Update Task' : 'Save Task' }}</span>
                            </button>
                       </form>
                    </div>
                </div>
            </div>
        </div>
@endsection
