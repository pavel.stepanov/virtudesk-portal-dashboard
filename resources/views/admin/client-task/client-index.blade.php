@extends('layouts.vatheme-master')
@section('page-title', "Client Tasks")
@php
    $currentPage = 'taskList'
@endphp
@section('content')

@include('admin.delete-modal')
<div class="content">
    <div class="container-fluid">
	<div class="row">
	    <div class="col-md-12">
		<div class="card">
		    <div class="card-header card-header-icon" data-background-color="purple">
			<i class="material-icons">receipt</i>
		    </div>
		    <div class="card-content">
			<h4 class="card-title">Client Tasks</h4>
			<div class="table-responsive">
                            <table class="table table-hover dataTable" id="task-table" role='grid' width='100%'>
                                <thead>
                                    <tr>
                                        <th>Project</th>
                                        <th>Name</th>
                                        <th>Description</th>
                                        <th>Assigned to</th>
                                        <th>Priority</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                            </table>

            </div>
        </div> <!-- end of .card-content -->
    </div>
</div>

</div>
</div>
</div>


@endsection

@push('view-styles')
<link href="//cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css" rel="stylesheet">
@endpush

@push('view-scripts')
<script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>

<script>
var _table;
$(function() {
    _table = $('#task-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '/dashboard/client-tasks/datatables',
        "columns": [
                { "data": "project_id" },
                { "data": "name" },
                { "data": "description" , width: "40%"},
                { "data": "va_id"},
                { "data": "priority"},
                { "data": "status" },
                { "data": "actions", width: "150px", orderable: false, searchable: false},
        ]
    });
});

     var temp_delete_id = 0;

    //this shows the modal
    $(document).on("click", ".button-delete", function(){
        var id = $(this).attr('data-id');
        temp_delete_id = id;
        $('.modal-body .content').html('<i class="fa fa-spin fa-refresh"></i>');
        $.ajax({
            url: "/dashboard/client-tasks/show/"+id,
            dataType: "json"
        }).done(function(data) {
            $('.modal-body .content').html(data.html);
            $('#modal-warning').modal('show');
        }); 
    });

    //this does the actual delete
    $("#button-delete").click(function(){
        $(this).html('<i class="fa fa-spin fa-refresh"></i> Delete');
        $.ajax({
            url: "/dashboard/client-tasks/delete/"+temp_delete_id,
            dataType: "json"
        }).done(function(data) {
            $('#modal-warning').modal('hide');
            $("#button-delete").html('Delete');
            _table.ajax.reload( null, false );
        }); 
    });

</script>

@endpush