@extends('layouts.adminlte-master')
@section('page-title', "Tasks")

@section('content')
<div class="col-lg-12">

    @if ($errors->any())
    <div class="box box-warning box-solid">
        <div class="box-header with-border">
            <h3 class="box-title">Errors</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
            <!-- /.box-tools -->
        </div>

        <!-- /.box-header -->
        <div class="box-body">
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </div>
        <!-- /.box-body -->
    </div>
    @endif

    @if (session()->has('notification_message'))
    <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-info"></i> Success!</h4>
        {{ session()->get('notification_message') }}
    </div>
    @endif  
    
    <div class="panel panel-default">

        <div class="panel-heading">
        @if (isset($q))
            <i class="fa fa-edit"></i> Edit Task
        @else
            <i class="fa fa-plus"></i> Add Task
        @endif
        </div>

        <div class="panel-body">
            <div class="col-md-12">
                @if (isset($q))
                <form enctype="multipart/form-data" method="POST" action="{{url('/dashboard/client-tasks/update')}}">
                @else
                <form enctype="multipart/form-data" method="POST" action="{{url('/dashboard/client-tasks/create')}}">
                @endif
                {{ csrf_field() }}

                        @if (isset($q))
                        <input type='hidden' value='{{$q->id}}' name='id'/>
                        @endif

                    <div class="row">
                        <div class="col-lg-12">

                            <div class="col-md-12 col-lg-12">

                                <div class="box box-info">
                                    <div class="box-header">
                                        <h3 class="box-title"><i class="fa fa-pencil"></i> Task Info</h3>
                                    </div>
                                    <div class="box-body">

                                        @if (isset($q))

                                            <div class="form-group">
                                                <label>Priority</label>
                                                <select name="priority" class="form-control input-lg">
                                                    @if ($q->priority == "High")
                                                    <option value="1" selected>High</option>
                                                    @else
                                                    <option value="1">High</option>
                                                    @endif

                                                    @if ($q->priority == "Normal")
                                                    <option value="2" selected>Normal</option>
                                                    @else
                                                    <option value="2">Normal</option>
                                                    @endif

                                                    @if ($q->priority == "Low")
                                                    <option value="3" selected>Low</option>
                                                    @else
                                                    <option value="3">Low</option>
                                                    @endif
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <label>Assign To</label>
                                                <select name="va_id" class="form-control input-lg">
                                                    @foreach ($va_users as $va)
                                                    @if ($q->va_id == $va->id)
                                                    <option selected value="{{$va->id}}">{{$va->first_name}} {{$va->last_name}}</option>
                                                    @else
                                                    <option value="{{$va->id}}">{{$va->first_name}} {{$va->last_name}}</option>
                                                    @endif
                                                    @endforeach
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <label>Project</label>
                                                <select name="project_id" class="form-control input-lg">
                                                    <option value="0">No Project</option>
                                                    @foreach ($projects as $project)
                                                    @if ($q->project_id == $project->id)
                                                    <option selected value="{{$project->id}}">{{$project->name}}</option>
                                                    @else
                                                    <option value="{{$project->id}}">{{$project->name}}</option>
                                                    @endif
                                                    @endforeach
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <label>Name</label>
                                                <input type="text" class="form-control input-lg" name="name" value="{{ $q->name }}" placeholder="Name">
                                            </div>

                                            <div class="form-group">
                                                <label>Description</label>
                                                <textarea class="form-control input-lg" name="description">{{ $q->description }}</textarea>
                                            </div>

                                        @else

                                            <div class="form-group">
                                                <label>Priority</label>
                                                <select name="priority" class="form-control input-lg">
                                                    <option value="1">High</option>
                                                    <option value="2" selected>Normal</option>
                                                    <option value="3">Low</option>
                                                </select>
                                            </div>
                                        
                                            <div class="form-group">
                                                <label>Assign To</label>
                                                <select name="va_id" class="form-control input-lg">
                                                    @foreach ($va_users as $va)
                                                    <option value="{{$va->id}}">{{$va->first_name}} {{$va->last_name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <label>Project</label>
                                                <select name="project_id" class="form-control input-lg">
                                                    <option value="0">No Project</option>
                                                    @foreach ($projects as $project)
                                                    <option value="{{$project->id}}">{{$project->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <label>Name</label>
                                                <input type="text" class="form-control input-lg" name="name" value="{{ old('name') }}" placeholder="Name">
                                            </div>

                                            <div class="form-group">
                                                <label>Description</label>
                                                <textarea class="form-control input-lg" name="description">{{ old('description') }}</textarea>
                                            </div>
                                        @endif

                                    </div>

                            </div>

                        </div>


                        <div class="col-xs-12">
                            <div class="box box-info">
                                <div class="box-footer clearfix">
                                @if (isset($q))
                                <button type="submit" class="pull-right btn btn-success btn-lg">Update</button>
                                @else
                                    <button type="submit" class="pull-right btn btn-success btn-lg">Add</button>
                                @endif
                                </div>
                            </div>
                        </div>

                    </div>
                    
                    </div>
                </form>
            </div>
        </div>
    </div>


</div>

@endsection
@push('view-scripts')
<script src="{{ asset('js/jquery.are-you-sure.js') }}"></script>
<script>
  $(function() {
    $('form').areYouSure(
      {
        message: 'It looks like you have been editing something. '
               + 'If you leave before saving, your changes will be lost.'
      }
    );
  });
</script>
@endpush