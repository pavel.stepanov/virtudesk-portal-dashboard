@extends('layouts.adminlte-master')
@section('page-title', "Roles")

@section('content')
<div class="col-lg-12">

    @if ($errors->any())
    <div class="box box-warning box-solid">
        <div class="box-header with-border">
            <h3 class="box-title">Errors</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
            <!-- /.box-tools -->
        </div>

        <!-- /.box-header -->
        <div class="box-body">
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </div>
        <!-- /.box-body -->
    </div>
    @endif

    @if (session()->has('notification_message'))
    <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-info"></i> Success!</h4>
        {{ session()->get('notification_message') }}
    </div>
    @endif     

    <div class="panel panel-default">

        <div class="panel-heading">
        @if (isset($q))
            <i class="fa fa-user-circle-o fa-minus"></i> Edit Role
        @else
            <i class="fa fa-user-circle-o fa-minus"></i> Add Role
        @endif
        </div>

        <div class="panel-body">
            <div class="col-md-12">
                @if (isset($q))
                <form enctype="multipart/form-data" method="POST" action="{{url('/dashboard/roles/update')}}">
                @else
                <form enctype="multipart/form-data" method="POST" action="{{url('/dashboard/roles/create')}}">
                @endif
                {{ csrf_field() }}

                        @if (isset($q))
                        <input type='hidden' value='{{$q->id}}' name='id'/>
                        @endif

                    <div class="row">
                        <div class="col-lg-12">

                        <div class="col-md-12 col-lg-12">

                        <div class="box box-info">
                            <div class="box-header">
                                <h3 class="box-title"><i class="fa fa-user"></i> Role Info</h3></div>
                                <div class="box-body">

                                    @if (isset($q))

                                        <div class="form-group">
                                            <label>Name</label>
                                            <input type="text" class="form-control input-lg" name="name" value="{{ $q->name }}" placeholder="Name">
                                        </div>

                                        <div class="form-group">
                                            <label>Slug</label>
                                            <input type="text" class="form-control input-lg" name="slug" value="{{ $q->slug }}" placeholder="Slug">
                                        </div>

                                        <div class="form-group">
                                            <label>Description</label>
                                            <input type="text" class="form-control input-lg" name="description" value="{{ $q->description }}" placeholder="Description">
                                        </div>

                                    
                                    @else
                                  

                                        <div class="form-group">
                                            <label>Name</label>
                                            <input type="text" class="form-control input-lg" name="name" value="{{ old('name') }}" placeholder="Name">
                                        </div>

                                        <div class="form-group">
                                            <label>Slug</label>
                                            <input type="text" class="form-control input-lg" name="slug" value="{{ old('slug') }}" placeholder="Slug">
                                        </div>

                                        <div class="form-group">
                                            <label>Description</label>
                                            <input type="text" class="form-control input-lg" name="description" value="{{ old('description') }}" placeholder="Description">
                                        </div>

                                        
                                    @endif

                                </div>
                            </div>
                        </div>




                        <div class="col-xs-12">
                            <div class="box box-info">
                                <div class="box-footer clearfix">
                                @if (isset($q))
                                <button type="submit" class="pull-right btn btn-success btn-lg">Update</button>
                                @else
                                    <button type="submit" class="pull-right btn btn-success btn-lg">Add</button>
                                @endif     
                                </div>
                            </div>
                        </div>

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>


</div>
<div class='clearfix'></div>
@endsection