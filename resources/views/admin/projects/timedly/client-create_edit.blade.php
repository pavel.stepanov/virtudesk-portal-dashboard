@extends('layouts.timedly.app')

@section('pageTitle')
    @if (isset($q))
        Edit Project
    @else
        Add Project
    @endif
@endsection

@section('pageScripts')
<script>
    var updated = false;
    $(document).ready(function () {
        // $('select, input, textarea').on('change input', function() {
        //     if( !updated ) {
        //         window.onbeforeunload = function(){
        //             return "Changes you've made may not be saved.";
        //         };

        //         updated = true;
        //     }
        // });

        @php
            if(session('notification_message') || $errors->any()):
                $message = '';
                $type = '';
                if( session('notification_message') ) {
                    $message = session('notification_message');
                    $type = 'success';
                    $title = 'Success!';
                } else {
                    $message = implode('<br/>', $errors->all());
                    $type = 'error';
                    $title = 'Something went wrong.';
                }
        @endphp

        swal({
            title: '{{$title}}',
            html: '{!!$message!!}',
            type: '{{$type}}',
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary'
        });

        @php
            endif;
        @endphp
    });
</script>
@endsection

@section('content')
    <div class="header bg-gradient-info pb-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <div class="col-lg-6 col-7">
                        @if (isset($q))
                    <h6 class="h2 text-white d-inline-block mb-0">Edit Project</h6>
                        @else
                            <h6 class="h2 text-white d-inline-block mb-0">Create a new Project</h6>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Page content -->

    <div class="container-fluid mt--6">
        <div class="row justify-content-md-center">
            <div class="col-sm-8">
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header">
                        <h3 class="mb-0">Project Info</h3>
                    </div>
                    <!-- Card body -->
                    <div class="card-body">
                        @if (isset($q))
                        <form enctype="multipart/form-data" method="POST" action="{{url('/dashboard/projects/update')}}" id="taskForm">
                        @else
                        <form enctype="multipart/form-data" method="POST" action="{{url('/dashboard/projects/create')}}" id="taskForm">
                        @endif
                            {{ csrf_field() }}

                            @if (isset($q))
                                {{ Form::hidden('id', $q->id) }}
                            @endif

                            <div class="form-group">
                                <label class="form-control-label" for="name">Name</label>
                                {{ Form::text('name', isset($q) ? $q->name : null, ['class' => 'form-control', 'placeholder' => 'Name of the task']) }}
                            </div>

                            <div class="form-group">
                                <label class="form-control-label" for="exampleFormControlTextarea1">Description</label>
                                {{ Form::textarea('description', isset($q) ? $q->description : null, ['class' => 'form-control', 'placeholder' => 'Description of the Project', 'rows' => 3]) }}
                            </div>

                            <button class="btn btn-icon btn-success float-right" type="submit">
                                <span class="btn-inner--text">{{ isset($q) ? 'Update Project' : 'Save Project' }}</span>
                            </button>
                       </form>
                    </div>
                </div>
            </div>
        </div>
@endsection
