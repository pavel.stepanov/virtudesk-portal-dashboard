@extends('layouts.adminlte-master')
@section('page-title', "Projects")

@section('content')

@include('admin.delete-modal')

<section class='content'>
    <div class='row'>
        <div class='col-xs-12'>
            <div class='box'>
                <div class='box-header'>
                    <h3 class='box-title'>Projects</h3>
                    <span class="pull-right"><a href="/dashboard/projects/create" class="btn btn-primary"><i class="fa fa-plus"></i> Create Project</a></span>
                </div>
                <div class='box-body'>
                    <div class='row'>
                        <div class='col-sm-12'>
                            <table class="table table-bordered table-hover dataTable" id="projects-table" role='grid' width='100%'>
                                <thead>
                                    <tr>
                                        <th>Project Name</th>
                                        <th>Description</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>                                
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

@push('view-styles')
<link href="//cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css" rel="stylesheet">
<style>
    .project-name-list {
        padding-bottom:5px;
    }
    .progress-group .progress-number {
        margin-left:5px;
    }
</style>
@endpush

@push('view-scripts')
<script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>

<script>

    var _table;
    $(function() {
        _table = $('#projects-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '/dashboard/projects/datatables',
            "columns": [
                    { "data": "name" },
                    { "data": "description", width:"50%" },
                    { "data": "actions", width: "150px", orderable: false, searchable: false},
            ]
        });
    });

     var temp_delete_id = 0;

    //this shows the modal
    $(document).on("click", ".button-delete", function(){
        var id = $(this).attr('data-id');
        temp_delete_id = id;
        $('.modal-body .content').html('<i class="fa fa-spin fa-refresh"></i>');
        $.ajax({
            url: "/dashboard/projects/show/"+id,
            dataType: "json"
        }).done(function(data) {
            $('.modal-body .content').html(data.html);
            $('#modal-warning').modal('show');
        }); 
    });

    //this does the actual delete
    $("#button-delete").click(function(){
        $(this).html('<i class="fa fa-spin fa-refresh"></i> Delete');
        $.ajax({
            url: "/dashboard/projects/delete/"+temp_delete_id,
            dataType: "json"
        }).done(function(data) {
            $('#modal-warning').modal('hide');
            $("#button-delete").html('Delete');
            _table.ajax.reload( null, false );
        }); 
    });

</script>

@endpush