<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        {{-- <meta charset="utf-8"> --}}
        <title>Invoice</title>
        <link rel="stylesheet" href="{{ public_path() }}/css/pdf.css">
    </head>
<body style='margin-top:-30px'>

<table width='100%' style='font-size:14px'>
    <tr>
        <td width="10%">&nbsp;</td>
        <td width="20%">
        <img width="200px" src="{{ public_path() }}/images/logo-virtudesk.jpg">
        <td width="5%">&nbsp;</td>
        </td>
        <td valign="center" width="65%" style="vertical-align: middle">
            <br/>
            <div><strong>Virtudesk</strong></div>
            <div>4020 Lake Washington Blvd NE, Suite #310</div>
            <div>Kirkland, WA 98033</div>
            <div>360 930 3579</div>
        </td>
    </tr>
</table>

<table width='100%' style='font-size:14px'>
    <tr>
        <td width="50%">
            <div>{{$client->first_name}} {{$client->last_name}}</div>
            <div>{{$client->address1}}</div>
            <div>{{$client->address2}}</div>
            <div>{{$client->city}}</div>        
        </td>
        <td width="50%">
            <div>INVOICE NUMBER: {{$invoice_number}}</div>
            <div>DATE: {{date("Y-m-d",time())}}</div>
            <div>BILLING PERIOD: {{$start_date}} - {{$end_date}}</div>
        </td>
    </tr>
</table>
<br/>
<table class="table table-bordered table-hover" id="bills-table" role='grid' width='100%'>
    <thead style='font-size:14px'>
        <tr>
            <th style='text-align:center'>Date</th>
            <th style='text-align:center'>Service</th>
            <th style='text-align:center'>Activity</th>
            <th style='text-align:center'>Time</th>
            <th style='text-align:center'>Rate</th>
            <th style='text-align:center'>Amount</th>
        </tr>
    </thead>
    <tbody style='font-size:10px;'>

        @if (count($timesheet)==0)
            <tr>
            <td colspan="6">
                <center>No data available in table</center>
            </td>
            </tr>
        @else
        @php
            $overall_total = 0;
        @endphp
            @foreach ($timesheet as $ts)
            @php
                $rate = $ts->client->getRateBilling($ts->user_id);
                $total = $ts->total_billable_hours * $rate;
                $overall_total = $overall_total + $total;
            @endphp
            <tr>
                <td style='line-height:6px;'>{{$ts->schedule_date}}</td>
                <td style='line-height:6px;'>{{$ts->user->first_name}} {{$ts->user->last_name}}</td>
                <td style='line-height:6px;'>Regular Hours</td>
                <td style='text-align:right;line-height:6px;'>{{$ts->total_billable_hours}}</td>
                <td style='text-align:center;line-height:6px;'>{{number_format($rate,2)}}</td>
                <td style='text-align:center;line-height:6px;'>{{number_format($total,2)}}</td>
            </tr>
            @endforeach
        @endif
    </tbody>
</table>

@if (count($timesheet)!=0)
    @php
        $overall_total = $overall_total + $setup_fee;
        $total_tax = $overall_total * ($tax/100);
        $amount_due = ( $overall_total + $total_tax ) - $advance_payments - $credits;
        $amount_due_text = "AMOUNT DUE:";
    @endphp
    @if ($amount_due<0)
        @php
            $amount_due = number_format(abs($amount_due),2);
            $amount_due = "(" .$amount_due . ")";
            $amount_due_text = "AMOUNT DUE: (ADVANCES BALANCE)";
        @endphp
    @else
        @php
        $amount_due = number_format($amount_due,2);
        @endphp
    @endif
    <table class="table table-bordered table-hover" role='grid' width='100%' style='font-size:14px'>
        <tbody>
            @if ($setup_fee!=0)
            <tr>
                <td class='text-left'>SETUP FEE:</td>
                <td class='text-right'>{{number_format($setup_fee,2)}}</td>
            </tr>
            @endif
            <tr>
                <td class='text-left'>INVOICE TOTAL:</td>
                <td class='text-right'>{{number_format($overall_total,2)}}</td>
            </tr>

            <tr>
                <td class='text-left'>TAXES:</td>
                <td class='text-right'>{{number_format($total_tax, 2)}}</td>
            </tr>
            @if ($advance_payments!=0)
            <tr>
                <td class='text-left'>LESS ADVANCES:</td>
                <td class='text-right'>{{number_format($advance_payments, 2)}}</td>
            </tr>
            @endif
            @if ($credits!=0)
            <tr>
                <td class='text-left'>LESS CREDITS: {{$credit_text}}</td>
                <td class='text-right'>{{number_format($credits, 2)}}</td>
            </tr>
            @endif
            <tr>
                <td class='text-left'>{{$amount_due_text}}</td>
                <td class='text-right'><strong>{{$amount_due}}</strong></td>
            </tr>
        </tbody>
    </table>
@endif
<br><br>
<section>
<p style='font-size:14px'>Thank you for choosing Virtudesk Virtual Assistants!<br>We look forward to doing more business with you!</p>
<br>
<div style='font-size:12px'>
<p><strong>IMPORTANT NOTICE</strong></p>
<p>We believe these are all the services rendered for this billing period. Should there be any corrections
please reach out so we can make adjustments on the next invoice.</p>
</div>
</section>

</body>
</html>