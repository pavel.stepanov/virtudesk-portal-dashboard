@extends('layouts.adminlte-master')
@section('page-title', "Client Bills")

@section('content')

<section class='content'>
    <div class='row'>
        <div class='col-xs-12'>
            <div class='box'>
                <div class='box-header'>
                    <h3 class='box-title'>Generate Billing for {{$client->first_name}} {{$client->last_name}}</h3>
                </div>
                <div class='box-body'>

                    <div class='row'>
                        <div class='col-md-4'>
                            <label>Start Date</label>
                            <div class="input-group date date_picker" data-id="start_date">
                            <input type="text" id='start_date' name='start_date' class="form-control input-lg" value="{{Cookie::get('bill-start-date',date('Y-m-d', strtotime(now())))}}">
                            <div class="input-group-addon">
                            <span class="glyphicon glyphicon-th"></span>
                            </div>
                            </div>
                        </div>
                        <div class='col-md-4'>
                        <label>End Date</label>
                            <div class="input-group date date_picker" data-id="end_date">
                            <input type="text" id='end_date' name='end_date' class="form-control input-lg" value="{{Cookie::get('bill-end-date',date('Y-m-d', strtotime(now())))}}">
                            <div class="input-group-addon">
                            <span class="glyphicon glyphicon-th"></span>
                            </div>
                            </div>
                        </div>

                        <div class='col-md-4'>
                            <label>Invoice Number</label>
                            <input required id="invoice_number" name="invoice_number" type="text" class="form-control input-lg" value=""/>
                        </div>
                    </div>

                    <div class='row'><hr></div>

                    <div class='row'>

                        <div class='col-md-4'>
                            <label>Credits</label>
                            <input id="credits" name="credits" type="text" class="form-control input-lg" value="0"/>
                        </div>

                        <div class='col-md-4'>
                            <label>Credit Info</label>
                            <input id="credit_text" name="credit_text" type="text" class="form-control input-lg" value=""/>
                        </div>

                        <div class='col-md-4'>
                            <label>VA</label>
                            <select class="form-control input-lg" id="va" name="va" multiple="multiple" required>
                                @foreach ($users as $va)
                                <option value="{{$va->id}}">{{$va->fullname}}</option>
                                @endforeach 
                            </select>
                        </div>

                    </div>
                    <br>
                    <div class='row'>

                        <div class='col-md-3'>
                            <label>Advance Payments</label>
                            <input id="advance_payments" name="advance_payments" type="text" class="form-control input-lg" value="0"/>
                        </div>

                        <div class='col-md-3'>
                            <label>Setup Fee</label>
                            <input id="setup_fee" name="setup_fee" type="text" class="form-control input-lg" value="0"/>
                        </div>

                        <div class='col-md-2'>
                            <label>Tax Percentage</label>
                            <input id="tax" name="tax" type="text" class="form-control input-lg" value="3.8"/>
                        </div>

                        <div class='col-md-4'>
                        <label>Actions</label>
                        <div class="input-group btn-toolbar">
                        <button class="btn btn-primary input-lg" id='generate-bill'><i class="fa fa-cog"></i> Generate Bill</button>
                        <button class="btn btn-primary input-lg" id='generate-pdf'><i class="fa fa-file"></i> Generate PDF</button>
                        </div>
                        </div>
                    </div>

                    <div class='row'><hr></div>


                    <div class='row'>
                        <div class='col-sm-12 bill-content'>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection


@push('view-styles')
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css" />
<link href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/css/bootstrap-multiselect.css" rel="stylesheet">
<style>
.multiselect-native-select {
    display:block;
}
</style>
@endpush

@push('view-scripts')
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/js/bootstrap-multiselect.js"></script>

<script>

// use this transport for "binary" data type
$.ajaxTransport("+binary", function(options, originalOptions, jqXHR){
    // check for conditions and support for blob / arraybuffer response type
    if (window.FormData && ((options.dataType && (options.dataType == 'binary')) || (options.data && ((window.ArrayBuffer && options.data instanceof ArrayBuffer) || (window.Blob && options.data instanceof Blob)))))
    {
        return {
            // create new XMLHttpRequest
            send: function(_, callback){
		// setup all variables
                var xhr = new XMLHttpRequest(),
                    url = options.url,
                    type = options.type,
		// blob or arraybuffer. Default is blob
                    dataType = options.responseType || "blob",
                    data = options.data || null;
				
                xhr.addEventListener('load', function(){
                    var data = {};
                    data[options.dataType] = xhr.response;
		// make callback and send data
                    callback(xhr.status, xhr.statusText, data, xhr.getAllResponseHeaders());
                });

                xhr.open(type, url, true);
                xhr.responseType = dataType;
                xhr.send(data);
            },
            abort: function(){
                jqXHR.abort();
            }
        };
    }
});




$(function () {

    $("#generate-pdf").hide();

    $('.date_picker').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true
    }).on('changeDate', function(e) {
        $("#generate-pdf").hide();
        if ($(this).attr('data-id')=='start_date') {
            Cookies.remove('bill-start-date');
            Cookies.set('bill-start-date', $('#start_date').val(), { expires: 7 });
            console.log($('#start_date').val());
        } else {
            Cookies.remove('bill-end-date');
            Cookies.set('bill-end-date', $('#end_date').val(), { expires: 7 });
        }
    });

    $('#va').multiselect({
        maxHeight: 200,
        buttonWidth: '150px',
        numberDisplayed	: 1,
        includeSelectAllOption: true,
        onChange: function(option, checked, select) {
            $("#generate-pdf").hide();
        }
    });



    $("#generate-bill").click(function(){

        start_date = $("#start_date").val();
        end_date = $("#end_date").val();
        advance_payments = $("#advance_payments").val();
        setup_fee = $("#setup_fee").val();
        credits = $("#credits").val();
        credit_text = $("#credit_text").val();
        tax = $("#tax").val();
        va = $("#va").val();

        if (va==0) {
            alert('Select a VA first');
            return;
        }

        $('.bill-content').html('<i class="fa fa-spin fa-refresh"></i>');
        $.ajax({
            url: "/dashboard/bills/generate/",
            dataType: "json",
            data: {
                "client_id" : "{{$client->id}}",
                "start_date" :  start_date,
                "end_date" :  end_date,
                "setup_fee" : setup_fee,
                "advance_payments" : advance_payments,
                "credits" : credits,
                "credit_text" : credit_text,
                "tax" : tax,
                "va_id" : va
            },
        }).done(function(data) {
            $('.bill-content').html(data.html);
            $("#generate-pdf").show();
        });

    });


    $("#generate-pdf").click(function(){

        start_date = $("#start_date").val();
        end_date = $("#end_date").val();
        advance_payments = $("#advance_payments").val();
        setup_fee = $("#setup_fee").val();
        credits = $("#credits").val();
        credit_text = $("#credit_text").val();
        tax = $("#tax").val();
        invoice_number = $("#invoice_number").val();
        va = $("#va").val();

        if (va==0) {
            alert('Select a VA first');
            return;
        }


        if (invoice_number=='') {
            alert('Invoice Number is required');
            return;
        }

        //$('.bill-content').html('<i class="fa fa-spin fa-refresh"></i>');
        $.ajax({
            url: "/dashboard/bills/generate/pdf",
            dataType: 'binary',
            data: {
                "client_id" : "{{$client->id}}",
                "start_date" :  start_date,
                "end_date" :  end_date,
                "advance_payments" : advance_payments,
                "setup_fee" : setup_fee,
                "credits" : credits,
                "credit_text" : credit_text,
                "tax" : tax,
                "invoice_number" : invoice_number,
                "va_id" : va
            },
        }).done(function(data) {

            var url = window.URL.createObjectURL(data);
            var $a = $('<a />', {
            'href': url,
            'download': 'invoice-{{$client->first_name}}-{{$client->last_name}}.pdf',
            'text': "click"
            }).hide().appendTo("body")[0].click();
            setTimeout(function() {
            window.URL.revokeObjectURL(url);
            }, 10000);

            });

    });

});
</script>
@endpush