@extends('layouts.adminlte-master')
@section('page-title', "Role's Permissions")

@section('content')

@include('admin.delete-modal')
<form enctype="multipart/form-data" method="POST" action="{{url('/dashboard/role-permissions/'.$role->id.'/update')}}">

<section class='content'>
    <div class='row'>
        <div class='col-xs-12'>

            @if (session()->has('notification_message'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-info"></i> Success!</h4>
                {{ session()->get('notification_message') }}
            </div>
            @endif 

            <div class='box'>
                <div class='box-header'>
                    <h3 class='box-title'>{{$role->name}} Role's Permissions</h3>
                </div>



                    {{ csrf_field() }}
                    <input type='hidden' name="role_id" value="{{$role->id}}" />
                    <div class='box-body'>
                        <div class='row'>
                            <div class='col-sm-12'>
                            @foreach ($permissions as $key => $permission)
                            <div class="col-sm-6 col-md-3">
                                    <div class="checkbox">
                                    <label>
                                        @if ($role->can($permission->name))
                                            <input name='permissions[{{$permission->id}}]' type="checkbox" checked> {{$permission->name}}
                                        @else
                                            <input name='permissions[{{$permission->id}}]' type="checkbox" > {{$permission->name}}
                                        @endif
                                    </label>
                                    </div>
                            </div>
                            @endforeach
                            </div>
                            
                        </div>

                        <div class='row'>
                        <div class="col-xs-12">
                        <div class="box box-info">
                            <div class="box-footer clearfix">
                            <button type="submit" class="pull-right btn btn-success btn-lg">Update</button>
                            </div>
                        </div>
                    </div>
                        </div>
                    </div>

                


            </div>
        </div>
    </div>
</section>
</form>
@endsection
@push('view-scripts')
<script src="{{ asset('js/jquery.are-you-sure.js') }}"></script>
<script>
  $(function() {
    $('form').areYouSure(
      {
        message: 'It looks like you have been editing something. '
               + 'If you leave before saving, your changes will be lost.'
      }
    );
  });
</script>
@endpush