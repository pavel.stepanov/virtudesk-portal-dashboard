@extends('layouts.adminlte-master')
@section('page-title', "Timeline")

@section('content')
<section class='content'>
    <div class='row'>
        <div class='col-xs-12'>
            <div class='box'>
                <div class='box-header'>
                    <h3 class='box-title'>Timeline</h3>
                </div>
                <div class='box-body'>


                    <div class='row'>
                        <div class='col-md-6'>
                            <label>Select Date</label>
                            <div class="input-group date" id="datepicker">
                            <input type="text" id='target_date' name='target_date' class="form-control input-lg" value="{{date('Y-m-d', strtotime(now()))}}">
                            <div class="input-group-addon">
                            <span class="glyphicon glyphicon-th"></span>
                            </div>
                            </div>
                        </div>
                        
                    </div>

                    <div class='row'><hr></div>
                    <div class='row'>
                        <div class='col-sm-12'>
                            <table class="table table-bordered table-hover dataTable" id="timeline-table" role='grid' width='100%'>
                                <thead>
                                    <tr>
                                        <th>VA</th>
                                        <th>Client</th>
                                        <th>Time Worked</th>
                                        <th>Timeline</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@push('view-styles')
<link href="//cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css" />
@endpush

@push('view-scripts')
<script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
<script src="{{ asset('js/jquery.sparkline.min.js') }}"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
<script>

$(function() {
    fetchData();
});

$(function () {
    $('#datepicker').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true
    }).on('changeDate', function(e) {
        target_date = $("#target_date").val();
        fetchData(target_date);
    });

});

function fetchData(target_date) {
    $('#timeline-table').DataTable().destroy();
    $('#timeline-table').DataTable({
        processing: true,
        serverSide: true,
        searching: false, info: false,
        pageLength: 25,
        ajax: { 
            url:'/dashboard/timeline/datatables',
            data: {
                target_date : target_date,
                type : 'GET'
            }
        },
        "columns": [
                { "data": "user_id", "width": "20%" },
                { "data": "client_id", "width": "20%" },
                { "data": "time_worked", "width": "10%" },
                { "data": "timeline", "width": "50%"}
        ],
        "drawCallback": function(settings) {
            $(".sparkline").sparkline('html', { 
                        enableTagOptions: true,
                        barWidth:2,
                        barSpacing:0,
                        colorMap: { '2': '#ffffff', '1': '#08e008', '0':'#909090', '-1':'#f04040' },
                        tooltipFormatFieldlist: ['Online', 'Break', 'Offline'], 
                        tooltipFormatFieldlistKey: 'field' 
            });
        }
    });
}

</script>
@endpush