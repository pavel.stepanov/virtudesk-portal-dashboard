<div class='content'>
    <table class="table table-hover" id="members-table">
        <thead> 
            <tr> 
                <th>Name</th>
                <th>Client</th> 
                <th>Date</th>  
            </tr>
        </thead>
        <tbody>
            @if($q) 
                @foreach($q as $user) 
                
                <tr>
                    <th scope="row">{{ $user->first_name }} {{ $user->last_name }}</th>
                    <td>{{ $user->getCurrentClientName() }} </td>
                    <td>{{ $user->getCurrentAttendance()->date_in }}</td>
                </tr>
                @endforeach
            @else 
                <tr>
                    <td colspan="3">No Team Members</td>
                </tr>
            @endif
            
        </tbody>
    </table>
   
</div>