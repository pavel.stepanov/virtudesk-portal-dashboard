<div class="col-md-4">
	<div class="box">
		<div class="box-header">
			<h3 class="box-title">Tasks</h3> 
		</div>
		<div class="box-body">
			<canvas id="pieChart" style="height:250px"></canvas>
		</div><!-- box-body -->
		<div class="box-footer">
		</div>
	</div>

</div>
