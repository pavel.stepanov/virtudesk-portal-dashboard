<div class="col-md-12">
	<div class="box">
		<div class="box-header">
			<h3 class="box-title">Tracker Activities</h3> 
		</div>
		<div class="box-body">
			<div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_activities" data-toggle="tab" aria-expanded="true"><i class="fa fa-user-o" aria-hidden="true"></i> User</a></li>
              <li class=""><a href="#tab_screenshot" data-toggle="tab" aria-expanded="false"><i class="fa fa-image" aria-hidden="true"></i> Screenshots</a></li>
              <li class=""><a href="#tab_idle" data-toggle="tab" aria-expanded="false"><i class="fa fa-clock-o" aria-hidden="true"></i> Idle</a></li>
             
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_activities">
                <div class="">
                	<h4>10 Latest User Activities</h4>
                </div>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_screenshot">
              	<div class="">
                	<h4>10 Latest User Screenshots</h4>
                </div>
                @if($screenshot) 
                  <table class="table table-hover">
                    <thead>
                      <th>Screenshot</th>
                      <th>Name</th>
                      <th>Time</th>
                    </thead>
                    @foreach($screenshot as $s) 
                      <tr>
                        <td></td>
                        <td>{{ $s->user->first_name }} {{ $s->user->last_name }}</td>
                        <td>{{ $s->created_at }}</td>
                      </tr>
                    @endforeach
                  </table>
                @endif
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_idle">
              	<div class="">
                  <h4>Idles</h4>
                </div>
                @if($idle) 
                  <table class="table table-hover">
                    <thead>
                      <th>Name</th>
                      <th>Idle (Hr:Min:Sec)</th>
                      <th>Time</th>
                    </thead>
                    @foreach($idle as $i) 
                      <tr>
                        <td>{{ $i->user->first_name }} {{ $i->user->last_name }}</td>
                        <td>{{ $i->idle_time }}</td>
                        <td>{{ $i->created_at }} </td>
                      </tr>
                    @endforeach
                  </table>
                @endif
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
		</div><!-- box-body -->
		<div class="box-footer">
		</div>
	</div>

</div>