@extends('layouts.adminlte-master')
@section('page-title', "Dashboard")

@section('content')

@include('admin.user.modal-clock')

@include('admin.user.modal-task')

<div class="row">
    <div class="col-md-6">

        <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Download Desktop App</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                    <span class="info-box-icon bg-blue"><i class="fa fa-desktop"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Download the Desktop Application</span>
                        <span class="info-box-number"><a href='{{$mac_download}}'>For Mac</a></span>
                        <span class="info-box-number"><a href='{{$win_download}}'>For Windows</a></span>
                    </div>
                    <!-- /.info-box-content -->
            </div>
        </div>

        <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Today's Schedule</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                <table class="table no-margin">
                  <thead>
                  <tr>
                    <th>Day</th>
                    <th>Time</th>
                    <th>Shift</th>
                    <th>Client</th>
                  </tr>
                  </thead>
                  <tbody>
                    @if (count($schedules))
                        @foreach ($schedules as $schedule)
                            <tr>
                                <td>{{$schedule->user_day_of_week}}</td>
                                <td>{!! $schedule->showVASchedule() !!}</td>
                                <td>{{$schedule->showShift()}}</td>
                                <td>{{$schedule->client->first_name}} {{$schedule->client->last_name}}</td>
                            </tr>
                        @endforeach
                    @else 
                        <tr>
                            <td colspan="5">No items to display</td>
                        </tr>
                    @endif

                  </tbody>
                </table>
              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
              <a href="/dashboard/schedules" class="btn btn-sm btn-default btn-flat pull-right">View All</a>
            </div>
            <!-- /.box-footer -->
        </div>

        <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Clients</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                <table class="table no-margin">
                  <thead>
                  <tr>
                    <th>Client</th>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Timezone</th>
                  </tr>
                  </thead>
                  <tbody>
                    @if (count($clients))
                        @foreach ($clients as $client)
                        @php
                        $client_timezone = new DateTime(null, new DateTimeZone($client->showTimezone()));
                        @endphp
                            <tr>
                                <td>{{$client->first_name}} {{$client->last_name}}</td>
                                <td>{{$client_timezone->format("m-d-Y")}}</td>
                                <td width='25%' class="clock_client" data-timestamp="{{$client_timezone->getTimestamp()+$client_timezone->getOffset()}}"></td>
                                <td>{{$client->timezone}}</td>
                            </tr>
                        @endforeach
                    @else 
                        <tr>
                            <td colspan="5">No items to display</td>
                        </tr>
                    @endif

                  </tbody>
                </table>
              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
        </div>

    </div>

    <div class="col-md-6">
        <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Your Timezone</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                    <span class="info-box-icon bg-blue"><i class="fa fa-calendar"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">{{$va_timezone->format("l")}}</span>
                        <span id="va-date" class="info-box-text">{{$va_timezone->format("M jS Y")}}</span>
                        <span id="clock" class="info-box-number clock" data-timestamp="{{$va_timestamp}}"></span>
                    </div>
                    <!-- /.info-box-content -->
            </div>
            <!--
            <div class="box-footer clearfix">
            @if (count($clients))
                @if ($is_clocked_in)
                    <div class='pull-left'>Clocked in: {{$clocked_in_timezone}}</div>
                    <a href="javascript:void(0)" data-title="Clock Out" data-status="out" class="btn-clock btn btn-danger btn-flat pull-right">Clock Out <i class="fa fa-edit"></i></a>
                @else
                    <a href="javascript:void(0)" data-title="Clock In" data-status="in" class="btn-clock btn btn-success btn-flat pull-right">Clock In <i class="fa fa-edit"></i></a>
                @endif
            @endif
            </div>
            -->
            <!-- /.box-footer -->
        </div>

        <!--
        <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Today's Tasks</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            @if (count($clients))
                    @if ($is_clocked_in)

                        @if (!empty($current_task))
                            <div class="box-body">
                            <p>Currently working on {{$current_task->name}}</p>
                            @if (count($client_tasks)==0)
                            <input type='hidden' value='generic' id='task_type' />
                            @else
                            <input type='hidden' value='client' id='task_type' />
                            @endif
                            </div>
                            <div class="box-footer clearfix">
                            <div class='pull-left'>Duration: <span id="timer-task" ></span></div>
                            <button type="button" data-title="Stop Task" data-status='stop' class="btn-task pull-right btn btn-info">Stop <i class="fa fa-stop"></i></button>
                            </div>
                        @else
                            <div class="box-body">
                                
                                @if (count($client_tasks)==0)
                                    <input type='hidden' value='generic' id='task_type' />
                                    <div>You have no tasks assigned from clients. You can work from default tasks.</div>
                                    <div class="form-group">
                                        <label>Select a task</label>
                                        <select class="form-control input-lg" id='generic-task'>
                                            @foreach ($tasks as $task)
                                                <option value="{{$task->id}}">
                                                    {{$task->name}}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group">
                                    <label>Provide details on what you are going to work on</label>
                                    <textarea rows="4" cols="50" class="form-control input-lg" id='task_description' maxlength="191"></textarea>
                                    </div>

                                @else 

                                    <input type='hidden' value='client' id='task_type' />
                                    <div class="form-group">
                                        <label>Select a client-assigned task</label>
                                        <select class="form-control input-lg" id="client-task">
                                            @foreach ($client_tasks as $t)
                                                <option value="{{$t->id}}">
                                                {{$t->client->first_name}} {{$t->client->last_name}} {{$t->name}}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                @endif

                            </div>

                            <div class="box-footer clearfix">
                                @if (count($clients))
                                <button type="button" data-title="Start Task" data-status='start' class="btn-task pull-right btn btn-info">Start <i class="fa fa-play"></i></button>
                                @else
                                <button type="button" disabled class="pull-right btn btn-info btn-flat">Start <i class="fa fa-play"></i></button>
                                @endif
                            </div>
                        @endif

                    @else
                    <div class="box-body">
                    <div>You have to clock-in to see available tasks.</div>
                    </div>
                    @endif
            @else
            <div class="box-body">
                <div>You have no schedule for today.</div>
            </div>
            @endif

        </div>
        -->

        <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Your Latest Task Logs</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                <table class="table no-margin">
                  <thead>
                  <tr>
                    <th>Date</th>
                    <th>Task Name</th>
                    <th>Time In</th>
                    <th>Time End</th>
                  </tr>
                  </thead>
                  <tbody>
                    @if (count($task_latest))
                        @foreach ($task_latest as $vlist)
                            <tr>
                                <td>{{$vlist->user->convertToUserTimezone($vlist->date_start . ' ' . $vlist->time_start)->format("Y-m-d")}}</td>
                                <td>{{$vlist->name}}</td>
                                <td>{{$vlist->user->convertToUserTimezone($vlist->date_start . ' ' . $vlist->time_start)->format('h:i:s a')}}</td>
                                <td>{{$vlist->user->convertToUserTimezone($vlist->date_end . ' ' . $vlist->time_end)->format('h:i:s a')}}</td>
                            </tr>
                        @endforeach
                    @else 
                        <tr>
                            <td colspan="5">No items to display</td>
                        </tr>
                    @endif

                  </tbody>
                </table>
              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
        </div>

    </div>
</div>

@endsection
@push('view-styles')
<style>
    #va-date {
        font-size:28px;
    }
</style>
@endpush
@push('view-scripts')
<script src="{{ asset('js/jqclock.min.js') }}"></script>
<script src="{{ asset('js/jquery.timer.js') }}"></script>
<script>
var _timestamp = $("#clock").data("timestamp");
$(".clock").clock({
    "calendar":false,
    "timestamp": _timestamp
});

$('.clock_client').each(function(i, obj) {
    _timestamp = $(this).data("timestamp");

    $(this).clock({
    "calendar":false,
    "timestamp": _timestamp
    });
});

@if ($is_clocked_in)
/*$('#timer').timer({
    seconds: {{$seconds_duration}}
});*/
@endif

@if (!empty($current_task))
$('#timer-task').timer({
    seconds: {{$seconds_duration_task}}
});
@endif

    //this shows the modal
    $(document).on("click", ".btn-clock", function(){
        var status = $(this).attr('data-status');
        var title = $(this).attr('data-title');
        
        $('.modal-body .content').html('<i class="fa fa-spin fa-refresh"></i>');
        $.ajax({
            url: "/dashboard/users/clock/" + status,
            dataType: "json"
        }).done(function(data) {
            console.log("show");
            $('#modal-clock .modal-title').html(title);
            $('.modal-body .content').html(data.html);
            $('#modal-clock').modal('show');

        }); 
    });

    $("#button-confirm").click(function(){
        var status = $("#clock-status").val();
        var url = (status === 'in') ? '/api/users/attendance' : '/api/users/attendance/0';
        var method = (status === 'in') ? 'POST' : 'PUT';
        var schedule_id = $("#schedule-select").find(":selected").val()
        var late_reason = $("#late-reason").val();
        $(this).html('<i class="fa fa-spin fa-refresh"></i> Confirm');
        $.ajax({
            url: url,
            dataType: "json",
            method: method,
            data: {
                late_reason : late_reason,
                schedule_id : schedule_id,
                user_id: {{Auth::user()->id}}
            }
        }).done(function(data) {
            $('#modal-clock').modal('hide');
            window.location.href="/dashboard";
        }); 
    });

    var task_status = "";

    //this shows the modal
    $(document).on("click", ".btn-task", function(){
        task_status = $(this).attr('data-status');
        var task_type = $("#task_type").val();
        var id = 0;
        var title = $(this).attr('data-title');

        if (task_status=='start') {
            id =  $("#"+task_type+"-task").find(":selected").val();
            url = "/dashboard/users/task/start/"+task_type+"/" + id;
        } else {
            url = "/dashboard/users/task/stop/"+task_type;
        }
        
        $('.modal-body .content').html('<i class="fa fa-spin fa-refresh"></i>');
        $.ajax({
            url: url,
            dataType: "json"
        }).done(function(data) {
            $('#modal-task .modal-title').html(title);
            $('#modal-task .modal-body .content').html(data.html);
            $('#modal-task').modal('show');

        }); 
    });

    $("#button-confirm-task").click(function(){
        var selected_task_id =  $("#selected_task_id").val();
        var selected_task_type =  $("#selected_task_type").val();
        var task_description = $('#task_description').val();
        $(this).html('<i class="fa fa-spin fa-refresh"></i> Confirm');
        $.ajax({
            url:  "/dashboard/users/task/status",
            dataType: "json",
            method: "POST",
            data: {
                selected_task_type : selected_task_type,
                selected_task_id : selected_task_id,
                status : task_status,
                task_description : task_description
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        }).done(function(data) {
            $('#modal-task').modal('hide');
            window.location.href="/dashboard";
        }); 
    });
</script>
@endpush