@extends('layouts.adminlte-master')
@section('page-title', "Notifications")

@section('content')

<section class='content'>
    <div class='row'>
        <div class='col-xs-12'>
            <div class='box'>
                <div class='box-header'>
                    <h3 class='box-title'>Notifications</h3>
                </div>
                <div class='box-body'>

                    <div class='row'>
                        <div class='col-md-3'>
                            <label>VA</label>
                            <select class="form-control input-lg" id="va" name="va" required>
                                <option value="0">All VA</option>
                                @foreach ($vas as $va)
                                <option value="{{$va->id}}">{{$va->fullname}}</option>
                                @endforeach 
                            </select>
                        </div>
                    </div>
                    <hr/>
                    <div class='row'>
                        <div class='col-sm-12'>
                            <table class="table table-bordered table-hover dataTable" id="notifications-table" role='grid' width='100%'>
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Notification</th>
                                        <th>Type</th>
                                        <th>Timestamp</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@push('view-styles')
<link href="//cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css" rel="stylesheet">
@endpush

@push('view-scripts')
<script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>

<script>
var _table;
$(function() {

var _table_notifications;

    function getNotifications(va) {

        $('#notifications-table').DataTable().destroy();

        _table_notifications = $('#notifications-table').DataTable({
            processing: true,
            serverSide: true,
            searching: false, info: false,
            pageLength: 25,
            ajax: { 
            url:'/dashboard/notifications/datatables',
            data: {
                'user_id' : va,
                'type' : 'GET'
            }
            },
            "columns": [
                    { "data": "id" },
                    { "data": "content"},
                    { "data": "type"},
                    { "data": "created_at" },
            ]
        });
    }

    $(document).ready(function() {

        getNotifications(0);

        $("#va").change(function(){
            va = $('#va').find(":selected").val();
            getNotifications(va);
        });

    });

});

</script>

@endpush