@extends('layouts.adminlte-master')
@section('page-title', "Dashboard")

@section('content')
<!-- Content Header (Page header) -->

@include('admin.preview-modal')

<section class="content">
    <div class="row">
            <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-orange">
                <div class="inner">
                <h3>{{$va_count}}</h3>

                <p>Virtual Assistants</p>
                </div>
                <div class="icon">
                <i class="ion ion-flag"></i>
                </div>
                <a href="/dashboard/va" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-orange">
                <div class="inner">
                <h3>{{$client_count}}</h3>

                <p>Clients</p>
                </div>
                <div class="icon">
                <i class="ion ion-ios-people"></i>
                </div>
                <a href="/dashboard/clients" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-orange">
                <div class="inner">
                <h3>{{$request_concern_count}}</h3>

                <p>Pending Requests & Concerns</p>
                </div>
                <div class="icon">
                <i class="ion ion-card"></i>
                </div>
                <a href="/dashboard/requests" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-orange">
                <div class="inner">
                <h3>{{$timesheets_count}}</h3>

                <p>Pending Timesheets</p>
                </div>
                <div class="icon">
                <i class="ion ion-clock"></i>
                </div>
                <a href="/dashboard/timesheets" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
            </div>
            <!-- ./col -->
    </div>

    <div class="row">

        <!-- Latest Screenshots -->
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Latest Screenshots</h3> 
                     <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                      </div>
                </div>
                <div class="box-body">
                    <div class="screenshots">
                        <table class="va-screenshots-table table table-hover dataTable" id="screenshots-table" role='grid' width='4620px'>
                            <thead>
                                <tr>
                                    <th>Screenshot</th>
                                    <th>Name</th>
                                    <th>Time</th>
                                </tr>
                            </thead>                                
                        </table>
                    </div>
                </div>
            </div><!-- box-body -->
        </div>
        <!-- End Latest Screenshots -->

        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">VA Status</h3> 
                     <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                      </div>
                </div>
                <div class="box-body">
                    <div>
                        <table class="va-online-table table table-hover dataTable" id="online-table" role='grid'>
                            <thead>
                                <tr>
                                    <th>Date In</th>
                                    <th>Time In</th>
                                    <th>Name</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div><!-- box-body -->
        </div>


        <!-- Latest attendance -->
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Latest Attendance</h3> 
                     <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                      </div>
                </div>
                <div class="box-body">
                    <div>
                        <table class="table table-bordered table-hover" role='grid' width='100%'>
                            <thead>
                                <tr>
                                    <th width="20%">Date In</th>
                                    <th width="20%">Time In</th>
                                    <th width="30%">Name</th>
                                    <th width="10%">Status</th>
                                    <th width="20%">Remark</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (count($attendance_latest))
                                    @foreach ($attendance_latest as $alist)
                                        <tr>
                                            <td>{{$alist->date_in}}</td>
                                            <td>{{date('h:i:s a',strtotime($alist->date_in . ' ' . $alist->time_in))}}</td>
                                            <td>{{$alist->user->first_name}} {{$alist->user->last_name}}</td>
                                            <td>{{$alist->status}}</td>
                                            <td>{{$alist->getRemark()}}</td>
                                        </tr>
                                    @endforeach
                                @else 
                                    <tr>
                                        <td colspan="5">No items to display</td>
                                    </tr>
                                @endif

                            </tbody>
                        </table>
                    </div>
                </div><!-- box-body -->
            </div>
        </div>

        <!-- Not logged in for sched after 10 minutes -->
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">10 Minute Grace Period</h3> 
                     <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                      </div>
                </div>
                <div class="box-body">
                    <div>
                        <table class="table table-bordered table-hover" role='grid' width='100%'>
                            <thead>
                                <tr>
                                    <th width="45%">Name</th>
                                    <th width="55%">Schedule</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (count($vas_grace10))
                                    @foreach ($vas_grace10 as $user)
                                        <tr>
                                            <td>{{$user->first_name}} {{$user->last_name}}</td>
                                            <td>{{$user->getScheduleOnTime($day_of_week)}}</td>
                                        </tr>
                                    @endforeach
                                @else 
                                    <tr>
                                        <td colspan="5">No items to display</td>
                                    </tr>
                                @endif

                            </tbody>
                        </table>
                    </div>
                </div><!-- box-body -->
            </div>
        </div>
        
    </div>
<audio id='notif'>
    <source src='/audio/unconvinced.mp3' type='audio/mpeg'>
</audio>
</section>

@endsection
@push('view-styles')
<link href="//cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css" rel="stylesheet">
@endpush


@push('view-styles')
<style>
    .va-screenshots-table tr {
        float:left;
        width:220px;
        height:300px;
        border:1px solid #dbdbdb;
        margin:5px;
    }
    .va-screenshots-table thead {
        display:none;
    }

    .va-screenshots-table tr td:nth-child(3) {
        width:100px;
        float:left;
        padding:0;
        padding-left:8px;
    }
    .va-screenshots-table tr td:nth-child(4) {
        width:100px;
        float:left;
        padding:0;
        padding-left:8px;
    }
    .va-screenshots-table tr td {
        float:left;
        width:210px;
        border: none !important;
    }

    .btn-sort {
        width:100px;
    }

    .dataTables_scroll {
        border: 1px solid #ccc;
    }
</style>
@endpush

@push('view-scripts')
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
<script src="{{ asset('js/notify.min.js') }}"></script>


<script>
var screenshots_table;
var online_table;
var audio = document.getElementById("notif");

    //refresh page every minute
    window.setInterval(function(){
        screenshots_table.ajax.reload();
        online_table.ajax.reload();
        },60000);

  (function ($) {

    function checkNotif() {
        $.ajax({
            url: "/dashboard/notifications/check_notif",
            dataType: "json",
        }).done(function(data) {

            if (!jQuery.isEmptyObject(data) ) {
                for(var k in data) {
                    if (data[k]['type']=='time-in' || data[k]['type']=='time-out')
                        $.notify(data[k]['content'], {autoHideDelay:10000,className:'info'});
                    if (data[k]['type']=='idle-start' || data[k]['type']=='idle-stop'|| data[k]['type']=='break-exceed') {
                        audio.play();
                        $.notify(data[k]['content'], {autoHideDelay:10000,className:'error'});
                    }
                    if (data[k]['type']=='break-start' || data[k]['type']=='break-stop') {
                        audio.play();
                        $.notify(data[k]['content'], {autoHideDelay:10000,className:'warn'});
                    }
                }
            }
        });
    }

    window.setInterval(function(){
        checkNotif();
        },30000);
        
        


    function loadScreenshots() {
        screenshots_table = $('#screenshots-table').DataTable({
            processing: true,
            serverSide: true,
            ordering: false,
            searching: false,
            pageLength: 20,
            paging: false,
            "scrollX": true,
            "bLengthChange": false,
            ajax: '/dashboard/screenshots/admin-dashboard',
            "columns": [
                { "data": "screenshot" },
                { "data": "name" },
                { "data": "time", width: "150px", orderable: false, searchable: false },
            ],
            'drawCallback': function(settings) {
                if ($('.va-screenshots-table tr').length==3 && $('.va-screenshots-table tr').find('td').hasClass('dataTables_empty')) {
                    $('.va-screenshots-table tr').css("height", "auto");
                    $('.va-screenshots-table tr').css("width", "100%");
                    $('.va-screenshots-table td').css("width", "95%");
                    $('.va-screenshots-table tr').css("margin", "0");
                    $('.va-screenshots-table tr').css("border", "0");
                    $('.va-screenshots-table').css("width", "100%");
                }
            }
        });
    }

    function loadOnlineVA() {
        online_table = $('#online-table').DataTable({
            processing: true,
            serverSide: false,
            ajax: '/dashboard/va/online_dashboard_datatables',
            "columns": [
                { "data": "date_in" },
                { "data": "time_in" },
                { "data": "user_id" },
                { "data": "status" },
            ]
        });
    }

    $(document).ready(function() {
        
        loadScreenshots();
        loadOnlineVA();

        //this shows the preview modal
        $(document).on("click", ".button-preview", function(){
            var id = $(this).attr('data-id');
            $('.modal-body .content').html('<i class="fa fa-spin fa-refresh"></i>');
            $.ajax({
                url: "/dashboard/screenshots/preview/"+id,
                dataType: "json"
            }).done(function(data) {
                $('.modal-body .content').html(data.html);
                //$('#modal-preview').modal('show');
            }); 
        });
    });

  }) (jQuery);
 </script>
 @endpush