<div class='content'>
<dl class='dl-horizontal'>
    <dt>Client</dt><dd>{{$q->client->fullname}}</dd>
    <dt>Total Time</dt><dd>{{$q->total_time}}</dd>
    <dt>Total Task Time</dt><dd>{{$q->total_task_time}}</dd>
    <dt>Total Idle Time</dt><dd>{{$q->total_idletime}}</dd>
    <dt>Total Break Time</dt><dd>{{$q->total_breaktime}}</dd>
    <dt>Billable Hours</dt><dd>{{$q->billable_hours}}</dd>
    @if (isset($q->attendance))
    <dt>Total Late Time</dt><dd>{{$q->attendance->late_time}}</dd>
    <dt>Late Reason</dt><dd>{{$q->attendance->late_reason}}</dd>
    @else
    <dt>Late Reason</dt><dd></dd>
    @endif
</dl>
</div>