@extends('layouts.adminlte-master')
@section('page-title', "Timesheets")

@section('content')
@include('admin.timesheet.modal-status')
@include('admin.delete-modal')

<section class='content'>
    <div class='row'>
        <div class='col-xs-12'>
            <div class='box'>
                <div class='box-header'>
                    <h3 class='box-title'>Timesheet of {{$user->first_name}} {{$user->last_name}}</h3>
                </div>

                <div class='box-body'>

                    <div class='row'>
                        <div class='col-md-6'>
                            <label>Select Week</label>
                            <div class="input-group date" id="datepicker">
                            <input type="text" id='target_date' name='target_date' class="form-control input-lg" value="{{date('Y-m-d', strtotime(now()))}}">
                            <div class="input-group-addon">
                            <span class="glyphicon glyphicon-th"></span>
                            </div>
                            </div>
                        </div>
                        <div class='col-md-6'>
                        </div>
                    </div>
                    <div class='row'><hr></div>
                    <div class='row'>
                        <div class='col-sm-12'>
                            <table class="table table-bordered table-hover dataTable" id="timesheet-table" role='grid' width='100%'>
                                <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>Client</th>
                                        <th>Total Time</th>
                                        <th>Billable Hours</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@push('view-styles')
<link href="//cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css" />
<style>
.datepicker tr:hover {
background-color: #808080;
}
</style>
@endpush

@push('view-scripts')
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.1/moment.min.js"></script>
<script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>

<script>
var _table;

$(function() {
    var value = "{{date('Y-m-d', strtotime(now()))}}";
    @if ($firstDate!=null && $lastDate!=null)
    var firstDate = '{{$firstDate}}';
    var lastDate =  '{{$lastDate}}';
    @else
    var firstDate = moment(value).isoWeekday(1).day(1).format("YYYY-MM-DD");
    var lastDate =  moment(value).isoWeekday(1).day(7).format("YYYY-MM-DD");
    @endif
    $("#target_date").val(firstDate + " - " + lastDate);

    fetchData(firstDate, lastDate);

    $('#datepicker').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        forceParse: false,
        weekStart: 1
    }).on('changeDate', function(e) {
        value = e.date;
        firstDate = moment(value).isoWeekday(1).day(1).format("YYYY-MM-DD");
        lastDate =  moment(value).isoWeekday(1).day(7).format("YYYY-MM-DD");
        $("#target_date").val(firstDate + " - " + lastDate);

        fetchData(firstDate, lastDate);
    });
});

function fetchData(firstDate, lastDate) {
    $('#timesheet-table').DataTable().destroy();

    _table = $('#timesheet-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: { 
            url:'/dashboard/timesheets/info_datatables/{{$user->id}}',
            data: {
                firstDate : firstDate,
                lastDate : lastDate,
                type : 'GET'
            }
        },
        "columns": [
                { "data": "target_date",  width: "10%" },
                { "data": "client_id",  width: "30%" },
                { "data": "total_time",  width: "20%" },
                { "data": "billable_hours",  width: "20%"},
                { "data": "status",  width: "10%"},
                { "data": "actions", width: "10%", orderable: false, searchable: false},
        ]
    });
}

     var temp_id = 0;
     var temp_delete_id = 0;

    //this shows the modal
    $(document).on("click", ".button-status", function(){
        var id = $(this).attr('data-id');
        temp_id = id;
        $('.modal-body .content').html('<i class="fa fa-spin fa-refresh"></i>');
        $.ajax({
            url: "/dashboard/timesheets/show/"+id,
            dataType: "json"
        }).done(function(data) {
            $('.modal-body .content').html(data.html);
            $('#modal-status').modal('show');
        }); 
    });

    //this does the actual change status
    $(".button-change-status").click(function(){
        var _status = $(this).attr('data-status');
        var _this = $(this);
        var _button_title = $(this).html();
        $(this).html('<i class="fa fa-spin fa-refresh"></i> ' + _button_title);

        $.ajax({
            url: "/dashboard/timesheets/change-status/"+_status,
            data: {
                id : temp_id,
                type : 'GET'
            }
        }).done(function(data) {
            $('#modal-status').modal('hide');
            _this.html(_button_title);
            _table.ajax.reload( null, false );
        }); 
    });

    //this shows the modal
    $(document).on("click", ".button-delete", function(){
        var id = $(this).attr('data-id');
        temp_delete_id = id;
        $('.modal-body .content').html('<i class="fa fa-spin fa-refresh"></i>');
        $.ajax({
            url: "/dashboard/timesheets/show/"+id,
            dataType: "json"
        }).done(function(data) {
            $('.modal-body .content').html(data.html);
            $('#modal-warning').modal('show');
        }); 
    });

    // this does the actual delete
    // delete timesheet
    $("#button-delete").click(function(){
        $(this).html('<i class="fa fa-spin fa-refresh"></i> Delete');
        $.ajax({
            url: "/dashboard/timesheets/delete/"+temp_delete_id,
            dataType: "json"
        }).done(function(data) {
            $('#modal-warning').modal('hide');
            $("#button-delete").html('Delete');
            _table.ajax.reload( null, false );
        }); 
    });


</script>

@endpush