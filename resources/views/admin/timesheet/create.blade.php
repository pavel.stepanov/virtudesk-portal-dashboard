@extends('layouts.adminlte-master')
@section('page-title', "Timesheets")

@section('content')
<div class="col-lg-12">

    @if ($errors->any())
    <div class="box box-warning box-solid">
        <div class="box-header with-border">
            <h3 class="box-title">Errors</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
            <!-- /.box-tools -->
        </div>

        <!-- /.box-header -->
        <div class="box-body">
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </div>
        <!-- /.box-body -->
    </div>
    @endif

    @if (session()->has('notification_message'))
    <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-info"></i> Success!</h4>
        {{ session()->get('notification_message') }}
    </div>
    @endif  
    
    <div class="panel panel-default">

        <div class="panel-heading">
            <i class="fa fa-user-circle-o fa-minus"></i> Add Timesheet
        </div>

        <div class="panel-body">
            <div class="col-md-12">
                <form enctype="multipart/form-data" method="POST" action="{{url('/dashboard/timesheets/create')}}">

                {{ csrf_field() }}

                    <div class="row">
                        <div class="col-lg-12">

                            <div class="col-md-12 col-lg-12">

                                <div class="box box-info">
                                    <div class="box-header">
                                        <h3 class="box-title"><i class="fa fa-lock"></i> Timesheet Info</h3>
                                    </div>
                                    <div class="box-body">

                                            <div class="form-group">
                                                <label>Work Date</label>
                                                <div class="input-group date date-pickers">
                                                <input type="text" name='target_date' class="form-control input-lg" value="{{date('Y-m-d')}}">
                                                <div class="input-group-addon">
                                                <span class="glyphicon glyphicon-th"></span>
                                                </div>
                                                </div>
                                                <span class="help-block">This is the date that the VA needs to have a log in</span>
                                            </div>

                                            <div class="form-group">
                                                <label>Schedule Date</label>
                                                <div class="input-group date date-pickers">
                                                <input type="text" name='schedule_date' class="form-control input-lg" value="{{date('Y-m-d')}}">
                                                <div class="input-group-addon">
                                                <span class="glyphicon glyphicon-th"></span>
                                                </div>
                                                </div>
                                                <span class="help-block">This is the date used for billing and date for the schedule of the client. This is usually the same as the Work Date, unless the schedule of the VA goes the next day, and this should be one day behind.</span>
                                            </div>

                                            <div class="form-group">
                                                <label>Client</label>
                                                <select name="client_id" class="form-control input-lg">
                                                @foreach ($clients as $client)
                                                    <option value="{{$client->id}}">{{$client->fullname}}</option>
                                                @endforeach
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <label>VA</label>
                                                <select name="user_id" class="form-control input-lg">
                                                @foreach ($vas as $va)
                                                    <option value="{{$va->id}}">{{$va->fullname}}</option>
                                                @endforeach
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <label>Billable Hours</label>
                                                <input type="text" class="form-control input-lg" name="billable_hours" value="" placeholder="Billable Hours">
                                            </div>

                                            <div class="form-group">
                                                <label>Status</label>
                                                <select name="status" class="form-control input-lg">
                                                    <option value="approved">Approved</option>
                                                    <option value="declined">Declined</option>
                                                    <option value="pending">Pending</option>
                                                </select>
                                            </div>

                                    </div>

                            </div>

                        </div>


                        <div class="col-xs-12">
                            <div class="box box-info">
                                <div class="box-footer clearfix">
                                <button type="submit" class="pull-right btn btn-success btn-lg">Create</button>   
                                </div>
                            </div>
                        </div>

                    </div>
                    
                    </div>
                </form>
            </div>
        </div>
    </div>


</div>
<div class='clearfix'></div>
@endsection
@push('view-styles')
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css" />
@endpush

@push('view-scripts')
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
<script src="{{ asset('js/jquery.are-you-sure.js') }}"></script>
<script>
  $(function() {
    $('.date-pickers').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        });
    $('form').areYouSure(
      {
        message: 'It looks like you have been editing something. '
               + 'If you leave before saving, your changes will be lost.'
      }
    );
  });
</script>
@endpush