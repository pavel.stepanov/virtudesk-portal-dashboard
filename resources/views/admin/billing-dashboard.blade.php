@extends('layouts.adminlte-master')
@section('page-title', "Dashboard")

@section('content')
<section class="content">
    <div class="row">
            <div class="col-lg-4 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-blue">
                <div class="inner">
                <h3>{{$va_count}}</h3>
                <p>Virtual Assistants</p>
                </div>
                <div class="icon">
                <i class="ion ion-flag"></i>
                </div>
                <div class="small-box-footer">&nbsp;</div>
            </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-4 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-blue">
                <div class="inner">
                <h3>{{$client_count}}</h3>

                <p>Clients</p>
                </div>
                <div class="icon">
                <i class="ion ion-ios-people"></i>
                </div>
                <a href="/dashboard/billing" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-4 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-blue">
                <div class="inner">
                <h3>{{$total_invoice_unpaid}}</h3>

                <p>Unpaid Invoice</p>
                </div>
                <div class="icon">
                <i class="ion ion-card"></i>
                </div>
                <a href="/dashboard/invoice" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
            </div>
    </div>

    <div class="row">
        <!-- Latest unpaid -->
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Latest Unpaid Invoice</h3> 
                     <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                      </div>
                </div>
                <div class="box-body">
                    <div>
                        <table class="table table-bordered table-hover" role='grid' width='100%'>
                            <thead>
                                <tr>
                                    <th width="20%">Date Generated</th>
                                    <th width="30%">Billing Period</th>
                                    <th width="30%">Client</th>
                                    <th width="20%">Total Amount Due</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (count($invoice_latest))
                                    @foreach ($invoice_latest as $alist)
                                        <tr>
                                            <td>{{$alist->date_generated}}</td>
                                            <td>{{$alist->billing_period}}</td>
                                            <td>{{$alist->client->first_name}} {{$alist->client->last_name}}</td>
                                            <td>{{$alist->total_amount_due}}</td>
                                        </tr>
                                    @endforeach
                                @else 
                                    <tr>
                                        <td colspan="5">No items to display</td>
                                    </tr>
                                @endif

                            </tbody>
                        </table>
                    </div>
                </div><!-- box-body -->
            </div>
        </div>
    </div>
</section>
@endsection