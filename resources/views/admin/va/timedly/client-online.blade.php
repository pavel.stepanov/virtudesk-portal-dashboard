@extends('layouts.timedly.app')

@section('pageTitle')
    Online Virtual Assistants
@endsection

@section('pageScripts')
<script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
<script>
    var _table;
    var temp_delete_id = 0;

    $(document).ready(function() {
        var options = {
            keys: !0,
            language: {
                paginate: {
                    previous: "<i class='fas fa-angle-left'>",
                    next: "<i class='fas fa-angle-right'>"
                }
            },
            "autoWidth": false,
            "bAutoWidth": false,
            processing: true,
            serverSide: true,
            ajax: '/dashboard/online-datatables',
            "columns": [
                { "data": "id" },
                { "data": "email" },
                { "data": "name" },
                { "data": "logged_in"},
            ]
        };

        _table = $('#va-data-table').DataTable(options);
    })
</script>
@endsection

@section('content')
    <div class="header bg-gradient-info pb-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <div class="col-lg-6 col-7">
                        <h6 class="h2 text-white d-inline-block mb-0">Online Virtual Assistants</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Page content -->

    <div class="container-fluid mt--6">
        <div class="row">

            <div class="col-12">
                <div class="card">
                    <!-- Card header -->
                    <div class="table-responsive py-4">
                        <table class="table table-flush" id="va-data-table">
                            <thead class="thead-light">
                            <tr>
                                <th>ID</th>
                                <th>Email</th>
                                <th>Name</th>
                                <th>Logged In</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>


        </div>
@endsection
