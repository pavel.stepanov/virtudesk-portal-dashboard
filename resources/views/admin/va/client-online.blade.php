@extends('layouts.vatheme-master')
@section('page-title', "Online Virtual Assistants")
@php
    $currentPage = 'online'
@endphp
@section('content')
<div class="content">
    <div class="container-fluid">
	<div class="row">
	    <div class="col-md-12">
		<div class="card">
		    <div class="card-header card-header-icon" data-background-color="purple">
			<i class="fa fa-user"></i>
		    </div>
		    <div class="card-content">
			<h4 class="card-title">Online Virtual Assistants</h4>
			<div class="table-responsive">
                            <table class="table table-hover dataTable" id="vas-table" role='grid' width='100%'>
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Email</th>
                                        <th>Name</th>
                                        <th>Logged In</th>
                                    </tr>
                                </thead>                                
                            </table>

            </div>
        </div> <!-- end of .card-content -->
    </div>
</div>

</div>
</div>
</div>

@endsection

@push('view-styles')
<link href="//cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css" rel="stylesheet">
@endpush

@push('view-scripts')
<script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>

<script>
$(function() {
    $('#vas-table').DataTable({
        processing: true,
        serverSide: false,
        ajax: '/dashboard/online-datatables',
        "columns": [
                { "data": "id" },
                { "data": "email" },
                { "data": "name" },
                { "data": "logged_in"},
        ]
    });
});

</script>

@endpush