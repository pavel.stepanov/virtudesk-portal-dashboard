@extends('layouts.adminlte-master')
@section('page-title', "Work Duration")

@section('content')


<section class='content'>
    <div class='row'>
        <div class='col-xs-12'>
            <div class='box'>
                <div class='box-header'>
                    <h3 class='box-title'>Work Duration</h3>
                </div>
                <div class='box-body'>
					<div class="form-group">
						<label>Date</label>
						<div class="input-group date" id="datepicker">
							<input type="text" id="target_date" class="form-control input-lg" value="{{ date('Y-m-d') }}">
						<div class="input-group-addon">
							<span class="glyphicon glyphicon-th"></span>
						</div>
						</div>
					</div>
                		
                	<div class='row'>
                        <div class='col-sm-12'>
                            <table class="table table-bordered table-hover dataTable" id="vas-table" role='grid' width='100%'>
                                <thead>
                                    <tr>
                                        <th width="20%">Client Name</th>
                                        <th width="20%">VA Name</th>
                                        <th width="25%">In</th>
                                        <th width="25%">Out</th>
                                        <th width="10%">Hours Rendered</th>
                                    </tr>
                                </thead>                                
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@push('view-styles')
<link href="//cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css" rel="stylesheet">
<!-- Include Bootstrap Datepicker -->
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css" />

@endpush

@push('view-scripts')

<script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>

<script>

$(function() {
    fetchData();

    $('#datepicker').datepicker({format: 'yyyy-mm-dd', autoclose: true})
    .on('changeDate', function() {
        fetchData();
    });
});

function fetchData() {
    $('#vas-table').DataTable().destroy();

    $('#vas-table').DataTable({
        processing: true,
        serverSide: false,
        ajax: {
        	'url':'/dashboard/work-duration-datatables',
        	'data': function(data) {
        		data.date = $('#target_date').val();
        	},
        },
        "columns": [
                { "data": "client_id" },
                { "data": "name" },
                { "data": "clock_in"},
                { "data": "clock_out"},
                { "data": "total_time"},
        ]
    });

}

</script>

@endpush
