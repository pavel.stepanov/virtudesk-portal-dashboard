<div class="modal fade" id="modal-va-client-rate-add">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title"></h4>
			</div>
			<div class="modal-body">
				<div class="alert modal-notification"></div>
				<form name="va-client-add-rate"> 
					<div class="form-group">
	                    <label>Select Client</label>

	            		<div class="client-html">
	            		</div> 
	            
	                </div>

					<div class="form-group">
	                    <label>Rate per Hour</label>
	                    <input type="text" class="form-control input-lg va-rate-per-hour" name="rate_per_hour" placeholder="{{ $q->rates->rate_per_hour_formatted }}">
	                </div>
	            </form>
			</div>
			<div class="modal-footer">
				<button id='button-cancel' type="button" class="btn btn-warning pull-left" data-dismiss="modal">Cancel</button>
				<button id='button-add-client-va-rate' type="button" class="btn btn-info">Add Client</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->