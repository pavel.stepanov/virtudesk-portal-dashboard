@extends('layouts.adminlte-master')
@section('page-title', "Virtual Assistants")

@section('content')
@include('admin.remove-user-modal');
@include('admin.delete-modal')
 @if (session()->has('notification_message'))
    <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-info"></i> Success!</h4>
        {{ session()->get('notification_message') }}
    </div>
    @endif  
<section class='content'>
    <div class='row'>
        <div class='col-xs-12'>
            <div class='box'>
                <div class='box-header'>
                    <h3 class='box-title'>Virtual Assistants</h3>
                    @if (Auth::user()->allowed('add_va'))
                    <span class="pull-right"><a href="/dashboard/va/create" class="btn btn-primary"><i class="fa fa-plus"></i> Create Virtual Assistant</a></span>
                    @endif
                    
                    
                   
                </div>
                <div class='box-body'>
                    <div class='row'>
                        <div class='col-sm-12'>
                            <table class="table table-bordered table-hover dataTable" id="vas-table" role='grid' width='100%'>
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Email</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>                                
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@push('view-styles')
<link href="//cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css" rel="stylesheet">
@endpush

@push('view-scripts')
<script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>

<script>
var _table;
$(function() {
    _table = $('#vas-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '/dashboard/va/datatables',
        "columns": [
                { "data": "id" },
                { "data": "first_name" },
                { "data": "last_name" },
                { "data": "email"},
                { "data": "actions", width: "230px", orderable: false, searchable: false},
        ]
    });
});

     var temp_delete_id = 0;

    //this shows the modal
    $(document).on("click", ".button-delete", function(){
        var id = $(this).attr('data-id');
        temp_delete_id = id;
        $('.modal-body .content').html('<i class="fa fa-spin fa-refresh"></i>');
        $.ajax({
            url: "/dashboard/va/show/"+id,
            dataType: "json"
        }).done(function(data) {
            $('.modal-body .content').html(data.html);
            $('#modal-warning').modal('show');
        }); 
    });

    //this does the actual delete
    $("#button-delete").click(function(){
        $(this).html('<i class="fa fa-spin fa-refresh"></i> Delete');
        $.ajax({
            url: "/dashboard/va/delete/"+temp_delete_id,
            dataType: "json"
        }).done(function(data) {
            $('#modal-warning').modal('hide');
            $("#button-delete").html('Delete');
            _table.ajax.reload( null, false );
        }); 
    });
    
    
        var redirect_to = '';
    
   $(document).on('click', '#logout_user', function(e){
            redirect_to = $(this).attr('href');
            e.preventDefault();
            $('.modal-body .modal-title-remove').html('Logout User');
            $('.modal-body .content-remove').html('Are you sure you want to log out this user?<p><small>User will be logged out from tracker.</small></p>');
            $('#modal-warning-remove').modal('show');
    });
    
    $(document).on('click', '#remove_from_team', function(e){
            redirect_to = $(this).attr('href');
            e.preventDefault();
            $('.modal-body .modal-title').html('Remove User from Team');
            $('.modal-body .content-remove').html('Are you sure you want to remove this user from your team?');
            $('#modal-warning-remove').modal('show');
    });
    
    $("#button-oks").click(function(){
        $('#modal-warning-remove').modal('hide');
        window.location.href = redirect_to;
    });

</script>

@endpush