@extends('layouts.adminlte-master')
@section('page-title', "Rate per Client ($q->full_name)")

@section('content')

@include('admin.va.client_rates_modal')
@include('admin.va.client_rates_add_modal')

<input type="hidden" id="va-id" value="{{ $q->id }}">
<input type="hidden" id="va-rate-per-hour" value="{{ $q->rates->rate_per_hour_formatted }}">

<section class='content'>
    <div class='row'>
        <div class='col-xs-12'>
            <div class='box'>
                <div class='box-header'>
                    <h3 class='box-title'>Rate per Client ({{ $q->full_name }})</h3>
                    <span class="pull-right"><a href="#" class="btn btn-primary btn-add-rate-to-client"><i class="fa fa-plus"></i> Add Client Rate to VA</a></span>
                </div>
                <div class='box-body'>
                    <div class='row'>
                        <div class='col-sm-12'>
                            <table class="table table-bordered table-hover dataTable" id="vas-table" role='grid' width='100%'>
                                <thead>
                                    <tr>
                                        <th>Client</th>
                                        <th>Rate per Hour</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>                                
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@push('view-styles')
<link href="//cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css" rel="stylesheet">
@endpush

@push('view-scripts')
<script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>

<script>
    var _table;
    $(function() {
        _table = $('#vas-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '/dashboard/va-client-rates/{{$q->id}}/datatables',
            "columns": [
                    { "data": "client_name" },
                    { "data": "rate_per_hour" },
                    { "data": "action", "orderable": false, 'width': '40' },
            ]
        });
    });

    $(document).on("click", ".btn-client-rate-update", function() {
        var _this = $(this),
            rate_id = _this.data('id'),
            modal_title = _this.data('title'),
            rate_per_hour = _this.data('rate-per-hour'),
            _modal = $("#modal-va-client-rate-update");

    
        $('.modal-title', _modal).text(modal_title);
        $('.va-rate-id', _modal).val(rate_id);
        $('.va-rate-per-hour', _modal).val(rate_per_hour);
        $('.modal-notification', _modal).hide();
        $('.modal-notification', _modal).removeClass('alert-error');
        $('.modal-notification', _modal).removeClass('alert-success');
        $("#modal-va-client-rate-update").modal("show");
    });

    $(document).on("click", "#button-update-client-va-rate", function() {
        var _modal = $("#modal-va-client-rate-update"),
            rate_id = $('.va-rate-id', _modal).val(),
            rate_per_hour = $('.va-rate-per-hour').val(),
            modal_title = $('.modal-title', _modal).text();

        $('.modal-title', _modal).html('<i class="fa fa-spin fa-refresh"></i> Saving');

        $.ajax({
            url: "/dashboard/va-client-rate-update/"+rate_id,
            dataType: "json",
            data: {
               "rate_per_hour": rate_per_hour
            },
        }).done(function(response) {
            $('.alert', _modal).removeClass('alert-success, alert-error');
            if(response.status === "success") {
                $('.alert', _modal).addClass('alert-success');
                $('.alert', _modal).html( response.message );
                
                _table.ajax.reload( null, false );

                $('.alert', _modal).show();
            } else {
                $('.alert', _modal).addClass('alert-error');
                $('.alert', _modal).show();
            }

            $('.modal-title', _modal).html( modal_title );
            $('.alert', _modal).html( response.message );
            $('.alert', _modal).show();

        });
    });

    $(document).on("click", ".btn-add-rate-to-client", function(e) {
        e.preventDefault();

        var _modal = $('#modal-va-client-rate-add'),
            _modal_title = "Add Client Rate for {{ $q->full_name }}";

        $.ajax({
            url: "/dashboard/va-client-rates/{{ $q->id }}/available-clients",
            dataType: "json",
        }).done(function(response) {

            if( response.status == "success") {
                
                
                $('.client-html', _modal).html(response.client_html);

                $('.modal-notification', _modal).hide();
                $('.modal-title', _modal).html(_modal_title); 
                _modal.modal("show");
            }
        });
    });

    $(document).on("click", "#button-add-client-va-rate", function() {
        var _modal = $("#modal-va-client-rate-add"),
            client_id = $("#client-id", _modal).val(),
            va_id = $("#va-id").val(),
            rate_per_hour = $(".va-rate-per-hour", _modal).val(),
            old_rate_per_hour = $("#va-rate-per-hour").val()

        $.ajax({
            url: "/dashboard/va-client-rate-add/"+va_id, 
            dataType: "json",
            data: {
                "rate_per_hour": rate_per_hour,
                "client_id": client_id,
                "old_rate_per_hour": old_rate_per_hour
            }
        }).done(function(response) {
            if(response.status == "success") {
                $(".modal-notification", _modal).hide();
                _modal.modal("hide");
                _table.ajax.reload( null, false );
            } else {
                $(".modal-notification", _modal).addClass("alert-error");
                $(".modal-notification", _modal).html(response.message);
                $(".modal-notification", _modal).show();
            
            }
        })
    });

    $(document).on("submit", 'form[name="va-client-add-rate"]', function(e) {
        e.preventDefault();
        $("#button-add-client-va-rate").trigger("click");  
    })

    $(document).on("submit", 'form[name="va-client-update-rate"]', function(e) {
        e.preventDefault();
        $("#button-update-client-va-rate").trigger("click");  
    })
    

</script>

@endpush