@extends('layouts.adminlte-master')
@section('page-title', "Online Virtual Assistants")

@section('content')


<section class='content'>
    <div class='row'>
        <div class='col-xs-12'>
            <div class='box'>
                <div class='box-header'>
                    <h3 class='box-title'>Online Virtual Assistants</h3>
                </div>
                <div class='box-body'>
                    <div class='row'>
                        <div class='col-sm-12'>
                            <table class="table table-bordered table-hover dataTable" id="vas-table" role='grid' width='100%'>
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Email</th>
                                        <th>Name</th>
                                        <th>Logged In</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>                                
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@push('view-styles')
<link href="//cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css" rel="stylesheet">
@endpush

@push('view-scripts')
<script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>

<script>
$(function() {
    $('#vas-table').DataTable({
        processing: true,
        serverSide: false,
        ajax: '/dashboard/online-datatables',
        "columns": [
                { "data": "id" },
                { "data": "email" },
                { "data": "name" },
                { "data": "logged_in"},
                { "data": "action"}
        ]
    });

    $(document).on("click", ".btn-ss", function(){
        var user_id = $(this).attr('data-id');
        $.ajax({
            url: "/dashboard/va/ss/"+user_id,
            dataType: "json"
        }).done(function(data) {
            if (data.status=='ok') {
                alert('Screenshot request sent! Please waif for desktop app to upload the screenshot. If successful, a new screenshot should be displayed on the VAs screenshot page. Wait a few minutes and refrsh the VA screenshot page.');
                window.location = "/dashboard/screenshots/" + user_id;
            } else {
                alert('VA not found!');
            }
        }); 
    });

});

</script>

@endpush