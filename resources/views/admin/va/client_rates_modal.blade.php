<div class="modal fade" id="modal-va-client-rate-update">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title"></h4>
			</div>
			<div class="modal-body">
				<div class="alert modal-notification"></div>
				<form name="va-client-update-rate"> 
					<div class="form-group">
	                    <label>Rate Per Hour</label>
	                    <input type="text" class="form-control input-lg va-rate-per-hour" name="rate_per_hour" placeholder="0.00">
	                </div>
	            </form>
				<div class="content">
				   	<input type="hidden" class="va-rate-id" id="va-rate-id">
				</div>
			</div>
			<div class="modal-footer">
				<button id='button-cancel' type="button" class="btn btn-warning pull-left" data-dismiss="modal">Cancel</button>
				<button id='button-update-client-va-rate' type="button" class="btn btn-info">Update</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->