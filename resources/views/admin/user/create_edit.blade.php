@extends('layouts.adminlte-master')
@section('page-title', "Users")

@section('content')
<div class="col-lg-12">

    @if ($errors->any())
    <div class="box box-warning box-solid">
        <div class="box-header with-border">
            <h3 class="box-title">Errors</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
            <!-- /.box-tools -->
        </div>

        <!-- /.box-header -->
        <div class="box-body">
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </div>
        <!-- /.box-body -->
    </div>
    @endif

    @if (session()->has('notification_message'))
    <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-info"></i> Success!</h4>
        {{ session()->get('notification_message') }}
    </div>
    @endif 

    <div class="panel panel-default">

        <div class="panel-heading">
        @if (isset($q))
            <i class="fa fa-user-circle-o fa-minus"></i> Edit User
        @else
            <i class="fa fa-user-circle-o fa-minus"></i> Add User
        @endif
        </div>

        <div class="panel-body">
            <div class="col-md-12">
                @if (isset($q))
                <form enctype="multipart/form-data" method="POST" action="{{url('/dashboard/users/update')}}">
                @else
                <form enctype="multipart/form-data" method="POST" action="{{url('/dashboard/users/create')}}">
                @endif
                {{ csrf_field() }}

                        @if (isset($q))
                        <input type='hidden' value='{{$q->id}}' name='id'/>
                        @endif

                    <div class="row">
                        <div class="col-lg-12">

                        <div class="col-md-12 col-lg-12">

                        <div class="box box-info">
                            <div class="box-header">
                                <h3 class="box-title"><i class="fa fa-user"></i> User Info</h3></div>
                                <div class="box-body">

                                    @if (isset($q))

                                         <div class="form-group">
                                            <label>Birthday</label>
                                            <div class="input-group date" id="datepicker">
                                            <input type="text" name='birthdate' class="form-control input-lg" value="{{$q->birthdate}}">
                                            <div class="input-group-addon">
                                            <span class="glyphicon glyphicon-th"></span>
                                            </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Role</label>
                                            <select name="role" class="form-control input-lg">
                                                <option> Select Role </option>
                                                @foreach ($roles as $role)
                                                    @if ($q->is($role->slug))
                                                    <option value="{{$role->slug}}" selected>{{$role->name}}</option>
                                                    @else
                                                    <option value="{{$role->slug}}">{{$role->name}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label>First Name</label>
                                            <input type="text" class="form-control input-lg" name="first_name" value="{{ $q->first_name }}" placeholder="First Name">
                                        </div>

                                        <div class="form-group">
                                            <label>Last Name</label>
                                            <input type="text" class="form-control input-lg" name="last_name" value="{{ $q->last_name }}" placeholder="Last Name">
                                        </div>

                                        <div class="form-group">
                                            <label>Mobile Number</label>
                                            <input type="text" class="form-control input-lg" name="mobile_number" value="{{ $q->mobile_number }}" placeholder="Mobile Number">
                                        </div>

                                        <div class="form-group">
                                            <label>Alternate Contact Number</label>
                                            <input type="text" class="form-control input-lg" name="alternate_contact_number" value="{{ $q->alternate_contact_number }}" placeholder="Alternate Number">
                                        </div>

                                        <div class="form-group">
                                            <label>Work Email</label>
                                            <input type="text" class="form-control input-lg" name="email" value="{{ $q->email }}" placeholder="Work Email">
                                        </div>

                                        <div class="form-group">
                                            <label>Personal Email</label>
                                            <input type="text" class="form-control input-lg" name="personal_email" value="{{ $q->personal_email }}" placeholder="Personal Email">
                                        </div>

                                        <div class="form-group">
                                            <label>Address 1</label>
                                            <input type="text" class="form-control input-lg" name="address1" value="{{ $q->address1 }}" placeholder="Address 1">
                                        </div>

                                        <div class="form-group">
                                            <label>Address 2</label>
                                            <input type="text" class="form-control input-lg" name="address2" value="{{ $q->address2 }}" placeholder="Address 2">
                                        </div>

                                        <div class="form-group">
                                            <label>City</label>
                                            <input type="text" class="form-control input-lg" name="city" value="{{ $q->city }}" placeholder="City">
                                        </div>

                                        <div class="form-group">
                                            <label>Skype ID</label>
                                            <input type="text" class="form-control input-lg" name="skype_id" value="{{ $q->skype_id }}" placeholder="Skype ID">
                                        </div>

                                        <div class="form-group">
                                            <label>Idle Interval (Seconds)</label>
                                            <input type="text" class="form-control input-lg" name="idle_interval" value="{{ $q->idle_interval }}" placeholder="Idle Interval">
                                        </div>

                                        <div class="form-group">
                                            <label>Screenshot Interval (Seconds)</label>
                                            <input type="text" class="form-control input-lg" name="screenshot_interval" value="{{ $q->screenshot_interval }}" placeholder="Screenshot Interval">
                                        </div>

                                        <div class="form-group">
                                            <label>Timezone</label>
                                            <select name="timezone" class="form-control input-lg">
                                                @foreach ($timezone as $key => $value)
                                                @if ($q->timezone == $key)
                                                <option value="{{$key}}" selected>{{$key}} - {{$value}}</option>
                                                @else
                                                <option value="{{$key}}">{{$key}} - {{$value}}</option>
                                                @endif
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <div class="checkbox">
                                                <label>
                                                    @if ($q->is_active == 1)
                                                    <input type="checkbox" name="is_active" checked>Is Active?
                                                    @else 
                                                    <input type="checkbox" name="is_active"> Is Active?
                                                    @endif 
                                                </label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Set Password</label>
                                            <input type="text" class="form-control input-lg" name="password" value="" placeholder="Password">
                                            <span class="help-block has-warning">Leave blank if you do not want to set a new password.</span>
                                        </div>
                                    @else
                                  
                                        <div class="form-group">
                                            <label>Birthday</label>
                                            <div class="input-group date" id="datepicker">
                                            <input type="text" name='birthdate' class="form-control input-lg" value="">
                                            <div class="input-group-addon">
                                            <span class="glyphicon glyphicon-th"></span>
                                            </div>
                                            </div>
                                        </div>
                                        

                                        <div class="form-group">
                                            <label>Role</label>
                                            <select name="role" class="form-control input-lg">
                                                <option> Select Role </option>
                                                @foreach ($roles as $role)
                                                    @if (old('role')==$role->slug)
                                                    <option value="{{$role->slug}}" selected>{{$role->name}}</option>
                                                    @else
                                                    <option value="{{$role->slug}}">{{$role->name}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label>First Name</label>
                                            <input type="text" class="form-control input-lg" name="first_name" value="{{ old('first_name') }}" placeholder="First Name">
                                        </div>

                                        <div class="form-group">
                                            <label>Last Name</label>
                                            <input type="text" class="form-control input-lg" name="last_name" value="{{ old('last_name') }}" placeholder="Last Name">
                                        </div>

                                        <div class="form-group">
                                            <label>Mobile Number</label>
                                            <input type="text" class="form-control input-lg" name="mobile_number" value="{{ old('mobile_number') }}" placeholder="Mobile Number">
                                        </div>

                                        <div class="form-group">
                                            <label>Alternate Contact Number</label>
                                            <input type="text" class="form-control input-lg" name="alternate_contact_number" value="{{ old('alternate_contact_number') }}" placeholder="Alternate Number">
                                        </div>

                                        <div class="form-group">
                                            <label>Work Email</label>
                                            <input type="text" class="form-control input-lg" name="email" value="{{ old('email') }}" placeholder="Work Email">
                                        </div>

                                        <div class="form-group">
                                            <label>Personal Email</label>
                                            <input type="text" class="form-control input-lg" name="personal_email" value="{{ old('personal_email') }}" placeholder="Personal Email">
                                        </div>

                                        <div class="form-group">
                                            <label>Address 1</label>
                                            <input type="text" class="form-control input-lg" name="address1" value="{{ old('address1') }}" placeholder="Address 1">
                                        </div>

                                        <div class="form-group">
                                            <label>Address 2</label>
                                            <input type="text" class="form-control input-lg" name="address2" value="{{ old('address2') }}" placeholder="Address 2">
                                        </div>

                                        <div class="form-group">
                                            <label>City</label>
                                            <input type="text" class="form-control input-lg" name="city" value="{{ old('city') }}" placeholder="City">
                                        </div>

                                        <div class="form-group">
                                            <label>Skype ID</label>
                                            <input type="text" class="form-control input-lg" name="skype_id" value="{{ old('skype_id') }}" placeholder="Skype ID">
                                        </div>

                                        <div class="form-group">
                                            <label>Idle Interval (Seconds)</label>
                                            <input type="text" class="form-control input-lg" name="idle_interval" value="{{ old('idle_interval') }}" placeholder="Idle Interval">
                                        </div>

                                        <div class="form-group">
                                            <label>Screenshot Interval (Seconds)</label>
                                            <input type="text" class="form-control input-lg" name="screenshot_interval" value="{{ old('screenshot_interval') }}" placeholder="Screenshot Interval">
                                        </div>                                        

                                        <div class="form-group">
                                            <label>Timezone</label>
                                            <select name="timezone" class="form-control input-lg">
                                                @foreach ($timezone as $key => $value)
                                                <option value="{{$key}}">{{$key}} - {{$value}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label>Password</label>
                                            <input type="text" class="form-control input-lg" name="password" value="" placeholder="Password">
                                        </div>

                                        <div class="form-group">
                                            <label>Confirm Password</label>
                                            <input type="text" class="form-control input-lg" name="confirm_password" value="" placeholder="Confirm Password">
                                        </div>
                                    @endif

                                </div>
                            </div>
                        </div>




                        <div class="col-xs-12">
                            <div class="box box-info">
                                <div class="box-footer clearfix">
                                @if (isset($q))
                                <button type="submit" class="pull-right btn btn-success btn-lg">Update</button>
                                @else
                                    <button type="submit" class="pull-right btn btn-success btn-lg">Add</button>
                                @endif     
                                </div>
                            </div>
                        </div>

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>


</div>
<div class='clearfix'></div>
@endsection

@push('view-styles')
<!-- Include Bootstrap Datepicker -->
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css" />
@endpush

@push('view-scripts')
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
<script src="{{ asset('js/jquery.are-you-sure.js') }}"></script>
<script type="text/javascript">
$(function () {
    $('#datepicker').datepicker({format: 'yyyy-mm-dd'});
    $('form').areYouSure(
      {
        message: 'It looks like you have been editing something. '
               + 'If you leave before saving, your changes will be lost.'
      }
    );
});
</script>
@endpush