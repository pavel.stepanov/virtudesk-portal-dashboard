@extends('layouts.vatheme-master')
@section('page-title', "Profile")
@php
    $currentPage = 'vaList'
@endphp
@section('content')

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                    <i class="fa fa-user"></i>
                    </div>
                    <div class="card-content">
                    <h4 class="card-title">Profile of {{$user->fullname}}</h4>
                    <div class="col-lg-10 col-md-offset-1">

                            <div class="row">

                                    <div class="box box-info">

                                        <div class="box-body">

                                            <div class="form-group">
                                                <img style='width:300px' id='user-avatar-profile' src="{{$avatar}}" class="img-circle img-responsive center-block" alt="User Image">
                                            </div>

                                            <div class="form-group">
                                                <label>Work Email</label>
                                                <div>{{$user->email}}</div>
                                            </div>

                                            @if ($user->personal_email!='')
                                            <div class="form-group">
                                                <label>Personal Email</label>
                                                <div>{{$user->personal_email}}</div>
                                            </div>
                                            @endif

                                            @if ($user->birthdate!='')
                                            <div class="form-group">
                                                <label>Birthday</label>
                                                <div>{{$user->birthdate}}</div>
                                            </div>
                                            @endif

                                            @if ($user->city!='')
                                            <div class="form-group">
                                                <label>City</label>
                                                <div>{{$user->city}}</div>
                                            </div>
                                            @endif

                                            @if ($user->skype_id!='')
                                            <div class="form-group">
                                                <label>Skype ID</label>
                                                <div>{{$user->skype_id}}</div>
                                            </div>
                                            @endif

                                            @if ($user->mobile_number!='')
                                            <div class="form-group">
                                                <label>Mobile Number</label>
                                                <div>{{$user->mobile_number}}</div>
                                            </div>
                                            @endif

                                            @if ($user->timezone!='')
                                            <div class="form-group">
                                                <label>Timezone</label>
                                                <div>{{$user->timezone}}</div>
                                            </div>
                                            @endif


                                        </div>
                                    </div>

                            </div>
                            <hr>
                    </div>
                    </div>
                    
                </div>
                
            </div>
        </div>
    </div>
</div>

@endsection