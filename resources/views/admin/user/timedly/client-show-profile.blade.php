@extends('layouts.timedly.app')

@section('pageTitle')
    Profile
@endsection

@section('content')
    <div class="header bg-gradient-info pb-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <div class="col-lg-6 col-7">
                        <h6 class="h2 text-white d-inline-block mb-0">Profile of {{$user->fullname}}</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Page content -->

    <div class="container-fluid mt--6">
        <div class="row">

            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                            <div class="box box-info">

                                    <div class="box-body">

                                        <div class="form-group">
                                            <img style='width:300px' id='user-avatar-profile' src="{{$avatar}}" class="img-circle img-responsive center-block" alt="User Image">
                                        </div>

                                        <div class="form-group">
                                            <h6>Work Email</h6>
                                            <p>{{$user->email}}</p>
                                        </div>

                                        @if ($user->personal_email!='')
                                        <div class="form-group">
                                            <h6>Personal Email</h6>
                                            <p>{{$user->personal_email}}</p>
                                        </div>
                                        @endif

                                        @if ($user->birthdate!='')
                                        <div class="form-group">
                                            <h6>Birthday</h6>
                                            <p>{{$user->birthdate}}</p>
                                        </div>
                                        @endif

                                        @if ($user->city!='')
                                        <div class="form-group">
                                            <h6>City</h6>
                                            <p>{{$user->city}}</p>
                                        </div>
                                        @endif

                                        @if ($user->skype_id!='')
                                        <div class="form-group">
                                            <h6>Skype ID</h6>
                                            <p>{{$user->skype_id}}</p>
                                        </div>
                                        @endif

                                        @if ($user->mobile_number!='')
                                        <div class="form-group">
                                            <h6>Mobile Number</h6>
                                            <p>{{$user->mobile_number}}</p>
                                        </div>
                                        @endif

                                        @if ($user->timezone!='')
                                        <div class="form-group">
                                            <h6>Timezone</h6>
                                            <p>{{$user->timezone}}</p>
                                        </div>
                                        @endif


                                    </div>
                                </div>
                    </div>
                </div>
            </div>


        </div>
@endsection
