@extends('layouts.timedly.app')

@section('pageTitle')
    Edit Profile
@endsection

@section('pageScripts')
<script>
    var updated = false;
    $(document).ready(function () {
        @php
            if(session('notification_message') || $errors->any()):
                $message = '';
                $type = '';
                if( session('notification_message') ) {
                    $message = session('notification_message');
                    $type = 'success';
                    $title = 'Success!';
                } else {
                    $message = implode('<br/>', $errors->all());
                    $type = 'error';
                    $title = 'Something went wrong.';
                }
        @endphp

        swal({
            title: '{{$title}}',
            html: '{!!$message!!}',
            type: '{{$type}}',
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary'
        });

        @php
            endif;
        @endphp
    });
</script>
@endsection

@section('content')
    <div class="header bg-gradient-info pb-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <div class="col-lg-6 col-7">
                        <h6 class="h2 text-white d-inline-block mb-0">Edit Profile</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Page content -->

    <div class="container-fluid mt--6">
        <div class="row justify-content-md-center">
            <div class="col-sm-8">
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header">
                        <h3 class="mb-0">Pofile Info</h3>
                    </div>
                    <!-- Card body -->
                    <div class="card-body">
                        <form enctype="multipart/form-data" method="POST" action="{{url('/dashboard/users/profile')}}">
                            {{ csrf_field() }}

                            <div class="form-group">
                                <img style='width:300px' id='user-avatar-profile' src="{{$avatar}}" class="img-circle img-responsive center-block" alt="User Image">
                            </div>

                            <div class="form-group">
                                <label class="btn btn-default center-block">
                                    <span id="filename">
                                        Choose an image...
                                    </span>
                                    {{ Form::file('avatar', ['style' => 'display:none;']) }}
                                </label>
                            </div>

                            <div class="form-group">
                                <label class="form-control-label" for="email">Work Email</label>
                                {{ Form::text('email', isset($q) ? $q->email : null, ['class' => 'form-control', 'placeholder' => 'myemail@domain.com']) }}
                            </div>

                            <div class="form-group">
                                <label class="form-control-label" for="current_password">Current Password</label>
                                {{ Form::password('current_password', ['class' => 'form-control', 'placeholder' => 'Password']) }}
                            </div>

                            <div class="form-group">
                                <label class="form-control-label" for="new_password">New Password</label>
                                {{ Form::password('new_password', ['class' => 'form-control', 'placeholder' => 'New Password']) }}
                            </div>

                            <div class="form-group">
                                <label class="form-control-label" for="confirm_password">Confirm Password</label>
                                {{ Form::password('confirm_password', ['class' => 'form-control', 'placeholder' => 'Password Confirmation']) }}
                            </div>

                            <div class="form-group">
                                <label class="form-control-label" for="first_name">First Name</label>
                                {{ Form::text('first_name', isset($q) ? $q->first_name : null, ['class' => 'form-control', 'placeholder' => 'First Name']) }}
                            </div>

                            <div class="form-group">
                                <label class="form-control-label" for="last_name">Last Name</label>
                                {{ Form::text('last_name', isset($q) ? $q->last_name : null, ['class' => 'form-control', 'placeholder' => 'Last Name']) }}
                            </div>

                            <div class="form-group">
                                <label class="form-control-label" for="personal_email">Personal Email</label>
                                {{ Form::email('personal_email', isset($q) ? $q->personal_email : null, ['class' => 'form-control', 'placeholder' => 'Personal Email']) }}
                            </div>

                            <div class="form-group">
                                <label class="form-control-label" for="birthdate">Birthday</label>
                                <div class="input-group date" id="datepicker">
                                    {{ Form::text('birthdate', isset($q) ? $q->birthdate : null, ['class' => 'form-control', 'placeholder' => 'Birthday']) }}
                                    <div class="input-group-addon">
                                        <span class="glyphicon glyphicon-th"></span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="form-control-label" for="address1">Address 1</label>
                                {{ Form::text('address1', isset($q) ? $q->address1 : null, ['class' => 'form-control', 'placeholder' => 'Address 1']) }}
                            </div>

                            <div class="form-group">
                                <label class="form-control-label" for="address2">Address 2</label>
                                {{ Form::text('address2', isset($q) ? $q->address2 : null, ['class' => 'form-control', 'placeholder' => 'Address 2']) }}
                            </div>

                            <div class="form-group">
                                <label class="form-control-label" for="city">City</label>
                                {{ Form::text('city', isset($q) ? $q->city : null, ['class' => 'form-control', 'placeholder' => 'City']) }}
                            </div>

                            <div class="form-group">
                                <label class="form-control-label" for="skype_id">Skype ID</label>
                                {{ Form::text('skype_id', isset($q) ? $q->skype_id : null, ['class' => 'form-control', 'placeholder' => 'Skype ID']) }}
                            </div>

                            <div class="form-group">
                                <label class="form-control-label" for="mobile_number">Mobile Number</label>
                                {{ Form::text('mobile_number', isset($q) ? $q->mobile_number : null, ['class' => 'form-control', 'placeholder' => 'Mobile Number']) }}
                            </div>

                            <div class="form-group">
                                <label class="form-control-label" for="alternate_contact_number">Alternate Contact Number</label>
                                {{ Form::text('alternate_contact_number', isset($q) ? $q->alternate_contact_number : null, ['class' => 'form-control', 'placeholder' => 'Alternate Contact Number']) }}
                            </div>

                            <div class="form-group">
                                <label>Timezone:</label>
                                {{ $q->timezone }}
                            </div>

                            <button class="btn btn-icon btn-success float-right" type="submit">
                                <span class="btn-inner--text">Update</span>
                            </button>
                       </form>
                    </div>
                </div>
            </div>
        </div>
@endsection
