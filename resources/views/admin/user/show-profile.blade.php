@extends('layouts.adminlte-master')
@section('page-title', "Profile")

@section('content')
<div class="panel panel-default">

    <div class="panel-heading">
        <i class="fa fa-user-circle-o fa-minus"></i> Profile of {{$user->fullname}}
    </div>

    <div class="panel-body">

        <div class="col-md-12">

            <div class="row">

                <div class="box box-info">

                    <div class="box-body">

                        <div class="form-group">
                            <img id='user-avatar-profile' src="{{$avatar}}" class="img-circle img-responsive center-block" alt="User Image">
                        </div>

                        <div class="form-group">
                            <label>Work Email</label>
                            <div>{{$user->email}}</div>
                        </div>

                        @if ($user->personal_email!='')
                        <div class="form-group">
                            <label>Personal Email</label>
                            <div>{{$user->personal_email}}</div>
                        </div>
                        @endif

                        @if ($user->birthdate!='')
                        <div class="form-group">
                            <label>Birthday</label>
                            <div>{{$user->birthdate}}</div>
                        </div>
                        @endif

                        @if ($user->city!='')
                        <div class="form-group">
                            <label>City</label>
                            <div>{{$user->city}}</div>
                        </div>
                        @endif

                        @if ($user->skype_id!='')
                        <div class="form-group">
                            <label>Skype ID</label>
                            <div>{{$user->skype_id}}</div>
                        </div>
                        @endif

                        @if ($user->mobile_number!='')
                        <div class="form-group">
                            <label>Mobile Number</label>
                            <div>{{$user->mobile_number}}</div>
                        </div>
                        @endif

                        @if ($user->timezone!='')
                        <div class="form-group">
                            <label>Timezone</label>
                            <div>{{$user->timezone}}</div>
                        </div>
                        @endif
                    </div>

                </div>

            </div>

<!--
            <div class="row">

                <div class="box box-info">

                    <div class="box-body">

                        <div class="form-group">
                            <label>Total Hours Rendered</label>
                            <div>{{$user->profile->total_hours_rendered}}</div>
                        </div>

                        <div class="form-group">
                            <label>Total Lates</label>
                            <div>{{$user->profile->total_lates}}</div>
                        </div>

                        <div class="form-group">
                            <label>Total Overbreaks</label>
                            <div>{{$user->profile->total_overbreaks}}</div>
                        </div>

                        <div class="form-group">
                            <label>Total Idles</label>
                            <div>{{$user->profile->total_idle}}</div>
                        </div>

                    </div>

                </div>

            </div>
-->

        </div>

    </div>

</div>
@endsection