<div>
<h3>Are you sure you want to clock out?</h3>
<p>Clocking out will also stop any task that you have started.</p>
</div>
<input type="hidden" id="clock-status" value="{{$status}}">