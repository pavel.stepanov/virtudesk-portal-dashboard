<div>
<h3>Select a schedule to Clock in</h3>
</div>
<input type="hidden" id="clock-status" value="{{$status}}">
<div class="form-group">
<label>Schedules</label>
<select id="schedule-select">
    @foreach ($schedules as $schedule)
    <option value="{{$schedule->id}}">{{date("h:i A",strtotime($schedule->user_start_time))}} {{date("h:i A",strtotime($schedule->user_end_time))}}</option>
    @endforeach
</select>
<div class="form-group">
<label>If you are late, provide reason for being late.</label>
<textarea id='late-reason' class="form-control input-lg"></textarea>
</div>
</div>