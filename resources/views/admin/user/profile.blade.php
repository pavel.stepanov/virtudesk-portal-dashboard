@extends('layouts.adminlte-master')
@section('page-title', "Profile")

@section('content')
<div class="col-lg-6 col-md-offset-3">

    @if ($errors->any())
    <div class="box box-warning box-solid">
        <div class="box-header with-border">
            <h3 class="box-title">Errors</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
            <!-- /.box-tools -->
        </div>

        <!-- /.box-header -->
        <div class="box-body">
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </div>
        <!-- /.box-body -->
    </div>
    @endif

    @if (session()->has('notification_message'))
    <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-info"></i> Success!</h4>
        {{ session()->get('notification_message') }}
    </div>
    @endif 

    <div class="panel panel-default">

        <div class="panel-heading">
            <i class="fa fa-user-circle-o fa-minus"></i> Edit Profile
        </div>

        <div class="panel-body">
            <div class="col-md-12">

                <form enctype="multipart/form-data" method="POST" action="{{url('/dashboard/users/profile')}}">

                {{ csrf_field() }}

                    <div class="row">

                            <div class="box box-info">

                                <div class="box-body">

                                        <div class="form-group">
                                            <img id='user-avatar-profile' src="{{$avatar}}" class="img-circle img-responsive center-block" alt="User Image">
                                        </div>

                                        <div class="form-group">
                                            <label class="btn btn-default center-block">
                                                <span id="filename">
                                                    Choose an image...
                                                </span>
                                                <input type="file" style='display:none' name="avatar" id="file">
                                            </label>
                                        </div>

                                        <div class="form-group">
                                            <label>Work Email</label>
                                            <input type="text" class="form-control input-lg" name="email" value="{{ $q->email }}" placeholder="Work Email">
                                        </div>

                                        <div class="form-group">
                                            <label>Current Password</label>
                                            <input type="password" class="form-control input-lg" name="current_password" value="" placeholder="Current Password">
                                        </div>

                                        <div class="form-group">
                                            <label>New Password</label>
                                            <input type="password" class="form-control input-lg" name="new_password" value="" placeholder="New Password">
                                        </div>

                                        <div class="form-group">
                                            <label>Confirm Password</label>
                                            <input type="password" class="form-control input-lg" name="confirm_password" value="" placeholder="Confirm Password">
                                        </div>  

                                        <div class="form-group">
                                            <label>First Name</label>
                                            <input type="text" class="form-control input-lg" name="first_name" value="{{ $q->first_name }}" placeholder="First Name">
                                        </div>

                                        <div class="form-group">
                                            <label>Last Name</label>
                                            <input type="text" class="form-control input-lg" name="last_name" value="{{ $q->last_name }}" placeholder="Last Name">
                                        </div>

                                        <div class="form-group">
                                            <label>Personal Email</label>
                                            <input type="text" class="form-control input-lg" name="personal_email" value="{{ $q->personal_email }}" placeholder="Personal Email">
                                        </div>

                                        <div class="form-group">
                                            <label>Birthday</label>
                                            <div class="input-group date" id="datepicker">
                                            <input type="text" name='birthdate' class="form-control input-lg" value="{{$q->birthdate}}">
                                            <div class="input-group-addon">
                                            <span class="glyphicon glyphicon-th"></span>
                                            </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Address 1</label>
                                            <input type="text" class="form-control input-lg" name="address1" value="{{ $q->address1 }}" placeholder="Address 1">
                                        </div>

                                        <div class="form-group">
                                            <label>Address 2</label>
                                            <input type="text" class="form-control input-lg" name="address2" value="{{ $q->address2 }}" placeholder="Address 2">
                                        </div>

                                        <div class="form-group">
                                            <label>City</label>
                                            <input type="text" class="form-control input-lg" name="city" value="{{ $q->city }}" placeholder="City">
                                        </div>                                        

                                        <div class="form-group">
                                            <label>Skype ID</label>
                                            <input type="text" class="form-control input-lg" name="skype_id" value="{{ $q->skype_id }}" placeholder="Skype ID">
                                        </div>

                                        <div class="form-group">
                                            <label>Mobile Number</label>
                                            <input type="text" class="form-control input-lg" name="mobile_number" value="{{ $q->mobile_number }}" placeholder="Mobile Number">
                                        </div>

                                        <div class="form-group">
                                            <label>Alternate Contact Number</label>
                                            <input type="text" class="form-control input-lg" name="alternate_contact_number" value="{{ $q->alternate_contact_number }}" placeholder="Alternate Number">
                                        </div>    

                                        <div class="form-group">
                                            <label>Timezone:</label>
                                            {{ $q->timezone }}
                                        </div>
                                                                            

                                </div>
                            </div>

                            <div class="box">
                                <div class="box-footer clearfix">
                                <button type="submit" class="pull-right btn btn-success btn-lg">Update</button> 
                                </div>
                            </div>                            

                    </div>
                </form>

            </div>
        </div>
    </div>
</div>
<div class='clearfix'></div>
@endsection


@push('view-styles')
<style>
img#user-avatar-profile {
    object-fit: cover;
    width: 230px;
    height: 230px;
}
</style>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css" />
@endpush

@push('view-scripts')
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
<script>
    var file = document.getElementById("file");
    file.onchange = function(){
        if(file.files.length > 0)
        {
            readURL(this);
        }
    };

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#user-avatar-profile')
                    .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    $(function() {
        $('#datepicker').datepicker({format: 'yyyy-mm-dd'});
    });
</script>
@endpush