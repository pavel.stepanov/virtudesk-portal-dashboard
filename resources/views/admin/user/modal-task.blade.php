<div class="modal fade" id="modal-task">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">&times;</span></button>
<h4 class="modal-title"></h4>
</div>
<div class="modal-body">
<div class="content">
    <i class="fa fa-spin fa-refresh"></i>
</div>
</div>
<div class="modal-footer">
<button id='button-cancel-task' type="button" class="btn btn-warning btn-outline pull-left" data-dismiss="modal">Cancel</button>
<button id='button-confirm-task' type="button" class="btn btn-info btn-outline">Confirm</button>
</div>
</div>
<!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</div>
<!-- /.modal -->