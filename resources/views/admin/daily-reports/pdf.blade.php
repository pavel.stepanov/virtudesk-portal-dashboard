<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        {{-- <meta charset="utf-8"> --}}
        <title>Daily Report</title>
        <link rel="stylesheet" href="{{ public_path() }}/css/pdf.css">
    </head>
<body>
<h2>Daily Report</h2>
<div>{{$target_date}}</div>
<br>
@foreach ($attendances as $att)

@php
$tasks = App\Models\UserTask::where('attendance_id', $att->id)->get();
@endphp
    <div class="section">
    <h4>{{$att->user->fullname}}</h4>
    <p>{{$att->showShift()}} - {{$att->client->fullname}}</p>
    </div>
    <div class="section">

        <table class="table table-bordered table-hover" role='grid' width='100%'>
            <tr>
                <td>Time Started</td>
                <td>{{date("Y-m-d",strtotime($att->date_in))}} {{date("h:i:s A",strtotime($att->time_in))}}</td>
            </tr>
            <tr>
                <td>Total Finished</td>
                <td>{{date("Y-m-d",strtotime($att->date_end))}} {{date("h:i:s A",strtotime($att->time_end))}}</td>
            </tr>
            <tr>
                <td>Total Time</td>
                <td>{{$att->total_time}}</td>
            </tr>
            <tr>
                <td>Idle Time</td>
                <td>{{$att->total_idletime}}</td>
            </tr>
        </table>

    </div>

    <div class="section">

        <table class="table table-bordered table-hover" role='grid' width='100%'>
            <thead>
                <tr>
                    <th width="40%" style='text-align:center'>Task</th>
                    <th width="30%" style='text-align:center'>Time Started</th>
                    <th width="30%" style='text-align:center'>Time Finished</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($tasks as $t)
                <tr>
                    <td>{{$t->name}}</td>
                    <td>{{date("Y-m-d",strtotime($t->date_start))}} {{date("h:i:s A",strtotime($t->time_start))}}</td>
                    <td>{{date("Y-m-d",strtotime($t->date_end))}} {{date("h:i:s A",strtotime($t->time_end))}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
   </div>
    <br><br>
@endforeach

</body>
</html>