@foreach ($attendances as $att)

@php
$tasks = App\Models\UserTask::where('attendance_id', $att->id)->get();
@endphp
    <div class="section">
    <h4>{{$att->user->fullname}}</h4>
    <p>{{$att->showShift()}} - {{$att->client->fullname}}</p>
    </div>
    <div class="section">
                <div class="col-lg-3 col-xs-6">
                    <div class="small-box bg-orange">
                        <div class="inner">
                        <h3>{{$att->time_in}}</h3>
                        <p>{{$att->date_in}}</p>
                        <p>Time Started</p>
                        </div>
                        <div class="icon">
                        <i class="ion ion-play"></i>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-xs-6">
                    <div class="small-box bg-orange">
                        <div class="inner">
                        <h3>{{$att->time_end}}</h3>
                        <p>{{$att->date_end}}</p>
                        <p>Time Finished</p>
                        </div>
                        <div class="icon">
                        <i class="ion ion-stop"></i>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-xs-6">
                    <div class="small-box bg-orange">
                        <div class="inner">
                        <h3>{{$att->total_time}}</h3>
                        <p>&nbsp;</p>
                        <p>Total Time</p>
                        </div>
                        <div class="icon">
                        <i class="ion ion-flag"></i>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-xs-6">
                    <div class="small-box bg-orange">
                        <div class="inner">
                        <h3>{{$att->total_idletime}}</h3>
                        <p>&nbsp;</p>
                        <p>Idle Time</p>
                        </div>
                        <div class="icon">
                        <i class="ion ion-clock"></i>
                        </div>
                    </div>
                </div>
    </div>

    <div class="section">

        <table class="table table-bordered table-hover" role='grid' width='100%'>
            <thead>
                <tr>
                    <th width="50%" style='text-align:center'>Task</th>
                    <th width="25%" style='text-align:center'>Time Started</th>
                    <th width="25%" style='text-align:center'>Time Finished</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($tasks as $t)
                <tr>
                    <td>{{$t->name}}</td>
                    <td>{{date("Y-m-d",strtotime($t->date_start))}} {{date("h:i:s A",strtotime($t->time_start))}}</td>
                    <td>{{date("Y-m-d",strtotime($t->date_end))}} {{date("h:i:s A",strtotime($t->time_end))}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
   </div>


@endforeach