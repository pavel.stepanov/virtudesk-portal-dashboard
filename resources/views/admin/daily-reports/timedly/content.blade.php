@foreach ($attendances as $att)

@php
$tasks = App\Models\UserTask::where('attendance_id', $att->id)->get();
@endphp
<div class="row mt-4">
    <div class="col-12">
        <h4>{{$att->user->fullname}}</h4>
        <p>{{$att->showShift()}} - {{$att->client->fullname}}</p>
    </div>
    <div class="col-12">
        <div class="row">
            <div class="col-sm-3">
                <div class="card card-stats bg-gradient-info">
                    <!-- Card body -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                <h5 class="card-title text-uppercase text-muted text-white mb-0">Total
                                    hours</h5>
                                <span class="h2 font-weight-bold mb-0 text-white">{{$att->total_time}}</span>
                            </div>
                            <div class="col-auto">
                                <div class="icon icon-shape bg-gradient-secondary text-white rounded-circle shadow">
                                    <i class="fas fa-hourglass-half text-info"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer bg-gradient-info">
                        &nbsp;
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="card card-stats bg-gradient-success">
                    <!-- Card body -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                <h5 class="card-title text-uppercase text-muted text-white mb-0">Time
                                    Started</h5>
                                <span class="h2 font-weight-bold mb-0 text-white">{{$att->time_in}}</span>
                            </div>
                            <div class="col-auto">
                                <div class="icon icon-shape bg-gradient-secondary text-white rounded-circle shadow">
                                    <i class="ni ni-button-play text-success"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer text-white bg-gradient-success">
                        {{$att->date_in}}
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="card card-stats bg-gradient-danger">
                    <!-- Card body -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                <h5 class="card-title text-uppercase text-muted text-white mb-0">Time
                                    Stopped</h5>
                                <span class="h2 font-weight-bold mb-0 text-white">{{$att->time_end}}</span>
                            </div>
                            <div class="col-auto">
                                <div class="icon icon-shape bg-gradient-secondary text-white rounded-circle shadow">
                                    <i class="fas fa-stop text-danger"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer text-white bg-gradient-danger">
                        {{$att->date_end}}
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="card card-stats bg-gradient-purple">
                    <!-- Card body -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                <h5 class="card-title text-uppercase text-muted text-white mb-0">Idle
                                    Time</h5>
                                <span class="h2 font-weight-bold mb-0 text-white">{{$att->total_idletime}}</span>
                            </div>
                            <div class="col-auto">
                                <div class="icon icon-shape bg-gradient-secondary text-white rounded-circle shadow">
                                    <i class="ni ni-button-pause text-purple"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer bg-gradient-purple">
                        &nbsp;
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-12">
        <table class="table table-bordered table-hover" role='grid' width='100%'>
            <thead>
                <tr>
                    <th width="50%" style='text-align:center'>Task</th>
                    <th width="25%" style='text-align:center'>Time Started</th>
                    <th width="25%" style='text-align:center'>Time Finished</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($tasks as $t)
                <tr>
                    <td>{{$t->name}}</td>
                    <td>{{date("Y-m-d",strtotime($t->date_start))}} {{date("h:i:s A",strtotime($t->time_start))}}</td>
                    <td>{{date("Y-m-d",strtotime($t->date_end))}} {{date("h:i:s A",strtotime($t->time_end))}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
   </div>
</div>
<hr>

@endforeach