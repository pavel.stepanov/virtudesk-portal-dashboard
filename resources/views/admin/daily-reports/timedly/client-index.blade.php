@extends('layouts.timedly.app')

@section('pageTitle')
    Daily Reports
@endsection

@section('specialScripts')
<script>
// use this transport for "binary" data type
$.ajaxTransport("+binary", function(options, originalOptions, jqXHR){
    // check for conditions and support for blob / arraybuffer response type
    if (window.FormData && ((options.dataType && (options.dataType == 'binary')) || (options.data && ((window.ArrayBuffer && options.data instanceof ArrayBuffer) || (window.Blob && options.data instanceof Blob)))))
    {
        return {
            // create new XMLHttpRequest
            send: function(_, callback){
		// setup all variables
                var xhr = new XMLHttpRequest(),
                    url = options.url,
                    type = options.type,
		// blob or arraybuffer. Default is blob
                    dataType = options.responseType || "blob",
                    data = options.data || null;
				
                xhr.addEventListener('load', function(){
                    var data = {};
                    data[options.dataType] = xhr.response;
		// make callback and send data
                    callback(xhr.status, xhr.statusText, data, xhr.getAllResponseHeaders());
                });

                xhr.open(type, url, true);
                xhr.responseType = dataType;
                xhr.send(data);
            },
            abort: function(){
                jqXHR.abort();
            }
        };
    }
});
$(document).ready(function(){
    $("#generate-pdf").hide();

    $('#datepicker').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true
    }).on('changeDate', function(e) {
        target_date = $("#target_date").val();
        $("#generate-pdf").hide();
    });

    $('#va').multiselect({
        maxHeight: 200,
        buttonWidth: '150px',
        numberDisplayed	: 1,
        includeSelectAllOption: true,
        onChange: function(option, checked, select) {
            $("#generate-pdf").hide();
        }
    });

    $("#generate-report").click(function(){

        target_date = $("#target_date").val();
        va = $("#va").val();

        if (va==0) {
            alert('Select a VA first');
            return;
        }

        $('.daily-report-content').html('<i class="fa fa-spin fa-refresh"></i>');
        $.ajax({
            url: "/dashboard/daily-reports/generate",
            dataType: "json",
            data: {
                "va_id" : va,
                "target_date" :  target_date
            },
        }).done(function(data) {
            if (data.html!="<p>No attendance record found.</p>") $("#generate-pdf").show();
            $('.daily-report-content').html(data.html);
        });

    });


    $("#generate-pdf").click(function(){

        target_date = $("#target_date").val();
        va = $("#va").val();

        if (va==0) {
            alert('Select a VA first');
            return;
        }


        $.ajax({
                url: "/dashboard/daily-reports/generate/pdf",
                dataType: 'binary',
                data: {
                    "va_id" : va,
                    "target_date" :  target_date
                },
        }).done(function(data) {

            var url = window.URL.createObjectURL(data);
            var $a = $('<a />', {
            'href': url,
            'download': 'daily-report.pdf',
            'text': "click"
            }).hide().appendTo("body")[0].click();
            setTimeout(function() {
            window.URL.revokeObjectURL(url);
            }, 10000);

        });

    });
});
</script>
@endsection

@section('content')
    <div class="header bg-gradient-info pb-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <div class="col-lg-6 col-7">
                        <h6 class="h2 text-white d-inline-block mb-0">Daily Reports</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Page content -->

    <div class="container-fluid mt--6">
        <div class="row">

            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="mb-0">View daily task report</h3>
                        <small>Please select the VA and Date to view the report</small>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-4 mb-3">
                                <label class="form-control-label sr-only" for="exampleFormControlSelect1">Select
                                    VA</label>
                                <select class="form-control input-lg" id="va" name="va" multiple="multiple" required>
                                    @foreach ($vas as $va)
                                    <option value="{{$va->id}}">{{$va->fullname}}</option>
                                    @endforeach 
                                </select>
                            </div>
                            <div class="col-sm-4 mb-3">
                                <label class="form-control-label sr-only">Date</label>
                                <div class="input-group date" id="datepicker">
                                    <input type="text" id='target_date' name='target_date' class="form-control input-lg" value="{{date('Y-m-d', strtotime(now()))}}">
                                    <div class="input-group-addon">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4 mb-3">
                                <button class="btn btn-block btn-primary" id='generate-report'><i class="fa fa-cog"></i> Generate Daily Report</button>
                                <button class="btn btn-block btn-primary" id='generate-pdf'><i class="fa fa-file"></i> Generate PDF</button>
                            </div>
                        </div>
                        <div class='row'><hr></div>
                        <div class='row'>
                            <div class='col-sm-12 daily-report-content'>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
