@extends('layouts.adminlte-master')
@section('page-title', "VA Schedule")

@section('content')
@include('admin.delete-modal')

<section class='content'>
    <div class='row'>
        <div class='col-xs-12'>
            <div class='box'>
                <div class='box-header'>
                    <h3 class='box-title'>Schedule of {{$user->first_name}} {{$user->last_name}}</h3>
                    @if (Auth::user()->allowed('add_va_schedule'))
                    <span class="pull-right"><a href="/dashboard/va-schedules/{{$user->id}}/create" class="btn btn-primary"><i class="fa fa-plus"></i> Create Schedule</a></span>
                    @endif
                </div>
                <div class='box-body'>
                    <div class='row'>
                        <div class='col-sm-12'>
                            <table class="table table-bordered table-hover dataTable" id="va-schedule-table" role='grid' width='100%'>
                                <thead>
                                    <tr>
                                        <th>Day</th>
                                        <th>Client Time</th>
                                        <th>Shift</th>
                                        @if (!$auth_user->is('client'))
                                        <th>VA Time</th>
                                        <th>Client Name</th>
                                        <th>NumDate</th>
                                        <th>Actions</th>
                                        @endif
                                    </tr>
                                </thead>                                
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@push('view-styles')
<link href="//cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css" rel="stylesheet">
@endpush

@push('view-scripts')
<script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>

<script>
var _table;
$(function() {
    @if ($auth_user->is('client'))
    _table = $('#va-schedule-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '/dashboard/va-schedules/{{$user->id}}/datatables',
        "columns": [
                { "data": "day_of_week" },
                { "data": "time" },
                { "data": "shift"}
        ]
    });
    @else
    _table = $('#va-schedule-table').DataTable({
        processing: true,
        serverSide: false,
        "order": [[ 5, "asc" ]],
        ajax: '/dashboard/va-schedules/{{$user->id}}/datatables',
        "columns": [
                { "data": "day_of_week" },
                { "data": "time" },
                { "data": "shift"},
                { "data": "va_time"},
                { "data": "client_id"},
                { "data": "num_date", visible:false },
                { "data": "actions", width: "150px", orderable: false, searchable: false},
        ]
    });
    @endif
});

     var temp_delete_id = 0;

    //this shows the modal
    $(document).on("click", ".button-delete", function(){
        var id = $(this).attr('data-id');
        temp_delete_id = id;
        $('.modal-body .content').html('<i class="fa fa-spin fa-refresh"></i>');
        $.ajax({
            url: "/dashboard/va-schedules/show/"+id,
            dataType: "json"
        }).done(function(data) {
            $('.modal-body .content').html(data.html);
            $('#modal-warning').modal('show');
        }); 
    });

    //this does the actual delete
    $("#button-delete").click(function(){
        $(this).html('<i class="fa fa-spin fa-refresh"></i> Delete');
        $.ajax({
            url: "/dashboard/va-schedules/delete/"+temp_delete_id,
            dataType: "json"
        }).done(function(data) {
            $('#modal-warning').modal('hide');
            $("#button-delete").html('Delete');
            _table.ajax.reload( null, false );
        }); 
    });

</script>

@endpush