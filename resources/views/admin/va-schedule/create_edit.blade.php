@extends('layouts.adminlte-master')
@section('page-title', "VA Schedules")

@section('content')
<div class="col-lg-12">

    @if ($errors->any())
    <div class="box box-warning box-solid">
        <div class="box-header with-border">
            <h3 class="box-title">Errors</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
            <!-- /.box-tools -->
        </div>

        <!-- /.box-header -->
        <div class="box-body">
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </div>
        <!-- /.box-body -->
    </div>
    @endif

    @if (session()->has('notification_message'))
    <div class="alert {{ session()->get('alert_type') }} alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-info"></i> {{ session()->get('alert_title') }}</h4>
        {{ session()->get('notification_message') }}
    </div>
    @endif  
    
    <div class="panel panel-default">

        <div class="panel-heading">
        @if (isset($q))
            <i class="fa fa-user-circle-o fa-minus"></i> Edit Virtual Assistant Schedule for {{$user->first_name}} {{$user->last_name}}
        @else
            <i class="fa fa-user-circle-o fa-minus"></i> Add Virtual Assistant Schedule for {{$user->first_name}} {{$user->last_name}}
        @endif
        </div>

        <div class="panel-body">
            <div class="col-md-12">
                @if (isset($q))
                <form enctype="multipart/form-data" method="POST" action="{{url('/dashboard/va-schedules/update')}}">
                @else
                <form enctype="multipart/form-data" method="POST" action="{{url('/dashboard/va-schedules/'.$user->id.'/create')}}">
                @endif
                {{ csrf_field() }}

                        @if (isset($q))
                        <input type='hidden' value='{{$q->id}}' name='id'/>
                        @endif
                        <input type='hidden' value='{{$user->id}}' name='user_id'/>


                    <div class="row">
                        <div class="col-lg-12">

                        <div class="col-md-12 col-lg-12">

                        <div class="box box-info">
                            <div class="box-header">
                                <h3 class="box-title"><i class="fa fa-calendar"></i> Schedule Details</h3>
                            </div>
                                <div class="box-body">

                                    @if (isset($q))

                                        <div class="form-group">
                                            <label>Client</label>
                                            <div>{{$q->client->first_name}} {{$q->client->last_name}} - {{$q->client->timezone}}</div>
                                        </div>

                                        <div class="form-group">
                                            <label>Shift/Break</label>
                                            <div>{{$q->showShift()}}</div>
                                        </div>

                                        <div class="form-group">
                                            <label>Day of Week</label>
                                            <div>{{$q->day_of_week}}</div>
                                        </div>

                                        <div class="form-group">
                                            <label>Allowed Breaktime</label>
                                            <div class="input-group max-break">
                                            @if ($q->max_breaktime!='')
                                            <input name="max_breaktime" type="text" class="form-control input-lg" value="{{date('H:i',strtotime($q->max_breaktime))}}">
                                            @else
                                            <input name="max_breaktime" type="text" class="form-control input-lg" value="">
                                            @endif
                                            <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-time"></span>
                                            </span>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Paid Breaktime</label>
                                            <div class="input-group paid-break">
                                            @if ($q->paid_break!='')
                                            <input name="paid_break" type="text" class="form-control input-lg" value="{{date('H:i',strtotime($q->paid_break))}}">
                                            @else
                                            <input name="paid_break" type="text" class="form-control input-lg" value="">
                                            @endif
                                            <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-time"></span>
                                            </span>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Start Time</label>
                                            <div class="input-group clockpicker">
                                            <input name="start_time" type="text" class="form-control input-lg" value="{{date('h:iA',strtotime($q->start_time))}}">
                                            <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-time"></span>
                                            </span>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Hours Needed</label>
                                            <input name="hours_needed" type="text" class="form-control input-lg" value="{{$q->hours_needed}}">
                                        </div>
                                    @else

                                        <div class="form-group">
                                            <label>Day of Week</label>
                                            <br/>
                                            <label class="checkbox-inline">
                                                <input name='day_of_week[Monday]' type="checkbox" > Monday
                                            </label>
                                            <label class="checkbox-inline">
                                                <input name='day_of_week[Tuesday]' type="checkbox" > Tuesday
                                            </label>
                                            <label class="checkbox-inline">
                                                <input name='day_of_week[Wednesday]' type="checkbox" > Wednesday
                                            </label>
                                            <label class="checkbox-inline">
                                                <input name='day_of_week[Thursday]' type="checkbox" > Thursday
                                            </label>
                                            <label class="checkbox-inline">
                                                <input name='day_of_week[Friday]' type="checkbox" > Friday
                                            </label>
                                            <label class="checkbox-inline">
                                                <input name='day_of_week[Saturday]' type="checkbox" > Saturday
                                            </label>
                                            <label class="checkbox-inline">
                                                <input name='day_of_week[Sunday]' type="checkbox" > Sunday
                                            </label>                                            
                                        </div>


                                        <div class="form-group">
                                            <label>Clients</label>
                                            <select name="client_id" class="form-control input-lg">
                                                @foreach ($clients as $client)
                                                <option value="{{$client->id}}">{{$client->first_name}} {{$client->last_name}} - {{$client->timezone}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label>Shift</label>
                                            <select name="shift" class="form-control input-lg">
                                                @foreach ($shift as $key => $value)
                                                <option value="{{$key}}">{{$value}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label>Allowed Breaktime</label>
                                            <div class="input-group max-break">
                                            <input name="max_breaktime" type="text" class="form-control input-lg" value="">
                                            <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-time"></span>
                                            </span>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Paid Breaktime</label>
                                            <div class="input-group paid-break">
                                            <input name="paid_break" type="text" class="form-control input-lg" value="">
                                            <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-time"></span>
                                            </span>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Start Time</label>
                                            <div class="input-group clockpicker">
                                            <input name="start_time" type="text" class="form-control input-lg" value="">
                                            <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-time"></span>
                                            </span>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Hours Needed</label>
                                            <input name="hours_needed" type="text" class="form-control input-lg">
                                        </div>

                                    @endif

                                </div>
                            </div>

                        </div>


                        <div class="col-xs-12">
                            <div class="box box-info">
                                <div class="box-footer clearfix">
                                @if (isset($q))
                                <button type="submit" class="pull-right btn btn-success btn-lg">Update</button>
                                @else
                                    <button type="submit" class="pull-right btn btn-success btn-lg">Add</button>
                                @endif
                                </div>
                            </div>
                        </div>

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>


</div>
<div class='clearfix'></div>
@endsection

@push('view-styles')
<link rel="stylesheet" href="{{ asset('css/bootstrap-clockpicker.min.css') }}" />
@endpush

@push('view-scripts')
<script src="{{ asset('js/bootstrap-clockpicker.min.js') }}"></script>
<script src="{{ asset('js/jquery.are-you-sure.js') }}"></script>

<script type="text/javascript">
$(function () {
    $('.clockpicker').clockpicker({
        placement: 'top',
        align: 'left',
        donetext: 'Set',
        twelvehour:true
    });

    $('.max-break').clockpicker({
        placement: 'top',
        align: 'left',
        donetext: 'Set',
    });

    $('.paid-break').clockpicker({
        placement: 'top',
        align: 'left',
        donetext: 'Set',
    });

    $('form').areYouSure({
        message: 'It looks like you have been editing something. '
               + 'If you leave before saving, your changes will be lost.'
      });
    /*$('#datepicker').datepicker({
        format: 'yyyy-mm-dd',
        calendarWeeks: true,
        autoclose: true,
        weekStart: 1,
        daysOfWeekDisabled: "0,2,3,4,5,6",
    });

    $("#datepicker-month").datepicker( {
        format: "mm-yyyy",
        startView: "months", 
        minViewMode: "months",
        autoclose: true,
    });

    $("#schedule-type").change(function(){
        var selected = $('#schedule-type').find(":selected").val();
        $("#weekly-container").hide();
        $("#monthly-container").hide();
        $("#"+selected+"-container").show();
    });*/
});
</script>
@endpush