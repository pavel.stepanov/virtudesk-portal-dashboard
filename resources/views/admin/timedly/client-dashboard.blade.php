@extends('layouts.timedly.app')

@section('pageTitle')
    Dashboard
@endsection

@section('pageScripts')
<script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script src="https://momentjs.com/downloads/moment.min.js"></script>
    <script src="https://momentjs.com/downloads/moment-timezone-with-data.js"></script>
    <script src="{{ asset('js/notify.min.js') }}"></script>


    <script>
    var screenshots_table;
    var online_table;
    var audio = document.getElementById("notif");

    //refresh page every minute
    window.setInterval(function(){
      //screenshots_table.ajax.reload();
      loadScreenshots();
      online_table.ajax.reload();
    }, 50000);

    function loadScreenshots() {
      $.ajax({
        url: "/get-all-sc-clients",
        dataType: "json"
      }).done(function(data) {
        //$('.modal-body .content').html(data.html);
        //$('#modal-preview').modal('show');
        //console.log(data.filename);
        $('.dashboard__tracker-activities .row').html('');
        var html = '';
        var dd_format = '';
        var dt_format = '';
        var db_date = '';
        var user = '';
        var client_tz = '{{ Auth::user()->timezone }}';
        var timezones = [];
        timezones['EST'] = 'America/New_York';
        timezones['PST'] = 'America/Los_Angeles';
        timezones['CST'] = 'America/Chicago';
        timezones['MST'] = 'America/Denver';
        timezones['MST-Phoenix'] = 'America/Phoenix';
        timezones['MNL'] = ' Asia/Manila';

        console.log('{{ Auth::user() }}');

        //var client_timezone = timezones[client_tz];

        $.each(data, function(k, v){
          //console.log(v.user.first_name);
          if(v.filename != undefined){
            const FORMAT_DATE = "MMM DD, YYYY";
            const FORMAT_TIME = "hh:mm:ss A";

            //dd_format = moment(v.created_at).format(FORMAT_DATE);
            //dt_format = moment(v.created_at).format(FORMAT_TIME);
            //moment.tz("2013-11-18 11:55", "America/Toronto");

            //dt_format = moment.tz(v.created_at,  timezones[client_tz]).format(FORMAT_TIME);

            var dte = moment(v.created_at);
            dt_format = dte.tz(timezones[client_tz], false).format(FORMAT_TIME);
            dd_format = dte.tz(timezones[client_tz], false).format(FORMAT_DATE);

            user = v.user.first_name + ' ' + v.user.last_name;
            //Taken on: ' + dd_format + ' at ' + dt_format + ' ' + user + ' 

            html += '<div class="col-lg-6 col-12" style="margin-bottom: 20px;"><img style="width: 200px !important; height: 200px !important; cursor: pointer;" src="' + v.path + v.filename + '" data-id="' +v.id+ '" alt="" class="button-preview img-responsive img-thumbnail" data-toggle="modal" data-target="#screenshot" style="cursor:pointer;""><div class="tracker-summary"><div style="font-weight: 600;">'+ user +'</div><div style="text-transform: uppercase;"><small>'+dd_format + ' ' + dt_format +'</small></div></div></div><div class="modal fade" id="screenshot"><div class="modal-dialog modal-lg" role="dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div><div class="modal-body"><div class="content"><i class="fa fa-spin fa-refresh"></i></div></div></div></div></div>';
          }
        });

        $('.dashboard__tracker-activities .row').html(html);

      });
    }

    (function ($) {

      function checkNotif() {
        $.ajax({
          url: "/dashboard/notifications/check_notif",
          dataType: "json",
        }).done(function(data) {

           // console.log(data)

          if (!jQuery.isEmptyObject(data) ) {
            for(var k in data) {
              if (data[k]['type']=='time-in' || data[k]['type']=='time-out')
              $.notify(data[k]['content'], {autoHideDelay:10000,className:'info'});

              if (data[k]['type']=='idle-start' || data[k]['type']=='idle-stop') {
                audio.play();
                $.notify(data[k]['content'], {autoHideDelay:10000,className:'error'});
              }

               if (data[k]['type']=='break-exceed') {
                audio.play();
                $.notify(data[k]['content'], {autoHideDelay:10000,className:'error'});
              }

              if (data[k]['type']=='break-start' || data[k]['type']=='break-stop') {
                audio.play();
                $.notify(data[k]['content'], {autoHideDelay:10000,className:'warn'});
              }
            }
          }
        });
      }





      function loadOnlineVA() {
        online_table = $('#online-table').DataTable({
          processing: false,
          serverSide: true,
          ajax: '/dashboard/va/online_dashboard_datatables_client',
          "columns": [
            { "data": "date_in" },
            { "data": "user_id" },
            { "data": "time_in" },
            { "data": "status" },
          ]
        });
      }

      $(document).ready(function() {

        loadScreenshots();
        loadOnlineVA();

        //this shows the preview modal
        $(document).on("click", ".button-preview", function(){
        var id = $(this).attr('data-id');
        $('.modal-body .content').html('<i class="fa fa-spin fa-refresh"></i>');
        $.ajax({
            url: "/dashboard/screenshots/preview/"+id,
            dataType: "json"
        }).done(function(data) {
            $('.modal-body .content').html(data.html);
        }); 
    });
});   

    }) (jQuery);
    </script>


@endsection

@section('content')
    <div class="header bg-gradient-info pb-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <div class="col-lg-6 col-7">
                        <h6 class="h2 text-white d-inline-block mb-0">Dashboard</h6>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xl-4 col-md-6">
                        <div class="card card-stats">
                            <!-- Card body -->
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <h5 class="card-title text-uppercase text-muted mb-0">My Virtual Assistants</h5>
                                        <span class="h2 font-weight-bold mb-0">{{$va_count}}</span>
                                    </div>
                                    <div class="col-auto">
                                        <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                                            <i class="fas fa-users"></i>
                                        </div>
                                    </div>
                                </div>
                                <p class="mt-3 mb-0 text-sm">
                                    <a href="/dashboard/va">View all</a>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-6">
                        <div class="card card-stats">
                            <!-- Card body -->
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <h5 class="card-title text-uppercase text-muted mb-0">Total Working Hours</h5>
                                        <span class="h2 font-weight-bold mb-0">{{$total_time}}</span>
                                    </div>
                                    <div class="col-auto">
                                        <div class="icon icon-shape bg-gradient-orange text-white rounded-circle shadow">
                                            <i class="far fa-clock"></i>
                                        </div>
                                    </div>
                                </div>
                                <p class="mt-3 mb-0 text-sm">
                                    &nbsp;
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-6">
                        <div class="card card-stats">
                            <!-- Card body -->
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <h5 class="card-title text-uppercase text-muted mb-0">Total Screenshots</h5>
                                        <span class="h2 font-weight-bold mb-0">{{$screenshot_count}}</span>
                                    </div>
                                    <div class="col-auto">
                                        <div class="icon icon-shape bg-gradient-green text-white rounded-circle shadow">
                                            <i class="ni ni-tv-2"></i>
                                        </div>
                                    </div>
                                </div>
                                <p class="mt-3 mb-0 text-sm">
                                    <a href="/dashboard/screenshots">View all screenshots</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col-xl-8">
                <div class="card">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="mb-0">Latest Virtual Assistant Attendance</h3>
                            </div>
                            <div class="col text-right">
                                <a href="/dashboard/attendance" class="btn btn-sm btn-primary">See all</a>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <!-- Projects table -->
                        <div class="table-responsive">
                            <table class="va-online-table table table-hover dataTable" id="online-table" role='grid'>
                            <thead>
                                <tr>
                                    <th>Date In</th>
                                    <th>Time In</th>
                                    <th>Name</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                        </table>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="mb-0">Virtual Assistants Logs</h3>
                            </div>
                            <div class="col text-right">
                                {{-- <a href="attendance.html" class="btn btn-sm btn-primary">See all</a> --}}
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <!-- Projects table -->
                        <div class="table-responsive">
                            <table class="table align-items-center table-flush">
                                <thead class="thead-light">
                                <tr>
                                    <th scope="col" data-sort="name">Date</th>
                                    <th scope="col" data-sort="budget">VA</th>
                                    <th scope="col" data-sort="status">Task Name</th>
                                    <th scope="col" data-sort="status">Time In</th>
                                    <th scope="col" data-sort="status">Time End</th>
                                </tr>
                                </thead>
                                <tbody class="list">
                                    @if (count($vatask_latest))
                                        @foreach ($vatask_latest as $vlist)
    
                                            @php
                                                $time_start = $user->convertToUserTimezone($vlist->date_start . ' ' . $vlist->time_start);
                                                $time_end = $user->convertToUserTimezone($vlist->date_end . ' ' . $vlist->time_end);
                                                $date_start = $user->convertToUserTimezone($vlist->date_start);
                                            @endphp
    
                                            <tr>
                                                <td scope="row">{{$date_start->format('Y-m-d')}}</td>
                                                <td>{{$vlist->user->first_name}} {{$vlist->user->last_name}}</td>
                                                <td>{{$vlist->name}}</td>
                                                <td>{{$time_start->format('h:i:s A')}}</td>
                                                <td>{{$time_end->format('h:i:s A')}}</td>
                                            </tr>
                                        @endforeach
                                    @else 
                                        <tr>
                                            <td colspan="5">No items to display</td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-4">
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <!-- Title -->
                                <h6 class="text-uppercase text-muted ls-1 mb-1">{{count($projects)}}/15 Projects</h6>
                                <h5 class="h3 mb-0">Progress track</h5>
                            </div>
                        </div>
                    </div>
                    <!-- Card body -->
                    <div class="card-body">
                        <!-- List group -->
                        <ul class="list-group list-group-flush list my--3">

                            @if (!empty($projects))

                                @foreach($projects as $p) 

                                @php
                                    $percent = $p->getPercentDone();
                                @endphp

                                <li class="list-group-item px-0">
                                <div class="row align-items-center">
                                    <div class="col">
                                        <h5><a href="task-board.html">{{$p->name}}</a></h5>
                                        <div class="progress progress-xs mb-0">
                                            <div class="progress-bar bg-gradient-info" role="progressbar" aria-valuenow="{{$percent}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$percent}}%;"></div>
                                        </div>
                                    </div>
                                    <div class="col-auto">
                                        <a href='/dashboard/taskboard/{{$p->id}}' class="btn btn-sm btn-primary">View</a>
                                    </div>
                                </div>
                                </li>

                                @endforeach

                            @endif

                        </ul>
                    </div>
                </div>

                <div class="card">
                    <!-- Card header -->
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <!-- Title -->
                                <h5 class="h3 mb-0">Tracker Activities</h5>
                            </div>
                        </div>
                    </div>
                    <!-- Card body -->
                    <div class="card-body dashboard__tracker-activities">
                        <div class="row">
                            @if (count($screenshot_latest))
                                @foreach ($screenshot_latest as $slist)
                                    <div class="col-lg-6 col-12">
                                        <img src="{{$slist->thumbnail_url}}" data-id='{{$slist->id}}' alt="" class="button-preview img-responsive img-thumbnail" data-toggle="modal" data-target="#screenshot" style='cursor:pointer;'>
                                        <button type="button" data-id='{{$slist->id}}' class="btn button-preview btn-warning btn-sm btn-round pull-right" data-toggle="modal" data-target="#screenshot">View</button>
                                        <div class="tracker-summary">
                                        @php
                                            $created_at = $user->convertToUserTimezone($slist->created_at);
                                        @endphp
                                        <p>Taken on: {{$created_at->format('M d, Y')}} at {{$created_at->format('h:i:s A')}} <strong>{{$slist->user->first_name}} {{$slist->user->last_name}}</strong></p>
                                        </div>
                                    </div>
                                    <div class="modal fade" id="screenshot">
                                        <div class="modal-dialog modal-lg" role="dialog">
                                            <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span></button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="content">
                                                <i class="fa fa-spin fa-refresh"></i>
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    
@endsection
