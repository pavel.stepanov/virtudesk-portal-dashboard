@extends('layouts.master')
@section('page-title', "Reset Password")

@section('content')
    <div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
        <div class="panel panel-login">
            <div class="panel-heading">
            <div class="row">
                <div class="col-xs-12">
                <a href="#" class="active" id="login-form-link">
                <img src="/images/bg/virtudesklogo.png" class="img-responsive">
                </a>
                </div>
            </div>
            <hr>
            </div>
            <div class="panel-body">

                @if ($errors->any())
                <div class="box box-warning box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Errors</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                        <!-- /.box-tools -->
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body">
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </div>
                    <!-- /.box-body -->
                </div>
                @endif

                @if (isset($result_message))
                    <div class="box box-warning box-solid">
                        <div class="box-header with-border">
                            <h3 class="box-title"></h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            </div>
                            <!-- /.box-tools -->
                        </div>

                        <!-- /.box-header -->
                        <div class="box-body">
                            {{ $result_message }}
                        </div>
                        <!-- /.box-body -->
                    </div>
                @endif

                @if( isset($status) && $status == 'is-success' )
                    <div class="row">
                        <div class="col-lg-12 text-center">
                        <a href="{{ url('auth/login') }}" class="btn btn-success">Go to Login</a>
                        </div>
                    </div>
                @else
                    <div class="row">
                        <div class="col-lg-12">
                        <form method="POST" action="{{url('/auth/reset-pass')}}">
                        {{ csrf_field() }}
                            @if (isset($pr))
                            <input type="hidden" value="{{$pr->token}}" name="token" />
                            @endif
                            <div class="form-group">
                                <label>Password: <span class="required">*</span></label>
                                <input type="password" name="password" id="password" class="form-control" placeholder="Password"  required="required">
                            </div>
                            <div class="form-group">
                                <label>Confirm Password: <span class="required">*</span></label>
                                <input type="password" name="confirm_password" id="confirm_password" class="form-control" placeholder="Password Confirmation" required="required">
                            </div>
                            
                            <div class="form-group">
                            <div class="row">
                                <div class="col-sm-12 text-center" >
                                <!-- <input type="submit" name="login-submit" id="login-submit" tabindex="4" class="form-control btn btn-login" value="Log In"> -->
                                <button type="submit" class="btn btn-success">Set New Password</button>
                            
                                </div>
                            </div>
                            </div>
                        </form>
                        </div>
                    </div>
                @endif
            <span class="login-error" style="display:none;"></span>
            </div>
        </div>
        </div>
    </div>
    </div>
@endsection

@push('view-scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-backstretch/2.0.4/jquery.backstretch.min.js"></script>
    <script>
        $.backstretch("/images/bg/lock.jpg");
    </script>
@endpush

@push('view-styles')
    <style>
    body {
        padding-top: 90px;
    }
    .panel-login {
        background: rgba(255, 255, 255, 0.43);
        border: 0px;
        border-radius: 0;
    }

    .panel-login>.panel-heading a.active{
        color: #029f5b;
        font-size: 18px;
    }

    .panel-login input[type="text"],.panel-login input[type="email"],.panel-login input[type="password"] {
        height: 45px;
        border: 1px solid #ddd;
        font-size: 16px;
        -webkit-transition: all 0.1s linear;
        -moz-transition: all 0.1s linear;
        transition: all 0.1s linear;
    }
    .panel-login input:hover,
    .panel-login input:focus {
        outline:none;
        -webkit-box-shadow: none;
        -moz-box-shadow: none;
        box-shadow: none;
        border-color: #ccc;
    }
    .btn-login {
        background-color: #59B2E0;
        outline: none;
        color: #fff;
        font-size: 14px;
        height: auto;
        font-weight: normal;
        padding: 14px 0;
        text-transform: uppercase;
        border-color: #59B2E6;
    }
    .btn-login:hover,
    .btn-login:focus {
        color: #fff;
        background-color: #53A3CD;
        border-color: #53A3CD;
    }
    .forgot-password {
        text-decoration: underline;
        color: #888;
    }
    .forgot-password:hover,
    .forgot-password:focus {
        text-decoration: underline;
        color: #666;
    }

    .btn-register {
        background-color: #1CB94E;
        outline: none;
        color: #fff;
        font-size: 14px;
        height: auto;
        font-weight: normal;
        padding: 14px 0;
        text-transform: uppercase;
        border-color: #1CB94A;
    }
    .btn-register:hover,
    .btn-register:focus {
        color: #fff;
        background-color: #1CA347;
        border-color: #1CA347;
    }

    .login-error {
    color: #ff0000;
    }

    .login-success {
        color: #1CA347;
    }

    a {
        text-decoration: none;
        color: white;
    }

    .required {
        color: red;
    }
    </style>
@endpush