@extends('layouts.master')
@section('page-title', "Register")

@section('content')
    <div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
        <div class="panel panel-login">
            <div class="panel-heading">
            <div class="row">
                <div class="col-xs-12">
                <a href="#" class="active" id="login-form-link">
                <img src="/images/bg/virtudesklogo.png" class="img-responsive">
                </a>
                </div>
            </div>
            <hr>
            </div>
            <div class="panel-body">
                @if ($errors->any())
                <div class="box box-warning box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title"></h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                        <!-- /.box-tools -->
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body">
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </div>
                    <!-- /.box-body -->
                </div>
                @endif
            <div class="row">
                <div class="col-lg-12">
                <form method="POST" action="{{url('/auth/register')}}">
                {{ csrf_field() }}

                    <div class="form-group">
                    <label>Email: <span class="required">*</span></label>
                    <input type="text" name="email" id="email" tabindex="1" class="form-control" placeholder="Email" value="{{old('email')}}" required="required">
                    </div>
                    <div class="form-group">
                    <label>Password: <span class="required">*</span></label>
                    <input type="password" name="password" id="password" tabindex="2" class="form-control" placeholder="Password"  required="required">
                    </div>
                    <div class="form-group">
                    <label>Confirm Password: <span class="required">*</span></label>
                    <input type="password" name="password_confirmation" id="password_confirmation" tabindex="3" class="form-control" placeholder="Password" required="required">
                    </div>
                    <hr>
                    <div class="form-group">
                    <label>First Name: <span class="required">*</span></label>
                    <input type="text" name="first_name" id="first_name" tabindex="4" class="form-control" placeholder="First Name" value="{{old('first_name')}}" required="required">
                    </div>
                    <div class="form-group">
                    <label>Last Name: <span class="required">*</span></label>
                    <input type="text" name="last_name" id="last_name" tabindex="5" class="form-control" placeholder="Last Name" value="{{old('last_name')}}" required="required">
                    </div>
                    <div class="form-group">
                    <label>Address 1: <span class="required">*</span></label>
                    <input type="text" name="address_1" id="address_1" tabindex="6" class="form-control" placeholder="Address 1" value="{{old('address_1')}}" required="required">
                    </div>
                    <div class="form-group">
                    <label>Address 2: </label>
                    <input type="text" name="address_2" id="address_2" tabindex="7" class="form-control" placeholder="Address 2" value="{{old('address_2')}}">
                    </div>
                    <div class="form-group">
                    <label>City: <span class="required">*</span></label>
                    <input type="text" name="city" id="city" tabindex="8" class="form-control" placeholder="City" value="{{old('city')}}" required="required">
                    </div>
                    <div class="form-group">
                    <label>Phone Number: <span class="required">*</span></label>
                    <input type="text" name="phone_number" id="phone_number" tabindex="9" class="form-control" placeholder="Phone Number" value="{{old('phone_number')}}" required="required">
                    </div>
                    <div class="form-group">
                    <label>Timezone: <span class="required">*</span></label>
                    <select name="timezone" class="form-control input-lg" tabindex="10">
                        <option value="" disabled>Please select</option>
                        @foreach ($timezone as $key => $value)
                            @if( old('timezone') == $key)
                                <option value="{{$key}}" selected="selected">{{$key}} - {{$value}}</option>
                            @else
                                <option value="{{$key}}">{{$key}} - {{$value}}</option>
                            @endif
                        @endforeach
                    </select>
                    </div>
                    <div class="form-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="read_terms"> I have read and accepted the <a href="{{ config('vatimetracker.terms_and_condition') }}" target="_blank">terms and conditions</a>
                            </label>
                        </div>
                    </div>

                    <div class="form-group">
                    <div class="row">
                        <div class="col-sm-12 text-center" >
                        <button type="submit" tabindex="11" class="btn btn-success">Register</button>
                        <a href="/" tabindex="12" class="btn btn-danger">Cancel</a>
                        </div>
                    </div>
                    </div>
                </form>
                </div>
            </div>
            <span class="login-error" style="display:none;"></span>
            </div>
        </div>
        </div>
    </div>
    </div>
@endsection

@push('view-scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-backstretch/2.0.4/jquery.backstretch.min.js"></script>
    <script>
        $.backstretch("/images/bg/login-bg.jpg");
    </script>
@endpush

@push('view-styles')
    <style>
    body {
        padding-top: 90px;
    }
    .panel-login {
        background: rgba(255, 255, 255, 0.43);
        border: 0px;
        border-radius: 0;
    }

    .panel-login>.panel-heading a.active{
        color: #029f5b;
        font-size: 18px;
    }

    .panel-heading img {
    	width: 200px;
    	margin: auto;
    }

    .panel-login input[type="text"],.panel-login input[type="email"],.panel-login input[type="password"] {
        height: 45px;
        border: 1px solid #ddd;
        font-size: 16px;
        -webkit-transition: all 0.1s linear;
        -moz-transition: all 0.1s linear;
        transition: all 0.1s linear;
    }
    .panel-login input:hover,
    .panel-login input:focus {
        outline:none;
        -webkit-box-shadow: none;
        -moz-box-shadow: none;
        box-shadow: none;
        border-color: #ccc;
    }
    .btn-login {
        background-color: #59B2E0;
        outline: none;
        color: #fff;
        font-size: 14px;
        height: auto;
        font-weight: normal;
        padding: 14px 0;
        text-transform: uppercase;
        border-color: #59B2E6;
    }
    .btn-login:hover,
    .btn-login:focus {
        color: #fff;
        background-color: #53A3CD;
        border-color: #53A3CD;
    }
    .forgot-password {
        text-decoration: underline;
        color: #888;
    }
    .forgot-password:hover,
    .forgot-password:focus {
        text-decoration: underline;
        color: #666;
    }

    .btn-register {
        background-color: #1CB94E;
        outline: none;
        color: #fff;
        font-size: 14px;
        height: auto;
        font-weight: normal;
        padding: 14px 0;
        text-transform: uppercase;
        border-color: #1CB94A;
    }
    .btn-register:hover,
    .btn-register:focus {
        color: #fff;
        background-color: #1CA347;
        border-color: #1CA347;
    }

    .login-error {
    color: #ff0000;
    }

    .login-success {
        color: #1CA347;
    }

    a {
        text-decoration: none;
        color: white;
    }

    .required {
        color: red;
    }
    </style>
@endpush