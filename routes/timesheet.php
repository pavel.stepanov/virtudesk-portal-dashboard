<?php

Route::group(array(
    'middleware' => ['web', 'admin.auth'],
    ), function() {	

        Route::get('/dashboard/timesheets', [
            'uses' => 'Admin\TimesheetController@index'
        ]);

        Route::get('/dashboard/timesheets/datatables', [
            'uses' => 'Admin\TimesheetController@datatables'
        ]);

        Route::get('/dashboard/timesheets/info/{id}', [
            'uses' => 'Admin\TimesheetController@info'
        ])->where('id', '[0-9]+'); 

        Route::post('/dashboard/timesheets/update', [
            'uses' => 'Admin\TimesheetController@update'
        ]);

        Route::get('/dashboard/timesheets/info_datatables/{id}', [
            'uses' => 'Admin\TimesheetController@info_datatables'
        ]);

        Route::get('/dashboard/timesheets/edit/{id}', [
            'uses' => 'Admin\TimesheetController@edit'
        ])->where('id', '[0-9]+');

        Route::get('/dashboard/timesheets/show/{id}', [
            'uses' => 'Admin\TimesheetController@show'
        ])->where('id', '[0-9]+'); 

        Route::get('/dashboard/timesheets/change-status/{status}', [
            'uses' => 'Admin\TimesheetController@changeStatus'
        ]);

        Route::get('/dashboard/timesheets/create', [
            'uses' => 'Admin\TimesheetController@create'
        ]);

        Route::post('/dashboard/timesheets/create', [
            'uses' => 'Admin\TimesheetController@store'
        ]);

        Route::get('/dashboard/timesheets/delete/{id}', [
            'uses' => 'Admin\TimesheetController@delete'
        ])->where('id', '[0-9]+');
});