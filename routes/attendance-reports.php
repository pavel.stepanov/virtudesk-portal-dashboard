<?php
//TODO: set permissions
Route::group(array(
    'middleware' => ['web', 'admin.auth'],
    ), function() {	

        Route::get('/dashboard/attendance-reports', [
            'uses' => 'Admin\AttendanceReportsController@index'
        ]);

        Route::get('/dashboard/attendance-reports/generate', [
            'uses' => 'Admin\AttendanceReportsController@generateReports'
        ]);

        Route::get('/dashboard/attendance-reports/generate/pdf', [
            'uses' => 'Admin\AttendanceReportsController@generatePDF'
        ]);
});