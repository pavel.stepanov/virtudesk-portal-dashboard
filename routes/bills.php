<?php

Route::group(array(
    'middleware' => ['web', 'admin.auth'],
    ), function() {

        Route::get('/dashboard/billing', [
            'uses' => 'Admin\BillsController@index'
        ]);

        Route::get('/dashboard/bills/datatables', [
            'uses' => 'Admin\BillsController@datatables'
        ]);

        Route::get('/dashboard/bills/client/{id}', [
            'uses' => 'Admin\BillsController@clientBills'
        ]);

        Route::get('/dashboard/bills/generate', [
            'uses' => 'Admin\BillsController@generateBills'
        ]);

        Route::get('/dashboard/bills/generate/pdf', [
            'uses' => 'Admin\BillsController@downloadPDF'
        ]);
});