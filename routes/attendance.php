<?php

Route::group(array(
    'middleware' => ['web', 'admin.auth'],
    ), function() {	

        Route::get('/dashboard/attendance', [
            'uses' => 'Admin\AttendanceController@index'
        ]);

        Route::get('/dashboard/attendance/datatables', [
            'uses' => 'Admin\AttendanceController@datatables'
        ]);
});	