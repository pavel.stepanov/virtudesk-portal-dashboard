<?php

Route::group(array(
	'middleware' => ['web', 'admin.auth'],
	), function() {	

	
	Route::get('/dashboard/va', [
		'middleware' => 'check.permission:list_va',
		'uses' => 'Admin\VirtualAssistantController@index'
	]);

        Route::get('/dashboard/va/datatables', [
                'middleware' => 'check.permission:list_va',
                'uses' => 'Admin\VirtualAssistantController@datatables'
        ]);

        Route::get('/dashboard/va/create', [
                'middleware' => 'check.permission:add_va',
                'uses' => 'Admin\VirtualAssistantController@create'
        ]); 

        Route::post('/dashboard/va/create', [
            'middleware' => 'check.permission:add_va',
            'uses' => 'Admin\VirtualAssistantController@store'
        ]);  

        Route::get('/dashboard/va/edit/{id}', [
                'middleware' => 'check.permission:edit_va',
                'uses' => 'Admin\VirtualAssistantController@edit',
                'selected_nav_path' => 'dashboard/va/edit',
                'selected_parent_path' => 'dashboard/va'
        ])->where('id', '[0-9]+');   

        Route::post('/dashboard/va/update', [
                'middleware' => 'check.permission:edit_va',
                'uses' => 'Admin\VirtualAssistantController@update'
        ]);

        Route::get('/dashboard/va/show/{id}', [
                'middleware' => 'check.permission:list_va',
                'uses' => 'Admin\VirtualAssistantController@show'
        ])->where('id', '[0-9]+'); 

        Route::get('/dashboard/va/delete/{id}', [
                'middleware' => 'check.permission:delete_va',
                'uses' => 'Admin\VirtualAssistantController@delete'
        ])->where('id', '[0-9]+');  

        Route::get('/dashboard/online-va', [
                'middleware' => 'check.permission:online_va',
                'uses' => 'Admin\VirtualAssistantController@online'
        ]); 

        Route::get('/dashboard/online-datatables', [
                'middleware' => 'check.permission:online_va',
                'uses' => 'Admin\VirtualAssistantController@online_datatables'
        ]); 

        Route::get('/dashboard/va-work-duration', [
                'middleware' => 'check.permission:va_work_duration',
                'uses' => 'Admin\VirtualAssistantController@va_work_duration'
        ]);

        Route::get('/dashboard/work-duration-datatables', [
                'middleware' => 'check.permission:va_work_duration',
                'uses' => 'Admin\VirtualAssistantController@va_work_duration_datatables'
        ]);

        Route::get('/dashboard/va-client-rates/{id}', [
                //'middleware' => 'check.permission:edit_va',
                'uses' => 'Admin\VirtualAssistantController@va_client_rate',
                'selected_nav_path' => 'dashboard/va/edit',
                'selected_parent_path' => 'dashboard/va'
        ]);

        Route::get('/dashboard/va-client-rates/{id}/available-clients', [
                //'middleware' => 'check.permission:edit_va',
                'uses' => 'Admin\VirtualAssistantController@va_client_rate_available_clients'
        ]);

        Route::get('/dashboard/va-client-rates/{id}/datatables', [
                //'middleware' => 'check.permission:edit_va',
                'uses' => 'Admin\VirtualAssistantController@va_client_rate_datatables'
        ]);

        Route::get('/dashboard/va-client-rate-update/{id}', [
                //'middleware' => 'check.permission:edit_va',
                'uses' => 'Admin\VirtualAssistantController@va_client_rate_update'
        ]);

        Route::get('/dashboard/va-client-rate-add/{id}', [
               // 'middleware' => 'check.permission:edit_va',
                'uses' => 'Admin\VirtualAssistantController@va_client_rate_add'
        ]);

        Route::get('/dashboard/va/ss/{id}', [
                'uses' => 'Admin\VirtualAssistantController@requestSS'
        ])->where('id', '[0-9]+'); 

        Route::get('/dashboard/va/online_dashboard_datatables', [
                'middleware' => 'check.permission:list_va',
                'uses' => 'Admin\VirtualAssistantController@online_dashboard_datatables'
        ]);
        
        Route::get('/dashboard/va/online_dashboard_datatables_client', [
                'middleware' => 'check.permission:list_va',
                'uses' => 'Admin\VirtualAssistantController@online_dashboard_datatables_client'
        ]);
  
});	