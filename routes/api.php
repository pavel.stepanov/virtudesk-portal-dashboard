<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/version', 'API\UserController@getVersion');
Route::post('/auth/login', 'API\AuthController@postLogin');

Route::group(array('middleware' => ['api']), function() {
    Route::get('/auth/logout', 'API\AuthController@getLogout');

    Route::post('/users/screenshot', 'API\UserController@postScreenshot');
    Route::post('/users/process-list', 'API\UserController@postProcessList');
    Route::post('/users/browser-history', 'API\UserController@postBrowserHistory');
    Route::post('/users/idle-start', 'API\UserController@postIdleStart');
    Route::post('/users/idle-stop', 'API\UserController@postIdleStop');
    Route::post('/users/time-in', 'API\UserController@postTimeIn'); // DEPRECATED - use the RESTful version
    Route::post('/users/time-out', 'API\UserController@postTimeOut'); // DEPRECATED - use the RESTful version
    Route::post('/users/sod', 'API\UserController@postSOD');
    Route::post('/users/eod', 'API\UserController@postEOD');
    // Route::post('/users/schedules', 'API\UserController@postSchedules'); // DEPRECATED - use the RESTful version
    Route::post('/users/reports', 'API\UserController@postReports');
    Route::post('/users/has-sod', 'API\UserController@postHasSOD');
    Route::post('/users/has-eod', 'API\UserController@postHasEOD');
    Route::post('/users/break-start', 'API\UserController@postBreakStart');
    Route::post('/users/break-stop', 'API\UserController@postBreakStop');
    // Route::post('/users/attendance', 'API\UserController@postAttendance'); // DEPRECATED - use the RESTful version
    // Route::post('/users/confirm-attendance', 'API\UserController@postConfirmAttendance'); // DEPRECATED - no longer used
    // Route::post('/users/dispute-attendance', 'API\UserController@postDisputeAttendance'); // DEPRECATED - no longer used
    Route::post('/users/ping-pong', 'API\UserController@pingPong');
    Route::post('/users/requests-concerns', 'API\UserController@postRequestConcern');
    Route::post('/users/requests-save', 'Admin\RequestController@saveRequest');
    
    Route::post('/users/requests-concerns', 'API\UserController@postRequestConcern');

    Route::post('/users/messages', 'API\UserController@getMessages');
    Route::post('/users/message-save', 'API\UserController@postMessages');
    Route::post('/users/message-read', 'API\UserController@postMessageRead');
    Route::post('/users/message', 'API\UserController@getMessage');

    Route::resource('/users/tasks', 'API\UserTasksController');
    Route::resource('/users/schedules', 'API\UserScheduleController');
    Route::resource('/users/attendance', 'API\UserAttendanceController');
    Route::resource('/users/timesheets', 'API\UserTimesheetController');
});