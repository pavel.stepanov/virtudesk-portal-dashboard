<?php

//TODO: update middleware for permissions

Route::group(array(
	'middleware' => ['web', 'admin.auth'],
	), function() {	

        Route::get('/dashboard/projects', [
            'uses' => 'Admin\ProjectsController@index'
        ]);

        Route::get('/dashboard/projects/datatables', [
            'uses' => 'Admin\ProjectsController@datatables'
        ]);

        Route::get('/dashboard/projects/show/{id}', [
            'uses' => 'Admin\ProjectsController@show'
        ])->where('id', '[0-9]+'); 

        Route::get('/dashboard/projects/create', [
            'uses' => 'Admin\ProjectsController@create'
        ]);        

        Route::post('/dashboard/projects/create', [
            'uses' => 'Admin\ProjectsController@store'
        ]);  

        Route::post('/dashboard/projects/update', [
            'uses' => 'Admin\ProjectsController@update'
        ]);

        Route::get('/dashboard/projects/edit/{id}', [
            'uses' => 'Admin\ProjectsController@edit',
            'selected_nav_path' => 'dashboard/projects/edit',
            'selected_parent_path' => 'dashboard/projects'
        ])->where('id', '[0-9]+');         

        Route::get('/dashboard/projects/delete/{id}', [
            'uses' => 'Admin\ProjectsController@delete'
        ])->where('id', '[0-9]+');      

});