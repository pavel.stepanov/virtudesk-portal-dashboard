<?php
//TODO: set permissions
Route::group(array(
    'middleware' => ['web', 'admin.auth'],
    ), function() {	

        Route::get('/dashboard/activity-log', [
            'uses' => 'Admin\ActivityLogController@index'
        ]);

        Route::get('/dashboard/activity-log/datatables', [
            'uses' => 'Admin\ActivityLogController@datatables'
        ]);

});	