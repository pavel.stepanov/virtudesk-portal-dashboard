<?php

Route::group(array(
    'middleware' => ['web', 'admin.auth'],
    ), function() {

        Route::get('/dashboard/billing/bulk', [
            'uses' => 'Admin\BulkBillController@index'
        ]);

        Route::get('/dashboard/bills/bulk/generate', [
            'uses' => 'Admin\BulkBillController@bulkGenerate'
        ]);

});