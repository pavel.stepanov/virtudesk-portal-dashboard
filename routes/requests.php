<?php

Route::group(array(
	'middleware' => ['web', 'admin.auth'],
	), function() {	

        Route::get('/dashboard/requests', [
            'uses' => 'Admin\RequestController@index'
        ]);

        Route::get('/dashboard/requests/datatables', [
            'uses' => 'Admin\RequestController@datatables'
        ]);

        Route::get('/dashboard/requests/leave', [
            'uses' => 'Admin\RequestController@leave'
        ]); 

        Route::post('/dashboard/requests/leave', [
            'uses' => 'Admin\RequestController@saveLeave'
        ]);         

        Route::get('/dashboard/requests/show/{id}', [
            'uses' => 'Admin\RequestController@show'
        ])->where('id', '[0-9]+'); 

        Route::get('/dashboard/requests/delete/{id}', [
            'uses' => 'Admin\RequestController@delete'
        ])->where('id', '[0-9]+');

        Route::get('/dashboard/requests/take-action/{id}', [
            'uses' => 'Admin\RequestController@takeAction',
            'selected_nav_path' => 'dashboard/requests/take-action',
            'selected_parent_path' => 'dashboard/requests',
        ])->where('id', '[0-9]+');

        Route::post('/dashboard/requests/take-action', [
            'uses' => 'Admin\RequestController@takeActionApproveDecline'
        ]); 

        Route::get('/dashboard/requests/overtime', [
            'uses' => 'Admin\RequestController@overtime'
        ]); 

        Route::get('/dashboard/requests/sick-leave', [
            'uses' => 'Admin\RequestController@sickleave'
        ]);
       
        Route::get('/dashboard/requests/shift', [
            'uses' => 'Admin\RequestController@shift'
        ]);

        Route::post('/dashboard/requests/save', [
            'uses' => 'Admin\RequestController@saveRequest'
        ]); 
        
        Route::get('/dashboard/requests/info/{id}', [
            'uses' => 'Admin\RequestController@info',
            'selected_nav_path' => 'dashboard/requests/info',
            'selected_parent_path' => 'dashboard/requests',
        ]); 

});