<?php

Route::group(array(
	'middleware' => ['web', 'admin.auth'],
	), function() {	

        Route::get('/dashboard/users', [
            'middleware' => 'check.permission:list_users',
            'uses' => 'Admin\UserController@index'
        ]);

        Route::get('/dashboard/users/datatables', [
            'middleware' => 'check.permission:list_users',
            'uses' => 'Admin\UserController@datatables'
        ]);

        Route::get('/dashboard/users/show/{id}', [
            'middleware' => 'check.permission:list_users',
            'uses' => 'Admin\UserController@show'
        ])->where('id', '[0-9]+'); 

        Route::get('/dashboard/users/create', [
            'middleware' => 'check.permission:add_users',
            'uses' => 'Admin\UserController@create'
        ]);        

        Route::post('/dashboard/users/create', [
            'middleware' => 'check.permission:add_users',
            'uses' => 'Admin\UserController@store'
        ]);  

        Route::post('/dashboard/users/update', [
            'middleware' => 'check.permission:edit_users',
            'uses' => 'Admin\UserController@update'
        ]);

        Route::get('/dashboard/users/edit/{id}', [
            'middleware' => 'check.permission:edit_users',
            'uses' => 'Admin\UserController@edit',
            'selected_nav_path' => 'dashboard/users/edit',
            'selected_parent_path' => 'dashboard/users'
        ])->where('id', '[0-9]+');         

        Route::get('/dashboard/users/delete/{id}', [
            'middleware' => 'check.permission:delete_users',
            'uses' => 'Admin\UserController@delete'
        ])->where('id', '[0-9]+');        
        
        Route::get('/dashboard/users/profile', [
            'uses' => 'Admin\UserController@profile'
        ]);

        Route::get('/dashboard/users/show-profile/{id}', [
            'uses' => 'Admin\UserController@showProfile'
        ]);

        Route::post('/dashboard/users/profile', [
            'uses' => 'Admin\UserController@updateProfile'
        ]);

        Route::get('/dashboard/users/read-terms', [
            'uses' => 'Admin\UserController@readTerms'
        ]);


        Route::get('/dashboard/users/clock/in', [
            'uses' => 'Admin\UserController@getClockIn'
        ]);

        Route::get('/dashboard/users/clock/out', [
            'uses' => 'Admin\UserController@getClockOut'
        ]);

        Route::get('/dashboard/users/task/start/{task_type}/{id}', [
            'uses' => 'Admin\UserController@getTaskStart'
        ]);

        Route::get('/dashboard/users/task/stop/{task_type}', [
            'uses' => 'Admin\UserController@getTaskStop'
        ]);

        Route::post('/dashboard/users/task/status', [
            'uses' => 'Admin\UserController@postTaskStatus'
        ]);
        
        Route::get('/dashboard/user/logout_user/{id}', [
            'middleware' => 'check.permission:delete_users',
            'uses' => 'Admin\UserController@logout_user'
        ])->where('id', '[0-9]+');        
        
        Route::get('/dashboard/user/logout-by-admin/{id}', [
            'uses' => 'Admin\UserController@getLogoutByAdmin'
        ])->name('logout-by-admin');
        
        Route::get('/dashboard/user/remove-from-team/{id}', [
            'uses' => 'Admin\UserController@removeVaFromTeam'
        ])->name('remove-from-team');
        
});	

Route::post('/dashboard/users/screenshot', [
    'middleware' => ['web'],
    'uses' => 'Admin\UserController@postScreenshot'
]);

Route::post('/dashboard/users/process-list', [
    'middleware' => ['web'],
    'uses' => 'Admin\UserController@postProcessList'
]);