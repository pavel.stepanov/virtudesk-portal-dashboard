<?php

Route::group(array(
	'middleware' => ['web', 'admin.auth'],
	), function() {

        Route::get('/dashboard/messages', [
            'uses' => 'Admin\MessagesController@index'
        ]);


        Route::get('/dashboard/messages/datatables', [
            'uses' => 'Admin\MessagesController@datatables'
        ]);

        Route::get('/dashboard/messages/info/{id}', [
            'uses' => 'Admin\MessagesController@info'
        ]);
        
        Route::post('/dashboard/messages/save', [
            'uses' => 'Admin\MessagesController@save'
        ]);


});
