<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

require base_path('routes/users.php');
require base_path('routes/roles.php');
require base_path('routes/permissions.php');
require base_path('routes/clients.php');
require base_path('routes/vas.php');
require base_path('routes/teams.php');
require base_path('routes/schedules.php');
require base_path('routes/requests.php');
require base_path('routes/attendance.php');
require base_path('routes/screenshots.php');
require base_path('routes/tasks.php');
require base_path('routes/app-web.php');
require base_path('routes/client-task.php');
require base_path('routes/settings.php');
require base_path('routes/activity-log.php');
require base_path('routes/timesheet.php');
require base_path('routes/bills.php');
require base_path('routes/client-billing.php');
require base_path('routes/daily-reports.php');
require base_path('routes/summary-reports.php');
require base_path('routes/bulkbill.php');
require base_path('routes/attendance-reports.php');
require base_path('routes/timeline.php');
require base_path('routes/notifications.php');
require base_path('routes/idle.php');
require base_path('routes/scheduled-va.php');
require base_path('routes/taskboard.php');
require base_path('routes/projects.php');
require base_path('routes/messages.php');

Route::get('/', function () {
    return redirect()->intended('/dashboard');
});

Route::get('get-all-sc-clients', 'Admin\ScreenshotController@loadAllScreenshotsClients')->name('get-all-sc-clients');

Route::group(array(
	'middleware' => 'web',
	), function() {

        Route::get('auth/register', 'AuthenticateController@getRegister');
        Route::post('auth/register', 'AuthenticateController@postRegister');

        Route::get('auth/verify', 'AuthenticateController@getVerify');
        Route::get('auth/reset', 'AuthenticateController@getReset');
        Route::post('auth/reset', 'AuthenticateController@postReset');
        Route::get('auth/reset-pass', 'AuthenticateController@getResetPass');
        Route::post('auth/reset-pass', 'AuthenticateController@postResetPass');

        Route::get('/auth/login', 'AuthenticateController@getLogin');
        Route::get('/login', 'AuthenticateController@getLogin');

        Route::post('/auth/login', 'AuthenticateController@postLogin');
        Route::get('/auth/logout', 'AuthenticateController@getLogout');
        Route::get('/auth/token', 'AuthenticateController@getToken');
    });

Route::group(array(
	'middleware' => ['web', 'admin.auth'],
	), function() {	

        Route::get('/dashboard', 'AdminController@index');
        Route::get('/dashboard/dashboard-online-va-ajax', 'Admin\VirtualAssistantController@dashboard_online_va_ajax');
        Route::get('/dashboard/dashboard-va-summary', 'Admin\VirtualAssistantController@dashboard_va_summary_ajax');
        Route::get('/dashboard/forcelogout/{id}', 'AdminController@forceLogout');
        Route::get('/dashboard/upload-s3-per-directory/{id}', 'AdminController@performForceS3Upload');
        Route::get('/dashboard/get-manager-data', 'AdminController@getManagerData');
});	

// FOR TEST AND REFERENCE
Route::get('fire', function () {
    //$user = App\Models\User::find(4);
    //$user->broadcast()->screenshot();
    // $user->broadcast()->processList();
    // $user->broadcast()->browserHistory();
    return " [event fired]";
});

Route::get('dst_schedule_updater', function () {

        $schedules = App\Models\Schedule::get();

        foreach ($schedules as $schedule) {
                $dst = $schedule->DST();
                
                $schedule->user_start_time = $dst['user_start_time'];
                $schedule->user_end_time = $dst['user_end_time'];
                $schedule->user_day_of_week = $dst['user_day_of_week'];
                $schedule->user_day_of_week_end = $dst['user_day_of_week_end'];
                $schedule->save();
        }

        return "DST Schedule Update Done!";

});