<?php

Route::group(array(
    'middleware' => ['web', 'admin.auth'],
    ), function() {

        Route::get('/dashboard/screenshots', 'Admin\ScreenshotController@index');
        Route::get('/dashboard/screenshots/datatables', [
            'uses' => 'Admin\ScreenshotController@datatables'
        ]); 
       
        Route::get('/dashboard/screenshots/{id}/s/{date?}', 'Admin\ScreenshotController@vaScreenshotIndex');
        Route::get('/dashboard/screenshots/{id}/datatables', [
            'uses' => 'Admin\ScreenshotController@vaScreenshotDataTables'
        ]);
        
        Route::get('/dashboard/screenshots-all', 'Admin\ScreenshotController@indexAll');
        Route::get('/dashboard/screenshots/datatables_all', [
            'uses' => 'Admin\ScreenshotController@datatablesAll'
        ]); 

        
        Route::get('/dashboard/screenshots/admin-dashboard', [
            'uses' => 'Admin\ScreenshotController@datatablesAdminDashboard'
        ]); 

        Route::get('/dashboard/screenshots/{id}', 'Admin\ScreenshotController@vaScreenshotIndex');
        Route::get('/dashboard/screenshots/{id}/datatables', [
            'uses' => 'Admin\ScreenshotController@vaScreenshotDataTables'
        ]);

        Route::get('/dashboard/screenshots/preview/{id}', [
            'uses' => 'Admin\ScreenshotController@preview'
        ]);

        Route::get('/dashboard/screenshots/show/{id}', [
            'uses' => 'Admin\ScreenshotController@show'
        ]);

        Route::get('/dashboard/screenshots/delete/{id}', [
            'uses' => 'Admin\ScreenshotController@delete'
        ])->where('id', '[0-9]+');
        
        Route::post('/dashboard/screenshots/loadclients', [
            'uses' => 'Admin\ScreenshotController@loadClients'
        ]);

});