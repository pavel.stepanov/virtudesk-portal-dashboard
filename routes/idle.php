<?php

Route::group(array(
    'middleware' => ['web', 'admin.auth'],
    ), function() {	

        Route::get('/dashboard/idle', [
            'uses' => 'Admin\IdleController@index'
        ]);

});