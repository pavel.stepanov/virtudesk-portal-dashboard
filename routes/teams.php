<?php

Route::group(array(
	'middleware' => ['web', 'admin.auth'],
	), function() {	
        
        Route::get('/dashboard/teams', [
                'middleware' => 'check.permission:list_team',
                'uses' => 'Admin\TeamController@index'
        ]);

        Route::get('/dashboard/teams/datatables', [
                'middleware' => 'check.permission:list_team',
                'uses' => 'Admin\TeamController@datatables'
        ]);

        Route::get('/dashboard/teams/create', [
                'middleware' => 'check.permission:add_team',
                'uses' => 'Admin\TeamController@create'
        ]);

        Route::post('/dashboard/teams/create', [
                'middleware' => 'check.permission:add_team',
                'uses' => 'Admin\TeamController@store'
        ]);  

        Route::get('/dashboard/teams/edit/{id}', [
                'middleware' => 'check.permission:edit_team',
                'uses' => 'Admin\TeamController@edit',
                'selected_parent_path' => 'dashboard/teams',
                'selected_nav_path' => 'dashboard/teams/edit',
        ])->where('id', '[0-9]+');   

        Route::get('/dashboard/teams/show/{id}', [
                'middleware' => 'check.permission:list_team',
                'uses' => 'Admin\TeamController@show'
        ])->where('id', '[0-9]+'); 

        Route::get('/dashboard/teams/delete/{id}', [
                'middleware' => 'check.permission:delete_team',
                'uses' => 'Admin\TeamController@delete'
        ])->where('id', '[0-9]+'); 

        Route::post('/dashboard/teams/update', [
                'middleware' => 'check.permission:edit_team',
                'uses' => 'Admin\TeamController@update'
        ]);

        Route::get('/dashboard/teams/show-members/{id}', [
                'middleware' => 'check.permission:edit_team',
                'uses' => 'Admin\TeamController@show_team_members'
        ])->where('id', '[0-9]+'); 

        Route::get('/dashboard/teams/show-available-va/{id}', [
                'middleware' => 'check.permission:edit_team',
                'uses' => 'Admin\TeamController@show_available_va'
        ])->where('id', '[0-9]+'); 

        Route::get('/dashboard/teams/team-member-add', [
                'middleware' => 'check.permission:edit_team',
                'uses' => 'Admin\TeamController@team_member_add'
        ]);

        Route::get('/dashboard/teams/team-member-remove', [
                'middleware' => 'check.permission:edit_team',
                'uses' => 'Admin\TeamController@team_member_remove'
        ]);
   
});