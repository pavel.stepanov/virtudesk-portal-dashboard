<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Role;


class RolePermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Role::truncate();
        // Insert Roles
        $this->command->info('Seeding <question>roles</question> table.');
        $roles_array = array(
            'super_administrator' => array(
                'name' =>  'Super Adminstrator',
                'slug' =>  'super_administrator',
                'description' => 'Super Administrator Role',
            ),
            'administrator' => array(
                'name' =>  'Administrator',
                'slug' =>  'administrator',
                'description' => 'Administrator Role',
            ),
            'client' => array(
                'name' =>  'Client',
                'slug' =>  'client',
                'description' => 'Client Role',
            ),
            'manager' => array(
                'name' =>  'Manager',
                'slug' =>  'manager',
                'description' => 'Manager Role',
            ),            
            'va' => array(
                'name' =>  'Virtual Assistant',
                'slug' =>  'va',
                'description' => 'Virtual Assistant Role',
            ),
            'billing' => array(
                'name' =>  'Billing',
                'slug' =>  'billing',
                'description' => 'Billing Role',
            ),


        );
        foreach( $roles_array as $key => $role ) {
            $role_db[$key] = DB::table('roles')->insertGetId( $role );
        }
        // End Insert Roles

        // Add Permissions
        $this->command->info('Seeding <question>permissions</question> table.');
        DB::table('permissions')->truncate();

        // NOTE: When adding new permissions, add them at the bottom of the array
        $url_slugs_array = array(
            'list_users' => array(
                'name' =>  'List Users',
                'slug' =>  'list_users',
                'description' => 'List all users.',
                'permissions' => array(
                    'super_administrator',
                ),
            ),
            'add_users' => array(
                'name' =>  'Add Users',
                'slug' =>  'add_users',
                'description' => 'Be able to add users.',
                'permissions' => array(
                    'super_administrator',
                ),
            ),  
            'edit_users' => array(
                'name' =>  'Edit Users',
                'slug' =>  'edit_users',
                'description' => 'Be able to edit users.',
                'permissions' => array(
                    'super_administrator',
                ),
            ),  
            'delete_users' => array(
                'name' =>  'Delete Users',
                'slug' =>  'delete_users',
                'description' => 'Be able to delete users.',
                'permissions' => array(
                    'super_administrator',
                ),
            ),  

            'list_clients' => array(
                'name' =>  'List Clients',
                'slug' =>  'list_clients',
                'description' => 'List all clients.',
                'permissions' => array(
                    'administrator',
                ),
            ),
            'add_clients' => array(
                'name' =>  'Add Clients',
                'slug' =>  'add_clients',
                'description' => 'Be able to add clients.',
                'permissions' => array(
                    'administrator',
                ),
            ),  
            'edit_clients' => array(
                'name' =>  'Edit Clients',
                'slug' =>  'edit_clients',
                'description' => 'Be able to edit clients.',
                'permissions' => array(
                    'administrator',
                ),
            ),  
            'delete_clients' => array(
                'name' =>  'Delete clients',
                'slug' =>  'delete_clients',
                'description' => 'Be able to delete clients.',
                'permissions' => array(
                    'administrator',
                ),
            ),  

            'list_va' => array(
                'name' =>  'List VA',
                'slug' =>  'list_va',
                'description' => 'List all VAs.',
                'permissions' => array(
                    'administrator',
                    'client'
                ),
            ),
            'add_va' => array(
                'name' =>  'Add VA',
                'slug' =>  'add_va',
                'description' => 'Be able to add VAs.',
                'permissions' => array(
                    'administrator',
                ),
            ),  
            'edit_va' => array(
                'name' =>  'Edit VA',
                'slug' =>  'edit_va',
                'description' => 'Be able to edit VAs.',
                'permissions' => array(
                    'administrator',
                ),
            ),  
            'delete_va' => array(
                'name' =>  'Delete VA',
                'slug' =>  'delete_va',
                'description' => 'Be able to delete VAs.',
                'permissions' => array(
                    'administrator',
                ),
            ),     
            
            'list_team' => array(
                'name' =>  'List Teams',
                'slug' =>  'list_team',
                'description' => 'List all Teams.',
                'permissions' => array(
                    'administrator',
                ),
            ),
            'add_team' => array(
                'name' =>  'Add Team',
                'slug' =>  'add_team',
                'description' => 'Be able to add Teams.',
                'permissions' => array(
                    'administrator',
                ),
            ),  
            'edit_team' => array(
                'name' =>  'Edit Team',
                'slug' =>  'edit_team',
                'description' => 'Be able to edit Teams.',
                'permissions' => array(
                    'administrator',
                ),
            ),  
            'delete_team' => array(
                'name' =>  'Delete Team',
                'slug' =>  'delete_team',
                'description' => 'Be able to delete Teams.',
                'permissions' => array(
                    'administrator',
                ),
            ),   
            'online_va' => array(
                'name' =>  'Online VA',
                'slug' =>  'online_va',
                'description' => 'Check online VA',
                'permissions' => array(
                    'administrator',
                    'client',
                ),
            ),
            'va_work_duration' => array(
                'name' =>  'Work Duration',
                'slug' =>  'va_work_duration',
                'description' => 'Check VA Duration',
                'permissions' => array(
                    'administrator',
                    'client',
                ),
            ),             

            'add_va_schedule' => array(
                'name' =>  'Add VA Schedule',
                'slug' =>  'add_va_schedule',
                'description' => 'Add VA Schedule',
                'permissions' => array(
                    'administrator',
                    'manager',
                ),
            ),

            'edit_va_schedule' => array(
                'name' =>  'Edit VA Schedule',
                'slug' =>  'edit_va_schedule',
                'description' => 'Edit VA Schedule',
                'permissions' => array(
                    'administrator',
                    'manager',
                ),
            ),

            'delete_va_schedule' => array(
                'name' =>  'Delete VA Schedule',
                'slug' =>  'delete_va_schedule',
                'description' => 'Delete VA Schedule',
                'permissions' => array(
                    'administrator',
                    'manager',
                ),
            ),

            'list_va_schedule' => array(
                'name' =>  'List VA Schedule',
                'slug' =>  'list_va_schedule',
                'description' => 'List VA Schedule',
                'permissions' => array(
                    'administrator',
                    'manager',
                    'client',
                    'va'
                ),
            ),

            'list_screenshot' => array(
                'name' =>  'List Screenshot',
                'slug' =>  'list_screenshot',
                'description' => 'List Screenshots.',
                'permissions' => array(
                    'administrator',
                    'manager',
                    'client',
                    'va'
                ),
            ),
            'add_screenshot' => array(
                'name' =>  'Add Screenshot',
                'slug' =>  'add_screenshot',
                'description' => 'Be able to add Screenshots.',
                'permissions' => array(
                    'administrator',
                    'va'
                ),
            ),  
            'edit_screenshot' => array(
                'name' =>  'Edit Screenshot',
                'slug' =>  'edit_screenshot',
                'description' => 'Be able to edit Screenshots.',
                'permissions' => array(
                    'administrator',
                ),
            ),  
            'delete_screenshot' => array(
                'name' =>  'Delete Screenshot',
                'slug' =>  'delete_screenshot',
                'description' => 'Be able to delete Screenshots.',
                'permissions' => array(
                    'administrator',
                ),
            ),

            'list_settings' => array(
                'name' =>  'List Settings',
                'slug' =>  'list_settings',
                'description' => 'List Settings.',
                'permissions' => array('administrator'),
            ),
            'add_settings' => array(
                'name' =>  'Add Settings',
                'slug' =>  'add_settings',
                'description' => 'Be able to add Settings.',
                'permissions' => array(),
            ),  
            'edit_settings' => array(
                'name' =>  'Edit Settings',
                'slug' =>  'edit_settings',
                'description' => 'Be able to edit Settings.',
                'permissions' => array('administrator'),
            ),  
            'delete_settings' => array(
                'name' =>  'Delete Settings',
                'slug' =>  'delete_settings',
                'description' => 'Be able to delete Settings.',
                'permissions' => array(),
            ),
        );
        
        foreach( $url_slugs_array as $key => $url_slug ) {
            $permission_roles = $url_slug['permissions'];
            unset( $url_slug['permissions'] );

            $permission_id = DB::table('permissions')->insertGetId( $url_slug );

            foreach( $permission_roles as $role ) {
                $role = Role::findOrFail( $role_db[$role] );
                $role->permissions()->attach( $permission_id );
            }
        }

        $this->command->info('Done seeding permissions table.');

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        $this->command->info('Done seeding roles table.');
    }
}