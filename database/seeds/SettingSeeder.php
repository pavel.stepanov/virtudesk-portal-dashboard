<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Setting;


class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $settings = [
            [
                'name' => 'Screenshot Interval',
                'slug' => Setting::slugify('Screenshot Interval'),
                'value' => 300 // 300 secs
            ],
            [
                'name' => 'Download Link App (Mac)',
                'slug' => Setting::slugify('Download Link App Mac'),
                'value' => "https://drive.google.com/open?id=1N2K0YhuMx6_s6_ARM5Iu01y9yHQ1VOkp"
            ],
            [
                'name' => 'Download Link App (Win)',
                'slug' => Setting::slugify('Download Link App Win'),
                'value' => "https://drive.google.com/open?id=1V9PykoTWojgJS2H4UvjfMsZo7jPlgHza"
            ],
            [
                'name' => 'Idle Interval',
                'slug' => Setting::slugify('Idle Interval'),
                'value' => "300" // 300 seconds
            ],
            [
                'name' => 'App Version',
                'slug' => Setting::slugify('App Version'),
                'value' => "0.1.1"
            ]
        ];

        $this->command->info('Truncating <question>settings</question> table.');
        Setting::truncate();

        $this->command->info('Seeding <question>settings</question> table.');
        foreach ($settings as $setting) {
            Setting::create($setting);
        }
        $this->command->info('Done seeding <question>settings</question> table.');
    }
}