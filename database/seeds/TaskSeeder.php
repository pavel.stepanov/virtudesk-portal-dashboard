<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Task;


class TaskSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $tasks_array = [
            [
                'name' => 'Prospecting',
                'description' => 'Prospecting'
            ],
            [
                'name' => 'Marketing',
                'description' => 'Marketing'
            ],
            [
                'name' => 'Coach',
                'description' => 'Coach'
            ],
            [
                'name' => 'Client Services',
                'description' => 'Client Services'
            ],
            [
                'name' => 'Training',
                'description' => 'Training'
            ],
            [
                'name' => 'Recruitment',
                'description' => 'Recruitment'
            ],
            [
                'name' => 'Administrative',
                'description' => 'Administrative'
            ]


        ];

        $this->command->info('Truncating <question>tasks</question> table.');
        Task::truncate();

        $this->command->info('Seeding <question>tasks</question> table.');
        foreach ($tasks_array as $task) {
            Task::create([
                'name' => $task['name'],
                'description' => $task['description']
            ]);
        }
        $this->command->info('Done seeding <question>tasks</question> table.');
    }
}