<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Rate;

class FakeUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $this->command->info('Started creating fake Client and VAs.');

        // CREATE CLIENTS
        for( $i = 0 ; $i < 50 ; $i++ ) {
            $faker = Faker\Factory::create();
            $name = explode(' ', $faker->name);
            $client =  array(
                'first_name' => $name[0],
                'last_name' => $name[1],
                'mobile_number' => $faker->phoneNumber,
                'is_verified' => 1,
                'is_active' => 1,
                'email' => $faker->unique()->email,
                'password' => bcrypt('password'),
                'created_at' => date("Y-m-d H:i:s"),
                'timezone' => 'PST'
            );

            $user_id = DB::table('users')->insertGetId( $client );
            $user = User::find( $user_id );
            $user->addRole( 'client' );
        }

        // CREATE VA
        for( $i = 0 ; $i < 50 ; $i++ ) {
            $faker = Faker\Factory::create();
            $name = explode(' ', $faker->name);
            $client =  array(
                'first_name' => $name[0],
                'last_name' => $name[1],
                'mobile_number' => $faker->phoneNumber,
                'is_verified' => 1,
                'is_active' => 1,
                'email' => $faker->unique()->email,
                'password' => bcrypt('password'),
                'created_at' => date("Y-m-d H:i:s"),
                'timezone' => 'MNL'
            );

            $user_id = DB::table('users')->insertGetId( $client );
            $user = User::find( $user_id );
            $user->addRole( 'va' );

           $rate = Rate::create([
                'user_id' => $user->id,
                'rate_per_hour' => '100',
            ]);

            $user->rates->addLog(array(
                'user_id' => $user->id,
                'old_rate_per_hour' => 0,
                'new_rate_per_hour' => '100',
                'updated_by' => 1,
            ));
        }

        $this->command->info('Done creating fake Client and VAs.');
    }
}