<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        \Schema::defaultStringLength(191);

        \Validator::extend('break_check', function($attribute, $value, $parameters, $validator)
        {
            $inputs = $validator->getData();
            $max_breaktime = $inputs['max_breaktime'];
            $hours_needed = $inputs['hours_needed'];
            if (($max_breaktime=='') || ($max_breaktime=='00:00')) {
                return true;
            }
            $hours = date("H", strtotime($max_breaktime));
            if ($hours >=  $hours_needed) {
                return false;
            } else {
                return true;
            }

        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
