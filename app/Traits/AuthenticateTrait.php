<?php
namespace App\Traits;

use App\Models\Role;
use Illuminate\Support\Collection;

trait AuthenticateTrait
{

    /**
     * Relation belongs-to roles.
     *
     * @return mixed
     */

    public function roles()
    {
        return $this->belongsToMany("App\Models\Role");
    }

    /**
     * Add role to user.
     *
     * @param $role_slug
     */

    public function addRole($role_slug)
    {

        $role = Role::where("slug","=",$role_slug)->firstOrFail();
        if (isset($role))
            $this->roles()->attach($role->id);
    }

    /**
     * Remove role from user.
     *
     * @param $role_slug
     */
    public function removeRole($role_slug)
    {

        $role = Role::where("slug","=",$role_slug)->firstOrFail();
        if (isset($role))
            $this->roles()->detach($role->id);
    }


    /**
     * Remove all roles.
     */
    public function removeAllRoles()
    {
        foreach ($this->roles as $role) {
            $this->removeRole($role->slug);
        }
    }

    /**
     * Determine whether the user has role that given by name parameter.
     *
     * @param $name
     *
     * @return bool
     */
    public function is($name)
    {
        foreach ($this->roles as $role) {
            if ($role->name == $name || $role->slug == $name || $role->id == $name) {
                return true;
            }
        }
        return false;
    }


    /**
     * Get 'permissions' attribute.
     *
     * @return Collection
     */
    public function getPermissionsAttribute()
    {
       $permissions = new Collection();
        foreach ($this->roles as $role) {
            foreach ($role->permissions as $permission) {
                $permissions->push($permission);
            }
        }
        return $permissions;
    }


    /**
     * Determine whether the user can do specific permission that given by name parameter.
     *
     * @param $name
     *
     * @return bool
     */
    public function allowed($name)
    {

        if ($this->is('super_administrator')) return true;

        foreach ($this->roles as $role) {
            foreach ($role->permissions as $permission) {
                if ($permission->name == $name || $permission->slug == $name || $permission->id == $name) {
                    return true;
                }
            }
        }
        return false;
    }

}
