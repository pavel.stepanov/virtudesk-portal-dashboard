<?php namespace App\Events;

use App\Events\Event;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Queue\SerializesModels;
use App\Models\User;

class Broadcast extends Event implements ShouldBroadcast
{
    use SerializesModels;

    public $channel, $data;

    public function __construct($channel, $method = null, $userId = null, $params = [])
    {
        $socketId = null;
        if(!is_null($userId)) {
            $user = User::find($userId);
            $socketId = $user->socket_id;
        }

        $method = (is_null($method)) ? '' : '-' . $method;
        $userId = (is_null($userId)) ? '' : '-' . $userId;
        $this->channel = $channel . $method . $userId;
        $this->data = $params;
        $this->data['socket_id'] = $socketId;
    }

    public function broadcastOn()
    {
        // print_r('broadcasting > ' . $this->channel); // FOR DEBUGING
        return ['broadcast'];
    }
}