<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Screenshot;

class ScreenshotCleanup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'screenshot:cleanup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cleanup today\'s screenshot';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        exec('sudo chmod -R 777 /var/www/html/virtudesk-portal-dashboard/public/screenshots/');
        exec('aws s3 mv /var/www/html/virtudesk-portal-dashboard/public/screenshots/  s3://virtudesk-crm/screenshots --recursive');
        
        $today = date('Y-m-d h:i:s');
        $screenshots = Screenshot::whereDate('created_at', '<=', $today)->update(['storage' => 's3']);
        
        //aws s3 mv /var/www/html/virtudesk-portal-dashboard/public/screenshots/825/thumb_16107729941828959900.jpg  s3://virtudesk-crm/screenshots/90/thumb_16107729941828959900.jpg --debug 177/1617026517332592880.jpg 1610/16170260471103103071.jpg 1599/thumb_16173993211147172403.jpg
        
        //upload: public/screenshots/1886/1610809655420976934.jpg to s3://virtudesk-crm/screenshots/1886/1610809655420976934.jpg
        
        //$today = date('Y-m-d');
        //echo 'today is '.$today.'<br>';
        
        //$target_directory = '/var/www/html/virtudesk-portal-dashboard/public/screenshots';
        //$sub_directories = glob($target_directory . '/*' , GLOB_ONLYDIR);
        
        
        
        /**
        foreach($sub_directories as $sub_target){
            $files = glob($sub_target.'/*.jpg');
            foreach($files as $file){
                $date_created = date("Y-m-d", filemtime($file));
                $file_exp = explode('/screenshots/', $file);
                $user_dir = $file_exp[0];
                
                //echo $target_directory.'/'.$file_exp[1].'\r\n\r\n';
            
                if($date_created != $today){
                    //echo $file.' created at '.$date_created.'<br>';
                   echo $file_exp[1].'<br>\r\n\r\n';
                   exec('aws s3 mv '.$target_directory.'/'.$file_exp[1].'  s3://virtudesk-crm/screenshots/'.$file_exp[1]); 
               }
                
            }
        }
        **/
        
        //end function
    }
    
    //end class
}
