<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Screenshot;

class ScreenshotPathChanger extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'screenshot:modifiy';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Modify yesterday\'s screenshot path from local to s3';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $yesterday = date('Y-m-d ', strtotime('-1 days'));
      $today = date('Y-m-d h:i:s');
      $screenshots = Screenshot::whereDate('created_at', '<=', $today)->update(['storage' => 's3']);
      //exec('sudo find /var/www/html/virtudesk-portal-dashboard/public/screenshots/ -name "*.jpg" -type f -mtime +5 -exec rm -f {} \;');
      //sudo find /var/www/html/virtudesk-portal-dashboard/public/screenshots/ -name "*.jpg" -type f -mtime +5 -exec rm -f {} \;'
      
      //exec('aws s3 mv /var/www/html/virtudesk-portal-dashboard/public/screenshots/90/thumb_16107729941828959900.jpg  s3://virtudesk-crm/screenshots/90/thumb_16107729941828959900.jpg');
    }
}