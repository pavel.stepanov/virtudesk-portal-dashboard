<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class PermissionRoles {
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $permission)
    {

       if ( !Auth::user()->allowed($permission) ) {

            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect("/dashboard");
            }

        }

        return $next($request);
    }
}
