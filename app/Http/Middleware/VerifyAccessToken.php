<?php

namespace App\Http\Middleware;

use App\Models\User;
use Auth;
use Closure;

class VerifyAccessToken {
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handle($request, Closure $next)
    {
        if(!is_null($request->access_token)) {
            $user = User::where('access_token', $request->access_token)->first();
            if($user) {
                Auth::login($user);
                return $next($request);
            }
        }
        return response()->json([
            'status' => 'error',
            'case' => 'auth',
            'error_msg' => 'Authentication Failed'
        ]);
    }
}
