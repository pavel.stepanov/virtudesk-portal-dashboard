<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Attendance;
use App\Models\User;
use App\Models\Screenshot;
use App\Models\Schedule;
use App\Models\Task;
use App\Models\Team;
use App\Models\TeamUser;
use App\Models\UserTask;
use App\Models\ClientTask;
use App\Models\ClientBilling;
use App\Models\RequestConcerns;
use App\Models\Setting;
use App\Models\Timesheet;
use App\Models\Project;
//use Symfony\Component\Process\Process;
//use Symfony\Component\Process\Exception\ProcessFailedException;
use DateTime;
use DateTimeZone;
use App\Http\Controllers\API\UserAttendanceController;
use App\Models\ManagerClient;

class AdminController extends Controller
{
    public function index()
    {
        $user = \Auth::user();

        if ($user->is('billing')) {

            $va_count = User::HasRole('va')
            ->where('is_active', 1)
            ->count();

            $client_count = User::HasRole('client')
            ->where('is_active', 1)
            ->count();

            $total_invoice_unpaid = ClientBilling::where('status','unpaid')
            ->count();

            $invoice_latest = ClientBilling::where('status','unpaid')
            ->orderBy('created_at', 'desc')->get();

            return view('admin.billing-dashboard', compact(
                'va_count',
                'client_count',
                'total_invoice_unpaid',
                'invoice_latest'
            ));
        }

        if ($user->is('manager')) {
            //get all VAs under this manager
            $team = Team::where("lead_user_id", $user->id)->first();
            $vas = null;
            if ($team->id) {
                $vas = TeamUser::where("team_id", $team->id)->pluck('user_id');
            }
            
            $billing_count = 0;
            $target_date = date("Y-m-d", strtotime(now()));
            $screenshot_count = 0;

            $va_count = User::whereHas('schedules', function ($query) use ($vas) {
               $query->whereIn('user_id', $vas);
            })->where('is_active', 1)->count();

            //TODO: This computes hours by attendance. Needs to be changed later after timesheets are created.
            $total_time = 0;
            
            $attendance_latest = null;
            $screenshot_latest = null;
            $vatask_latest = null;

            return view('admin.manager-dashboard', compact(
                'billing_count', 
                'va_count', 
                'screenshot_count', 
                'total_time' , 
                'attendance_latest', 
                'screenshot_latest',
                'vatask_latest',
                'user'
            ));
        }

        if ($user->is('va')) {
            $target_date = date("Y-m-d", strtotime(now()));
            $day_of_week = date("l", strtotime(now()));
            $schedules = Schedule::where('user_day_of_week', $day_of_week)
            ->select(['id','day_of_week', 'start_time','end_time','shift', 'client_id', 'timezone', 'user_day_of_week', 'user_start_time', 'user_end_time'])->where('user_id', $user->id)->get();

            $clients_today = Schedule::where('user_day_of_week', $day_of_week)
            ->where('user_id', $user->id)
            ->pluck('client_id')->all();

            $clients = User::HasRole('client')->whereIn('id', $clients_today)
            ->select('id', 'first_name', 'last_name', 'email', 'timezone')
            ->get();

            $current_task = $user->getCurrentTask();
            $seconds_duration_task = 0;
            if (!empty($current_task)) {
                $seconds_duration_task = strtotime(date("Y-m-d H:i:s")) - strtotime($current_task->date_start . " " . $current_task->time_start);
            }

            $is_clocked_in = $user->isClockedIn();
            $clocked_in_timezone = "";
            $seconds_duration = 0;
            $attendance = null;
            $client_tasks = null;
            if ($is_clocked_in) {
                $attendance = Attendance::where('user_id', $user->id)
                ->where('status','in')->select('date_in', 'time_in', 'client_id')->first();
                $client_tasks = ClientTask::where('client_id', $attendance->client_id)
                ->where("va_id", $user->id)->get();
                $va_clock_in_timezone = $user->convertToUserTimezone($attendance->date_in . " " . $attendance->time_in);
                $clocked_in_timezone = $va_clock_in_timezone->format("Y-m-d h:i:s A");
            }

            $va_timezone = new DateTime(null, new DateTimeZone($user->showTimezone()));
            $va_timestamp = $va_timezone->getTimestamp()+$va_timezone->getOffset();

            $mac_download = Setting::getValueBySlug('download-link-app-mac');
            $win_download = Setting::getValueBySlug('download-link-app-win');

            $task_latest = UserTask::where('user_id', $user->id)
            ->where('time_end','<>', null)
            ->orderBy('created_at')->take(10)->get();

            $tasks = Task::all();
            return view('admin.va-dashboard', compact(
                'schedules', 
                'target_date', 
                'user', 'clients', 
                'tasks', 
                'is_clocked_in',
                'va_timezone',
                'va_timestamp',
                'client_tasks',
                'seconds_duration',
                'current_task',
                'seconds_duration_task',
                'attendance',
                'clocked_in_timezone',
                'mac_download',
                'win_download',
                'task_latest'
            ));

        }

        if ($user->is('client')) {

            $projects = Project::where('user_id', $user->id)->get();

            //TODO determine billing count
            $billing_count = 0;
            $target_date = date("Y-m-d", strtotime(now()));
            $work_week = date("W", strtotime(now()));
            $client_id = $user->id;
            $va_count = User::HasRole('va')
            ->whereHas('schedules', function ($query) use ($client_id) {
                $query->where('client_id', $client_id);
            })->count();

            $screenshot_count = Screenshot::where('client_id', $client_id)->count();

            //TODO: This computes hours by attendance. Needs to be changed later after timesheets are created.
            $total_time = 0;
            /*$total_att = Attendance::select('total_time', 'client_id')->where("client_id", $client_id)->get();
            if (!empty($total_att)) {
                $all_seconds = 0;
                foreach ($total_att as $att) {
                    list($hour, $minute, $second) = explode(':', $att->total_time);
                    $all_seconds += $hour * 3600;
                    $all_seconds += $minute * 60; $all_seconds += $second;
                }
                $total_minutes = floor($all_seconds/60); $seconds = $all_seconds % 60;  $hours = floor($total_minutes / 60); $minutes = $total_minutes % 60;
                $total_time = sprintf('%02d:%02d:%02d', $hours, $minutes,$seconds);
            }*/

            $timesheet = Timesheet::where('status', 'approved')
            ->where('client_id', $client_id)
            ->where('target_date', $target_date)
            ->get();
            if (count($timesheet)) {
                foreach ($timesheet as $ts) {
                    $total_time += $ts->billable_hours;
                }
            }
            
            $attendance_latest = Attendance::select(['schedule_date', 'user_id', 'date_in', 'time_in','status', 'created_at'])
            ->orderBy('updated_at', 'DESC')
            ->where("client_id", $client_id)->take(10)->get();

            $screenshot_latest = Screenshot::select(['id', 'client_id', 'user_id', 'path', 'filename', 'created_at'])
            ->orderBy('created_at', 'DESC')
            ->where("client_id", $client_id)->take(5)->get();

            $vatask_latest = UserTask::where('client_id', $user->id)
            ->where('time_end','<>', null)
            ->orderBy('created_at','DESC')->take(10)->get();

            return va_view('admin.client-dashboard', compact(
                'billing_count', 
                'va_count', 
                'screenshot_count', 
                'total_time' , 
                'attendance_latest', 
                'screenshot_latest',
                'vatask_latest',
                'user',
                'projects'
            ));
        }

        if ($user->is('super_administrator') || $user->is('administrator')) {

            /*$process_node = new Process("ps aux | grep 'node'");
            $process_node->run();
            
            // executes after the command finishes
            if (!$process_node->isSuccessful()) {
                throw new ProcessFailedException($process);
            }
            
            $process_node_output = $process_node->getOutput();
            $process_node = false;
            if (strpos($process_node_output, 'node socket.js') !== false) {
                $process_node = true;
            }*/
            $target_date = date("Y-m-d", strtotime(now()));
            $day_of_week = date("l", strtotime($target_date));
            $users_attended = Attendance::where('date_in', $target_date)->pluck('user_id')->all();

            $vas_grace10 = User::HasRole('va')
            ->whereHas('schedules', function ($query) use ($day_of_week) {
                $query->where('user_day_of_week', $day_of_week)
                ->whereRaw( "TIME('". date("H:i:s", strtotime(now())) . "') > AddTime(user_start_time,'00:10:00')")
                ->whereRaw( "TIME('". date("H:i:s", strtotime(now())) . "') < AddTime(user_start_time,'01:00:00')");
            })
            ->whereNotIn('id', $users_attended)
            ->select('id', 'first_name', 'last_name', 'email')->get();

            $attendance_latest = Attendance::select(['schedule_date' , 'start_time', 'client_id', 'user_id', 'date_in', 'time_in','status', 'created_at'])
            ->orderBy('created_at', 'DESC')->take(10)->get();

            $request_concern_count = RequestConcerns::where('status','pending')->count();

            $timesheets_count = Timesheet::where('status', "pending")
            //->distinct('user_id')
            //->groupBy('user_id')
            ->select('id')->count();

            $va_count = User::HasRole('va')
            ->where('is_active', 1)
            ->count();

            $client_count = User::HasRole('client')
            ->where('is_active', 1)
            ->count();

            return view('admin.dashboard', compact(
                'va_count',
                'client_count',
                'request_concern_count',
                'timesheets_count',
                'attendance_latest',
                'vas_grace10',
                'target_date',
                'day_of_week'
            ));
            //return view('admin.dashboard', compact('va_count', 'client_count', 'billing_count', 'timesheets_count', 'process_node'));
        }
    }

    public function forceLogout($att_id) 
    {
        $attendance = Attendance::find($att_id);
        UserAttendanceController::doTimeout($attendance);
        return "done!";
    }
    
    public function performForceS3Upload($folder_id){
        
        
        $today = date('Y-m-d');
        //echo 'today is '.$today.'<br>';
        //exit;
        
        $target_directory = '/var/www/html/virtudesk-portal-dashboard/public/screenshots';
        $sub_directories = glob($target_directory . '/*' , GLOB_ONLYDIR);
        foreach($sub_directories as $sub_target){
            $files = glob($sub_target.'/*.jpg');
            foreach($files as $file){
                $date_created = date("Y-m-d", filemtime($file));
                $file_exp = explode('/screenshots/', $file);
                $user_dir = $file_exp[1];
            
                if($date_created != $today){
                    //echo $file.' created at '.$date_created.'<br>';
                    echo $user_dir.'<br>';
                    //exec('aws s3 mv '.$file.'  s3://virtudesk-crm/screenshots/'.$user_dir);
                    
                    exec('aws s3 mv '.$target_directory.'/'.$file_exp[1].'  s3://virtudesk-crm/screenshots/'.$file_exp[1]);
                }
            }
        }
        
    }
    
    public function getManagerData(){
        
        $user = \Auth::user();
        $dashboard_data = [];
        $target_date = date("Y-m-d", strtotime(now()));
        $yesterday = date('Y-m-d', strtotime("-1 days"));
        
         $team = Team::where("lead_user_id", $user->id)->first();
         $vas = null;
         
         if ($team->id) {
            $vasa = TeamUser::where("team_id", $team->id)->pluck('user_id');
            $vas = User::whereIn('id', $vasa)->where('is_active', 1)->pluck('id');
        }
        
        //attendace
            $attendance_latest = Attendance::select(['schedule_date', 'user_id', 'date_in', 'time_in','status', 'created_at'])
                ->whereIn('user_id', $vas)
                ->where('date_in', '>=', $yesterday)
                ->orderBy('status', 'asc')
                ->orderBy('updated_at', 'desc')
                ->with('user')
                ->get();
           
           $statuses = array('in', 'break', 'idle');
            
            $attendance_latest_count = Attendance::whereIn('user_id', $vas)
                ->where('date_in', '>=', $yesterday)
                ->whereIn('status', $statuses)
                ->pluck('id');
            
            $va_attendance_latest = Attendance::select('user_id')
                ->where('date_in', '>=', $yesterday)
                ->orderBy('updated_at', 'desc')
                ->whereIn('user_id', $vas)->pluck('user_id');
                
            $screenshot_latest = Screenshot::select(['user_id', 'path', 'filename', 'storage', 'created_at'])
                ->orderBy('created_at', 'DESC')
                ->whereIn('user_id', $va_attendance_latest)
                ->take(10)
                ->with('user')
                ->get();

            $vatask_latest = UserTask::whereNotNull('time_end')->whereIn('user_id', $vas)
                ->orderBy('updated_at','desc')->with('user')->take(10)->get();
  
           
           $screenshot_count = Screenshot::whereIn('user_id', $vas)
                ->whereDate('created_at', $target_date)
               ->count();
            
            $dashboard_data = [
                'attendance_latest' => $attendance_latest,
                'screenshot_latest' => $screenshot_latest,
                'vatask_latest' => $vatask_latest,
                'attendance_count' => count($attendance_latest_count),
                'screenshot_count' => $screenshot_count
            ];
            
            return $dashboard_data;
   }
    
//end class
}