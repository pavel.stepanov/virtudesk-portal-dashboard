<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Illuminate\Support\Facades\Input;
use Auth;
use App\Models\User;
use App\Models\ActivityLog;
use App\Models\PasswordReset;
use DateTime;

class AuthenticateController extends Controller
{

	public function getReset() 
	{
		return va_view('auth.reset');
	}

	public function postReset(Request $request)
	{
    	$request->validate([
			'email' => 'required|email'
		]);

		$user = User::where('email',$request->email)->first();

		if (empty($user)) {
			$status = 'is-warning';
			$result_message = "Account not found.";
			return view('auth.reset', compact('result_message','status'));
		}

		//delete previous password reset requests
		PasswordReset::where('email',$request->email)->delete();

		$token = sha1($request->email . now());
		PasswordReset::create([
			'email' => $request->email,
			'token' => $token,
			'created_at' => now(),
		]);

		//Send verification email
		$content = "emails.password-reset";

		$data = [
			//'to' => 'a7e9ba71de-274164@inbox.mailtrap.io',
			'to' => [$request->email],
			'from' => [config('vatimetracker.noreply_email')],
			'subject' => "Password Reset Request",
			'token' => $token,
			'url' => url('/auth/reset-pass'),
		];

		try {

			\Mail::send( $content, $data, function ( $message ) use ( $data ) {
				if ( isset($data['from']) ) {
					$message->from( $data['from'] );
				}
				if ( isset($data['to']) ) {
					$message->to( $data['to'] );
				}
				if ( isset($data['subject']) ) {
					$message->subject( $data['subject'] );
				}
			} );

		} catch(\Swift_TransportException $e) {
				//echo 'Message: ' .$e->getMessage();
		}

		$status = 'is-success';
		$result_message = "Please check your email for instructions to reset your password.";
		return va_view('auth.reset', compact('result_message','status'));
	}

	public function getResetPass(Request $request) 
	{
		if (!isset($request->token)) {
			return redirect('/auth/login');
		}
		$pr = PasswordReset::where('token',$request->token)->first();

		if (empty($pr)) {
			return redirect('/auth/login');
		}

		return va_view('auth.reset-pass', compact('pr'));
	}

	function postResetPass(Request $request)
	{
        $request->validate([
			'token' => 'required',
            'password' => 'required',
			'confirm_password' => 'required|same:password'
		]);
		
		$pr = PasswordReset::where('token',$request->token)->first();
		if (empty($pr)) {
			return redirect('/auth/login');
		}
		$user = User::where('email', $pr->email)->first();
		if (empty($user)){
			return redirect('/auth/login');
		}

		$user->password = bcrypt($request->password);
		$user->save();

		//delete request
		PasswordReset::where('token',$request->token)->delete();

		$status = 'is-success';
		$result_message = "Your password has been changed. Login using your new password.";
		return view('auth.reset-pass', compact('result_message', 'status'));
	}


	public function getVerify()
	{
		$verify_token = Input::get('verify_token');

		$user = User::where('verify_token', "=", $verify_token)
					  ->where('is_verified', '=', '0')
					  ->first();

		$result_message = '';

		if (!empty($user)) {
			$user->verify_token = null;
			$user->is_verified = 1;
			$user->save();
			$status = 'is-success';
			$result_message = "You have been successfully verified! You can now log in.";
		} elseif( !empty($verify_token)) {
			$status = 'is-warning';
			$result_message = "Account not found or already verified.";
		}

		return view('auth.verify', compact('result_message','status'));
	}

    public function getRegister() 
    {

        if (Auth::check()) {
			return redirect('/');
        }

        $timezone = config('vatimetracker.timezone');
        
        return view('auth.register', compact('timezone'));

    }

    public function postRegister(Request $request)
    {
    	$request->validate([
			'email' => 'required|email|unique:users',
			'first_name' => 'required|max:50|min:5',
			'last_name' => 'required|max:50|min:5',
			'password' => 'required',
			'password_confirmation' => 'required|same:password',
			'read_terms' => 'required'
		]);

		$verify_token = md5($request->email . $request->first_name . $request->last_name . now());
		$user = User::create([
			'email' => $request->email,
			'first_name' => $request->first_name,
			'last_name' => $request->last_name,
			'address1' => $request->address_1,
			'address2' => $request->address_2,
			'city' => $request->city,
			'mobile_number' => $request->phone_number,
			'timezone' => $request->timezone,
			'is_verified' => 0,
			'password' => bcrypt($request->password),
			'verify_token' => $verify_token,
			'read_terms' => 1
		]);

		$user->addRole("client");

		//Send verification email
		$content = "emails.verify-reg";

		$data = [
			//'to' => 'a7e9ba71de-274164@inbox.mailtrap.io',
			'to' => [$request->email],
			'from' => [config('vatimetracker.noreply_email')],
			'subject' => "Verify your registration to VIRTUDESK!",
			'verify_token' => $verify_token,
			'verify_url' => url('/auth/verify'),
		];

		try {

			\Mail::send( $content, $data, function ( $message ) use ( $data ) {
				if ( isset($data['from']) ) {
					$message->from( $data['from'] );
				}
				if ( isset($data['to']) ) {
					$message->to( $data['to'] );
				}
				if ( isset($data['subject']) ) {
					$message->subject( $data['subject'] );
				}
			} );

		} catch(\Swift_TransportException $e) {
  			//echo 'Message: ' .$e->getMessage();
		}

		return redirect('/auth/verify');	
    }

    public function getLogin()
	{

		$return_url = trim(Input::get('return_url', ''));
		if ($return_url == '') $return_url = "/";

		//if user authenticated with remember cookie
		if (Auth::viaRemember()) {
		    return redirect($return_url);
		}		

		if (Auth::check()) {
			return redirect($return_url);
		}		

		return va_view('auth.login')->with(['return_url'=> $return_url]);
	}	

	public function postLogin(Request $request)
	{
		$request->validate([
			'email' => 'required',
			'password' => 'required'
		]);

		$email = Input::get('email');
		$password = Input::get('password');
		$remember = Input::get('remember');

		$return_url = Input::get('return_url');

        if (Auth::attempt(['email' => $email, 'password' => $password], $remember)) {
			// Authentication passed...
			// Check if verified
			if (Auth::user()->is_verified == 0) {

				$error_msg = "Account is not verified";
				Auth::logout();
				return redirect()->intended('/auth/login')->with('error_msg', $error_msg);	

			}

			if (Auth::user()->is_active == 0) {
				$error_msg = "Account is not active";
				Auth::logout();
				return redirect()->intended('/auth/login')->with('error_msg', $error_msg);	
			}

			$date = new DateTime();
			$auth_user = Auth::user();
			$auth_user->last_login = $date->getTimestamp();
			$auth_user->is_online = 1;
			$auth_user->save();
			
			ActivityLog::addLog("User [{$auth_user->first_name} {$auth_user->last_name}] logged in.");

			if ($return_url=='') {
				return redirect()->intended('/');
			} else {
				return redirect()->intended($return_url);
			}

            
        } else {
        	$error_msg = "Invalid email or password";

        	return redirect()->intended('/auth/login')->with('error_msg', $error_msg);
        }		
	}

	public function getLogout()
	{
		$auth_user = Auth::user();
		if (!empty($auth_user)) {
			$auth_user->is_online = 0;
			$auth_user->save();
			ActivityLog::addLog("User [{$auth_user->first_name} {$auth_user->last_name}] logged out.");
		}
		Auth::logout();
		return redirect()->intended('/');
	}

	public function getToken()
	{
		return response()->json([
			'token' => csrf_token()
		]);
	}


}
