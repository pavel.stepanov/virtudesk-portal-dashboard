<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use DataTables;
use App\Models\User;
use App\Models\Rate;
use App\Models\Schedule;
use App\Models\Attendance;
use App\Models\Team;
use App\Models\TeamUser;
use Illuminate\Validation\Rule;
use App\Models\ManagerClient;
use Auth;
use DateTime;
use DateTimeZone;
use Carbon\Carbon;

class VirtualAssistantController extends Controller
{
    public function index()
    {
        $auth_user = \Auth::user();
        if ($auth_user->is('client')) {
            return va_view('admin.va.client-index');
        }
        return view('admin.va.index');
    }

    public function create()
    {
        $timezone = config('vatimetracker.timezone');
        return view('admin.va.create_edit', compact('timezone'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'email' => 'required|email|unique:users',
            'first_name' => 'required|max:100',
            'last_name' => 'required|max:100',
            'password' => 'required',
			'confirm_password' => 'required|same:password',
            'rate_per_hour' => 'numeric',
            'alternate_contact_number' => 'max:100',
            'mobile_number' => 'max:32',
            'address1' => 'max:255',
            'address2' => 'max:255',
            'city' => 'max:50',
            'screenshot_interval' => 'required|integer',
            'idle_interval' => 'required|integer',
        ]);

        $user = User::create([
            'email' => $request->email,
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'alternate_contact_number' => $request->alternate_contact_number,
            'mobile_number' => $request->mobile_number,
            'address1' => $request->address1,
            'address2' => $request->address2,
            'city' => $request->city,
            'timezone' => $request->timezone,
            'is_verified' => 1,
            'is_active' => 1,
            'screenshot_interval' => $request->screenshot_interval,
            'idle_interval' => $request->idle_interval,
            'password' => bcrypt($request->password),
        ]);

        $user->addRole("va");

        $rate = Rate::create([
            'user_id' => $user->id,
            'client_id' => 0,
            'rate_per_hour' => $request->rate_per_hour,
        ]);

        $user->rates->addLog(array(
            'user_id' => $user->id,
            'client_id' => 0,
            'old_rate_per_hour' => 0,
            'new_rate_per_hour' => $request->rate_per_hour,
            'updated_by' => \Auth::user()->id,
        ));

        return redirect('/dashboard/va/edit/'. $user->id)->with('notification_message', 'VA has been created.');
    } 

    public function edit($id)
    {
        $q = User::find($id);
        $timezone = config('vatimetracker.timezone');
        if (empty($q)) {
            return redirect('/dashboard');            
        }
        return view('admin.va.create_edit', compact('q','timezone'));        
    }

    public function update(Request $request) 
    {
        $q = User::find($request->id);

        $request->validate([
            'email' => [
                'required',
                Rule::unique('users')->ignore($q->id),
            ],
            'first_name' => 'required|max:100',
            'last_name' => 'required|max:100',
			'confirm_password' => 'same:password',
            'rate_per_hour' => 'numeric',
            'alternate_contact_number' => 'max:100',
            'mobile_number' => 'max:32',
            'address1' => 'max:255',
            'address2' => 'max:255',
            'city' => 'max:50',
            'screenshot_interval' => 'required|integer',
            'idle_interval' => 'required|integer',
        ]);

        if (!empty($q)) {
            $q->first_name = $request->first_name;
            $q->last_name = $request->last_name;
            $q->email = $request->email;
            $q->alternate_contact_number = $request->alternate_contact_number;
            $q->mobile_number = $request->mobile_number;
            $q->address1 = $request->address1;
            $q->address2 = $request->address2;
            $q->city = $request->city;
            $q->timezone = $request->timezone;
            $q->screenshot_interval = $request->screenshot_interval;
            $q->idle_interval = $request->idle_interval;
            $q->save();

            if($q->rates->rate_per_hour !== $request->rate_per_hour) {

                $q->rates->addLog(array(
                    'user_id' => $q->id,
                    'client_id' => 0,
                    'old_rate_per_hour' => $q->rates->rate_per_hour,
                    'new_rate_per_hour' => $request->rate_per_hour,
                    'updated_by' => \Auth::user()->id,
                ));

                $q->rates->rate_per_hour = $request->rate_per_hour;
                $q->rates->save();
            }
        }

        return redirect()->intended('/dashboard/va/edit/'. $q->id)->with('notification_message', 'Virtual Assitant has been updated.'); 

    }  

    public function show($id)
    {
        $q = User::find($id);
        $html = view('admin.client.show', compact('q'))->render();
        $response['html'] = $html;
        return json_encode($response);
    }

    public function delete($id)
    {
        $q = User::find($id);
        if (!empty($q)) {
            $q->delete();

            DB::table('attendance')->where('user_id',$id)->delete();
            DB::table('client_tasks')->where('va_id',$id)->delete();
            DB::table('client_tasks')->where('client_id',$id)->delete();
            DB::table('dispute')->where('user_id',$id)->delete();
            DB::table('idles')->where('user_id',$id)->delete();
            DB::table('process_lists')->where('user_id',$id)->delete();
            DB::table('rates')->where('user_id',$id)->delete();
            DB::table('rates_log')->where('user_id',$id)->delete();
            DB::table('reports')->where('user_id',$id)->delete();
            DB::table('requests')->where('user_id',$id)->delete();
            DB::table('schedules')->where('user_id',$id)->delete();
            DB::table('screenshots')->where('user_id',$id)->delete();
            DB::table('team_user')->where('user_id',$id)->delete();
            DB::table('teams')->where('lead_user_id',$id)->delete();
            DB::table('time_in_out')->where('user_id',$id)->delete();
            DB::table('timesheets')->where('user_id',$id)->delete();
            DB::table('timesheets')->where('client_id',$id)->delete();
            DB::table('user_tasks')->where('user_id',$id)->delete();
            DB::table('user_tasks')->where('client_id',$id)->delete();
            DB::statement('SET FOREIGN_KEY_CHECKS=0;');
            DB::table('role_user')->where('user_id',$id)->delete();
            DB::statement('SET FOREIGN_KEY_CHECKS=1;');
            
        }        
        $response['status'] = "ok";
        return json_encode($response);
    }

    public function datatables()
    {

        $auth_user = \Auth::user();

        if ($auth_user->is('client')) {
            //get all VAs under this client
            $vas = Schedule::where('client_id', $auth_user->id)
            ->pluck('user_id')->all();

            $users = User::HasRole('va')
            ->select(['id','first_name', 'last_name','email','mobile_number'])
            ->where('is_active', 1)
            ->whereIn('id', $vas);

        } elseif ($auth_user->is('manager')) {
            //get all VAs under this manager
              $team = Team::where("lead_user_id", $auth_user->id)->first();
            $vas = null;
            if ($team->id) {
                $vas = TeamUser::where("team_id", $team->id)->pluck('user_id')->all();
                $tid = $team->id;
            }
            
            //$tid = $team->id;
            //$clients = ManagerClient::where('user_id', $auth_user->id)->pluck('client_id');
            
            //$vas = Schedule::whereIn('client_id', $clients)
            //->pluck('user_id')->all();

            $users = User::HasRole('va')->select(['id','first_name', 'last_name','email','mobile_number'])->where('is_active', 1)
            ->whereIn('id', $vas)->whereHas('myTeam');

            

        } else {
            $users = User::HasRole('va')->select(['id','first_name', 'last_name','email','mobile_number']);
        }

        return DataTables::of($users)
            ->addColumn('actions', function($user) use ($auth_user){
                $sched_btn = '';
                $logout_btn  = '';
                
                if (!$auth_user->is('billing'))
                $sched_btn = '<a class="btn btn-success" href="/dashboard/va-schedules/'.$user->id.'"><i class="fa fa-calendar"></i></a>';
                
                $delete_btn = "";
                if ($auth_user->allowed('delete_va'))
                $delete_btn = '<a data-toggle="modal" data-target="#modal-danger" class="btn btn-danger button-delete" data-id="'.$user->id.'"><i class="fa fa-trash"></i></a>';
                
                $edit_btn = "";
                if ($auth_user->allowed('edit_va'))
                $edit_btn = '<a class="btn btn-info" href="/dashboard/va/edit/'.$user->id.'"><i class="fa fa-edit"></i></a>';    

                $rate_btn = "";
                if ($auth_user->allowed('edit_va') || ($auth_user->is('billing')) ){
                    $rate_btn = '<a class="btn btn-info" href="/dashboard/va-client-rates/'.$user->id.'" title="Edit Client Rate per Hour"><i class="fa fa-money"></i></a>';
                    $logout_btn = '<a title="sign out user" class="btn btn-warning" href="/dashboard/user/logout-by-admin/'.$user->id.'"><i class="fa fa-sign-out"></i></a>';  
                }

                 $show_profile_btn = '';
                 $remove_from_team = '';
                 
                if (!$auth_user->is('billing')){
                    $show_profile_btn = '<a class="btn btn-warning" href="/dashboard/users/show-profile/'.$user->id.'" title="Show Profile"><i class="fa fa-user"></i></a>';
                }
                
                if($auth_user->is('manager') || $auth_user->is('super_admin')){
                    $logout_btn = '<a id="logout_user" title="sign out user" class="btn btn-warning" href="/dashboard/user/logout-by-admin/'.$user->id.'"><i class="fa fa-sign-out"></i></a>';  
                    $remove_from_team = '<a id="remove_from_team" a title="remove user from team" class="btn btn-danger" href="/dashboard/user/remove-from-team/'.$user->id.'"><strong>x</strong></a>';
                }
                
                return '<div class="btn-toolbar">'. $show_profile_btn . $edit_btn . $sched_btn . $rate_btn . $delete_btn .$logout_btn .$remove_from_team .'</div>';
            
        })->rawColumns(['actions'])
        ->make(true);        

    }

    public function online() 
    {
        $auth_user = \Auth::user();
        if ($auth_user->is('client')) {
            return va_view('admin.va.client-online');
        }
        return view('admin.va.online');
    }

    public function online_datatables() 
    {
        $auth_user = \Auth::user();

        if ($auth_user->is('client')) {
            //get all VAs under this client
            $vas = Schedule::where('client_id', $auth_user->id)
            ->pluck('user_id')->all();
            $users = User::HasRole('va')->select(DB::raw('
                id, 
                email, 
                CONCAT (first_name, " ", last_name) as name, 
                updated_at as logged_in'
            ))
            ->where('is_online', '1')
            ->whereIn('id', $vas);

        } elseif ($auth_user->is('manager')) {
            //get all VAs under this manager
            $team = Team::where("lead_user_id", $auth_user->id)->first();
            $vas = null;
            if ($team->id) {
                $vasa = TeamUser::where("team_id", $team->id)->pluck('user_id');
                $vas = User::whereIn('id', $vasa)->where('is_active', '1')->pluck('id');
            }
            
            //$clients = ManagerClient::where('user_id', $auth_user->id)->pluck('client_id');
            
            //$vas = Schedule::whereIn('client_id', $clients)
            //->pluck('user_id')->all();

            $users = User::select(DB::raw('
                id, 
                email, 
                CONCAT (first_name, " ", last_name) as name, 
                updated_at as logged_in'
            ))
            ->where('is_online', '1')
            ->whereIn('id', $vas);

        } else {
            //TODO: finalize where to get last login
            $users = User::HasRole('va')->select(DB::raw('
                id, 
                email, 
                CONCAT (first_name, " ", last_name) as name, 
                updated_at as logged_in'
            ))
            ->where('is_online', '1');
        }

        return DataTables::of($users)
            ->editColumn('logged_in', function($user) use ($auth_user) {

                if ($auth_user->is('client')) {
                    $u_timezone = $auth_user->convertToUserTimezone($user->logged_in);
                    return $u_timezone->format('Y-m-d h:i:s A');
                }

                return date('Y-m-d h:i:s A', strtotime($user->logged_in));
            })
            ->addColumn('action', function($user){
                $ss_btn = '<a class="btn btn-info btn-ss" href="#" data-id="' . $user->id . '" ><i class="fa fa-camera"></i></a>';   
                return $ss_btn;
            })
            ->rawColumns(['action'])
            ->make(true);  
    }

    public function va_work_duration() {
        return view('admin.va.va_work_duration');
    }
    
    public function va_work_duration_datatables(Request $request) {

            $attendance = Attendance::select(['id','user_id', 'client_id' , 'date_in', 'time_in', 'start_time', 'end_time', 'date_end', 'time_end', 'timezone', 'schedule_date', 'total_time'])
                    ->where('schedule_date', $request->date)->where('status','out');

            return DataTables::of($attendance)
            ->removeColumn('user_id')
            ->removeColumn('date_in')
            ->removeColumn('time_in')
            ->removeColumn('start_time')
            ->removeColumn('end_time')
            ->removeColumn('timezone')
            ->removeColumn('schedule_date')
            ->removeColumn('date_end')
            ->removeColumn('time_end')
            ->editColumn('id',function($a) {
                return $a->user->id;
            })
            ->editColumn('client_id', function($a){
                return $a->client->fullname;
            })
            ->addColumn('name', function($a){
                return $a->user->first_name . ' ' . $a->user->last_name;
            })
            ->addColumn('clock_in', function($a) {
                return date("Y-m-d h:i:s A", strtotime($a->date_in . ' ' . $a->time_in)) . " - MNL";
            })
            ->addColumn('clock_out', function($a){
                return date("Y-m-d h:i:s A", strtotime($a->date_end . ' ' . $a->time_end)) . " - MNL";
            })
            ->make(true);
    }

    public function va_client_rate($id) {
        $q = User::find($id);
        if (empty($q)) {
            return redirect('/dashboard');
        }

        return view('admin.va.client_rates', compact('q'));
    }

    public function va_client_rate_update($id, Request $request) {
        $validator = \Validator::make($request->all(), [
            'rate_per_hour' => 'numeric'
        ]);
        
        if ($validator->fails()) {
            $response["status"] = "error";
            $response["message"] = "Invalid Rate per hour";
            return json_encode($response);
        }

        $rate = Rate::find($id);

        if( $rate ) {
            $old_rate_per_hour = $rate->rate_per_hour;
            $rate->rate_per_hour = $request->rate_per_hour;
            $rate->save();

            $rate->addLog(array(
                'user_id' => $rate->user_id,
                'client_id' => $rate->client_id,
                'old_rate_per_hour' => $old_rate_per_hour,
                'new_rate_per_hour' => $request->rate_per_hour,
                'updated_by' => \Auth::user()->id,
            ));

            $response['message'] = 'Rate has been successfully updated!';
            $response['status'] = 'success';
            return json_encode($response);
        } else {
            $response['message'] = 'Failed to update rate!';
            $response['status'] = 'error';
            return json_encode($response);
        }
    }

    public function va_client_rate_datatables($id) {
        $rates = Rate::select(['rates.id', 'first_name', 'client_id', 'user_id', 'rate_per_hour'])
                       ->leftJoin('users', 'users.id', '=', 'rates.client_id')
                       ->where('user_id', $id)->where('client_id', '!=', 0);
        return DataTables::of($rates)
            ->addColumn('client_name', function ($rate) {
                return $rate->client->full_name;
            })

            ->addColumn('action', function($rate) {
                $title = 'Update Rate of ' . $rate->va->full_name .' for ' . $rate->client->full_name;
                return '<a class="btn btn-info btn-client-rate-update" href="#" data-id="' . $rate->id . '" data-title="' . $title . '" data-rate-per-hour="' . $rate->rate_per_hour_formatted . '"><i class="fa fa-edit"></i></a>';   
            })
            ->editColumn('rate_per_hour', function($rate) {
                return number_format( $rate->rate_per_hour, 2, '.', '' );
            })
            ->orderColumn('client_name', 'first_name $1')
            ->rawColumns(['action'])
            ->make(true);
    }

    public function va_client_rate_available_clients($id) {
        $client_html = '';
        $current_clients = Rate::select(['client_id'])->where('user_id', $id)->where('client_id', '!=', '0')->get();

        $clients = User::HasRole('client')
                   ->select(['id', 'first_name', 'last_name'])
                   ->whereNotIn('id', $current_clients)
                   ->orderBy('first_name', 'ASC')
                   ->get();


        if( !empty($clients) ) {
            $client_html .= '<select class="form-control" id="client-id">';
            foreach($clients as $client) {
                $client_html .= '<option value="' . $client->id . '">' . $client->full_name . '</option>'; 
            }
            $client_html .= '</select>';
        } else {
            $client_html = 'All client has been assigned to this VA or no client available';
        }

        $response["status"] = "success";
        $response["client_html"] = $client_html;
        return json_encode($response);
    }

    public function va_client_rate_add($id, Request $request) {

        $validator = \Validator::make($request->all(), [
            'client_id' => 'required',
            'rate_per_hour' => 'numeric'
        ]);
        
        if ($validator->fails()) {
            $response["status"] = "error";
            $response["message"] = "Invalid Rate per hour";
            return json_encode($response);
        } else {

            $rate = Rate::where('user_id', $id)->where('client_id', $request->client_id)->first();
            if($rate) {
                $response["status"] = "error";
                $response["message"] = "rate is already existing";
                return json_encode($response);
            }

            $rate = Rate::create([
                'user_id' => $id,
                'client_id' => $request->client_id,
                'rate_per_hour' => $request->rate_per_hour,
            ]);

            $rate->addLog(array(
                'user_id' => $rate->user_id,
                'client_id' => $rate->client_id,
                'old_rate_per_hour' => $request->old_rate_per_hour,
                'new_rate_per_hour' => $request->rate_per_hour,
                'updated_by' => \Auth::user()->id,
            ));

            $response["status"] = "success";
            $response["message"] = "Rate has been added";
            return json_encode($response);
        }
        
    }

    public function requestSS($id) 
    {
        $user = User::find($id);
        if (!empty($user)) {
            $user->broadcast()->screenshot();
            $response['status'] = "ok";
            return json_encode($response);
        } 

        $response['status'] = "error";
        return json_encode($response);
    }

    public function online_dashboard_datatables()
    {
        $attendance_online = Attendance::select(['schedule_date' , 'start_time', 'user_id', 'date_in', 'time_in','status', 'created_at'])
            ->where('status','in');

            return DataTables::of($attendance_online)
            ->editColumn('user_id', function($ao) {
                return $ao->user->full_name;
            })
            ->editColumn('time_in', function($ao){
                return date('h:i:s a',strtotime($ao->date_in . ' ' . $ao->time_in));
            })
            ->editColumn('status', function($ao) {
                return "<img src='/images/custom/online_status.jpg' width='32px'/>";
            })->rawColumns(['status'])
            ->make(true);
    }
    

    
      public function online_dashboard_datatables_client()
  {
    //Log::info(Auth::user()->id);
    $hour = 12;
    $today = date('Y-m-d', strtotime("today"));
    $yesterday = date('Y-m-d', strtotime("yesterday"));
    $dates = array($yesterday, $today);

    $attendance_online = Attendance::whereIn('date_in', $dates)->where('client_id',  Auth::user()->id)->orderBy('status', 'asc')->orderBy('updated_at', 'desc');
    //Log::info($attendance_online);

    return DataTables::of($attendance_online)
    ->editColumn('user_id', function($ao) {
      return $ao->user->full_name;
    })
    ->editColumn('time_in', function($ao){

     //$tz = Auth::user()->timezone;
     //$timezone = config('vatimetracker.timezone');
     //$client_timezone = $timezone[$tz];

   // r//eturn date('h:i:s a',strtotime($ao->date_in . ' ' . $ao->time_in));

    //$remote_user = User::whereId($ao->user_id)->first();

    //$datein = $ao->date_in . ' ' . $ao->time_in;
    //$dbdate = Carbon::createFromFormat('Y-m-d H:i:s', $datein, $timezone[$remote_user->timezone]);

    //return date('h:i:s a', strtotime($dbdate->setTimezone($client_timezone)));
    return date('h:i:s a',strtotime($ao->date_in . ' ' . $ao->time_in));

    })
    ->editColumn('status', function($ao) {
      $html = '';
      if($ao->status == 'in'){
        $html = '<span class="badge badge-dot mr-4"><i class="bg-success"></i><span class="status"> in</span></span>';
      }elseif($ao->status == 'out'){
        $html = '<span class="badge badge-dot mr-4"><i class="bg-danger"></i><span class="status"> out</span></span>';
      }elseif($ao->status == 'break'){
        $html = '<span class="badge badge-dot mr-4"><i style="background: #ff8f00"></i><span class="status"> break</span></span>';
      }else{
        $html = '<span class="badge badge-dot mr-4"><i style="background: #78909c"></i><span class="status"> idle</span></span>';
      }

      $notifications = \DB::table('notifications')->where('user_id', $ao->user->id)->orderBy('id', 'desc')->first();

      //if($notifications->type == 'break-exceed'){
        //$html = $html;
      //}

      return $html;
    })->rawColumns(['status'])
    ->make(true);
  }
}
