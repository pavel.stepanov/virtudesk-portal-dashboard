<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use App\Models\Task;
use Illuminate\Validation\Rule;

class TaskController extends Controller
{
    public function index()
    {
        return view('admin.task.index');
    }

    public function create()
    {
        return view('admin.task.create_edit');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:32',
        ]);

        $task = Task::create([
            'name' => $request->name,
            'description' => $request->description
        ]);

        return redirect('/dashboard/tasks/edit/'. $task->id)->with('notification_message', 'Task has been created.'); 
    } 

    public function edit($id)
    {
        $q = Task::find($id);
        if (empty($q)) {
            return redirect('/dashboard');
        }
        return view('admin.task.create_edit', compact('q'));        
    }

    public function update(Request $request) 
    {
        $q = Task::find($request->id);

        $request->validate([
            'name' => 'required|max:32'
        ]);

        if (!empty($q)) {
            $q->name = $request->name;
            $q->description = $request->description;
            $q->save();
        }

        return redirect()->intended('/dashboard/tasks/edit/'. $q->id)->with('notification_message', 'Task has been updated.'); 

    }  

    public function show($id)
    {
        $q = Task::find($id);
        $html = view('admin.task.show', compact('q'))->render();
        $response['html'] = $html;
        return json_encode($response);
    }

    public function delete($id)
    {
        $q = Task::find($id);
        if (!empty($q)) {
            $q->delete();
        }        
        $response['status'] = "ok";
        return json_encode($response);
    }

    public function datatables()
    {

        $tasks = Task::select(['id','name', 'description']);

        return DataTables::of($tasks)
            ->addColumn('actions', function($task){
                $delete_btn = '<a data-toggle="modal" data-target="#modal-danger" class="btn btn-danger button-delete" data-id="'.$task->id.'"><i class="fa fa-trash"></i></a>';
                $edit_btn = '<a class="btn btn-info" href="/dashboard/tasks/edit/'.$task->id.'"><i class="fa fa-edit"></i></a>';
                return '<div class="btn-toolbar">'. $edit_btn .  $delete_btn .'</div>';
            
        })->rawColumns(['actions'])
        ->make(true);

    }

}