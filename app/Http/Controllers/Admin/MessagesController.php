<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ActivityLog;
use App\Models\Messaging;
use DataTables;
use App\Models\User;

class MessagesController extends Controller
{
  public function index()
  {
      return view('admin.messages.index');
  }


  public function datatables()
  {
      $user = \Auth::user();

      if ($user->is('administrator')|| $user->is('super_administrator')) {
          $messages = Messaging::where('from_id', '1')->orWhere('to_id', '1')->where('reply_to', NULL)->select(['id',	'to_id',	'from_id',	'message',	'file',	'updated_at',	'status'])->with('replies')->latest();
      } else {
          $messages = Messaging::where('from_id', '1')->orWhere('to_id', '1')->where('reply_to', NULL)->select(['id',	'to_id',	'from_id',	'message',	'file',	'updated_at',	'status'])->with('replies')->latest();
      }

      return DataTables::of($messages)

      ->removeColumn('to_id')
      ->removeColumn('status')

      ->editColumn('message', function($r){

        $d = [];
        $d['id'] = $r->id;
        $d['orig'] = $r->message;
        $d['status'] = $r->status;

        if(count($r->replies) >= 1){
          foreach($r->replies as $k=>$v){
            if($v->status == 'read'){ //replied
              $sender = User::whereId($v->from_id)->select(['first_name', 'last_name'])->first();
              $d['reply'] = array($sender->first_name.' '.$sender->last_name, $v->message, $v->updated_at);
              //$d['reply'] = $sender->first_name.' '.$sender->last_name;
            }
          }
        }else{
          $sender = User::whereId($r->from_id)->select(['first_name', 'last_name'])->first();
          $d['reply'] = array($sender->first_name.' '.$sender->last_name, $r->message, $r->updated_at);
        }
        return $d;
      })

      ->editColumn('from_id', function($r){
        $sender = User::whereId($r->from_id)->select(['first_name', 'last_name'])->first();
        return $sender->first_name.' '.$sender->last_name;
      })

      ->editColumn('updated_at', function($r){
        return date('M d, y h:i', strtotime($r->updated_at));
      })

      /***
      ->addColumn('actions', function($r) use ($user) {

          $delete_btn = '';
          $action_btn = '';
          $info_btn = '';
          if ($r->status=='sent') {
              $delete_btn = '<a data-toggle="modal" data-target="#modal-danger" class="btn btn-danger button-delete" data-id="'.$r->id.'"><i class="fa fa-trash"></i></a>';
              $action_btn = '<a class="btn btn-primary" href="/dashboard/messages/take-action/'.$r->id.'"><i class="fa fa-edit"></i></a>';
          } else {
              $info_btn = '<a class="btn btn-primary" href="/dashboard/messages/info/'.$r->id.'"><i class="fa fa-info-circle"></i></a>';
          }

        if ($user->is('administrator')||$user->is('super_administrator')) {
            //return '<div class="btn-toolbar">'. $info_btn . $action_btn . $delete_btn . '</div>';
        } else {
            //return '<div class="btn-toolbar">'. $info_btn . $delete_btn . '</div>';
        }

        return $r ->updated_at;

  })

      ->rawColumns(['actions'])
      ***/
      ->make(true);

  }

  public function info($id){
    $user = \Auth::user();
    $messages = Messaging::whereId($id)->orWhere('reply_to', $id)->with('sender')->get();
    $html = '';
    $header_color = 'style="color: blue;"';
    $reply_icon = '';
    $message_infos = '';

    foreach($messages as $message){

      if($message->reply_to != null){
        $reply_icon = '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-reply-fill" viewBox="0 0 16 16"><path d="M9.079 11.9l4.568-3.281a.719.719 0 0 0 0-1.238L9.079 4.1A.716.716 0 0 0 8 4.719V6c-1.5 0-6 0-7 8 2.5-4.5 7-4 7-4v1.281c0 .56.606.898 1.079.62z"/></svg> ';
      }


      if($message->sender->id == $user->id){
        $header_color = 'style="color: black;"';
      }else{
        $header_color = 'style="color: gray;"';
      }

      $message_infos = $messages[0]['to_id'].'|'.$messages[0]['from_id'];


      $html .= '<div id="msg-infos" rel="'.$message_infos.'" style="width: 100%; padding-bottom: 10px;"><div class="mbox">'.$reply_icon.'<small class="small-text-header"><strong '.$header_color.'>'.$message->sender->first_name.' '.$message->sender->last_name.'</strong> <span style="text-align: right !important">'.date('D m, Y h:i a', strtotime($message->updated_at)).'</span></small><div><div style="width: 100%;margin-top: 10px; min-height: 40px; font-size: 13px;">'.$message->message.'</div><br></div>';

    }


    return $html;
  }

  public function save(Request $request){

    $uploaded_filename = null;


    if($request->hasFile('uploaded_file')){

        $request->validate([
            'uploaded_file' => 'required',
        ]);

        if (!is_dir(public_path('/uploads/files/'))) {
            mkdir(public_path('/uploads/files/'), 0775, true);
        }

        $uploaded_file = $request->file('uploaded_file');
        $uploaded_filename = md5( $user->id . now() . time() ). '.' . $uploaded_file->getClientOriginalExtension();
        $uploaded_file->move(public_path('/uploads/files/'), $uploaded_filename);

        $path = public_path('/uploads/files/');
        $status = file_put_contents($path, base64_decode($request->img-base64));
    }

    $message = Messaging::create([
      'message' => $request->message,
      'to_id' => $request->to_id,
      'from_id' => $request->from_id,
      'reply_to' => $request->reply_to,
      'file'  => $uploaded_filename
    ]);

    if(isset($request->reply_to)){
      $messages = Messaging::where('id', $request->reply_to)->update(['status' => 'read']);
    }

    return redirect()->back();

  }



}
