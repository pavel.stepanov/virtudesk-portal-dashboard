<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\UserTask;
use App\Models\Attendance;
use App\Models\Schedule;
use App\Models\Team;
use App\Models\TeamUser;
use PDF;
use App\Models\ManagerClient;

class DailyReportsController extends Controller
{
    public function index()
    {
        $auth_user = \Auth::user();

        if ($auth_user->is('client')) {

            //get all VAs under this client
            $vas_pluck = Schedule::where('client_id', $auth_user->id)
            ->pluck('user_id')->all();

            $vas = User::HasRole('va')->orderBy('first_name')
            ->whereIn('id', $vas_pluck)->get();

            return va_view('admin.daily-reports.client-index', compact('vas'));

        }

        if ($auth_user->is('manager')) {

            //get all VAs under this manager
            //$team = Team::where("lead_user_id", $auth_user->id)->first();
            //$vas_pluck = null;
            //if ($team->id) {
                //$vas_pluck = TeamUser::where("team_id", $team->id)->pluck('user_id')->all();
            //}
            
            $clients = ManagerClient::where('user_id', $auth_user->id)->pluck('client_id');
           $vas = Schedule::whereIn('client_id', $clients)
                ->pluck('user_id')->all();

            $vas = User::HasRole('va')->select(['id','first_name', 'last_name'])
            ->whereIn('id', $vas)->get();

            return view('admin.daily-reports.index', compact('vas'));

        }

        $vas = User::HasRole('va')->orderBy('first_name')->get();

        return view('admin.daily-reports.index', compact('vas'));
    }

    public function generateDailyReports(Request $request)
    {
        $auth_user = \Auth::user();

        if ($auth_user->is('client') || $auth_user->is('manager')) {
            $client_id = $auth_user->id;
            $attendances = Attendance::where('date_in', $request->target_date)
            ->whereIn('user_id', $request->va_id)
            ->where('status',"<>","in")
            ->where('client_id', $client_id)
            ->leftJoin('users','users.id', '=', 'attendance.user_id')
            ->orderBy("shift")
            ->orderBy('users.first_name', 'ASC')
            ->select('attendance.id', 'user_id', 'client_id', 'date_in', 'time_in', 'date_end', 'time_end', 'total_time', 'total_idletime')
            ->get();
        } else {
            $attendances = Attendance::where('date_in', $request->target_date)
            ->whereIn('user_id', $request->va_id)
            ->where('status',"<>","in")
            ->leftJoin('users','users.id', '=', 'attendance.user_id')
            ->orderBy("shift")
            ->orderBy('users.first_name', 'ASC')
            ->select('attendance.id', 'user_id', 'client_id', 'date_in', 'time_in', 'date_end', 'time_end', 'total_time', 'total_idletime')
            ->get();
        }

        if (count($attendances)<=0) {
            $html = "<p>No attendance record found.</p>";
        } else {
            $html = va_view("admin.daily-reports.content", compact('attendances'))
            ->render();
        }

        $response['status'] = "ok";
        $response['html'] = $html;
        return json_encode($response);
    }

    public function generatePDF(Request $request)
    {
        $auth_user = \Auth::user();

        if ($auth_user->is('client') || $auth_user->is('manager')) {
            $client_id = $auth_user->id;
            $attendances = Attendance::where('date_in', $request->target_date)
            ->whereIn('user_id', $request->va_id)
            ->where('status',"<>","in")
            ->where('client_id', $client_id)
            ->leftJoin('users','users.id', '=', 'attendance.user_id')
            ->orderBy("shift")
            ->orderBy('users.first_name', 'ASC')
            ->select('attendance.id', 'user_id', 'client_id', 'date_in', 'time_in', 'date_end', 'time_end', 'total_time', 'total_idletime')
            ->get();
        } else {
            $attendances = Attendance::where('date_in', $request->target_date)
            ->whereIn('user_id', $request->va_id)
            ->where('status',"<>","in")
            ->leftJoin('users','users.id', '=', 'attendance.user_id')
            ->orderBy("shift")
            ->orderBy('users.first_name', 'ASC')
            ->select('attendance.id', 'user_id', 'client_id', 'date_in', 'time_in', 'date_end', 'time_end', 'total_time', 'total_idletime')
            ->get();
        }

        $target_date = $request->target_date;

        if (count($attendances)>0) {
            $pdf = PDF::loadView("admin.daily-reports.pdf", compact('attendances', 'target_date' ));
        
            return $pdf->download('dailyreport.pdf');
        }

        return "No Attendace";
    }
}