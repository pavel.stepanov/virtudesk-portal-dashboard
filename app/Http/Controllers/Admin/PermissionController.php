<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use App\Models\Permission;
use Illuminate\Validation\Rule;

class PermissionController extends Controller
{
    public function index()
    {
        return view('admin.permission.index');
    }


    public function create()
    {
        return view('admin.permission.create_edit');
    }

    public function store(Request $request)
    {
        $request->validate([
            'slug' => 'required|max:100',
            'name' => 'required|max:100',
            'description' => 'required|max:100',
        ]);

        Permission::create([
            'name' => $request->name,
            'slug' => $request->slug,
            'description' => $request->description,
        ]);

        return redirect('/dashboard/permissions');
    }

    public function edit($id)
    {
        $q = Permission::find($id);
        if (empty($q)) {
            return redirect('/dashboard');            
        }
        return view('admin.permission.create_edit', compact('q'));        
    }    

    public function update(Request $request) 
    {
        $q = Permission::find($request->id);

        $request->validate([
            'name' => 'required|max:100',
            'slug' => 'required|max:100',
            'description' => 'required|max:100'
        ]);

        if (!empty($q)) {
            $q->name = $request->name;
            $q->slug = $request->slug;
            $q->description = $request->description;
            $q->save();
        }

        return redirect()->intended('/dashboard/permissions/edit/'. $q->id)->with('notification_message', 'Permission has been updated.'); 

    } 

    public function show($id)
    {
        $q = Permission::find($id);
        $html = view('admin.permission.show', compact('q'))->render();
        $response['html'] = $html;
        return json_encode($response);
    }

    public function delete($id)
    {
        $q = Permission::find($id);
        if (!empty($q)) {
            $q->delete();
        }        
        $response['status'] = "ok";
        return json_encode($response);
    }


    public function datatables()
    {

        $permissions = Permission::select(['id','name', 'slug','description']);

        return DataTables::of($permissions)
            /*->addColumn('actions', function($permission){

                $delete_btn = '<a data-toggle="modal" data-target="#modal-danger" class="btn btn-danger button-delete" data-id="'.$permission->id.'"><i class="fa fa-trash"></i></a>';
                $edit_btn = '<a class="btn btn-info" href="/dashboard/permissions/edit/'.$permission->id.'"><i class="fa fa-edit"></i></a>';    

                return '<div class="btn-toolbar">'. $edit_btn .  $delete_btn . '</div>';

        })->rawColumns(['actions'])*/
        ->make(true);        

    }

}