<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Schedule;
use App\Models\User;
use DataTables;
use Auth;
//use Carbon\Carbon;
use DateTime;
use DateTimeZone;
use App\Models\ActivityLog;
use App\Models\Team;
use App\Models\TeamUser;
use App\Models\Rate;
use App\Models\ManagerClient;

class ScheduleController extends Controller
{

    private $auser;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->auser = \Auth::user();
            return $next($request);
        });
    }

    public function index()
    {
        $user = Auth::user();
        if ($user->is('va')) {
            return view('admin.schedule.index');
        } else {    
            
            if ($user->is('client')) {
                return va_view('admin.schedule.client-all');
            }
            return view('admin.schedule.all');
        }
    }

    public function datatables(Request $request)
    {
        $user = Auth::user();
        $target_date = date("Y-m-d", strtotime(now()));
        
        if (isset($request->target_date)) {
            $target_date = $request->target_date;
        }
        $day_of_week = date("l", strtotime($target_date));

        $schedules = Schedule::where('user_id', $user->id)
        ->where('day_of_week', $day_of_week)
        ->select(['id', 'user_id', 'client_id', 'day_of_week', 'start_time', 'end_time', 'shift', 'timezone', 'user_timezone']);

        return DataTables::of($schedules)
        ->removeColumn('id')
        ->removeColumn('start_time')
        ->removeColumn('user_id')
        ->removeColumn('user_timezone')
        ->removeColumn('end_time')
        ->removeColumn('timezone')
        ->removeColumn('day_of_week')
        ->removeColumn('shift')
        ->addColumn('client_schedule', function($schedule) use ($target_date){
            $sched_timezone = "";
            $timezone_config = config('vatimetracker.timezone');
            foreach ($timezone_config as $key => $value) {
                if ($key == $schedule->timezone) {
                    $sched_timezone = $value;
                    break;
                }
            }
            $start_time = new DateTime(date("Y-m-d h:i A",strtotime($target_date . " " . $schedule->start_time)), new DateTimeZone($sched_timezone));
            $end_time = new DateTime(date("Y-m-d h:i A",strtotime($target_date . " " . $schedule->end_time)), new DateTimeZone($sched_timezone));
            
           return $start_time->format("j M D h:i A") . " - " . $end_time->format("h:i A") . ": " . $schedule->timezone . " " .$schedule->showShift();

        })
        ->addColumn('va_schedule', function($schedule) use ($user, $target_date) {
            $sched_timezone = "";
            $timezone_config = config('vatimetracker.timezone');
            foreach ($timezone_config as $key => $value) {
                if ($key == $schedule->timezone) {
                    $sched_timezone = $value;
                    break;
                }
            }

            $user_timezone = "";
            $timezone_config = config('vatimetracker.timezone');
            foreach ($timezone_config as $key => $value) {
                if ($key == $user->timezone) {
                    $user_timezone = $value;
                    break;
                }
            }

            $start_time = new DateTime(date("Y-m-d h:i A",strtotime($target_date . " " . $schedule->start_time)), new DateTimeZone($sched_timezone));
            $end_time = new DateTime(date("Y-m-d h:i A",strtotime($target_date . " " . $schedule->end_time)), new DateTimeZone($sched_timezone));
            
            $start_time->setTimezone(new DateTimeZone($user_timezone));
            $end_time->setTimezone(new DateTimeZone($user_timezone));
            
            return $start_time->format("j M D h:i A") . " - " . $end_time->format("h:i A") . ": " . $user->timezone . " " .$schedule->showShift();
        })
        /* ->addColumn('time', function($schedule) use ($user, $target_date) {

            $sched_timezone = "";
            $timezone_config = config('vatimetracker.timezone');
            foreach ($timezone_config as $key => $value) {
                if ($key == $schedule->timezone) {
                    $sched_timezone = $value;
                    break;
                }
            }

            $user_timezone = "";
            $timezone_config = config('vatimetracker.timezone');
            foreach ($timezone_config as $key => $value) {
                if ($key == $user->timezone) {
                    $user_timezone = $value;
                    break;
                }
            }

            $start_time = new DateTime(date("Y-m-d h:i A",strtotime($target_date . " " . $schedule->start_time)), new DateTimeZone($sched_timezone));
            $end_time = new DateTime(date("Y-m-d h:i A",strtotime($target_date . " " . $schedule->end_time)), new DateTimeZone($sched_timezone));
            
            $orig_schedule = $start_time->format("h:i A l") . " - " . $end_time->format("h:i A l") . ": " . $schedule->timezone;

            $start_time->setTimezone(new DateTimeZone($user_timezone));
            $end_time->setTimezone(new DateTimeZone($user_timezone));
            
            $updated_schedule = $start_time->format("h:i A l") . " - " . $end_time->format("h:i A l") . ": " . $user->timezone;
            
            return "<div>" . $orig_schedule . "</div><div>" . $updated_schedule . "</div>";
        }) */
        ->editColumn('client_id', function($schedule){
            return $schedule->client->first_name . " " . $schedule->client->last_name;
        })->rawColumns(['time'])
        ->make(true); 
    }

    public function datatablesAll(Request $request)
    {
        $auth_user = \Auth::user();
        $target_date = date("Y-m-d", strtotime(now()));
        if (isset($request->target_date)) {
            $target_date = $request->target_date;
        }

        //$work_week = date("W", strtotime($target_date));
        $day_of_week = date("l", strtotime($target_date));
        $client_id = 0;
        if ($auth_user->is('client')) {
            $client_id = $auth_user->id;
            //get all VAs under this client
            $vas = Schedule::where('client_id', $auth_user->id)
            ->pluck('user_id')->all();
            $users = User::HasRole('va')->select(['id','first_name','last_name', 'timezone'])->whereIn('id', $vas);
        } elseif ($auth_user->is('manager')) {
            

              //get all VAs under this manager
                $team = Team::where("lead_user_id", $auth_user->id)->first();
                $vas = null;
                if ($team->id) {
                    $vas = TeamUser::where("team_id", $team->id)->pluck('user_id')->all();
                }

           
            $users = User::select(['id','first_name','last_name', 'timezone'])->where('is_active', '1')->whereIn('id', $vas);

        
        
        }else {
            $users = User::HasRole('va')->select(['id','first_name','last_name', 'timezone']);
        }

        return DataTables::of($users)
            ->addColumn('schedule', function($user) use ($day_of_week, $client_id, $target_date){
                if ($client_id==0) {
                    return $user->getScheduleList($target_date, $day_of_week);
                } else {
                    return $user->getScheduleList($target_date, $day_of_week, $client_id);
                }
                
            })
            ->addColumn("va_schedule", function($user) use ($day_of_week, $client_id, $target_date) {

                if ($client_id == 0) {
                    $schedules = Schedule::where('user_id', $user->id)
                    ->where('day_of_week', $day_of_week)
                    ->select(['id', 'user_id', 'client_id', 'day_of_week', 'start_time', 'end_time', 'shift', 'timezone', 'user_timezone'])->get();
                } else {
                    $schedules = Schedule::where('user_id', $user->id)
                    ->where('day_of_week', $day_of_week)
                    ->where('client_id', $client_id)
                    ->select(['id', 'user_id', 'client_id', 'day_of_week', 'start_time', 'end_time', 'shift', 'timezone', 'user_timezone'])->get();    
                }

                if (count($schedules)==0) {
                    return "No Schedule for selected date.";
                }

                $html = "";
                foreach ($schedules as $schedule) { 

                    $sched_timezone = "";
                    $timezone_config = config('vatimetracker.timezone');
                    foreach ($timezone_config as $key => $value) {
                        if ($key == $schedule->timezone) {
                            $sched_timezone = $value;
                            break;
                        }
                    }
        
                    $user_timezone = "";
                    $timezone_config = config('vatimetracker.timezone');
                    foreach ($timezone_config as $key => $value) {
                        if ($key == $user->timezone) {
                            $user_timezone = $value;
                            break;
                        }
                    }
        
                    $start_time = new DateTime(date("Y-m-d h:i A",strtotime($target_date . " " . $schedule->start_time)), new DateTimeZone($sched_timezone));
                    $end_time = new DateTime(date("Y-m-d h:i A",strtotime($target_date . " " . $schedule->end_time)), new DateTimeZone($sched_timezone));
                    
                    $start_time->setTimezone(new DateTimeZone($user_timezone));
                    $end_time->setTimezone(new DateTimeZone($user_timezone));
                    
                    $html .= "<div>" .  $start_time->format("j M D h:i A") . " - " . $end_time->format("h:i A") . ": " . $user->timezone . " " .$schedule->showShift() . "</div>";
                }

                return $html;

            })
            ->addColumn('actions', function($user){
                $sched_btn = '<a class="btn btn-success" href="/dashboard/va-schedules/'.$user->id.'"><i class="fa fa-calendar"></i></a>';

                return '<div class="btn-toolbar">'. $sched_btn . '</div>';

            })->rawColumns(['actions', 'schedule', 'va_schedule'])
        ->make(true);  
    }

    public function datatablesToday() {
       $day_of_week = date("l");
       $schedules = Schedule::where('user_day_of_week', $day_of_week);

  
       return DataTables::of($schedules)
        ->addColumn('name', function($s) {
            $u = User::find($s->user_id);
            return $u->full_name;
        })
        ->addColumn('client', function($s) {
            $u = User::find($s->client_id);
            if (!empty($u)) {
                return $u->full_name;
            } else {
                return "Client deleted";
            }
        })
        ->addColumn('schedule', function($s) {
            return date("h:i A",strtotime($s->user_start_time)) . " - " . date("h:i A",strtotime($s->user_end_time)) . " : " . $s->user_timezone;
            //return $s->start_time . ' - ' . $s->end_time;
        })
        ->addColumn('status', function($s) {
            $u = User::find($s->user_id);
            $a = \App\Models\Attendance::where('user_id',$u->id)->where('status','in')->first();
            if (!empty($a)) {
                return "<img src='/images/custom/online_status.jpg' width='32px'/>";
            } else {
                return "<img src='/images/custom/offline_status.jpg' width='32px'/>";
            }
        })
        ->addColumn('user_start_time', function($s){
            return $s->user_start_time;
        })->rawColumns(['status'])
        ->make(true);  
    }

    public function vaScheduleIndex($id) 
    {
        $set_menu = [
            'active_menu' => "dashboard/schedules",
            'active_path' => "dashboard/va-schedules/{id}",
            'active_parent' => "dashboard/va"
        ];
        $user = User::find($id);
        $auth_user = \Auth::user();

        if ($auth_user->is('client')) {
            //check if VA is under this client
            $vas = Schedule::where('client_id', $auth_user->id)
            ->where('user_id', $id)->first();

            if (empty($vas)) {
                return redirect('/dashboard/schedules');
            }
        }

        if ($auth_user->is('manager')) {
            //check VA if under this manager
            $team = Team::where("lead_user_id", $auth_user->id)->first();
            $vas = null;
            $vas_id = null;
            if (empty($team)) {
                return redirect('/dashboard/schedules');
            }

            $vas = TeamUser::where("team_id", $team->id)->where('user_id', $id)->first();
            if (empty($vas)) {
                return redirect('/dashboard/schedules');
            }
        }

        if ($auth_user->is('client')) {
            return va_view('admin.va-schedule.client-index', compact('user', 'auth_user', 'set_menu'));
        }

        return view('admin.va-schedule.index', compact('user', 'auth_user', 'set_menu'));
    }

    public function vaScheduleCreate($id)
    {
        $user = User::find($id);
        $shift = config('vatimetracker.shift');
        $clients = User::HasRole('client')
        ->where('is_active', 1)
        ->orderBy("first_name", "ASC")
        ->orderBy("last_name", "ASC")
        ->select(['id','first_name', 'last_name', 'timezone'])->get();

        $set_menu = [
            'active_menu' => "dashboard/schedules",
            'active_path' => "dashboard/va-schedules/{id}/create",
            'active_parent' => "dashboard/va"
        ];
        return view('admin.va-schedule.create_edit', compact('user', 'clients', 'shift', 'set_menu'));
    }

    public function vaScheduleStore(Request $request)
    {
        $user = User::find($request->user_id);
        $client = User::find($request->client_id);

        $request->validate([
            'day_of_week' => 'required',
            'client_id' => 'required',
            'shift' => 'required',
            'start_time' => 'required',
            'hours_needed' => 'required',
            'max_breaktime' => 'break_check'
        ],[
            'break_check' => 'Break cannot be higher than required hours',
        ]);
 
        $day_of_week = $request->input('day_of_week');
        
        $year = date("Y", strtotime(now()));

        if (!empty($day_of_week)) {

            foreach($day_of_week as $key => $value) 
            {

                $sched_exist = Schedule::where('user_id',$request->user_id)
                ->where('client_id', $request->client_id)
                ->where('shift', $request->shift)
                ->where('day_of_week', $key)
                ->where('year', $year)->first();

                if (empty($sched_exist)) {

                    $target_date = new DateTime();
                    $target_date->modify('next '. $key);

                    $seconds = ($request->hours_needed * 3600);
                    // we're given hours, so let's get those the easy way
                    $hours = floor($request->hours_needed);
                    // since we've "calculated" hours, let's remove them from the seconds variable
                    $seconds -= $hours * 3600;
                    // calculate minutes left
                    $minutes = floor($seconds / 60);
            
                    $start_time = new DateTime(date("Y-m-d h:i A",strtotime($target_date->format("Y-m-d"). " ".$request->start_time)), new DateTimeZone($client->showTimezone()));
                    $end_time = new DateTime(date("Y-m-d h:i A",strtotime($target_date->format("Y-m-d"). " ".$request->start_time)), new DateTimeZone($client->showTimezone()));
                    $end_time->modify("+".$hours." hours +".$minutes . " minutes");
                    $end_time_value = $end_time->format("H:i:s");

                    $start_time->setTimezone(new DateTimeZone($user->showTimezone()));
                    $end_time->setTimezone(new DateTimeZone($user->showTimezone()));

                    $va_sched_exist = Schedule::where("user_id", $request->user_id)
                    ->where("user_day_of_week", $start_time->format('l'))
                    ->get();
                    
                    $cct = date('H:i:s', strtotime($request->start_time));

                    if (count($va_sched_exist)) {

                        foreach ($va_sched_exist as $s) {
                            if ($start_time->format("H:i:s") >= $s->user_start_time && $start_time->format("H:i:s")<= $s->user_end_time) {
                            //if (strtotime($cct) >= strtotime($s->start_time) && strtotime($cct) <= strtotime($s->end_time)) {
                                return redirect('/dashboard/va-schedules/'.$user->id . '/create')->with([
                                    'notification_message'=>'Cannot create new schedule as it will overlap with existing schedule.',
                                    'alert_type' => 'alert-danger',
                                    'alert_title' => 'Error'
                                    ]);
                            }
                        }

                    }

                    $user_day_of_week_end = null;
                    if ($start_time->format('l')!=$end_time->format('l')) {
                        $user_day_of_week_end = $end_time->format('l');
                    }

                    Schedule::create([
                        'user_id' => $request->user_id,
                        'client_id' => $request->client_id,
                        'shift' =>$request->shift,
                        'start_time' => date("H:i:s",strtotime($request->start_time)),
                        'end_time' => $end_time_value,
                        'day_of_week' => $key,
                        'timezone' => $client->timezone,
                        'year' => $year,
                        'max_breaktime' => $request->max_breaktime,
                        'paid_break' => $request->paid_break,
                        'hours_needed' => $request->hours_needed,
                        'user_start_time' => $start_time->format("H:i:s"),
                        'user_end_time' => $end_time->format("H:i:s"),
                        'user_timezone' => $user->timezone,
                        'user_day_of_week' => $start_time->format('l'),
                        'user_day_of_week_end' => $user_day_of_week_end
                    ]);

                    // get client rate
                    $rate = $user->rates->getClientRate($request->client_id);

                    // add client rate
                    if( !$rate ) {
                        Rate::create([
                            'user_id' => $request->user_id,
                            'client_id' => $request->client_id,
                            'rate_per_hour' => $user->rates->getDefaultRate()
                        ]);
                    }

                    ActivityLog::addLog("User [{$this->auser->first_name} {$this->auser->last_name}] created schedule for [{$user->first_name} {$user->last_name}] for {$key}.");

                } else {
                    return redirect('/dashboard/va-schedules/'.$user->id . '/create')->with([
                        'notification_message'=>'Shift for the same client exists.',
                        'alert_type' => 'alert-danger',
                        'alert_title' => 'Error'
                        ]);
                }
            }

        }

        return redirect('/dashboard/va-schedules/'.$user->id);
    } 

    public function vaScheduleDataTables($id, Request $request)
    {
        $auth_user = \Auth::user();

        if ($auth_user->is('client')) {
            $schedules = Schedule::select(['id','day_of_week', 'start_time','end_time','shift', 'client_id'])
            ->where('client_id', $auth_user->id)
            ->where('user_id', $id);
            $schedules->selectRaw("CASE `day_of_week` 
                                    WHEN 'Monday' THEN 1
                                    WHEN 'Tuesday' THEN 2
                                    WHEN 'Wednesday' THEN 3
                                    WHEN 'Thursday' THEN 4
                                    WHEN 'Friday' THEN 5
                                    WHEN 'Saturday' THEN 6
                                    WHEN 'Sunday' THEN 7
                                    END as num_date");

            return DataTables::of($schedules)
                ->removeColumn('id')
                ->removeColumn('start_time')
                ->removeColumn('end_time')
                ->removeColumn('client_id')
                ->orderColumn('day_of_week', 'num_date $1')
                ->editColumn('shift', function($schedule){
                    return $schedule->showShift();
                })
                ->addColumn('time', function($schedule){
                    return $schedule->startTimeAMPM() . " - " . $schedule->endTimeAMPM();
                })                
            ->make(true); 
        } elseif ($auth_user->is('manager')) {

            //check VA if under this manager
            $team = Team::where("lead_user_id", $auth_user->id)->first();
            $vas = null;
            $vas_id = null;
            if ($team->id) {
                $vas = TeamUser::where("team_id", $team->id)->where('user_id', $id)->first();
                if ($vas->id) {
                    $vas_id = $vas->user_id;
                }
            }

            $schedules = Schedule::select(['id', 'user_id', 'day_of_week', 'start_time','end_time','shift', 'client_id', 'user_start_time', 'user_end_time', 'timezone', 'user_timezone'])
            ->where('user_id', $vas_id);
            $schedules->selectRaw("CASE `day_of_week` 
                                    WHEN 'Monday' THEN 1
                                    WHEN 'Tuesday' THEN 2
                                    WHEN 'Wednesday' THEN 3
                                    WHEN 'Thursday' THEN 4
                                    WHEN 'Friday' THEN 5
                                    WHEN 'Saturday' THEN 6
                                    WHEN 'Sunday' THEN 7
                                    END as num_date");


            return DataTables::of($schedules)
            ->removeColumn('id')
            ->removeColumn('user_id')
            ->removeColumn('start_time')
            ->removeColumn('end_time')
            ->removeColumn('user_start_time')
            ->removeColumn('user_end_time')
            ->removeColumn('timezone')
            ->removeColumn('user_timezone')
            ->orderColumn('day_of_week', 'num_date $1')
            ->editColumn('shift', function($schedule){
                return $schedule->showShift();
            })
            ->addColumn('time', function($schedule){
                return $schedule->startTimeAMPM() . " - " . $schedule->endTimeAMPM() . " : " . $schedule->timezone;
            })
            ->editColumn('client_id', function($schedule){
                return $schedule->client->first_name . " " . $schedule->client->last_name;
            })
            ->addColumn('va_time', function($schedule){
                return date("h:i A",strtotime($schedule->user_start_time)) . " - " . date("h:i A",strtotime($schedule->user_end_time)) . " : " . $schedule->user_timezone;
            })
            ->addColumn('actions', function($schedule)use ($auth_user) {

                $delete_btn = "";
                if ($auth_user->allowed('delete_va_schedule'))
                $delete_btn = '<a data-toggle="modal" data-target="#modal-danger" class="btn btn-danger button-delete" data-id="'.$schedule->id.'"><i class="fa fa-trash"></i></a>';
                
                $edit_btn = "";
                if ($auth_user->allowed('edit_va_schedule'))
                $edit_btn = '<a class="btn btn-info" href="/dashboard/va-schedules/edit/'.$schedule->id.'"><i class="fa fa-edit"></i></a>';    

                return '<div class="btn-toolbar">'. $edit_btn .  $delete_btn .'</div>';

            })->rawColumns(['actions'])
        ->make(true); 
        }
        
        
        
        else {
            $schedules = Schedule::select(['id', 'user_id', 'day_of_week', 'start_time','end_time','shift', 'client_id', 'user_start_time', 'user_end_time', 'timezone', 'user_timezone'])->where('user_id', $id);
            $schedules->selectRaw("CASE `day_of_week` 
                                    WHEN 'Monday' THEN 1
                                    WHEN 'Tuesday' THEN 2
                                    WHEN 'Wednesday' THEN 3
                                    WHEN 'Thursday' THEN 4
                                    WHEN 'Friday' THEN 5
                                    WHEN 'Saturday' THEN 6
                                    WHEN 'Sunday' THEN 7
                                    END as num_date");

            return DataTables::of($schedules)
                ->removeColumn('id')
                ->removeColumn('user_id')
                ->removeColumn('start_time')
                ->removeColumn('end_time')
                ->removeColumn('user_start_time')
                ->removeColumn('user_end_time')
                ->removeColumn('timezone')
                ->removeColumn('user_timezone')
                ->editColumn('shift', function($schedule){
                    return $schedule->showShift();
                })
                ->addColumn('time', function($schedule){
                    return $schedule->startTimeAMPM() . " - " . $schedule->endTimeAMPM() . " : " . $schedule->timezone;
                })
                ->editColumn('client_id', function($schedule){
                    return $schedule->client->first_name . " " . $schedule->client->last_name;
                })
                ->addColumn('va_time', function($schedule){
                    return date("h:i A",strtotime($schedule->user_start_time)) . " - " . date("h:i A",strtotime($schedule->user_end_time)) . " : " . $schedule->user_timezone;
                })
                ->addColumn('actions', function($schedule)use ($auth_user) {

                    $delete_btn = "";
                    if ($auth_user->allowed('delete_va_schedule'))
                    $delete_btn = '<a data-toggle="modal" data-target="#modal-danger" class="btn btn-danger button-delete" data-id="'.$schedule->id.'"><i class="fa fa-trash"></i></a>';
                    
                    $edit_btn = "";
                    if ($auth_user->allowed('edit_va_schedule'))
                    $edit_btn = '<a class="btn btn-info" href="/dashboard/va-schedules/edit/'.$schedule->id.'"><i class="fa fa-edit"></i></a>';    

                    return '<div class="btn-toolbar">'. $edit_btn .  $delete_btn .'</div>';

                })->rawColumns(['actions'])
            ->make(true); 
        }
    }

    public function vaScheduleShow($id)
    {
        $q = Schedule::find($id);
        $html = view('admin.va-schedule.show', compact('q'))->render();
        $response['html'] = $html;
        return json_encode($response);
    }

    public function vaScheduleDelete($id)
    {
        $q = Schedule::find($id);
        if (!empty($q)) {
            ActivityLog::addLog("User [{$this->auser->first_name} {$this->auser->last_name}] deleted schedule for [{$q->user->first_name} {$q->user->last_name}].");
            $q->delete();
        }        
        $response['status'] = "ok";
        return json_encode($response);
    }

    public function vaScheduleEdit($id)
    {
        $q = Schedule::find($id);
        if (empty($q)) {
            return redirect('/dashboard/schedules');
        }
        $user = User::find($q->user_id);
        if (empty($user)) {
            return redirect('/dashboard');
        }
        $set_menu = [
            'active_menu' => "dashboard/schedules",
            'active_path' => "dashboard/va-schedules/edit/{id}",
            'active_parent' => "dashboard/va"
        ];
        return view('admin.va-schedule.create_edit', compact('q', 'user', 'set_menu'));        
    }

    public function vaScheduleUpdate(Request $request)
    {
        $request->validate([
            'start_time' => 'required',
            'hours_needed' => 'required',
            'max_breaktime' => 'break_check'
        ],[
            'break_check' => 'Break cannot be higher than required hours',
        ]);

        $q = Schedule::find($request->id);
        if (!empty($q)) {

            $target_date = new DateTime();
            $target_date->modify('next '. $q->day_of_week);

            $seconds = ($request->hours_needed * 3600);
            // we're given hours, so let's get those the easy way
            $hours = floor($request->hours_needed);
            // since we've "calculated" hours, let's remove them from the seconds variable
            $seconds -= $hours * 3600;
            // calculate minutes left
            $minutes = floor($seconds / 60);

            $start_time = new DateTime(date("Y-m-d h:i A",strtotime($target_date->format("Y-m-d"). " ".$request->start_time)), new DateTimeZone($q->client->showTimezone()));
            $end_time = new DateTime(date("Y-m-d h:i A",strtotime($target_date->format("Y-m-d"). " ".$request->start_time)), new DateTimeZone($q->client->showTimezone()));
            $end_time->modify("+".$hours." hours +".$minutes . " minutes");
            $end_time_value = $end_time->format("H:i:s");

            $start_time->setTimezone(new DateTimeZone($q->user->showTimezone()));
            $end_time->setTimezone(new DateTimeZone($q->user->showTimezone()));

            $va_sched_exist = Schedule::where("user_id", $q->user_id)
            ->where("user_day_of_week", $start_time->format('l'))
            ->where('id','<>', $q->id)
            ->get();

            if (count($va_sched_exist)) {

                foreach ($va_sched_exist as $s) {
                    if ($start_time->format("H:i:s") >= $s->user_start_time && $start_time->format("H:i:s")<= $s->user_end_time) {
                        return redirect('/dashboard/va-schedules/edit/'.$request->id)->with([
                            'notification_message'=>'Cannot update edited schedule as it will overlap with existing schedule.',
                            'alert_type' => 'alert-danger',
                            'alert_title' => 'Error'
                            ]);
                    }
                }

            }

            $user_day_of_week_end = null;
            if ($start_time->format('l')!=$end_time->format('l')) {
                $user_day_of_week_end = $end_time->format('l');
            }

            $q->start_time = date("H:i:s",strtotime($request->start_time));
            $q->end_time = $end_time_value;
            $q->max_breaktime = $request->max_breaktime;
            $q->paid_break = $request->paid_break;
            $q->hours_needed = $request->hours_needed;
            $q->user_start_time = $start_time->format("H:i:s");
            $q->user_end_time = $end_time->format("H:i:s");
            $q->user_timezone = $q->user->timezone;
            $q->user_day_of_week = $start_time->format('l');
            $q->user_day_of_week_end = $user_day_of_week_end;
            $q->save();

            ActivityLog::addLog("User [{$this->auser->first_name} {$this->auser->last_name}] updated schedule for [{$q->user->first_name} {$q->user->last_name}].");

        }

        return redirect()->intended('/dashboard/va-schedules/edit/'. $q->id)->with([
            'notification_message'=> 'Schedule has been updated.',
            'alert_type' => 'alert-success',
            'alert_title' => 'Success'
            ]); 
    }     
}