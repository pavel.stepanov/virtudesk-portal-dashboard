<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use App\Models\User;
use App\Models\ClientBilling;
use App\Models\ActivityLog;
use App\Models\Timesheet;
use App\Models\Schedule;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;
use PDF;

class BillsController extends Controller
{
    public function index()
    {
        return view('admin.bills.index');
    }

    public function datatables()
    {
        $clients = User::HasRole('client')->select(['id','first_name', 'last_name']);

        return DataTables::of($clients)
            ->addColumn('actions', function($user){
                $bills_btn = '<a class="btn btn-info" href="/dashboard/bills/client/'.$user->id.'"><i class="fa fa-money"></i></a>';
                return $bills_btn;
        })->rawColumns(['actions'])
        ->make(true); 
    }

    public function clientBills($id)
    {
        $client = User::find($id);
        //get all VAs under this client
        $vas = Schedule::where('client_id', $id)
        ->pluck('user_id')->all();

        $users = User::HasRole('va')->orderBy('first_name')->whereIn('id', $vas)->get();

        return view('admin.bills.client', compact('client', 'users'));

    }

    public function generateBills(Request $request)
    {
        $timesheet = Timesheet::groupBy(['target_date', 'user_id'])
        ->whereIn('user_id', $request->va_id)
        ->where('client_id', $request->client_id)
        ->where('status','approved')
        ->whereBetween('schedule_date', [$request->start_date, $request->end_date])
        //->whereHas('attendance', function($query) use ($request) {
        //    $query->whereBetween('schedule_date', [$request->start_date, $request->end_date]);
        //})
        ->orderBy('u.first_name')
        ->orderBy('target_date')
        ->selectRaw('*,u.first_name, sum(billable_hours) as total_billable_hours')
        ->join('users as u', 'u.id','=','user_id')
        ->get();

        $advance_payments = $request->advance_payments;
        $setup_fee = $request->setup_fee;
        $credits = $request->credits;
        $credit_text = $request->credit_text;
        $tax = $request->tax;
        $client = User::find($request->client_id);
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $html = view("admin.bills.content", compact(
            'timesheet', 
            'advance_payments', 
            'tax', 
            'client', 
            'start_date', 
            'end_date', 
            'setup_fee',
            'credits',
            'credit_text'
            ))->render();

        $response['status'] = "ok";
        $response['html'] = $html;
        return json_encode($response);
    }

    public function downloadPDF(Request $request) 
    {

        $timesheet = Timesheet::groupBy(['target_date', 'user_id'])
        ->whereIn('user_id', $request->va_id)
        ->where('client_id', $request->client_id)
        ->where('status','approved')
        ->whereBetween('schedule_date', [$request->start_date, $request->end_date])
        //->whereHas('attendance', function($query) use ($request) {
        //    $query->whereBetween('schedule_date', [$request->start_date, $request->end_date]);
        //})
        ->orderBy('u.first_name')
        ->orderBy('target_date')
        ->selectRaw('*,u.first_name, sum(billable_hours) as total_billable_hours')
        ->join('users as u', 'u.id','=','user_id')
        ->get();

        $advance_payments = $request->advance_payments;
        $tax = $request->tax;
        $client = User::find($request->client_id);
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $invoice_number = $request->invoice_number;
        $setup_fee = $request->setup_fee;
        $credits = $request->credits;
        $credit_text = $request->credit_text;

        $overall_total = 0;
        foreach ($timesheet as $ts) {
            $rate = $ts->client->getRateBilling($ts->user_id);
            $total = $ts->total_billable_hours * $rate;
            $overall_total = $overall_total + $total;
        }

        $overall_total = $overall_total + $setup_fee;

        $total_tax = $overall_total * ($tax/100);
        $amount_due = ( $overall_total + $total_tax ) - $advance_payments - $credits;

        $status = 'unpaid';
        if ($amount_due<0) $status = 'advance';

        $cb = ClientBilling::create([
            'client_id' => $request->client_id,
            'date_generated' => date("Y-m-d",time()),
            'billing_period' => $start_date . ' - ' . $end_date,
            'invoice_number' => $invoice_number,
            'pre_payment' => $advance_payments,
            'taxes_due' => $total_tax,
            'total_billable_hours' => $overall_total,
            'total_amount_due' => $amount_due,
            'status' => $status,
            'email_status' => "unsent"
        ]);

        $pdf = PDF::loadView("admin.bills.pdf", compact(
            'timesheet', 
            'advance_payments', 
            'tax', 
            'client', 
            'start_date', 
            'end_date', 
            'invoice_number', 
            'setup_fee',
            'credits',
            'credit_text'
        ));

        $path = public_path() . "/invoices/" .$request->client_id;

        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }

        $client = User::find($request->client_id);
        $pdf_filename = strtolower("invoice-" . $client->first_name . "-" . $client->last_name . "-" . bin2hex(random_bytes(10)) . "-" . date("Y-m-d") . ".pdf");
        
        $cb->pdf_path = "/invoices/" .$request->client_id . "/" . $pdf_filename;
        $cb->save();

        return $pdf->save($path .'/'. $pdf_filename)
        ->download($pdf_filename);
    }
}