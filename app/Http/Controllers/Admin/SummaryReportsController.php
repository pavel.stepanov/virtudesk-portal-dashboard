<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\UserTask;
use App\Models\Timesheet;
use App\Models\Schedule;
use App\Models\Team;
use App\Models\TeamUser;
use PDF;
use App\Models\ManagerClient;

class SummaryReportsController extends Controller
{
    public function index()
    {

        $auth_user = \Auth::user();

        if ($auth_user->is('client')) {

            //get all VAs under this client
            $vas_pluck = Schedule::where('client_id', $auth_user->id)
            ->pluck('user_id')->all();

            $vas = User::HasRole('va')->orderBy('first_name')
            ->whereIn('id', $vas_pluck)->get();

            return va_view('admin.summary-reports.client-index', compact('vas'));

        }

        if ($auth_user->is('manager')) {
            
           $clients = ManagerClient::where('user_id', $auth_user->id)->pluck('client_id');
           $vas = Schedule::whereIn('client_id', $clients)
                ->pluck('user_id')->all();

            //get all VAs under this manager
            //$team = Team::where("lead_user_id", $auth_user->id)->first();
            //$vas_pluck = null;
            //if ($team->id) {
                //$vas_pluck = TeamUser::where("team_id", $team->id)->pluck('user_id')->all();
            //}

            $vas = User::HasRole('va')->select(['id','first_name', 'last_name'])
           // ->whereIn('id', $vas_pluck)->get();
           ->whereIn('id', $vas)->get();

            return view('admin.summary-reports.index', compact('vas'));

        }

        $vas = User::HasRole('va')->orderBy('first_name')->get();

        return view('admin.summary-reports.index', compact('vas'));
    }

    public function generateSummaryReports(Request $request)
    {
        $auth_user = \Auth::user();

        if ($auth_user->is('client') || $auth_user->is('manager')) {
            $client_id = $auth_user->id;
            $timesheets = Timesheet::whereBetween('target_date',[$request->start_date, $request->end_date])
            ->whereIn('user_id', $request->va_id)
            ->where('status', 'approved')
            ->where('client_id', $client_id)
            ->leftJoin('users','users.id', '=', 'timesheets.user_id')
            ->orderBy('users.first_name', 'ASC')
            ->orderBy('target_date')
            ->select('timesheets.id', 'target_date','user_id', 'client_id', 'total_time', 'total_task_time', 'total_breaktime', 'total_idletime', 'billable_hours')
            ->get();
        
        } else {
            $timesheets = Timesheet::whereBetween('target_date',[$request->start_date, $request->end_date])
            ->whereIn('user_id', $request->va_id)
            ->where('status', 'approved')
            ->leftJoin('users','users.id', '=', 'timesheets.user_id')
            ->orderBy('users.first_name', 'ASC')
            ->orderBy('target_date')
            ->select('timesheets.id', 'target_date','user_id', 'client_id', 'total_time', 'total_task_time', 'total_breaktime', 'total_idletime', 'billable_hours')
            ->get();
        }    

        if (count($timesheets)<=0) {
            $html = "<p>No timesheet records found.</p>";
        } else {
            $html = view("admin.summary-reports.content", compact('timesheets'))
            ->render();
        }

        $response['status'] = "ok";
        $response['html'] = $html;
        return json_encode($response);
    }

    public function generatePDF(Request $request)
    {

        $auth_user = \Auth::user();

        if ($auth_user->is('client') || $auth_user->is('manager')) {
            $client_id = $auth_user->id;
            $timesheets = Timesheet::whereBetween('target_date',[$request->start_date, $request->end_date])
            ->whereIn('user_id', $request->va_id)
            ->where('status', 'approved')
            ->where('client_id', $client_id)
            ->leftJoin('users','users.id', '=', 'timesheets.user_id')
            ->orderBy('users.first_name', 'ASC')
            ->orderBy('target_date')
            ->select('timesheets.id', 'target_date','user_id', 'client_id', 'total_time', 'total_task_time', 'total_breaktime', 'total_idletime', 'billable_hours')
            ->get();
        } else {
            $timesheets = Timesheet::whereBetween('target_date',[$request->start_date, $request->end_date])
            ->whereIn('user_id', $request->va_id)
            ->where('status', 'approved')
            ->leftJoin('users','users.id', '=', 'timesheets.user_id')
            ->orderBy('users.first_name', 'ASC')
            ->orderBy('target_date')
            ->select('timesheets.id', 'target_date','user_id', 'client_id', 'total_time', 'total_task_time', 'total_breaktime', 'total_idletime', 'billable_hours')
            ->get();
        }

        $start_date = $request->start_date;
        $end_date = $request->end_date;

        if (count($timesheets)>0) {
            $pdf = PDF::loadView("admin.summary-reports.pdf", compact('timesheets', 'start_date', 'end_date' ));
        
            return $pdf->download('summaryreport.pdf');
        }

        return "No Timesheet";
    }

}