<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use App\Models\User;
use App\Models\Team;
use Illuminate\Validation\Rule;
use App\Models\ActivityLog;

class TeamController extends Controller
{

    private $auser;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->auser = \Auth::user();
            return $next($request);
        });
    }

    public function index()
    {
        return view('admin.team.index');
    }

    public function create()
    {
        //$available_va = Team::get_available_va();
        $available_manager = Team::getAvailableManager();
        return view('admin.team.create_edit', compact('available_manager'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:100',
            'lead_user_id' => 'required',
        ]);

        $team = Team::create([
            'name' => $request->name,
            'lead_user_id' => $request->lead_user_id,
        ]);

        $team->addUser( $request->lead_user_id );

        ActivityLog::addLog("User [{$this->auser->first_name} {$this->auser->last_name}] created a new team [{$request->name}].");

        return redirect('/dashboard/teams/edit/' . $team->id)->with('notification_message', 'Team has been created.'); 
    } 

    public function edit( $id )
    {
        $q = Team::find( $id );
        if ( empty( $q ) ) {
            return redirect( '/dashboard' );            
        }

        $available_manager = Team::getAvailableManager($q->lead_user_id);
        return view('admin.team.create_edit', compact('q', 'available_manager'));        
    }

    public function update(Request $request) 
    {
        $q = Team::find($request->id);

        $request->validate([
            'name' => 'required|max:100',
            'lead_user_id' => 'required',
        ]);

        if (!empty($q)) {
            $q->name = $request->name;
            $q->lead_user_id = $request->lead_user_id;
            $q->save();

            ActivityLog::addLog("User [{$this->auser->first_name} {$this->auser->last_name}] updated team [{$request->name}].");
        }

        return redirect()->intended('/dashboard/teams/edit/'. $q->id)->with('notification_message', 'Team has been updated.'); 

    }  

    public function show($id)
    {
        $q = Team::find($id);
        $html = view('admin.team.show', compact('q'))->render();
        $response['html'] = $html;
        return json_encode($response);
    }

    public function delete($id)
    {
        $q = Team::find($id);
        if (!empty($q)) {
            ActivityLog::addLog("User [{$this->auser->first_name} {$this->auser->last_name}] deleted team [{$q->name}].");
            $q->delete();
        }        
        $response['status'] = "ok";
        return json_encode($response);
    }

    public function datatables()
    {

    	$teams = Team::select(DB::raw("teams.id, name, CONCAT( first_name, ' ', last_name ) AS lead_user"))
                    ->join('users', 'teams.lead_user_id', 'users.id');

        return DataTables::of($teams)
            ->addColumn('team_count', function($team){
                $team_user_count = DB::table('team_user')->where('team_id', $team->id)->count();
                return $team_user_count;
            })
            ->addColumn('client_count', function($team){
                $team_info = DB::table('teams')->where('id', $team->id)->first();
                if (!empty($team_info)) {
                    $client_count = DB::table('manager_client')->where('user_id', $team_info->lead_user_id)->count();
                } else {
                    $client_count = 0;
                }
                return $client_count;
            })
            ->addColumn('actions', function($team){
                $team_info_btn = '<a data-toggle="modal" title="Show Team Members" data-target="#team-members" class="btn btn-success button-team-members" data-id="'.$team->id.'"><i class="fa fa-info"></i></a>';
                $team_add_btn = '<a data-toggle="modal" title="Add Team Member" data-target="#team-add-member" class="btn btn-info button-team-add-members-modal" data-id="'.$team->id.'"><i class="fa fa-plus"></i></a>'; 
                $delete_btn = '<a data-toggle="modal" title="Delete Team" data-target="#modal-danger" class="btn btn-danger button-delete" data-id="'.$team->id.'"><i class="fa fa-trash"></i></a>';
                $edit_btn = '<a class="btn btn-info" title="Edit Team" href="/dashboard/teams/edit/'.$team->id.'"><i class="fa fa-edit"></i></a>';    

                return '<div class="btn-toolbar">'. $team_info_btn . $team_add_btn . $edit_btn .  $delete_btn .'</div>';

        })->rawColumns(['actions'])
        ->make(true);        

    }


    function show_team_members($id) {
        $q = Team::find($id);

        $html = view('admin.team.team-members', compact('q'))->render();
        $response['team_name'] = $q->name;
        $response['html'] = $html;
        return json_encode($response);
    }

    function show_available_va($id) {
        $q = Team::get_available_va($id);
        $html = view('admin.team.team-members-add', compact('q', 'id'))->render();
        $response['va_count'] = count($q);
        $response['html'] = $html;
        return json_encode($response);
    }

    function team_member_add(Request $request) {
        $q = Team::find($request->team_id);
        $q->addUser( $request->va_id );
        $response['html'] = 'Team member has been added';
        return json_encode($response); 
    }

    function team_member_remove(Request $request) {
        $q = Team::find($request->team_id);
        $q->removeUser( $request->va_id );
        $response['html'] = 'Team member has been removed';
        return json_encode($response); 
    }

}