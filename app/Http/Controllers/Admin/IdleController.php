<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Idle;


class IdleController extends Controller
{
    public function index()
    {
        $vas_idle = Idle::where('idle_start', '!=', 'NULL')
        ->whereNull('idle_stop')->get();

        return view('admin.idle.index', compact('vas_idle'));
    }

}