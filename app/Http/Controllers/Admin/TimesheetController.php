<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use App\Models\Timesheet;
use App\Models\User;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;
use App\Models\ActivityLog;

class TimesheetController extends Controller
{

    private $auser;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->auser = \Auth::user();
            return $next($request);
        });
    }

    public function index()
    {
        return view('admin.timesheet.index');
    }

    public function create()
    {
        $clients = User::HasRole('client')
        ->orderBy('first_name')
        ->get();

        $vas = User::HasRole('va')
        ->orderBy('first_name')
        ->get();
        return view('admin.timesheet.create', compact('clients', 'vas'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'target_date' => 'required',
            'schedule_date' => 'required',
            'client_id' => 'required',
            'user_id' => 'required',
            'billable_hours' => 'required',
            'status' => 'required'
        ]);

        $ts = Timesheet::create([
            'target_date' => $request->target_date,
            'schedule_date' => $request->schedule_date,
            'client_id' => $request->client_id,
            'user_id' => $request->user_id,
            'billable_hours' => $request->billable_hours,
            'attendance_id' => 0,
            'total_time' => "00:00:00",
            'total_task_time' => "00:00:00",
            'total_idletime' => "00:00:00",
            'total_breaktime' => "00:00:00",
            'status' => $request->status
        ]);

        $va = User::find($request->user_id);

        ActivityLog::addLog("User [{$this->auser->fullname}] created a new timesheet for [{$va->fullname}].");

        return redirect('/dashboard/timesheets/edit/' . $ts->id)->with('notification_message', 'Timesheet has been created.'); 
    } 

    public function edit($id)
    {
        $set_menu = [
            'active_menu' => "dashboard/timesheets",
            'active_path' => "dashboard/timesheets/edit/{id}",
            'active_parent' => "Tools"
        ];

        $q = Timesheet::find($id);

        if (empty($q)) {
            return redirect('/dashboard');
        }
        return view('admin.timesheet.edit', compact('q','set_menu'));
    }

    public function update(Request $request)
    {
        $q = Timesheet::find($request->id);

        $request->validate([
            'billable_hours' => 'required|numeric',
            'status' => 'required'
        ]);

        if (!empty($q)) {
            if (isset($request->target_date)) $q->target_date = $request->target_date;
            if (isset($request->schedule_date)) $q->schedule_date = $request->schedule_date;
            $q->billable_hours = $request->billable_hours;
            $q->status = $request->status;
            $q->save();
        }

        return redirect()->intended('/dashboard/timesheets/edit/'. $q->id)->with('notification_message', 'Timesheet has been updated.'); 

    }

    public function datatables(Request $request)
    {
        $user = \Auth::user();

        $firstDate = $request->firstDate;
        $lastDate = $request->lastDate;

        $status = "pending";
        if (isset($request->status)) {
            $status = $request->status;
        }

        $timesheet = Timesheet::where('status', $status)
        ->distinct('user_id')
        ->groupBy('user_id')
        ->whereBetween('target_date',[$firstDate, $lastDate])
        ->select('id','target_date','user_id', 'status', DB::raw("SUM(billable_hours) as total_billable_hours"));

        return DataTables::of($timesheet)
            ->removeColumn('status')
            ->editColumn('user_id', function($ts){
                return $ts->user->first_name . " " . $ts->user->last_name;
            })
            ->addColumn('actions', function($ts) use ($firstDate, $lastDate) {
                //$delete_btn = '<a data-toggle="modal" data-target="#modal-danger" class="btn btn-danger button-delete" data-id="'.$ts->id.'"><i class="fa fa-trash"></i></a>';
                $info_btn = '<a class="btn btn-info" href="/dashboard/timesheets/info/'.$ts->user_id.'?firstDate='.$firstDate.'&lastDate='.$lastDate.'"><i class="fa fa-info"></i></a>';
                return '<div class="btn-toolbar">'. $info_btn .'</div>';
            
        })->rawColumns(['actions'])
        ->make(true);

    }

    public function info(Request $request, $user_id)
    {   
        $firstDate = null;
        $lastDate = null;
        if ($request->firstDate!='' && $request->lastDate!='') {
            $firstDate = $request->firstDate;
            $lastDate = $request->lastDate;
        }

        $set_menu = [
            'active_menu' => "dashboard/timesheets",
            'active_path' => "dashboard/timesheets/info/{id}",
            'active_parent' => "Tools"
        ];

        $user = User::find($user_id);
        return view('admin.timesheet.info', compact('user','set_menu','firstDate', 'lastDate'));
    }

    public function info_datatables(Request $request, $id)
    {
        $user = \Auth::user();

        $firstDate = $request->firstDate;
        $lastDate = $request->lastDate;

        $timesheet = Timesheet::where('user_id', $id)
        ->whereBetween('target_date',[$firstDate, $lastDate])
        ->select('id','target_date','client_id', 'total_time', 'billable_hours', 'status');

        return DataTables::of($timesheet)
            ->removeColumn('id')
            ->editColumn('client_id', function($ts){
                return $ts->client->first_name . " " . $ts->client->last_name;
            })
            ->editColumn('status', function($ts){
                return ucwords($ts->status);
            })
            ->addColumn('actions', function($ts){
                $delete_btn = '<a data-toggle="modal" data-target="#modal-danger" class="btn btn-danger button-delete" data-id="'.$ts->id.'"><i class="fa fa-trash"></i></a>';
                $status_btn = '<a data-toggle="modal" title="Approve or Decline" data-target="#modal-status" class="btn btn-success button-status" data-id="'.$ts->id.'"><i class="fa fa-check-square-o"></i></a>';
                $edit_btn = '<a class="btn btn-info" title="Edit Timesheet" href="/dashboard/timesheets/edit/'.$ts->id.'"><i class="fa fa-edit"></i></a>';
                return '<div class="btn-toolbar">'. $edit_btn  . $status_btn . $delete_btn . '</div>';
            
        })->rawColumns(['actions'])
        ->make(true);

    }

    public function show($id) {
        $q = Timesheet::find($id);
        $html = view('admin.timesheet.show', compact('q'))->render();
        $response['html'] = $html;
        return json_encode($response);
    }

    public function delete($id)
    {
        $q = Timesheet::find($id);
        if (!empty($q)) {
            $q->delete();
        }
        $response['status'] = "ok";
        return json_encode($response);
    }

    public function changeStatus(Request $request, $status) 
    {
        $q = Timesheet::find($request->id);
        if ($status == 'approve') {
            $q->status =  'approved';
            $q->save();
        }

        if ($status == 'decline') {
            $q->status =  'declined';
            $q->save();
        }

        $response['status'] = $status;
        return json_encode($response);
    }
}