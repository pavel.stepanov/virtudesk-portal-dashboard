<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Attendance;
use PDF;

class AttendanceReportsController extends Controller
{
    public function index()
    {
        $vas = User::HasRole('va')->orderBy('first_name')->get();

        return view('admin.attendance-reports.index', compact('vas'));
    }

    public function generateReports(Request $request)
    {
        $typepick = $request->typepick;
        if ($typepick=='present') {
            $attendances = Attendance::whereBetween('date_in',[$request->start_date, $request->end_date])
            ->whereIn('user_id', $request->va_id)
            ->where('status',"<>","in")
            ->leftJoin('users','users.id', '=', 'attendance.user_id')
            ->orderBy('users.first_name', 'ASC')
            ->orderBy("date_in")
            ->orderBy("shift")
            ->select('attendance.id', 'user_id', 'client_id', 'date_in', 'date_end',  'time_in',  'time_end', 'total_time', 'total_idletime', 'total_breaktime', 'max_breaktime', 'late_time')
            ->get();
        }

        if ($typepick=='late') {
            $attendances = Attendance::whereBetween('date_in',[$request->start_date, $request->end_date])
            ->whereIn('user_id', $request->va_id)
            ->where('status',"<>","in")
            ->where('late_time',"<>","00:00:00")
            ->leftJoin('users','users.id', '=', 'attendance.user_id')
            ->orderBy('users.first_name', 'ASC')
            ->orderBy("date_in")
            ->orderBy("shift")
            ->select('attendance.id', 'user_id', 'client_id', 'date_in', 'date_end',  'time_in',  'time_end', 'total_time', 'total_idletime', 'total_breaktime', 'max_breaktime', 'late_time')
            ->get();
        }

        if ($typepick=='overbreak') {
            $attendances = Attendance::whereBetween('date_in',[$request->start_date, $request->end_date])
            ->whereIn('user_id', $request->va_id)
            ->where('status',"<>","in")
            ->whereRaw('max_breaktime < total_breaktime')
            ->leftJoin('users','users.id', '=', 'attendance.user_id')
            ->orderBy('users.first_name', 'ASC')
            ->orderBy("date_in")
            ->orderBy("shift")
            ->select('attendance.id', 'user_id', 'client_id', 'date_in', 'date_end',  'time_in',  'time_end', 'total_time', 'total_idletime', 'total_breaktime', 'max_breaktime', 'late_time')
            ->get();
        }

        if ($typepick=='idle') {
            $attendances = Attendance::whereBetween('date_in',[$request->start_date, $request->end_date])
            ->whereIn('user_id', $request->va_id)
            ->where('status',"<>","in")
            ->where('total_idletime','<>', '00:00:00')
            ->leftJoin('users','users.id', '=', 'attendance.user_id')
            ->orderBy('users.first_name', 'ASC')
            ->orderBy("date_in")
            ->orderBy("shift")
            ->select('attendance.id', 'user_id', 'client_id', 'date_in', 'date_end',  'time_in',  'time_end', 'total_time', 'total_idletime', 'total_breaktime', 'max_breaktime', 'late_time')
            ->get();
        }

        if (count($attendances)<=0) {
            $html = "<p>No attendance records found.</p>";
        } else {
            $html = view("admin.attendance-reports.content", compact('attendances', 'typepick'))
            ->render();
        }

        $response['status'] = "ok";
        $response['html'] = $html;
        return json_encode($response);
    }

    public function generatePDF(Request $request)
    {
        $typepick = $request->typepick;
        if ($typepick=='present') {
            $attendances = Attendance::whereBetween('date_in',[$request->start_date, $request->end_date])
            ->whereIn('user_id', $request->va_id)
            ->where('status',"<>","in")
            ->leftJoin('users','users.id', '=', 'attendance.user_id')
            ->orderBy('users.first_name', 'ASC')
            ->orderBy("date_in")
            ->orderBy("shift")
            ->select('attendance.id', 'user_id', 'client_id', 'date_in', 'date_end',  'time_in',  'time_end', 'total_time', 'total_idletime', 'total_breaktime', 'max_breaktime', 'late_time')
            ->get();
        }

        if ($typepick=='late') {
            $attendances = Attendance::whereBetween('date_in',[$request->start_date, $request->end_date])
            ->whereIn('user_id', $request->va_id)
            ->where('status',"<>","in")
            ->where('late_time',"<>","00:00:00")
            ->leftJoin('users','users.id', '=', 'attendance.user_id')
            ->orderBy('users.first_name', 'ASC')
            ->orderBy("date_in")
            ->orderBy("shift")
            ->select('attendance.id', 'user_id', 'client_id', 'date_in', 'date_end',  'time_in',  'time_end', 'total_time', 'total_idletime', 'total_breaktime', 'max_breaktime', 'late_time')
            ->get();
        }

        if ($typepick=='overbreak') {
            $attendances = Attendance::whereBetween('date_in',[$request->start_date, $request->end_date])
            ->whereIn('user_id', $request->va_id)
            ->where('status',"<>","in")
            ->whereRaw('max_breaktime < total_breaktime')
            ->leftJoin('users','users.id', '=', 'attendance.user_id')
            ->orderBy('users.first_name', 'ASC')
            ->orderBy("date_in")
            ->orderBy("shift")
            ->select('attendance.id', 'user_id', 'client_id', 'date_in', 'date_end',  'time_in',  'time_end', 'total_time', 'total_idletime', 'total_breaktime', 'max_breaktime', 'late_time')
            ->get();
        }

        if ($typepick=='idle') {
            $attendances = Attendance::whereBetween('date_in',[$request->start_date, $request->end_date])
            ->whereIn('user_id', $request->va_id)
            ->where('status',"<>","in")
            ->where('total_idletime','<>', '00:00:00')
            ->leftJoin('users','users.id', '=', 'attendance.user_id')
            ->orderBy('users.first_name', 'ASC')
            ->orderBy("date_in")
            ->orderBy("shift")
            ->select('attendance.id', 'user_id', 'client_id', 'date_in', 'date_end',  'time_in',  'time_end', 'total_time', 'total_idletime', 'total_breaktime', 'max_breaktime', 'late_time')
            ->get();
        }

        $start_date = $request->start_date;
        $end_date = $request->end_date;

        if (count($attendances)>0) {
            $pdf = PDF::loadView("admin.attendance-reports.pdf", compact('attendances', 'start_date', 'end_date', 'typepick'));
        
            return $pdf->download('attendance-report.pdf');
        }

        return "No Attendace";
    }
}