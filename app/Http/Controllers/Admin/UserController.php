<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use App\Models\User;
use App\Models\UserProfile;
use App\Models\Role;
use App\Models\Schedule;
use App\Models\ClientTask;
use App\Models\Task;
use App\Models\UserTask;
use App\Models\ClientUser;
use Illuminate\Validation\Rule;
use Image;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use App\Models\PlanSubscription;
use App\Models\ActivityLog;
//use Srmklive\PayPal\Services\ExpressCheckout;
//use Srmklive\PayPal\Services\AdaptivePayments;
//use App\Http\Controllers\Admin\PaypalController;

use App\Models\Team;
use App\Models\TeamUser;

class UserController extends Controller
{
  public function index()
  {
    return view('admin.user.index');
  }

  public function create()
  {
    $roles = Role::all();
    $timezone = config('vatimetracker.timezone');
    return view('admin.user.create_edit', compact('roles','timezone'));
  }

  public function store(Request $request)
  {

    $request->validate([
      'role' => [
        'required',
        Rule::notIn(['Select Role']),
      ],
      'email' => 'required|email|unique:users',
      'first_name' => 'required|max:100',
      'last_name' => 'required|max:100',
      'mobile_number' => 'required|max:100',
      'password' => 'required',
      'timezone' => 'required',
      'skype_id' => 'max:64',
      'address1' => 'max:255',
      'address2' => 'max:255',
      'personal_email' => 'max:100',
      'alternate_contact_number' => 'max:100',
      'city' => 'max:50',
      'confirm_password' => 'required|same:password'
    ]);

    $user = User::create([
      'email' => $request->email,
      'first_name' => $request->first_name,
      'last_name' => $request->last_name,
      'mobile_number' => $request->mobile_number,
      'skype_id' => $request->skype_id,
      'personal_email' => $request->personal_email,
      'alternate_contact_number' => $request->alternate_contact_number,
      'birthdate' => $request->birthdate,
      'address1' => $request->address1,
      'address2' => $request->address2,
      'city' => $request->city,
      'is_verified' => 1,
      'is_active' => 1,
      'timezone' => $request->timezone,
      'screenshot_interval' => $request->screenshot_interval,
      'idle_interval' => $request->idle_interval,
      'password' => bcrypt($request->password)
    ]);

    $user->addRole($request->role);

    return redirect('/dashboard/users/edit/'.$user->id)->with('notification_message', 'User has been created.');
  }

  public function show($id)
  {
    $q = User::find($id);
    $html = view('admin.user.show', compact('q'))->render();
    $response['html'] = $html;
    return json_encode($response);
  }

  public function delete($id)
  {
    $q = User::find($id);
    if (!empty($q)) {
      $q->delete();

      DB::table('attendance')->where('user_id',$id)->delete();
      DB::table('client_tasks')->where('va_id',$id)->delete();
      DB::table('client_tasks')->where('client_id',$id)->delete();
      DB::table('dispute')->where('user_id',$id)->delete();
      DB::table('idles')->where('user_id',$id)->delete();
      DB::table('process_lists')->where('user_id',$id)->delete();
      DB::table('rates')->where('user_id',$id)->delete();
      DB::table('rates_log')->where('user_id',$id)->delete();
      DB::table('reports')->where('user_id',$id)->delete();
      DB::table('requests')->where('user_id',$id)->delete();
      DB::table('schedules')->where('user_id',$id)->delete();
      DB::table('schedules')->where('client_id',$id)->delete();
      DB::table('screenshots')->where('user_id',$id)->delete();
      DB::table('team_user')->where('user_id',$id)->delete();
      DB::table('teams')->where('lead_user_id',$id)->delete();
      DB::table('time_in_out')->where('user_id',$id)->delete();
      DB::table('timesheets')->where('user_id',$id)->delete();
      DB::table('timesheets')->where('client_id',$id)->delete();
      DB::table('user_tasks')->where('user_id',$id)->delete();
      DB::table('user_tasks')->where('client_id',$id)->delete();
      DB::table('client_user')->where('client_id',$id)->delete();
      DB::statement('SET FOREIGN_KEY_CHECKS=0;');
      DB::table('role_user')->where('user_id',$id)->delete();
      DB::table('manager_client')->where('client_id',$id)->delete();
      DB::statement('SET FOREIGN_KEY_CHECKS=1;');

    }
    $response['status'] = "ok";
    return json_encode($response);
  }

  public function edit($id)
  {
    $q = User::find($id);
    $roles = Role::all();
    $timezone = config('vatimetracker.timezone');
    if (empty($q)) {
      return redirect('/dashboard');
    }
    return view('admin.user.create_edit', compact('q','roles','timezone'));
  }

  public function update(Request $request)
  {
    $q = User::find($request->id);

    $request->validate([
      'role' => [
        'required',
        Rule::notIn(['Select Role']),
      ],
      'email' => [
        'required',
        Rule::unique('users')->ignore($q->id),
      ],
      'first_name' => 'required|max:100',
      'last_name' => 'required|max:100',
      'mobile_number' => 'required|max:100',
      'timezone' => 'required',
      'skype_id' => 'max:64',
      'address1' => 'max:255',
      'address2' => 'max:255',
      'personal_email' => 'max:100',
      'alternate_contact_number' => 'max:100',
      'city' => 'max:50',
    ]);

    if (!empty($q)) {
      $q->first_name = $request->first_name;
      $q->last_name = $request->last_name;
      $q->email = $request->email;
      $q->mobile_number = $request->mobile_number;
      $q->skype_id = $request->skype_id;
      $q->personal_email = $request->personal_email;
      $q->alternate_contact_number = $request->alternate_contact_number;
      $q->birthdate = $request->birthdate;
      $q->address1 = $request->address1;
      $q->address2 = $request->address2;
      $q->city = $request->city;
      $q->timezone = $request->timezone;
      $q->screenshot_interval = $request->screenshot_interval;
      $q->idle_interval = $request->idle_interval;

      if (!isset($request->is_active)) {
        $q->is_active = 0;
      } else {
        $q->is_active = 1;
      }

      if ($request->password!='') {
        $q->password = bcrypt($request->password);
      }

      $q->save();

      $q->removeAllRoles();
      $q->addRole($request->role);
    }

    return redirect()->intended('/dashboard/users/edit/'. $q->id)->with('notification_message', 'User has been updated.');

  }


  public function datatables()
  {

    $users = User::select(['id','first_name', 'last_name','email','mobile_number']);

    return DataTables::of($users)
    ->addColumn('role', function($user){
      return $user->roles()->first()->name;
    })
    ->addColumn('actions', function($user){

      $delete_btn = '<a data-toggle="modal" data-target="#modal-danger" class="btn btn-danger button-delete" data-id="'.$user->id.'"><i class="fa fa-trash"></i></a>';
      $edit_btn = '<a class="btn btn-info" href="/dashboard/users/edit/'.$user->id.'"><i class="fa fa-edit"></i></a>';
      //newton
      $logout_btn = '<a title="sign out user" class="btn btn-warning" href="/dashboard/user/logout_user/'.$user->id.'"><i class="fa fa-sign-out"></i></a>';  

      if ($user->id!=1) {
        return '<div class="btn-toolbar">'. $edit_btn.$delete_btn .$logout_btn.'</div>';
      } else {
        return '<div class="btn-toolbar">'. $edit_btn.$logout_btn . '</div>';
      }

    })->rawColumns(['actions'])
    ->make(true);

  }
  
  public function logout_user($id){
        $logout_user = User::whereId($id)->first();
        $session_id = \DB::table('sessions')->where('user_id', $id)->delete();       
        
        ActivityLog::addLog("User [{$logout_user->first_name} {$logout_user->last_name}] was logged out by Admin.");
        
        return redirect()->back();
    }
  

  public function profile()
  {
    $q = \Auth::user();

    if ($q->avatar != null) {
      $avatar = "/uploads/avatars/" . $q->avatar;
    } else {
      $avatar = "//www.gravatar.com/avatar/" . md5(\Auth::user()->email) . "?s=128&d=mm";
    }

    $auth_user = \Auth::user();
    if ($auth_user->is('client')) {
      return va_view('admin.user.client-profile', compact('q', 'avatar'));
    }

    return view('admin.user.profile', compact('q', 'avatar'));
  }

  public function showProfile($id)
  {

    $auth_user = \Auth::user();

    if ($auth_user->is('client')) {
      //get all VAs under this client
      $vas = Schedule::where('client_id', $auth_user->id)
      ->pluck('user_id')->all();

      //$user = User::HasRole('va')->where('id', $id)->whereIn('id', $vas)->first();
      $user = User::HasRole('va')->where('id', $id)->first();

    } else {
      $user = User::HasRole('va')->where('id', $id)->first();
    }

    if (empty($user)) {
      return redirect('/dashboard');
    }

    if ($user->avatar != null) {
      $avatar = "/uploads/avatars/" . $user->avatar;
    } else {
      $avatar = "//www.gravatar.com/avatar/" . md5(\Auth::user()->email) . "?s=128&d=mm";
    }

    if (empty($user->profile)) {
      UserProfile::insert(
        ['user_id' => $user->id]
      );
      $user = User::HasRole('va')->where('id', $id)->first();
    }

    if ($auth_user->is('client')) {
      return va_view('admin.user.client-show-profile', compact('user', 'avatar'));
    }

    return view('admin.user.show-profile', compact('user', 'avatar'));

  }


  public function updateProfile(Request $request)
  {
    $q = \Auth::user();
    $request->validate([
      'email' => [
        'required',
        Rule::unique('users')->ignore($q->id),
      ],
      'first_name' => 'required|max:100',
      'last_name' => 'required|max:100',
    ]);

    if (!empty($q)) {

      if ($request->current_password!='') {

        \Validator::extend('old_password', function ($attribute, $value, $parameters, $validator) {
          return \Hash::check($value, current($parameters));
        }, "Current password is incorrect");

        $request->validate([
          'current_password' => 'required|old_password:' . \Auth::user()->password,
          'new_password' => 'required',
          'confirm_password' => 'required|same:new_password'
        ]);

        $q->password = bcrypt($request->new_password);
      }

      if($request->hasFile('avatar')){

        $request->validate([
          'avatar' => 'required|image|mimes:jpeg,png,jpg|max:1000',
        ]);

        if (!is_dir(public_path('/uploads/avatars/'))) {
          mkdir(public_path('/uploads/avatars/'), 0777, true);
        }

        if ($q->avatar!=null) {
          unlink(sprintf(public_path() . "/uploads/avatars/" . '%s', $q->avatar));
        }

        $avatar = $request->file('avatar');
        $filename = md5( $q->id . now() . time() ). '.' . $avatar->getClientOriginalExtension();
        Image::make($avatar)->fit(400, 400)->save( public_path('/uploads/avatars/' . $filename ) );
        $q->avatar = $filename;
      }

      $q->first_name = $request->first_name;
      $q->last_name = $request->last_name;
      $q->email = $request->email;
      $q->personal_email = $request->personal_email;
      $q->mobile_number = $request->mobile_number;
      $q->skype_id = $request->skype_id;
      $q->alternate_contact_number = $request->alternate_contact_number;
      $q->birthdate = $request->birthdate;
      $q->city = $request->city;
      $q->address1 = $request->address1;
      $q->address2 = $request->address2;
      $q->save();
    }

    return redirect()->intended('/dashboard/users/profile')->with('notification_message', 'Profile has been updated.');
  }

  public function getClockIn()
  {
    $auth_user = \Auth::user();

    if ($auth_user->is('va')) {
      $day_of_week = date("l", strtotime(now()));
      $year = date("Y", strtotime(now()));
      $schedules = Schedule::where('user_day_of_week', $day_of_week)
      ->where('year', $year)
      ->where('user_id', $auth_user->id)
      ->select(['id','day_of_week', 'start_time','end_time','shift', 'client_id', 'timezone', 'user_day_of_week', 'user_start_time', 'user_end_time'])
      ->get();
      $status = "in";
      $html = view('admin.user.snippets.clock-in', compact('schedules', 'status'))->render();
      $response['status'] = "ok";
      $response['html'] = $html;
      return json_encode($response);
    }
  }

  public function getClockOut()
  {
    $auth_user = \Auth::user();

    if ($auth_user->is('va')) {
      $status = "out";
      $html = view('admin.user.snippets.clock-out', compact('status'))->render();
      $response['status'] = "ok";
      $response['html'] = $html;
      return json_encode($response);
    }

  }

  public function getTaskStart($task_type, $id)
  {
    $auth_user = \Auth::user();
    $task_status = "start";
    if ($auth_user->is('va')) {

      if ($task_type=='generic') {
        $task = Task::find($id);

      } else { //client task
        $task =ClientTask::find($id);
      }

      $html = view('admin.user.snippets.task', compact('task', 'task_type', 'task_status'))->render();
      $response['status'] = "ok";
      $response['html'] = $html;
      return json_encode($response);
    }
  }

  public function getTaskStop($task_type)
  {
    $auth_user = \Auth::user();
    $task_status = 'stop';
    if ($auth_user->is('va')) {

      $task = $auth_user->getCurrentTask();

      $html = view('admin.user.snippets.task', compact('task', 'task_type', 'task_status'))->render();
      $response['status'] = "ok";
      $response['html'] = $html;
      return json_encode($response);
    }
  }

  public function postTaskStatus(Request $request)
  {
    $auth_user = \Auth::user();
    $current_task = $auth_user->getCurrentTask();

    $now = time();
    if (!empty($current_task)) {
      $date_end = date('Y-m-d', $now);
      $time_end = date('H:i:s', $now);
      $current_task->date_end = $date_end;
      $current_task->time_end = $time_end;
      $current_task->save();
    } else {
      $attendance = $auth_user->getCurrentAttendance();
      $date_start = date('Y-m-d', $now);
      $time_start = date('H:i:s', $now);

      if ($request->selected_task_type=='generic') {
        $task = Task::find($request->selected_task_id);
        $task_description = $request->task_description;
      } else {
        $task = ClientTask::find($request->selected_task_id);
        $task_description = $task->description;
      }

      UserTask::create([
        'user_id' => $auth_user->id,
        'client_id' => $attendance->client_id,
        'attendance_id' => $attendance->id,
        'name' => $task->name,
        'description' => $task_description,
        'date_start' => $date_start,
        'time_start' => $time_start,
        'task_type' => $request->selected_task_type
      ]);
    }

    $response['status'] = "ok";
    return json_encode($response);
  }

  public function readTerms() {
    $auth_user = \Auth::user();
    $auth_user->read_terms = TRUE;
    $auth_user->save();
    $response['status'] = 'ok';
    return json_encode($response);
  }

  public function subscriptions(){
    $auth_user_id = \Auth::user()->id;
    $subs = PlanSubscription::where('client_id', '=', $auth_user_id)->orderBy('id', 'desc')->first();

    $ppc = new PaypalController();
    $subscription = $ppc->paypalBillingTransaction($subs->paypal_agreement_id);

    //dd($subscription);

    $clientuser = ClientUser::where('client_id', '=', \Auth::user()->id)->get();
    $workers_num = $clientuser->count();

    return view('admin.subscriptions.subscriptions', compact('auth_user_id', 'subscription', 'subs', 'workers_num'));
  }

  public function cancelSubscription($id){

    $subscription = PlanSubscription::where('paypal_agreement_id', '=', $id)->first();

    $ppc = new PaypalController();
    $subscription_cancel = $ppc->paypalCancelSubscription($id);

    $subscription->status = '2';
    $subscription->save();

    return redirect()->route('subscription')->with('success', 'Subscription canceled');
  }

  public function reactivateSubscription($id){

    $subscription = PlanSubscription::where('paypal_agreement_id', '=', $id)->first();
    $ppc = new PaypalController();
    $subscription_reactivate = $ppc->paypalReactivateSubscription($id);

    $subscription->status = '1';
    $subscription->save();

    return redirect()->back()->with('success', 'Subscription Reactivated');
  }

  public function changeSubscription($id){

    $auth_user_id = \Auth::user()->id;
    $subscription = PlanSubscription::where('paypal_agreement_id', '=', $id)->first();
    return view('admin.subscriptions.change', compact('auth_user_id', 'subscription'));
  }

  public function updateSubscription(Request $request){

    //dd($request);
    //$auth_user_id = \Auth::user()->id;
    //dd($auth_user_id);
    
    $subscription = PlanSubscription::where('paypal_agreement_id', '=', $request->subscription_id)->first();

    $ppc = new PaypalController();
    $subscription_info = $ppc->paypalBillingTransaction($request->subscription_id);

    //dd($subscription_info->agreement_details->next_billing_date);
    if(isset($subscription_info->agreement_details->next_billing_date)){
      $request->request->add(['start_date' => $subscription_info->agreement_details->next_billing_date]);
      $subscription_cancel = $ppc->paypalCancelSubscription($request->subscription_id);
      $subscription->status = '2';
      $subscription->save();

      $ppc->create_plan($request);
    }

    //$subscription_update = $ppc->paypalUpdatePlan($subscription->plan_id, $request->num_remote_user);
    //dd($subscription_update);

    //$subscription = PlanSubscription::where('client_id', '=', $auth_user_id)->first();
  //  $subscription->remote_workers_num = $request->num_remote_user;
    //$subscription->save();

    //return redirect()->route('subscription')->with('success', 'Subscription Updated');

  }
  
  
  public function getLogoutByAdmin($id)
	{
        //$auth_user = Auth::user();
        
        $auth_user = User::where('id', $id)->first();
		ActivityLog::addLog("User [{$auth_user->first_name} {$auth_user->last_name}] logged out.");

        // timeout
        $attendance = $auth_user->getCurrentAttendance();
        
        if($attendance) {
            $instance = new \App\Http\Controllers\API\UserAttendanceController();
            $instance->update(new \Illuminate\Http\Request, $attendance->id);
        }
        

        // logout
        $auth_user->access_token = null;
        $auth_user->socket_id = null;
        $auth_user->is_online = 0;
        $auth_user->save();
        //Auth::logout();
        

        
        return redirect('/dashboard/va')->with('notification_message', $auth_user->first_name.' has been logged out.');

		//return response()->json([
            //'/status' => 'success'
        //]);
	}
	
	public function removeVaFromTeam($id){
	    $auth_user = \Auth::user();
	    
	     $team = Team::where("lead_user_id", $auth_user->id)->first();
         $vas = null;
        
        if ($team->id) {
            $va = TeamUser::where("team_id", $team->id)->where('user_id', $id)->delete();
            
            return redirect()->intended('/dashboard/va')->with('notification_message', 'Profile has been updated.');         
        }
        
        return redirect()->intended('/dashboard/va')->with('notification_message', 'User not found');   

	 }


}
