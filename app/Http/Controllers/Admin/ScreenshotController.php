<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use App\Models\Screenshot;
use App\Models\Schedule;
use App\Models\Team;
use App\Models\TeamUser;
use Auth;
use App\Models\User;
use App\Models\ManagerClient;
use App\Models\Attendance;

class ScreenshotController extends Controller
{
    public function index()
    {
        $auth_user = \Auth::user();
        if ($auth_user->is('client')) {
            return va_view('admin.screenshot.client-index');
        }
        
        //$client = ManagerClient::where('user_id', $auth_user->id)->pluck('client_id');
        
        //dd($client);

        return view('admin.screenshot.index');
    }
    
    public function loadClients(Request $request){
        
        $user = Auth::user();
   
        $target_date = $request->target_date;

        $start_time = "12:00AM";
        $end_time = "11:59PM";
        
        $start_time = date("H:i:s", strtotime($target_date . " " . $request->start_time));
        $end_time = date("H:i:s", strtotime($target_date . " " . $start_time));
        
        $start_date = date("Y-m-d H:i:s", strtotime($target_date . " " . $start_time));
        $end_date = date("Y-m-d H:i:s", strtotime($target_date . " " . $end_time));
        
        $team = Team::where("lead_user_id", $user->id)->first();
                $vas = null;
                if ($team->id) {
                    $vas = TeamUser::where("team_id", $team->id)->pluck('user_id')->all();
                }

                $sc = Screenshot::whereIn('user_id', $vas)->whereDate('created_at', '=', $target_date)->groupBy('client_id')->get();
                $clients = [];
                
                foreach($sc as $s){
                    $clients[] = $s->client_id;
                }
                
                $all_clients = User::whereIn('id', $clients)->get();
                
                
                return $all_clients;
    }


    public function datatables(Request $request)
    {

        $user = Auth::user();

        $target_date = date("Y-m-d", strtotime(now()));
        $start_time = "12:00AM";
        $end_time = "11:59PM";

        if (isset($request->target_date)) {
            $target_date = date("Y-m-d", strtotime($request->target_date));
        }

        if (isset($request->start_time)) {
            $start_time = date("H:i:s", strtotime($target_date . " " . $request->start_time));
            $end_time = date("H:i:s", strtotime($target_date . " " . $start_time));
        }

        $start_date = date("Y-m-d H:i:s", strtotime($target_date . " " . $start_time));
        $end_date = date("Y-m-d H:i:s", strtotime($target_date . " " . $end_time));

        $client_id = 0;

        if ($user->is('client')) {
            $client_id = $user->id;

            //get all VAs under this client
            $vas = Schedule::where('client_id', $client_id)->pluck('user_id')->all();

            $users = User::HasRole('va')
            ->whereHas('screenshots', function ($query) use ($client_id, $target_date, $start_date, $end_date) {
                $query->whereDate('created_at', $target_date)
               //->whereBetween('created_at', [$start_date, $end_date])
                ->where('client_id', $client_id);
            })->select(['id','first_name', 'last_name'])->whereIn('id', $vas);

        } elseif ($user->is('manager')) {
            
            //$team = Team::where("lead_user_id", $user->id)->first();
               // $vas = null;
                //if ($team->id) {
                   // $vasa = TeamUser::where("team_id", $team->id)->pluck('user_id');
                    //$vas = User::whereIn('id', $vasa)->where('is_active', '1')->pluck('id');
               // }
                
               // $users = User::HasRole('va')
                    //->whereIn('id', $vas)
                 //   ->whereHas('screenshots', function ($query) use ($target_date, $vas) {
                        //$query->whereDate('created_at', $target_date) ->whereIn('user_id', $vas);
                  //  })->select(['id','first_name', 'last_name']);
                    //->whereId('50');
                $team = Team::where("lead_user_id", $user->id)->first();
                $vas = null;
                if ($team->id) {
                    $vas = TeamUser::where("team_id", $team->id)->pluck('user_id');
                }
                
                $sc = Screenshot::whereIn('user_id', $vas)->whereDate('created_at', $target_date)->pluck('user_id');
                $users = User::select(['id','first_name', 'last_name'])
                ->where('is_active', '1')
                ->whereIn('id', $sc);
                
        } else {
            $users = User::HasRole('va')
            ->whereHas('screenshots', function ($query) use ($target_date, $start_date, $end_date) {
                $query->whereDate('created_at', $target_date);
                //->whereBetween('created_at',[$start_date, $end_date]);
            })->select(['id','first_name', 'last_name']);
        }

        return DataTables::of($users)
            ->removeColumn('id')
            ->addColumn('screenshots', function($user) use ($target_date) {

                return "<div>Total Screenshots: " . $user->screenshotCount($target_date) . "</div>";
            })
            ->addColumn('actions', function($user) use ($target_date){

                $btn = '<a class="btn btn-success" href="/dashboard/screenshots/'.$user->id.'/s/'.$target_date.'"><i class="fa fa-desktop"></i></a>';

                return '<div class="btn-toolbar">'. $btn . '</div>';

        })->rawColumns(['actions', 'screenshots'])
        ->make(true);

    }

    public function indexAll()
    {
        return view('admin.screenshot.index-all');
    }

    public function datatablesAll(Request $request)
    {
        $target_date = date("Y-m-d", strtotime(now()));
        $start_time = "12:00AM";
        $end_time = "11:59PM";
        $user = Auth::user();

        if ($user->is('manager')) {
            //get all VAs under this manager
            $team = Team::where("lead_user_id", $user->id)->first();
            $vas = null;
            if ($team->id) {
                $vasa = TeamUser::where("team_id", $team->id)->pluck('user_id');
                $vas = User::whereIn('id', $vasa)->where('is_active', '1')->pluck('id');
            }
        }

        if (isset($request->target_date)) {
            $target_date = $request->target_date;
        }

        if (isset($request->start_time)) {
            $start_time = date("H:i:s", strtotime($target_date . " " . $request->start_time));
        }

        if (isset($request->end_time)) {
           $end_time = date("H:i:s", strtotime($target_date . " " . $request->end_time));
        }

        $start_date = date("Y-m-d H:i:s", strtotime($target_date . " " . $start_time));
        $end_date = date("Y-m-d H:i:s", strtotime($target_date . " " . $end_time));




            $screenshots = Screenshot::select(['id','path', 'user_id','client_id', 'process_list_id', 'browser_history_id', 'filename', 'mouse_activity', 'keyboard_activity', 'created_at', 'storage'])
            ->whereDate('created_at', $target_date)
            ->whereBetween('created_at', [$start_date, $end_date]);


        return DataTables::of($screenshots)
        ->removeColumn('id')
        ->removeColumn('storage')
        //->removeColumn('created_at')
        ->editColumn('user_id', function($s){
            return "<div>". $s->user->first_name . " " . $s->user->last_name . "<br/>" . date("h:ia", strtotime($s->created_at)) . "</div>";
        })
        ->removeColumn('client_id')
        ->removeColumn('process_list_id')
        ->removeColumn('browser_history_id')
        ->removeColumn('filename')
        ->editColumn('path', function($s){
            return "<img style='cursor:pointer;' data-id='".$s->id."' data-toggle='modal' data-target='#modal-preview' class='button-preview img-thumbnail' width='200px' src='" . $s->thumbnail_url . "' />";
        })
        ->editColumn('mouse_activity', function($s){
            return "<div><img src='/images/custom/mouse.png' width='32px'/> " . $s->mouseActivity() . "</div>";
        })
        ->editColumn('keyboard_activity', function($s){
            return "<div><img src='/images/custom/keyboard.png' width='32px'/> " . $s->keyboardActivity() . "</div>";
        })
        ->addColumn('actions', function($s){
            $preview_btn = '<a title="Preview Screenshot" data-toggle="modal" data-target="#modal-preview" class="btn btn-info button-preview" data-id="'.$s->id.'"><i class="fa fa-eye"></i></a>';
            $delete_btn = '<a title="Delete Screenshot" data-toggle="modal" data-target="#modal-danger" class="btn btn-danger button-delete" data-id="'.$s->id.'"><i class="fa fa-trash"></i></a>';
            return '<div class="btn-toolbar">' . $preview_btn . $delete_btn .'</div>';

        })->rawColumns(['actions', 'path', 'mouse_activity', 'keyboard_activity', 'user_id'])
        ->make(true);
    }

    public function vaScreenshotIndex($id, $target_date)
    {

	$screenshots = '';
        $set_menu = [
            'active_menu' => "dashboard/screenshots",
            'active_path' => "dashboard/screenshots/{id}",
            'active_parent' => "dashboard/va"
        ];

        $user = User::find($id);

        $auth_user = \Auth::user();

        $client_id = $auth_user->id;
        $screenshots = Screenshot::select(['id','path', 'user_id','client_id', 'process_list_id', 'browser_history_id', 'filename', 'mouse_activity', 'keyboard_activity', 'created_at', 'storage'])
            ->whereDate('created_at', $target_date)
            ->where('client_id', $client_id)
            //->whereBetween('created_at', [$start_date, $end_date])
            ->where('user_id', $id);



        if ($auth_user->is('client')) {
            return va_view('admin.screenshot.client-va-index', compact('user', 'set_menu'));
        }

        return view('admin.screenshot.va-index', compact('user', 'set_menu'));
    }

    public function vaScreenshotDataTables($id, Request $request)
    {
        $target_date = date("Y-m-d", strtotime(now()));
        $start_time = "12:00AM";
        $end_time = "11:59PM";

        if (isset($request->target_date)) {
            $target_date = $request->target_date;
        }

        if (isset($request->start_time)) {
            $start_time = date("H:i:s", strtotime($target_date . " " . $request->start_time));
        }

        if (isset($request->end_time)) {
            $end_time = date("H:i:s", strtotime($target_date . " " . $request->end_time));
        }

        $start_date = date("Y-m-d H:i:s", strtotime($target_date . " " . $start_time));
        $end_date = date("Y-m-d H:i:s", strtotime($target_date . " " . $end_time));

        $user = Auth::user();

        if ($user->is('client')) {
            $client_id = $user->id;
            $screenshots = Screenshot::select(['id','path', 'user_id','client_id', 'process_list_id', 'browser_history_id', 'filename', 'mouse_activity', 'keyboard_activity', 'created_at', 'storage'])
            ->whereDate('created_at', $target_date)
            ->where('client_id', $client_id)
            ->whereBetween('created_at', [$start_date, $end_date])
            ->where('user_id', $id);
        } else { //assumed Admins for now
            $screenshots = Screenshot::select(['id','path', 'user_id', 'process_list_id', 'browser_history_id', 'filename', 'mouse_activity', 'keyboard_activity', 'created_at', 'storage'])
            ->whereDate('created_at', $target_date)
            ->whereBetween('created_at', [$start_date, $end_date])
            ->where('user_id', $id);
        }


        return DataTables::of($screenshots)
        ->removeColumn('id')
        ->removeColumn('user_id')
        ->removeColumn('client_id')
        ->removeColumn('process_list_id')
        ->removeColumn('browser_history_id')
        ->removeColumn('filename')
        //->removeColumn('created_at')
        ->removeColumn('storage')
        ->addColumn('time_taken', function($s){
            return "<div>" . date("h:ia", strtotime($s->created_at)) . "</div>";
        })
        ->editColumn('path', function($s){
            $thumbnail = $s->thumbnail_url;


            //return "<img style='cursor:pointer;' data-id='".$s->id."' data-toggle='modal' data-target='#modal-preview' class='button-preview img-thumbnail' width='200px' src='" . $thumbnail . "' />";
            return "<img style='cursor:pointer;' data-id='".$s->id."' data-toggle='modal' data-target='#modal-preview' class='button-preview img-thumbnail' width='200px' src='" . $s->thumbnail_url . "' />";
        })
        /*->addColumn('process_list', function($s){
            $html = "PS";
            foreach ($s->processList->list() as $ps) {
                $html .= "<div>" . $ps . "</div>";
            }
            return $html;
        })
        ->addColumn('browser_history', function($s){
            $html = "BH";
            foreach ($s->browserHistory->list() as $bh) {
                $html .= "<div>" . $bh->url . "</div>";
            }
            return $html;
        })*/
        ->editColumn('mouse_activity', function($s){
            return "<div><img src='/images/custom/mouse.png' style='width:32px'/> " . $s->mouseActivity() . "</div>";
        })
        ->editColumn('keyboard_activity', function($s){
            return "<div><img src='/images/custom/keyboard.png' style='width:32px'/> " . $s->keyboardActivity() . "</div>";
        })
        ->addColumn('actions', function($s) use ($user){
            $delete_btn = '';
            if ($user->allowed('delete_screenshot')) {
                $delete_btn = '<a title="Delete Screenshot" data-toggle="modal" data-target="#modal-danger" class="btn btn-danger button-delete" data-id="'.$s->id.'"><i class="fa fa-trash"></i></a>';
            }
            $preview_btn = '<a title="Preview Screenshot" data-toggle="modal" data-target="#modal-preview" class="btn btn-info button-preview" data-id="'.$s->id.'"><i class="fa fa-eye"></i></a>';
            return '<div class="btn-toolbar">'. $preview_btn . $delete_btn .'</div>';

        })->rawColumns(['actions', 'path', 'mouse_activity', 'keyboard_activity','time_taken'])
        ->make(true);

    }

    public function datatablesAdminDashboard() {
        $screenshots = Screenshot::orderBy('created_at', 'desc')->take(20);


        return DataTables::of($screenshots)
        ->addColumn('time', function ($s) use ($screenshots){
           return date('m-d-Y H:i:s', strtotime($s->created_at));
        })
        ->addColumn('name', function ($s) use ($screenshots){
            $u = User::find($s->user_id);
            return $u->full_name;
        })
        ->addColumn('screenshot', function ($s) use ($screenshots) {
            return "<img style='cursor:pointer;' data-id='".$s->id."' data-toggle='modal' data-target='#modal-preview' class='button-preview img-thumbnail' width='200px' src='" . $s->thumbnail_url ."' />";
        })
        ->rawColumns(['screenshot'])
        ->make(true);
    }

    public function preview($id)
    {
        $q = Screenshot::find($id);
        $html = "<div><img class='img-responsive img-thumbnail' src='". $q->url ."'></div>";
        $response['html'] = $html;
        return json_encode($response);
    }

    public function show($id)
    {
        $q = Screenshot::find($id);
        $html = $html = "<div><img class='img-responsive' src='". $q->url ."'></div>";
        $response['html'] = $html;
        return json_encode($response);
    }

    public function delete($id)
    {
        $q = Screenshot::find($id);
        if (!empty($q)) {
            $q->delete();
        }
        $response['status'] = "ok";
        return json_encode($response);
    }

    public function loadAllScreenshotsClients(){

      $auth_user = \Auth::user();
      $client_id = $auth_user->is('client') ? $auth_user->id : '';

      $q = Screenshot::where('client_id', '=', $client_id)->orderBy('id', 'desc')->limit(10)->with('user')->get();

      return $q;
    }
}
