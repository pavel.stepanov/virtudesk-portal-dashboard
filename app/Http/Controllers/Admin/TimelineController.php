<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use App\Models\User;
use App\Models\Attendance;
use Illuminate\Validation\Rule;
use DateTime;

class TimelineController extends Controller
{

    private $auser;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->auser = \Auth::user();
            return $next($request);
        });
    }

    public function index()
    {
        return view('admin.timeline.index');
    }

    public function datatables(Request $request)
    {

        $target_date = date("Y-m-d", strtotime(now()));
        if (isset($request->target_date)) {
            $target_date = $request->target_date;
        }

        $attendance = Attendance::where("date_in", $target_date)
        ->select(['attendance.id','client_id', 'user_id', 'date_in', 'time_in', 'total_time', 'date_end', 'time_end'])
        ->leftJoin('users','users.id', '=', 'attendance.user_id')
        ->orderBy('users.first_name', 'ASC');

        return DataTables::of($attendance)
            ->removeColumn('date_in')
            ->removeColumn('time_in')
            ->removeColumn('total_time')
            ->removeColumn('date_end')
            ->removeColumn('time_end')
            ->editColumn('user_id', function($att){
                return $att->user->fullname;
            })
            ->editColumn('client_id', function($att){
                return $att->client->fullname;
            })
            ->addColumn("time_worked", function($att){
                $total_time=$att->total_time;
                if ($att->total_time=='00:00:00') {
                    $total_time = 'On-going';
                }
                return "<div>".$total_time."</div>";

            })
            ->addColumn('timeline', function($att) use ($target_date) {

                $breaks = $att->getBreaks();
                $idles = $att->getIdles();
                $start = $att->date_in . " " . $att->time_in;
                $end = $att->date_end . " " . $att->time_end;
                $dt_end = new DateTime($end);
                if ($att->status == 'in') {
                    $dt_end = new DateTime();
                }
                //$dt = new DateTime($target_date. ' 00:00:00');
                $dt = new DateTime($start);
                $dt_start = new DateTime($start);
                $t = "2";
                $has_break = false;
                $has_idle = false;
//                $dt->modify('-1 hour');

                //60 divided by 3 = 20
                // 20 multiplied by 24 = 480
                for($i=0;$i<=400;$i++) {

                    if (!empty($breaks)) {
                        foreach ($breaks as $break) {
                            $b_start = new DateTime($break->break_start);
                            $b_end = new DateTime($break->break_stop);

                            if ($dt->getTimestamp() > $b_start->getTimestamp() && 
                                $dt->getTimestamp() < $b_end->getTimestamp()) {
                                    $t = $t . ",0";
                                    $has_break = true;
                            }
                        }
                    } 

                    if (!empty($idles)) {
                        foreach ($idles as $idle) {
                            $i_start = new DateTime($idle->idle_start);
                            $i_end = new DateTime($idle->idle_stop);

                            if ($dt->getTimestamp() > $i_start->getTimestamp() && 
                                $dt->getTimestamp() < $i_end->getTimestamp()) {
                                    $t = $t . ",-1";
                                    $has_idle = true;
                            }
                        }
                    }

                    if ( ($has_break==false) && ($has_idle==false) ) {

                        if ( ($dt->getTimestamp() >= $dt_start->getTimestamp()) && ( $dt->getTimestamp() < $dt_end->getTimestamp()) ) {
                            $t = $t . ",1";
                        } else {
                            $t = $t . ",2";
                        }
                    } else {
                        $has_break = false;
                        $has_idle = false;
                    }

                    $dt->modify('+3 minutes');
                }
                    
   
                return "<span class='sparkline' sparkType='tristate'>".$t."</span>";

        })->rawColumns(['time_worked','timeline'])
        ->make(true);

    }

}