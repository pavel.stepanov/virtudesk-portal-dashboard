<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use App\Models\User;
use App\Models\RequestConcerns;
use Illuminate\Validation\Rule;
use App\Models\ActivityLog;


class RequestController extends Controller
{
    private $auser;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->auser = \Auth::user();
            return $next($request);
        });
    }

    public function index()
    {
        return view('admin.requests.index');
    }

    public function takeAction($id)
    {
        $r = RequestConcerns::find($id);
        return view('admin.requests.take-action', compact('r'));
    }

    public function takeActionApproveDecline(Request $request)
    {
        $r = RequestConcerns::find($request->id);
        $user = \Auth::user();
        $r->status = $request->action;
        $r->notes = $request->notes;
        $r->updated_by_id = $user->id;
        $r->save();
        ActivityLog::addLog("User [{$this->auser->first_name} {$this->auser->last_name}] updated request for {$r->showType()} of {$r->user->first_name} {$r->user->last_name} to {$r->status}.");
        return redirect('/dashboard/requests');
    }

    public function datatables()
    {
        $user = \Auth::user();

        if ($user->is('administrator')||$user->is('super_administrator')) {
            $request_concerns = RequestConcerns::select(['id','user_id', 'details','type','status'])
            ->orderBy('created_at','desc');
        } else {
            $request_concerns = RequestConcerns::select(['id','user_id', 'details','type','status'])
            ->where('user_id', $user->id)->orderBy('created_at','desc');
        }


        return DataTables::of($request_concerns)
            ->editColumn('status', function($r){
                return $r->showStatus();
            })
            ->editColumn('type', function($r){
                return $r->showType();
            })            
            ->editColumn('user_id', function($r){
                return $r->user->first_name . " " . $r->user->last_name;
            })
            ->addColumn('actions', function($r) use ($user) {

                $delete_btn = '';
                $action_btn = '';
                $info_btn = '';
                if ($r->status=='pending') {
                    $delete_btn = '<a data-toggle="modal" data-target="#modal-danger" class="btn btn-danger button-delete" data-id="'.$r->id.'"><i class="fa fa-trash"></i></a>';
                    $action_btn = '<a class="btn btn-primary" href="/dashboard/requests/take-action/'.$r->id.'"><i class="fa fa-edit"></i></a>';
                } else {
                    $info_btn = '<a class="btn btn-primary" href="/dashboard/requests/info/'.$r->id.'"><i class="fa fa-info-circle"></i></a>';
                }
                
            if ($user->is('administrator')||$user->is('super_administrator')) {
                return '<div class="btn-toolbar">'. $info_btn . $action_btn . $delete_btn . '</div>';
            } else {
                return '<div class="btn-toolbar">'. $info_btn . $delete_btn . '</div>';
            }

        })->rawColumns(['actions'])
        ->make(true);

    }


    public function show($id)
    {
        $q = RequestConcerns::find($id);
        $html = view('admin.requests.show', compact('q'))->render();
        $response['html'] = $html;
        return json_encode($response);
    }

    public function delete($id)
    {
        $q = RequestConcerns::find($id);
        if (!empty($q)) {
            ActivityLog::addLog("User [{$this->auser->first_name} {$this->auser->last_name}] deleted request for {$q->showType()}.");
            $q->delete();
        }        
        $response['status'] = "ok";
        return json_encode($response);
    }

    public function info($id)
    {
        $q = RequestConcerns::find($id);
        return view('admin.requests.info', compact('q'));
    }

    public function leave()
    {
        return view('admin.requests.leave');
    }

    public function overtime()
    {
        return view('admin.requests.overtime');
    }

    public function sickleave()
    {
        return view('admin.requests.sl');
    }

    public function shift()
    {
        return view('admin.requests.shift-change');
    }

    public function saveRequest(Request $request)
    {
        $request->validate([
            'details' => 'required'
        ]);

        $user = \Auth::user();

        $target_date = null;
        if (isset($request->target_date)) $target_date = $request->target_date;

        $overtime = 0;
        if (isset($request->overtime)) $overtime = $request->overtime;

        $uploaded_file = null;
        $uploaded_filename = null;
        if($request->hasFile('uploaded_file')){    

            $request->validate([
                'uploaded_file' => 'required',
            ]);

            if (!is_dir(public_path('/uploads/files/'))) {
                mkdir(public_path('/uploads/files/'), 0775, true);
            }    

            $uploaded_file = $request->file('uploaded_file');
            $uploaded_filename = md5( $user->id . now() . time() ). '.' . $uploaded_file->getClientOriginalExtension();
            $uploaded_file->move(public_path('/uploads/files/'), $uploaded_filename);
        }
        
        if(isset($request->img_base64)){
 
        $folderPath = public_path('/uploads/files/');

        $image_parts = explode(";base64,", $request->img_base64);
        $image_type_aux = explode("image/", $image_parts[0]);
        $image_type = $image_type_aux[1];
        $image_base64 = base64_decode($image_parts[1]);
        $image_uniqid = uniqid();
        $file = $folderPath.$image_uniqid.'.'.$image_type;
    
        file_put_contents($file, $image_base64);
        $uploaded_filename = $image_uniqid.'.'.$image_type;

        }


        $r = RequestConcerns::create([
            'user_id' => $user->id,
            'details' => $request->details,
            'type' => $request->type,
            'status' => 'pending',
            'target_date' => $target_date,
            'uploaded_file' => $uploaded_filename,
            'overtime' => $overtime
        ]);
        ActivityLog::addLog("User [{$this->auser->first_name} {$this->auser->last_name}] created request for {$r->showType()}.");
        return redirect('/dashboard/requests');
    }

}