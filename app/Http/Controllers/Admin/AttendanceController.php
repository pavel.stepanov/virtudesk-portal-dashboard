<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use App\Models\Attendance;
use App\Models\Schedule;
use App\Models\User;
use App\Models\Team;
use App\Models\TeamUser;
use DateTime;
use DateTimeZone;
use App\Models\ManagerClient;

class AttendanceController extends Controller

{
    public function index()
    {
        $auth_user = \Auth::user();
        if ($auth_user->is('client')) {
            return va_view('admin.attendance.client-index');
        }

        return view('admin.attendance.index');
    }

    public function datatables(Request $request)
    {
        $auth_user = \Auth::user();
        $type_pick = "present";
        if (isset($request->type_pick)) {
            $type_pick = $request->type_pick;
        }

        $target_date = date("Y-m-d", strtotime(now()));
        if (isset($request->target_date)) {
            $target_date = $request->target_date;
        }

        //$work_week = date("W", strtotime($target_date));
        $day_of_week = date("l", strtotime($target_date));

        if ( ($type_pick == 'present') || ($type_pick == 'breaks') || ($type_pick == 'break') || ($type_pick == 'late') || ($type_pick == 'overbreak') || ($type_pick == 'undertime')) {

            if ($auth_user->is('client')) {

                if ($type_pick=='present') {
                    $attendance = Attendance::select(['id','user_id', 'client_id', 'date_in', 'time_in', 'start_time', 'end_time', 'timezone', 'schedule_date'])
                    ->where('schedule_date', $target_date)->where('client_id', $auth_user->id);
                } elseif ($type_pick =='undertime') {
                  $attendance = Attendance::select(['id','user_id', 'client_id', 'date_in', 'time_in', 'start_time', 'end_time', 'timezone', 'schedule_date', 'hours_needed', 'total_time'])
                  ->where('date_in', $target_date)->whereRaw('ceil(TIME_TO_SEC(total_time) / 3600) < hours_needed')->where('status', '=', 'out');
                } else {
                    $attendance = Attendance::select(['id','user_id', 'client_id', 'date_in', 'time_in', 'start_time', 'end_time', 'timezone', 'schedule_date'])
                    ->where('status', 'break')
                    ->where('schedule_date', $target_date)->where('client_id', $auth_user->id);
                }

            } elseif ($auth_user->is('manager')) {
                
                    //get all VAs under this manager
                    $team = Team::where("lead_user_id", $auth_user->id)->first();
                    $vas = null;
                    if ($team->id) {
                        $vasa = TeamUser::where("team_id", $team->id)->pluck('user_id');
                        $vas = User::whereIn('id', $vasa)->where('is_active', '1')->pluck('id');
                    }

                if ($type_pick=='present') {
                    $attendance = Attendance::select(['id','user_id', 'client_id', 'date_in', 'time_in', 'start_time', 'end_time', 'timezone', 'schedule_date'])
                    ->where('date_in', $target_date)->whereIn('user_id', $vas);
                } elseif ($type_pick == 'overbreak') {
                    $attendance = Attendance::select(['id','user_id', 'client_id', 'date_in', 'time_in', 'start_time', 'end_time', 'timezone', 'schedule_date', 'total_breaktime', 'max_breaktime'])
                    ->where('date_in', $target_date)
                    //->whereRaw('max_breaktime < total_breaktime')->whereIn('user_id', $vas);
                     ->whereRaw('total_breaktime > (max_breaktime + interval 5 minute)')
                     ->whereIn('user_id', $vas);
                } elseif ($type_pick == 'late') {
                    $attendance = Attendance::select(['id','user_id', 'client_id', 'date_in', 'time_in', 'start_time', 'end_time', 'timezone', 'schedule_date', 'total_breaktime', 'max_breaktime'])
                    ->where('date_in', $target_date)
                    ->where('late_time',">=","00:05:00")
                    ->whereIn('user_id', $vas);
                    //->whereRaw('UNIX_TIMESTAMP(late_time) > 300')
                    //->whereIn('user_id', $vas);
                } elseif ($type_pick =='breaks') {
                    $attendance = Attendance::select(['id','user_id', 'client_id', 'date_in', 'time_in', 'start_time', 'end_time', 'timezone', 'schedule_date','total_breaktime'])
                    ->where('total_breaktime','<>', '00:00:00')
                    ->where('date_in', $target_date)->whereIn('user_id', $vas);
                } elseif ($type_pick =='undertime') {
                      $attendance = Attendance::select(['id','user_id', 'client_id', 'date_in', 'time_in', 'start_time', 'end_time', 'timezone', 'schedule_date', 'hours_needed', 'total_time'])
                      ->where('date_in', $target_date)->whereRaw('ceil(TIME_TO_SEC(total_time) / 3600) < hours_needed')->where('status', '=', 'out');
                } else {
                    $attendance = Attendance::select(['id','user_id', 'client_id', 'date_in', 'time_in', 'start_time', 'end_time', 'timezone', 'schedule_date'])
                    ->where('status', 'break')
                    ->where('date_in', $target_date)->whereIn('user_id', $vas);
                }
                

            } elseif ($auth_user->is('va')) {
                $attendance = Attendance::select(['id','user_id', 'client_id', 'date_in', 'time_in', 'start_time', 'end_time', 'timezone', 'schedule_date'])
                ->where('date_in', $target_date)->where('user_id', $auth_user->id);
            } else {
                if ($type_pick=='present') {
                    $attendance = Attendance::select(['id','user_id', 'client_id', 'date_in', 'time_in', 'start_time', 'end_time', 'timezone', 'schedule_date'])
                    ->where('date_in', $target_date);
                } elseif ($type_pick == 'overbreak') {
                    $attendance = Attendance::select(['id','user_id', 'client_id', 'date_in', 'time_in', 'start_time', 'end_time', 'timezone', 'schedule_date', 'total_breaktime', 'max_breaktime'])
                    ->where('date_in', $target_date)
                    //->whereRaw('max_breaktime < total_breaktime');
                    ->whereRaw('total_breaktime > (max_breaktime + interval 5 minute)');
                } elseif ($type_pick == 'late') {
                    $attendance = Attendance::select(['id','user_id', 'client_id', 'date_in', 'time_in', 'start_time', 'end_time', 'timezone', 'schedule_date', 'total_breaktime', 'max_breaktime'])
                    ->where('date_in', $target_date)
                    //->where('late_time',"<>","00:00:00");
                    ->where('late_time',">=","00:05:00");
                } elseif ($type_pick =='breaks') {
                    $attendance = Attendance::select(['id','user_id', 'client_id', 'date_in', 'time_in', 'start_time', 'end_time', 'timezone', 'schedule_date','total_breaktime'])
                    ->where('total_breaktime','<>', '00:00:00')
                    ->where('date_in', $target_date);
                } elseif ($type_pick =='undertime') {
                  $attendance = Attendance::select(['id','user_id', 'client_id', 'date_in', 'time_in', 'start_time', 'end_time', 'timezone', 'schedule_date', 'hours_needed', 'total_time'])
                  ->where('date_in', $target_date)->whereRaw('ceil(TIME_TO_SEC(total_time) / 3600) < hours_needed')->where('status', '=', 'out');
                }else {
                    $attendance = Attendance::select(['id','user_id', 'client_id', 'date_in', 'time_in', 'start_time', 'end_time', 'timezone', 'schedule_date'])
                    ->where('status', 'break')
                    ->where('date_in', $target_date);
                }
            }

            return DataTables::of($attendance)
            ->removeColumn('total_break_time')
            ->removeColumn('max_breaktime')
            ->removeColumn('id')
            ->removeColumn('start_time')
            //->removeColumn('client_id')
            ->removeColumn('end_time')
            ->removeColumn('schedule_date')
            ->removeColumn('timezone')

            ->editColumn('date_in', function($att) use ($auth_user) {

                if ($auth_user->is('client')) {
                    $u_timezone = $auth_user->convertToUserTimezone($att->date_in . " " . $att->time_in);
                    return $u_timezone->format('Y-m-d');
                }

                $u_timezone = $att->user->convertToUserTimezone($att->date_in . " " . $att->time_in);
                return $u_timezone->format('Y-m-d');
            })
            ->editColumn('time_in', function($att) use ($auth_user) {

                if ($auth_user->is('client')) {
                    $u_timezone = $auth_user->convertToUserTimezone($att->date_in . " " . $att->time_in);
                    return $u_timezone->format('h:i:s A');
                }

                $u_timezone = $att->user->convertToUserTimezone($att->date_in . " " . $att->time_in);
                return $u_timezone->format('h:i:s A');
            })
            ->editColumn('user_id', function($att){
                return $att->user->first_name . " " . $att->user->last_name;
            })
            ->editColumn('client_id', function($att){
              $user = User::whereId($att->client_id)->first();
                return $user->first_name.' '.$user->last_name;
            })
            ->addColumn('schedule', function($att) use ($auth_user) {
                $sched_timezone = "";
                $timezone_config = config('vatimetracker.timezone');
                foreach ($timezone_config as $key => $value) {
                    if ($key == $att->timezone) {
                        $sched_timezone = $value;
                        break;
                    }
                }

                $user_timezone = "";
                $timezone_config = config('vatimetracker.timezone');
                foreach ($timezone_config as $key => $value) {
                    if ($key == $att->user->timezone) {
                        $user_timezone = $value;
                        break;
                    }
                }

                $start_time = new DateTime(date("Y-m-d h:i A",strtotime($att->schedule_date . " " . $att->start_time)), new DateTimeZone($sched_timezone));
                $end_time = new DateTime(date("Y-m-d h:i A",strtotime($att->schedule_date . " " . $att->end_time)), new DateTimeZone($sched_timezone));

                $orig_schedule = $start_time->format("j M D h:i A") . " - " . $end_time->format("h:i A") . ": " . $att->timezone;

                $start_time->setTimezone(new DateTimeZone($user_timezone));
                $end_time->setTimezone(new DateTimeZone($user_timezone));

                $updated_schedule = $start_time->format("j M D h:i A") . " - " . $end_time->format("h:i A") . ": " . $att->user->timezone;

                if ($auth_user->is('client')) {
                    return "<div>" . $orig_schedule . "</div>";
                } elseif ($auth_user->is('va')) {
                    return "<div>" . $updated_schedule . "</div>";
                } else {
                    return "<div>" . $orig_schedule . "</div><div>" . $updated_schedule . "</div>";
                }
            })
            ->addColumn('remark', function($att) use ($type_pick){

                if ($type_pick=='overbreak') {
                    return "Total Break: ". $att->total_breaktime;
                }

                if ($type_pick=='break') {

                    $break_start = $att->getBreakStart();
                    if ($break_start!=null) {
                        $u_timezone = $att->user->convertToUserTimezone($break_start);
                        return "Break Start: ". $u_timezone->format('h:i:s A');
                    }

                    return "Break not found.";
                }

                if ($type_pick=='breaks') {

                    $breaks = $att->getBreaks();
                    $break_html = "";
                    foreach ($breaks as $break) {
                        $u_timezone_start = $att->user->convertToUserTimezone($break->break_start);
                        $u_timezone_stop = $att->user->convertToUserTimezone($break->break_stop);
                        $break_html = $break_html . "<div>" . $u_timezone_start->format('h:i:s A') . " - " . $u_timezone_stop->format('h:i:s A') . "</div>";
                    }

                    return $break_html . "<div><strong>Total Breaks: ". $att->total_breaktime . "</strong></div>";

                }elseif($type_pick=='undertime'){
                  return 'Total time: ' . $att->total_time.'<br>Hours Needed: '.$att->hours_needed;
                }

                return $att->getRemark();
            })
            ->rawColumns(['schedule', 'remark'])
            ->make(true);
        }

        if ($type_pick == 'absent') {
            $date_today = date("Y-m-d", strtotime(now()));
            $client_id = null;

            if ($auth_user->is('client')) {
                $client_id = $auth_user->id;
                //get all VAs under this client
                $vas = Schedule::where('client_id', $auth_user->id)
                ->pluck('user_id')->all();
                if ($target_date == $date_today) {

                    $absent_users = User::HasRole('va')
                    ->whereIn('id', $vas)
                    ->whereHas('schedules', function ($query) use ($day_of_week) {
                        $query->where('user_day_of_week', $day_of_week)
                        ->whereRaw( "TIME('". date("H:i:s", strtotime(now())) . "') > AddTime(user_start_time,'01:00:00')");
                    })
                    ->select('id', 'first_name', 'last_name', 'email')->get();

                    foreach ($absent_users as $key => &$absent_user) {

                        $attendance_counter = 0;
                        $schedules_count = $absent_user->schedules()
                        ->where('user_id', $absent_user->id)
                        ->where('user_day_of_week', $day_of_week)
                        ->count();

                        $user_schedules = $absent_user->schedules()
                        ->where('user_id', $absent_user->id)
                        ->where('user_day_of_week', $day_of_week)->get();

                        foreach ($user_schedules as $u_sched) {
                            $att = Attendance::where('shift', $u_sched->shift)
                            ->where('user_id', $u_sched->user_id)
                            ->where('client_id', $u_sched->client_id)
                            ->where('date_in', $target_date)
                            ->first();

                            if (!empty($att)) {
                                $attendance_counter = $attendance_counter + 1;
                            } else {
                                if (isset($absent_user['absent'])) {
                                    $absent_user['absent'] = $absent_user['absent'] . ", " . $u_sched->showShift();
                                } else {
                                    $absent_user['absent'] = $u_sched->showShift();
                                }
                            }

                        }

                        if ($attendance_counter == $schedules_count) {
                            $absent_users->forget($key);
                        }
                    }

                    $absent = $absent_users;


                //check if date is future, give dummy blank result
                } elseif (strtotime($target_date) > strtotime($date_today)) {
                    $absent = User::where('id', 0)
                    ->select('id', 'first_name', 'last_name', 'email');
                } else {

                    $absent_users = User::HasRole('va')
                    ->whereIn('id', $vas)
                    ->whereHas('schedules', function ($query) use ($day_of_week) {
                        $query->where('user_day_of_week', $day_of_week);
                    })
                    ->select('id', 'first_name', 'last_name', 'email')->get();

                    foreach ($absent_users as $key => &$absent_user) {

                        $attendance_counter = 0;
                        $schedules_count = $absent_user->schedules()
                        ->where('user_id', $absent_user->id)
                        ->where('user_day_of_week', $day_of_week)
                        ->count();

                        $user_schedules = $absent_user->schedules()
                        ->where('user_id', $absent_user->id)
                        ->where('user_day_of_week', $day_of_week)->get();

                        foreach ($user_schedules as $u_sched) {
                            $att = Attendance::where('shift', $u_sched->shift)
                            ->where('user_id', $u_sched->user_id)
                            ->where('client_id', $u_sched->client_id)
                            ->where('date_in', $target_date)
                            ->first();

                            if (!empty($att)) {
                                $attendance_counter = $attendance_counter + 1;
                            } else {
                                if (isset($absent_user['absent'])) {
                                    $absent_user['absent'] = $absent_user['absent'] . ", " . $u_sched->showShift();
                                } else {
                                    $absent_user['absent'] =  $u_sched->showShift();
                                }
                            }

                        }

                        if ($attendance_counter == $schedules_count) {
                            $absent_users->forget($key);
                        }
                    }

                    $absent = $absent_users;

                }

            } elseif ($auth_user->is('manager')) {

                //get all VAs under this manager
                $team = Team::where("lead_user_id", $auth_user->id)->first();
                $vas = null;
                if ($team->id) {
                    $vas = TeamUser::where("team_id", $team->id)->pluck('user_id')->all();
                }

                if ($target_date == $date_today) {

                    $absent_users = User::HasRole('va')
                    ->whereIn('id', $vas)
                    ->whereHas('schedules', function ($query) use ($day_of_week) {
                        $query->where('user_day_of_week', $day_of_week)
                        ->whereRaw( "TIME('". date("H:i:s", strtotime(now())) . "') > AddTime(user_start_time,'01:00:00')");
                    })
                    ->select('id', 'first_name', 'last_name', 'email')->get();

                    foreach ($absent_users as $key => &$absent_user) {

                        $attendance_counter = 0;
                        $schedules_count = $absent_user->schedules()
                        ->where('user_id', $absent_user->id)
                        ->where('user_day_of_week', $day_of_week)
                        ->count();

                        $user_schedules = $absent_user->schedules()
                        ->where('user_id', $absent_user->id)
                        ->where('user_day_of_week', $day_of_week)->get();

                        foreach ($user_schedules as $u_sched) {
                            $att = Attendance::where('shift', $u_sched->shift)
                            ->where('user_id', $u_sched->user_id)
                            ->where('client_id', $u_sched->client_id)
                            ->where('date_in', $target_date)
                            ->first();

                            if (!empty($att)) {
                                $attendance_counter = $attendance_counter + 1;
                            } else {
                                if (isset($absent_user['absent'])) {
                                    $absent_user['absent'] = $absent_user['absent'] . ", " . $u_sched->client->fullname . " - " . $u_sched->showShift();
                                } else {
                                    $absent_user['absent'] = $u_sched->client->fullname . " - " . $u_sched->showShift();
                                }
                            }

                        }

                        if ($attendance_counter == $schedules_count) {
                            $absent_users->forget($key);
                        }
                    }

                    $absent = $absent_users;


                //check if date is future, give dummy blank result
                } elseif (strtotime($target_date) > strtotime($date_today)) {
                    $absent = User::where('id', 0)
                    ->select('id', 'first_name', 'last_name', 'email');
                } else {

                    $absent_users = User::HasRole('va')
                    ->whereIn('id', $vas)
                    ->whereHas('schedules', function ($query) use ($day_of_week) {
                        $query->where('user_day_of_week', $day_of_week);
                    })
                    ->select('id', 'first_name', 'last_name', 'email')->get();

                    foreach ($absent_users as $key => &$absent_user) {

                        $attendance_counter = 0;
                        $schedules_count = $absent_user->schedules()
                        ->where('user_id', $absent_user->id)
                        ->where('user_day_of_week', $day_of_week)
                        ->count();

                        $user_schedules = $absent_user->schedules()
                        ->where('user_id', $absent_user->id)
                        ->where('user_day_of_week', $day_of_week)->get();

                        foreach ($user_schedules as $u_sched) {
                            $att = Attendance::where('shift', $u_sched->shift)
                            ->where('user_id', $u_sched->user_id)
                            ->where('client_id', $u_sched->client_id)
                            ->where('date_in', $target_date)
                            ->first();

                            if (!empty($att)) {
                                $attendance_counter = $attendance_counter + 1;
                            } else {
                                if (isset($absent_user['absent'])) {
                                    $absent_user['absent'] = $absent_user['absent'] . ", " . $u_sched->client->fullname . " - " . $u_sched->showShift();
                                } else {
                                    $absent_user['absent'] = $u_sched->client->fullname . " - " . $u_sched->showShift();
                                }
                            }

                        }

                        if ($attendance_counter == $schedules_count) {
                            $absent_users->forget($key);
                        }
                    }

                    $absent = $absent_users;

                }


            } else {

                if ($target_date == $date_today) {

                    $absent_users = User::HasRole('va')
                    ->whereHas('schedules', function ($query) use ($day_of_week) {
                        $query->where('user_day_of_week', $day_of_week)
                        ->whereRaw( "TIME('". date("H:i:s", strtotime(now())) . "') > AddTime(user_start_time,'01:00:00')");
                    })
                    ->select('id', 'first_name', 'last_name', 'email')->get();

                    foreach ($absent_users as $key => &$absent_user) {

                        $attendance_counter = 0;
                        $schedules_count = $absent_user->schedules()
                        ->where('user_id', $absent_user->id)
                        ->where('user_day_of_week', $day_of_week)
                        ->count();

                        $user_schedules = $absent_user->schedules()
                        ->where('user_id', $absent_user->id)
                        ->where('user_day_of_week', $day_of_week)->get();

                        foreach ($user_schedules as $u_sched) {
                            $att = Attendance::where('shift', $u_sched->shift)
                            ->where('user_id', $u_sched->user_id)
                            ->where('client_id', $u_sched->client_id)
                            ->where('date_in', $target_date)
                            ->first();

                            if (!empty($att)) {
                                $attendance_counter = $attendance_counter + 1;
                            } else {
                                if (isset($absent_user['absent'])) {
                                    $absent_user['absent'] = $absent_user['absent'] . ", " . $u_sched->client->fullname . " - " . $u_sched->showShift();
                                } else {
                                    $absent_user['absent'] = $u_sched->client->fullname . " - " . $u_sched->showShift();
                                }
                            }

                        }

                        if ($attendance_counter == $schedules_count) {
                            $absent_users->forget($key);
                        }
                    }

                    $absent = $absent_users;


                //check if date is future, give dummy blank result
                } elseif (strtotime($target_date) > strtotime($date_today)) {
                    $absent = User::where('id', 0)
                    ->select('id', 'first_name', 'last_name', 'email');
                } else {


                    $absent_users = User::HasRole('va')
                    ->whereHas('schedules', function ($query) use ($day_of_week) {
                        $query->where('user_day_of_week', $day_of_week);
                    })
                    ->select('id', 'first_name', 'last_name', 'email')->get();

                    foreach ($absent_users as $key => &$absent_user) {

                        $attendance_counter = 0;
                        $schedules_count = $absent_user->schedules()
                        ->where('user_id', $absent_user->id)
                        ->where('user_day_of_week', $day_of_week)
                        ->count();

                        $user_schedules = $absent_user->schedules()
                        ->where('user_id', $absent_user->id)
                        ->where('user_day_of_week', $day_of_week)->get();

                        foreach ($user_schedules as $u_sched) {
                            $att = Attendance::where('shift', $u_sched->shift)
                            ->where('user_id', $u_sched->user_id)
                            ->where('client_id', $u_sched->client_id)
                            ->where('date_in', $target_date)
                            ->first();

                            if (!empty($att)) {
                                $attendance_counter = $attendance_counter + 1;
                            } else {
                                if (isset($absent_user['absent'])) {
                                    $absent_user['absent'] = $absent_user['absent'] . ", " . $u_sched->client->fullname . " - " . $u_sched->showShift();
                                } else {
                                    $absent_user['absent'] = $u_sched->client->fullname . " - " . $u_sched->showShift();
                                }
                            }

                        }

                        if ($attendance_counter == $schedules_count) {
                            $absent_users->forget($key);
                        }
                    }

                    $absent = $absent_users;


                }
            }

            return DataTables::of($absent)
            ->removeColumn('id')
            ->removeColumn('first_name')
            ->removeColumn('last_name')
            ->addColumn('date_in', function($u){
                return "Not Applicable";
            })
            ->addColumn('time_in', function($u){
                return "Not Applicable";
            })
            ->addColumn('user_id', function($u){
                return $u->first_name . " " . $u->last_name;
            })
            ->addColumn('schedule', function($u) use ($target_date, $day_of_week, $client_id) {
                return $u->getScheduleList($target_date, $day_of_week, $client_id);
            })
            ->addColumn('remark', function($u){
                return "Absent - " . $u['absent'];
            })
            ->addColumn('client_id', function($u){
              //$client = User::whereId($u->client_id)->first();
              $client = explode('-', $u->absent);
              return $client[0];
            })
            ->rawColumns(['schedule'])
            ->make(true);

        }

        if ($type_pick == 'idle') {

            if ($auth_user->is('client')) {

                $attendance = Attendance::select(['id','user_id', 'client_id', 'date_in', 'time_in', 'start_time', 'end_time', 'timezone', 'schedule_date','total_idletime'])
                ->where('total_idletime','<>', '00:00:00')
                ->where('schedule_date', $target_date)->where('client_id', $auth_user->id);

            } elseif ($auth_user->is('manager')) {

                $team = Team::where("lead_user_id", $auth_user->id)->first();
                $vas = null;
                if ($team->id) {
                    $vas = TeamUser::where("team_id", $team->id)->pluck('user_id')->all();
                }

                $attendance = Attendance::select(['id','user_id', 'client_id', 'date_in', 'time_in', 'start_time', 'end_time', 'timezone', 'schedule_date','total_idletime'])
                //->where('total_idletime','<>', '00:00:00')
                ->where('total_idletime','>', '00:05:00')
                ->where('schedule_date', $target_date)->whereIn('user_id', $vas);
            } else {
                $attendance = Attendance::select(['id','user_id', 'client_id', 'date_in', 'time_in', 'start_time', 'end_time', 'timezone', 'schedule_date','total_idletime'])
                //->where('total_idletime','<>', '00:00:00')
                ->where('total_idletime','>', '00:05:00')
                ->where('date_in', $target_date);
            }

            return DataTables::of($attendance)
            ->removeColumn('total_break_time')
            ->removeColumn('max_breaktime')
            ->removeColumn('id')
            ->removeColumn('start_time')
            //->removeColumn('client_id')
            ->removeColumn('end_time')
            ->removeColumn('schedule_date')
            ->removeColumn('timezone')
            ->editColumn('date_in', function($att) use ($auth_user) {
                $u_timezone = $att->user->convertToUserTimezone($att->date_in . " " . $att->time_in);
                return $u_timezone->format('Y-m-d');
            })
            ->editColumn('time_in', function($att) use ($auth_user) {
                $u_timezone = $att->user->convertToUserTimezone($att->date_in . " " . $att->time_in);
                return $u_timezone->format('h:i:s A');
            })
            ->editColumn('user_id', function($att){
                return $att->user->first_name . " " . $att->user->last_name;
            })
             ->editColumn('client_id', function($att){
                 $client = User::whereId($att->client_id)->first();
                return $client->first_name . " " . $client->last_name;
            })
            ->addColumn('schedule', function($att) use ($auth_user) {
                $sched_timezone = "";
                $timezone_config = config('vatimetracker.timezone');
                foreach ($timezone_config as $key => $value) {
                    if ($key == $att->timezone) {
                        $sched_timezone = $value;
                        break;
                    }
                }

                $user_timezone = "";
                $timezone_config = config('vatimetracker.timezone');
                foreach ($timezone_config as $key => $value) {
                    if ($key == $att->user->timezone) {
                        $user_timezone = $value;
                        break;
                    }
                }

                $start_time = new DateTime(date("Y-m-d h:i A",strtotime($att->schedule_date . " " . $att->start_time)), new DateTimeZone($sched_timezone));
                $end_time = new DateTime(date("Y-m-d h:i A",strtotime($att->schedule_date . " " . $att->end_time)), new DateTimeZone($sched_timezone));

                $orig_schedule = $start_time->format("j M D h:i A") . " - " . $end_time->format("h:i A") . ": " . $att->timezone;

                $start_time->setTimezone(new DateTimeZone($user_timezone));
                $end_time->setTimezone(new DateTimeZone($user_timezone));

                $updated_schedule = $start_time->format("j M D h:i A") . " - " . $end_time->format("h:i A") . ": " . $att->user->timezone;

                if ($auth_user->is('client')) {
                    return "<div>" . $orig_schedule . "</div>";
                } elseif ($auth_user->is('va')) {
                    return "<div>" . $updated_schedule . "</div>";
                } else {
                    return "<div>" . $orig_schedule . "</div><div>" . $updated_schedule . "</div>";
                }
            })
            ->addColumn('remark', function($att) use ($type_pick){

                    $idles = $att->getIdles();
                    $idle_html = "";
                    foreach ($idles as $idle) {
                        $u_timezone_start = $att->user->convertToUserTimezone($idle->idle_start);
                        $u_timezone_stop = $att->user->convertToUserTimezone($idle->idle_stop);
                        $idle_html = $idle_html . "<div>" . $u_timezone_start->format('h:i:s A') . " - " . $u_timezone_stop->format('h:i:s A') . "</div>";
                    }

                    return $idle_html . "<div><strong>Total Idle: ". $att->total_idletime . "</strong></div>";
            })
            ->rawColumns(['schedule', 'remark'])
            ->make(true);

        }
    }

}
