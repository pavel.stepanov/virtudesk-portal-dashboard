<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use App\Models\User;
use DateTime;

class NotificationController extends Controller
{
    public function index()
    {
        $vas = User::HasRole('va')->orderBy('first_name')->get();
        return view('admin.notifications.index',compact('vas'));
    }

    public function datatables(Request $request)
    {
        if ($request->user_id==0) {
            $notifications = \DB::table('notifications')->select('id','content','type','created_at')
            ->orderBy('id','desc');
        } else {
            $notifications = \DB::table('notifications')->where('user_id',$request->user_id)->select('id','content','type','created_at')
            ->orderBy('id','desc');
        }

        return DataTables::of($notifications)
        ->make(true);

    }

    public function checkNotif()
    {
        $dt = new DateTime();
        $dt->modify('-1 minutes');
        $md = $dt->format('Y-m-d H:i:s');
        
        $notifications = \DB::table('notifications')->orderBy("id", "DESC")
        ->whereRaw('`created_at` > "'.$md.'"')
        ->where('is_displayed',0)
        ->take(8)
        ->get();

        if (!empty($notifications)) {
            \DB::table('notifications')->where('is_displayed',0)
            ->update(['is_displayed' => 1]);
        }
        return json_encode($notifications);
    }
}