<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Auth;

class UserScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $current = Input::get('current');
        $auth_user = Auth::user();
        
        $timezone = config('vatimetracker.timezone');

        $schedules =  $auth_user
            ->schedules()
            ->orderByRaw("FIELD(user_day_of_week, 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday')")
            ->orderBy('user_start_time', 'asc')
            ->get();
        if($current=='true') {
            $schedule =  $auth_user->getCurrentSchedule();
            $schedules = ($schedule) ? [$schedule] : [];
        }
        if ($current=='today') {
            $timestamp = time();
            $day = date('l', $timestamp);
            $today_schedules =  $auth_user->schedules()
                ->where('user_day_of_week', $day)
                ->orderByRaw("FIELD(user_day_of_week, 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday')")
                ->orderBy('user_start_time', 'asc')
                ->get();

            //check if there is a schedule from previous day, that can be continued the next day
            $user_day_of_week_yesterday = date('l',strtotime("-1 days"));
            $yesterday_schedules =  $auth_user->schedules()
            ->where('user_day_of_week_end', $day)
            ->orderByRaw("FIELD(user_day_of_week, 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday')")
            ->orderBy('user_start_time', 'asc')
            ->get();

            //merge schedules from previous day
            $schedules = collect();
            foreach ($yesterday_schedules as $s) $schedules->push($s);
            foreach ($today_schedules as $s) $schedules->push($s);

            //if the schedule is empty, try to give VA all the schedule
            if (empty($schedules)) {
                $schedules =  $auth_user
                ->schedules()
                ->orderByRaw("FIELD(user_day_of_week, 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday')")
                ->orderBy('user_start_time', 'asc')
                ->get();
            }
        }

        $list = [];
        $now = time();
        $day = date('l', $now);

        if (!empty($schedules)) {
            $has_current = false;
            foreach($schedules as $key => $schedule) {
                $current = false;
                if($schedule->user_day_of_week == $day) {
                    $start = strtotime($schedule->user_start_time);
                    $end = strtotime($schedule->user_end_time);
                    if($now >= $start && $now <= $end) {
                        $current = true;
                        $has_current = true;
                    }
                    if (($key == count($schedules)-1) && ($has_current == false)) {
                        $current = true;
                    }
                }
                $list[] = [
                    'id' => $schedule->id,
                    'current' => $current,
                    'name' => $schedule->client->first_name . ' ' . $schedule->client->last_name,
                    'start' => date('h:i:sA', strtotime($schedule->user_start_time)),
                    'end' => date('h:i:sA', strtotime($schedule->user_end_time)),
                    'shift' => ucwords(str_replace('_', ' ', $schedule->shift)),
                    'day' => $schedule->user_day_of_week,
                    'client_name' => $schedule->client->first_name . " " . $schedule->client->last_name,
                    'user_day_of_week' => $schedule->user_day_of_week,
                    'user_day_of_week_end' => $schedule->user_day_of_week_end,
                    'client_tz' => $timezone[$schedule->timezone],
                    'user_tz' => $timezone[$schedule->user_timezone],
                ];
            }
        }
        
        $attendance = $auth_user->attendanceList()->whereNull('date_end')->whereNull('time_end')->first();
        
        return response()->json([
            'status' => 'success',
            'attendance_id' => ($attendance) ? $attendance->id : 0,
            'data' => $list
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
