<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Auth;
use DateTime;
use App\Models\Schedule;
use App\Models\Attendance;
use App\Models\Timesheet;
use App\Models\TimeInOut;
use App\Models\User;

class UserAttendanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $attendanceList = Auth::user()->attendanceList()
            ->where('status', Attendance::STATUS_OUT)
            ->get();

        $list = [];
        foreach($attendanceList as $attendance) {
            $list[] = [
                'id' => $attendance->id,
                'reviewed' => $attendance->acceptance,
                'client' => $attendance->client->first_name . ' ' . $attendance->client->last_name,
                'start' => $attendance->date_in . ' ' . $attendance->time_in,
                'end' => $attendance->date_end . ' ' . $attendance->time_end,
                'total_work' => $attendance->total_time,
                'total_break' => $attendance->total_breaktime,
                'total_idle' => $attendance->total_idletime,
            ];
        }

        return response()->json([
            'status' => 'success',
            'data' => $list
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $timestamp = NULL)
    {
        $schedule_id = Input::get('schedule_id');
        $late_reason = (Input::has('late_reason')) ? Input::get('late_reason') : '';

        $timestamp = (is_null($timestamp)) ? strtotime(now()) : $timestamp;
        
        $year = date('Y', $timestamp);
        $day = date('l', $timestamp);
        $work_week = date("W", $timestamp);

        if ($schedule_id) {
            $schedule = Schedule::find($schedule_id);
            $auth_user = $schedule->user;
        } else {
            $auth_user = Auth::user();
            $schedule = Auth::user()->schedules()
            ->where('user_day_of_week', $day)
            ->where('year', $year)
            ->first();
        }

        if(!$schedule) {
            return response()->json([
                'status' => 'error',
                'msg' => 'No schedule found today.'
            ]);
        }

        $date_in = date('Y-m-d', $timestamp);
        $time_in = date('H:i:s', $timestamp);

        //check if an attendance for the specific schedule is already existing
        $att = Attendance::where("user_id", $auth_user->id)
        ->where('date_in',$date_in)
        ->where('shift',$schedule->shift)
        ->where('day_of_week',$schedule->day_of_week)
        ->where('start_time', $schedule->start_time)
        ->where('end_time', $schedule->end_time)
        ->where('year',$schedule->year)->first();

        //check if an attendance for previous schedule exists
        if (empty($att) && $schedule->user_day_of_week_end != null) {
            $att = Attendance::where("user_id", $auth_user->id)
            ->where('date_in', date('Y-m-d', strtotime('-1 day', strtotime(now()))) )
            ->where('shift',$schedule->shift)
            ->where('day_of_week',$schedule->day_of_week)
            ->where('start_time', $schedule->start_time)
            ->where('end_time', $schedule->end_time)
            ->where('year',$schedule->year)->first();
        }

        if (empty($att)) {

            $schedule_date = $schedule->getClientScheduleDate();

            $attendance = $auth_user->attendanceList()->save(new Attendance([
                'date_in' => $date_in,
                'time_in' => $time_in,
                'total_time' => '00:00:00',
                'total_breaktime' => '00:00:00',
                'total_idletime' => '00:00:00',
                'status' => Attendance::STATUS_IN,
                'client_id' => $schedule->client_id,
                'schedule_date' => $schedule_date,
                'start_time' => $schedule->start_time,
                'end_time' => $schedule->end_time,
                'shift' => $schedule->shift,
                'timezone' => $schedule->timezone,
                'day_of_week' => $schedule->day_of_week,
                'year' => $schedule->year,
                'work_week' => $work_week,
                'max_breaktime' => $schedule->max_breaktime,
                'paid_break' => $schedule->paid_break,
                'hours_needed' => $schedule->hours_needed,
                'late_reason' => $late_reason
            ]));

            $u_timezone = new \DateTime($attendance->date_in . " " . $attendance->time_in, new \DateTimeZone($attendance->user->showTimezone()));
            $u_timezone->setTimezone(new \DateTimeZone($attendance->client->showTimezone()));
            $c_timezone = new \DateTime($attendance->schedule_date . ' ' . $attendance->start_time, new \DateTimeZone($attendance->client->showTimezone()));
            $interval = $u_timezone->diff($c_timezone);
            $min = round(( strtotime($u_timezone->format('Y-m-d H:i:s')) - strtotime($attendance->schedule_date . ' ' . $attendance->start_time)  ) / 60, 2);

            if ($min>0) {
                $attendance->late_time = $interval->format('%h:%I:%S');
                $attendance->save();
            }

        } else {
            $attendance = $att;
            $attendance->status = Attendance::STATUS_IN;
            $attendance->save();
        }

        TimeInOut::create([
            'user_id' => $auth_user->id,
            'attendance_id' => $attendance->id,
            'time_in' => $time_in
        ]);

        $notif['content'] = $auth_user->full_name . " has timed in.";
        $notif['user_id'] = $auth_user->id;
        $notif['type'] = "time-in";
        $notif['created_at'] = date("Y-m-d H:i:s");
        $notif['updated_at'] = date("Y-m-d H:i:s");
        \DB::table('notifications')->insert($notif);

        return response()->json([
            'status' => 'success',
            'id' => $attendance->id
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Input::has('user_id')) {
            $auth_user = User::find(Input::get('user_id'));
        } else{
            $auth_user = Auth::user();
        }

        if($id > 0) {
            $attendance = Attendance::find($id);
        } else {
            // Get current if $id is 0.
            $attendance = $auth_user->getCurrentAttendance();
        }

        self::doTimeout($attendance);

        $notif['content'] = $auth_user->full_name . " has timed out.";
        $notif['user_id'] = $auth_user->id;
        $notif['type'] = "time-out";
        $notif['created_at'] = date("Y-m-d H:i:s");
        $notif['updated_at'] = date("Y-m-d H:i:s");
        \DB::table('notifications')->insert($notif);

        return response()->json([
            'status' => 'success'
        ]);
    }

    public static function doTimeout(Attendance $attendance)
    {
        $t = TimeInOut::where("attendance_id", $attendance->id)->where('time_end', null)->first();
        $timestamp = time();
        $attendance->date_end = date('Y-m-d', $timestamp);
        $attendance->time_end = date('H:i:s', $timestamp);
        if (!empty($t)) {
            $t->time_end = $attendance->time_end;
            $t->save();
        }

        $timeinout = TimeInOut::where("attendance_id", $attendance->id)->get();
        $totalTime = 0;
        foreach($timeinout as $ut) {
            $start = new DateTime($ut->created_at);
            $end = new DateTime($ut->updated_at);
            $interval = $start->diff($end);
            $h = intval($interval->format('%h')) * 3600;
            $i = intval($interval->format('%i')) * 60;
            $s = intval($interval->format('%s'));
            $totalTime += $h + $i + $s;
        }

        $auth_user = User::find($attendance->user_id);
        $auth_user->stopIdle($attendance);
        $auth_user->stopBreak($attendance);
        $auth_user->stopCurrentTask();
        $auth_user->stopTask($attendance);

        //remove break monitoring if there are remnants due to connection issues
        \DB::table('monitor_breaks')->where('user_id', $auth_user->id)->delete();

        $attendance->total_time = gmdate("H:i:s", $totalTime);
        $attendance->status = Attendance::STATUS_OUT;
        $attendance->save();

        if (!empty($auth_user)) {
            $auth_user->is_online = 0;
            $auth_user->access_token = null;
            $auth_user->socket_id = null;
            $auth_user->save();
        }

        self::saveTimesheet($attendance);
    }

    public static function saveTimesheet(Attendance $attendance)
    {
        $billable_hours = Timesheet::convertBillableHours($attendance);
        $billable_hours = ($billable_hours < 0) ? 0 : $billable_hours;
        $timesheet = Timesheet::where('attendance_id', $attendance->id)->first();

        if($timesheet) {
            $timesheet->total_time = $attendance->total_time;
            $timesheet->total_task_time = $attendance->task_total_time;
            $timesheet->total_idletime = $attendance->total_idletime;
            $timesheet->total_breaktime = $attendance->total_breaktime;
            $timesheet->billable_hours = $billable_hours;
            $timesheet->save();
        } else {
            Timesheet::create([
                'target_date' => $attendance->date_in,
                'schedule_date' => $attendance->schedule_date,
                'client_id' => $attendance->client_id,
                'user_id' => $attendance->user_id,
                'attendance_id' => $attendance->id,
                'total_time' => $attendance->total_time,
                'total_task_time' => $attendance->task_total_time,
                'total_idletime' => $attendance->total_idletime,
                'total_breaktime' => $attendance->total_breaktime,
                'billable_hours' => $billable_hours,
                'status' => "pending"
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
