<?php
namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Models\Screenshot;
use App\Models\Idle;
use App\Models\ProcessList;
use App\Models\BrowserHistory;
use App\Models\Attendance;
use App\Models\Report;
use App\Models\BreakTime;
use App\Models\Dispute;
use App\Models\TimeInOut;
use App\Models\Schedule;
use App\Models\User;
use App\Models\UserPing;
use App\Models\Timesheet;
use App\Models\Setting;
use Auth;
use Image;
use DateTime;
use AWS;
use App\Models\RequestConcerns;
use App\Models\Messaging;

class UserController extends Controller
{
    public function postScreenshot(Request $request)
    {
        $client_id = Auth::user()->getCurrentClientID();
        $id = Auth::user()->id;
        $imgData = base64_decode(Input::get('img'));
        $userPublicPath = 'screenshots/' . $id;
        if(!file_exists($userPublicPath)) {
            mkdir('screenshots/' . $id);
        }
        $filename = time() . rand();
        $screenshot_key = $userPublicPath . '/' . $filename . '.jpg';
        $screenhost_path = public_path() . '/' . $userPublicPath . '/' . $filename . '.jpg';

        $img = Image::make($imgData);
        $img->save($screenhost_path);

        // generate thumbnail
        $thumnail_key = $userPublicPath . '/thumb_' . $filename . '.jpg';
        $thumbnail_path = public_path() . '/' . $userPublicPath . '/thumb_' . $filename . '.jpg';
        $img->fit(290, 290)->save( $thumbnail_path );

        if ( env('USE_AWS', 0) == 'test') {
            $s3 = AWS::createClient('s3');
            $s3->putObject(array(
                'Bucket'     => 'virtudesk-crm',
                'Key'        => $screenshot_key,
                'SourceFile' => $screenhost_path,
                'CacheControl' => 'public,max age=31536000',
                'Expires' => gmdate("D, d M Y H:i:s T", strtotime("+1 years")),
            ));

            $s3->putObject(array(
                'Bucket'     => 'virtudesk-crm',
                'Key'        => $thumnail_key,
                'SourceFile' => $thumbnail_path,
                'CacheControl' => 'public,max age=31536000',
                'Expires' => gmdate("D, d M Y H:i:s T", strtotime("+1 years")),
            ));

            $image_storage = 's3';

            // Uncomment to delete screenshots in local
            // unlink($screenhost_path);
            // unlink($thumbnail_path);
        } else {
            $image_storage = 'local';
        }

        // activity data
        $mouseActivity = Input::get('mouse_activity');
        $keyboardActivity = Input::get('keyboard_activity');

        $screenshot = Auth::user()->screenshots()->save(new Screenshot([
            'path' => $userPublicPath . '/',
            'filename' => $filename . '.jpg',
            'client_id' => $client_id,
            'mouse_activity' => $mouseActivity,
            'keyboard_activity' => $keyboardActivity,
            'storage' => $image_storage,
        ]));

        Auth::user()->broadcast()->processList($screenshot->id);
        Auth::user()->broadcast()->browserHistory(60, $screenshot->id);

        return response()->json([
            'test' => $screenshot->id,
            'status' => 'success'
        ]);
    }

    // Store user Process list.
    public function postProcessList()
    {
        $id = Auth::user()->id;
        $processes = Input::get('list'); // string, comma delimited list
        $screenshotId = Input::get('screenshot_id');

        $processList = Auth::user()->processLists()->save(new ProcessList([
            'data' => $processes
        ]));

        if($screenshotId > 0) {
            $screenshot = Screenshot::find($screenshotId);
            $screenshot->process_list_id = $processList->id;
            $screenshot->save();
        }

        return response()->json([
            'status' => 'success'
        ]);
    }

    // Store user Browser History.
    public function postBrowserHistory()
    {
        $id = Auth::user()->id;
        $history = (empty(Input::get('history'))) ? "" : Input::get('history');
        $minutes = Input::get('minutes');
        $screenshotId = Input::get('screenshot_id');

        $browserHistory = Auth::user()->browserHistories()->save(new BrowserHistory([
            'data' => $history,
            'minutes' => $minutes
        ]));

        if($screenshotId > 0) {
            $screenshot = Screenshot::find($screenshotId);
            $screenshot->browser_history_id = $browserHistory->id;
            $screenshot->save();
        }

        return response()->json([
            'status' => 'success'
        ]);
    }

    public function postIdleStart()
    {
        $auth_user = Auth::user();
        $auth_user->startIdle();
        //insert to notifications table
        $notif['content'] = $auth_user->full_name . " has started to idle.";
        $notif['user_id'] = $auth_user->id;
        $notif['type'] = "idle-start";
        $notif['created_at'] = date("Y-m-d H:i:s");
        $notif['updated_at'] = date("Y-m-d H:i:s");
        \DB::table('notifications')->insert($notif);
        return response()->json([
            'status' => 'success'
        ]);
    }

    public function postIdleStop()
    {
        $auth_user = Auth::user();
        $attendance = $auth_user->getCurrentAttendance();
        if($attendance) {
            $auth_user->stopIdle();
            $attendance->status = Attendance::STATUS_IN;
            $attendance->save();

            //insert to notifications table
            $notif['content'] = $auth_user->full_name . " has stopped idle.";
            $notif['user_id'] = $auth_user->id;
            $notif['type'] = "idle-stop";
            $notif['created_at'] = date("Y-m-d H:i:s");
            $notif['updated_at'] = date("Y-m-d H:i:s");
            \DB::table('notifications')->insert($notif);
        }

        return response()->json([
            'status' => 'success'
        ]);
    }

    /**
     * @deprecated
     * Converted to RESTful
     * Use \App\Http\Controllers\API\UserAttendanceController::store
     * Remove after release 1.0
     */
    public function postTimeIn(Request $request, $timestamp = NULL)
    {
        $instance = new \App\Http\Controllers\API\UserAttendanceController();
        return $instance->store($request, $timestamp);
    }

    /**
     * @deprecated
     * Converted to RESTful
     * Use \App\Http\Controllers\API\UserAttendanceController::update
     * Remove after release 1.0
     */
    public function postTimeOut(Request $request)
    {
        $instance = new \App\Http\Controllers\API\UserAttendanceController();
        return $instance->update($request, 0);
    }

    public function postSOD() {
        return $this->makeReport(Report::TYPE_SOD);
    }

    public function postEOD() {
        return $this->makeReport(Report::TYPE_EOD);
    }

    private function makeReport($type) {
        $report = Input::get('report');

        $now = date('Y-m-d');
        $existing = Auth::user()->reports()
            ->where('type', $type)
            ->where('created_at', '>=', $now . ' 00:00:00')
            ->where('created_at', '<=', $now . ' 23:59:59')
            ->get();
        
        if($existing->count() > 0) {
            return response()->json([
                'status' => 'error',
                'msg' => 'This report is already filed today'
            ]);
        }

        Auth::user()->reports()->save(new Report([
            'type' => $type,
            'report' => $report
        ]));

        return response()->json([
            'status' => 'success'
        ]);
    }

    /**
     * @deprecated
     * Converted to RESTful
     * Use \App\Http\Controllers\API\UserScheduleController::index
     * Remove after release 1.0
     */
    public function postSchedules() {
        $instance = new \App\Http\Controllers\API\UserScheduleController();
        return $instance->index();
    }

    public function postReports() {
        $reports = Auth::user()->reports()->orderBy('created_at','desc')->get();
        $list = [];

        $now = time();
        $dayStart = strtotime(date("Y-m-d 00:00:00", $now));
        $dayEnd = strtotime(date("Y-m-d 23:59:59", $now));

        foreach($reports as $report) {
            $today = false;
            $created = strtotime($report->created_at);
            if($dayStart <= $created && $dayEnd >= $created) {
                $today = true;
            }

            $list[] = [
                'type' => $report->type,
                'report' => $report->report,
                'date' => date("Y-m-d H:i:s", $created),
                'today' => $today
            ];
        }

        return response()->json([
            'status' => 'success',
            'data' => $list
        ]);
    }

    public function postHasSOD() {
        return $this->hasType(Report::TYPE_SOD);
    }

    public function postHasEOD() {
        return $this->hasType(Report::TYPE_EOD);
    }

    private function hasType($type) {
        $reports = Auth::user()->reports()->get();
        $list = [];

        $now = time();
        $dayStart = strtotime(date("Y-m-d 00:00:00", $now));
        $dayEnd = strtotime(date("Y-m-d 23:59:59", $now));

        $today = false;
        foreach($reports as $report) {
            $created = strtotime($report->created_at);
            if($report->type == $type && $dayStart <= $created && $dayEnd >= $created) {
                $today = true;
                break;
            }
        }

        return response()->json([
            'status' => 'success',
            'today' => $today
        ]);
    }

    public function postBreakStart() {

        $auth_user = Auth::user();
        $auth_user->startBreak();
        $auth_user->stopCurrentTask();

        //insert to notifications table
        $notif['content'] = $auth_user->full_name . " has started break.";
        $notif['user_id'] = $auth_user->id;
        $notif['type'] = "break-start";
        $notif['created_at'] = date("Y-m-d H:i:s");
        $notif['updated_at'] = date("Y-m-d H:i:s");
        \DB::table('notifications')->insert($notif);

        //check if user has break to be monitored
        $att = Attendance::where('user_id',$auth_user->id)->where('status','break')->first();
        if (!empty($att)) {
            if ($att->max_breaktime!=null && $att->max_breaktime!="00:00:00") {
                //compute for the maximum date and time of the break and insert it to monitor breaks table
                $dt1 = new DateTime($att->date_in. ' '. date("H:i:s"));
                $dt2 = new DateTime($att->date_in. ' '. $att->max_breaktime);
                $h = $dt2->format('H');
                $m = $dt2->format('i');

                $dt1->modify('+'.$h. " hour ".$m." minutes");
                $monitor_data['user_id'] = $auth_user->id;
                $monitor_data['max_breaktime'] = $att->max_breaktime;
                $monitor_data['due_breaktime'] = $dt1->format('Y-m-d H:i');
                \DB::table('monitor_breaks')->insert($monitor_data);
            }
        }

        return response()->json([
            'status' => 'success'
        ]);
    }

    public function postBreakStop() { 
        $auth_user = Auth::user();
        $attendance = $auth_user->getCurrentAttendance();
        if($attendance) {
            $auth_user->stopBreak();
            $attendance->status = Attendance::STATUS_IN;
            $attendance->save();
            $notif['content'] = $auth_user->full_name . " has stopped break.";
            $notif['user_id'] = $auth_user->id;
            $notif['type'] = "break-stop";
            $notif['created_at'] = date("Y-m-d H:i:s");
            $notif['updated_at'] = date("Y-m-d H:i:s");
            \DB::table('notifications')->insert($notif);
        }

        //remove any entry from monitor breaks table
        \DB::table('monitor_breaks')->where('user_id', $auth_user->id)->delete();

        return response()->json([
            'status' => 'success'
        ]);
    }

    /**
     * @deprecated
     * Converted to RESTful
     * Use \App\Http\Controllers\API\UserAttendanceController::index
     * Remove after release 1.0
     */
    public function postAttendance() {
        $instance = new \App\Http\Controllers\API\UserAttendanceController();
        return $instance->index();
    }

    /**
     * @deprecated
     * Attendance acceptance will no longer be updated via the app.
     * Remove after release 1.0
     */
    public function postConfirmAttendance()
    {
        $id = Input::get('id');
        $attendance = Auth::user()->attendanceList()->where('id', $id)->first();
        if($attendance) {
            $attendance->acceptance = Attendance::ACCEPTANCE_CONFIRM;
            $attendance->save();

            $billable_hours = Timesheet::convertBillableHours($attendance);
            //if ($attendance->hours_needed < $billable_hours ) {
            //    $billable_hours = $attendance->hours_needed;
            //}

            Timesheet::create([
                'target_date' => $attendance->date_in,
                'schedule_date' => $attendance->schedule_date,
                'client_id' => $attendance->client_id,
                'user_id' => $attendance->user_id,
                'attendance_id' => $attendance->id,
                'total_time' => $attendance->total_time,
                'total_task_time' => $attendance->task_total_time,
                'total_idletime' => $attendance->total_idletime,
                'total_breaktime' => $attendance->total_breaktime,
                'billable_hours' => $billable_hours,
                'status' => "pending"
            ]);

            return response()->json([
                'status' => 'success',
            ]);
        }

        return response()->json([
            'status' => 'error',
            'msg' => 'Attendance not found.'
        ]);
    }

    /**
     * @deprecated
     * Attendance acceptance will no longer be updated via the app.
     * Remove after release 1.0
     */
    public function postDisputeAttendance()
    {
        $id = Input::get('id');
        $desc = Input::get('description');
        
        $attendance = Auth::user()->attendanceList()->where('id', $id)->first();
        if($attendance) {
            $attendance->acceptance = Attendance::ACCEPTANCE_DISPUTE;
            $attendance->save();

            // new dispute instance here
            $attendance->dispute()->save(new Dispute([
                'user_id' => Auth::user()->id,
                'description' => $desc
            ]));

            $billable_hours = Timesheet::convertBillableHours($attendance);
            //if ($attendance->hours_needed < $billable_hours ) {
            //    $billable_hours = $attendance->hours_needed;
            //}

            Timesheet::create([
                'target_date' => $attendance->date_in,
                'schedule_date' => $attendance->schedule_date,
                'client_id' => $attendance->client_id,
                'user_id' => $attendance->user_id,
                'attendance_id' => $attendance->id,
                'total_time' => $attendance->total_time,
                'total_task_time' => $attendance->task_total_time,
                'total_idletime' => $attendance->total_idletime,
                'total_breaktime' => $attendance->total_breaktime,
                'billable_hours' => $billable_hours,
                'status' => "pending"
            ]);

            return response()->json([
                'status' => 'success',
            ]);
        }

        return response()->json([
            'status' => 'error',
            'msg' => 'Attendance not found.'
        ]);
    }

    public function getVersion()
    {
        return response()->json([
            'status' => 'success',
            'data' => [
                'version' => Setting::getValueBySlug('app-version'),
                'osx' => Setting::getValueBySlug('download-link-app-mac'),
                'win' => Setting::getValueBySlug('download-link-app-win')
            ]
        ]);
    }

    public function pingPong(Request $request)
    {
        $response['status'] = "ok";
        $response['user_id'] = $request->user_id;

        $user = User::find($request->user_id);
        if (empty($user)) {
            $response['status'] = "not found";
        } else {
            if (empty($user->pingUpdate)) {
                UserPing::insert([
                    'user_id' => $user->id,
                    'status' => 'ping',
                    'created_at' => now()
                ]);
            } else {
                if ($user->pingUpdate->status=='ping') {
                    $user->pingUpdate->status='pong';
                } else {
                    $user->pingUpdate->status='ping';
                }
                $user->pingUpdate->save();
            }
        }

        return json_encode($response);
    }
    
  public function postRequestConcern(Request $requests){
    $auth_user = Auth::user();

    $request_concerns = RequestConcerns::select(['id','user_id', 'details','type','status'])->where('user_id', $auth_user->id)->orderBy('created_at', 'desc')->get();
    
    return json_encode($request_concerns);
  }
  
  public function getMessages(Request $request){
    $auth_user = Auth::user();

    $messages = Messaging::where('to_id', $auth_user->id)->orWhere('from_id', $auth_user->id)->where('reply_to', null)->with('sender', 'replies')->orderBy('updated_at','asc')->get();
    return json_encode($messages);
  }
  
  public function getMessage(Request $request){
    $auth_user = Auth::user();

    $messages = Messaging::whereId($request->message_id)->with('replies', 'sender')->first();
    return json_encode($messages);
  }


  public function postMessages(Request $request){
       $auth_user = Auth::user();
      
      $uploaded_filename = '';
    
     if(isset($request->img_base64)){
 
        $folderPath = public_path('/uploads/files/');

        $image_parts = explode(";base64,", $request->img_base64);
        $image_type_aux = explode("image/", $image_parts[0]);
        $image_type = $image_type_aux[1];
        $image_base64 = base64_decode($image_parts[1]);
        $image_uniqid = uniqid();
        $file = $folderPath.$image_uniqid.'.'.$image_type;
    
        file_put_contents($file, $image_base64);
        $uploaded_filename = $image_uniqid.'.'.$image_type;

    }
      
    $message = Messaging::create([
      'message' => $request->message,
      'to_id' => $request->to,
      'from_id' => $auth_user->id,
      'reply_to' => $request->reply_to,
      'file'  => $uploaded_filename
    ]);
    
    $messages = Messaging::where('id', $request->reply_to)->update(['status' => 'read']);
    
    if($message){
      return '1';
    }else{
      return '0';
    }
  }

  public function postMessageRead(Request $request){
    $messages = Messaging::whereIn('id', $request->ids)->update(['status' => 'read']);

    if($messages){
      return $messages;
    }
  }


}