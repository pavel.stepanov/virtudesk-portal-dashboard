<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Auth;
use App\Models\Attendance;
use App\Models\Setting;
use App\Models\Task;
use App\Models\ActivityLog;
use DateTime;

class AuthController extends Controller
{
    public function postLogin(Request $request)
	{
		$email = Input::get('email');
		$password = Input::get('password');

        if (Auth::attempt(['email' => $email, 'password' => $password], 0)) {
			if (Auth::user()->is_verified == 0) {
				$error_msg = "Not verified";
				Auth::logout();
                return response()->json([
                    'status' => 'error',
                    'error_msg' => $error_msg
                ]);
            }

			if (Auth::user()->is_active == 0) {
				$error_msg = "Not active";
				Auth::logout();
                return response()->json([
                    'status' => 'error',
                    'error_msg' => $error_msg
                ]);
            }

            if (Auth::user()->is('va') == false) {
                $error_msg = "You need a VA account to log in.";
				Auth::logout();
                return response()->json([
                    'status' => 'error',
                    'error_msg' => $error_msg
                ]);
            }

            $date = new DateTime();
            $auth_user = Auth::user();
            $slugs = array_map(function($role) { return $role['slug']; }, Auth::user()->roles->toArray());
            Auth::user()->access_token = $this->generateAccessToken(Auth::user()->id);
            Auth::user()->is_online = 1;
            Auth::user()->last_login = $date->getTimestamp();
            Auth::user()->save();
            ActivityLog::addLog("User [{$auth_user->first_name} {$auth_user->last_name}] logged in.");
            // check if user has idle
            $attendance = Auth::user()->getCurrentAttendance();
            if($attendance) {
                Auth::user()->stopIdle();
                $attendance->status = Attendance::STATUS_IN;
                $attendance->save();
            }

            

            return response()->json([
                'status' => 'success',
                'user' => [
                    'id' => Auth::user()->id,
                    'first_name' => Auth::user()->first_name,
                    'last_name' => Auth::user()->last_name,
                    'roles' => $slugs,
                    'access_token' => Auth::user()->access_token,
                    'attendance_id' => ($attendance) ? $attendance->id : 0,
                ],
                'settings' => [
                    'idle-interval' => (Auth::user()->idle_interval == 0) ? Setting::getValueBySlug('idle-interval') : Auth::user()->idle_interval,
                    'screenshot-interval' => (Auth::user()->screenshot_interval == 0) ? Setting::getValueBySlug('screenshot-interval') : Auth::user()->screenshot_interval,
                    'task-list' => Task::get()->toArray(),
                ]
            ]);
        }

        $error_msg = "Invalid email/password";
        return response()->json([
            'status' => 'error',
            'error_msg' => $error_msg
        ]);
    }
    
    public function generateAccessToken($id)
    {
        return password_hash(time() . $id . env('API_SECRET'), PASSWORD_BCRYPT);
    }

    public function getLogout()
	{
        $auth_user = Auth::user();
		ActivityLog::addLog("User [{$auth_user->first_name} {$auth_user->last_name}] logged out.");

        // timeout
        $attendance = $auth_user->getCurrentAttendance();
        if($attendance) {
            $instance = new \App\Http\Controllers\API\UserAttendanceController();
            return $instance->update(new \Illuminate\Http\Request, $attendance->id);
        }

        // logout
        Auth::user()->access_token = null;
        Auth::user()->socket_id = null;
        Auth::user()->is_online = 0;
        Auth::user()->save();
        Auth::logout();

		return response()->json([
            'status' => 'success'
        ]);
	}

}