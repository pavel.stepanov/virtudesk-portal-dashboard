<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Auth;
use App\Models\ClientTask;
use App\Models\Task;
use App\Models\UserTask;

class UserTasksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $current = (Input::has('current')) ? Input::get('current') : false;
        if($current) {
            $userTask = Auth::user()->tasks()
                ->whereNull('date_end')
                ->whereNull('time_end')
                ->first();
            $seconds = 0;
            $id = 0;
            $data = [];
            if($userTask) {
                $start = strtotime($userTask->date_start . " " . $userTask->time_start);
                $now = time();
                $seconds = floor($now - $start);
                $id = $userTask->id;

                $userTask->priority = ($userTask->priority == NULL) ? 2 : $userTask->priority;
                $priorityList = [
                    1 => "High",
                    2 => "Normal",
                    3 => "Low",
                ];

                $oldSameTasks = UserTask::where('user_id', Auth::user()->id)
                    ->where('client_id', $userTask->client_id)
                    ->where('attendance_id', $userTask->attendance_id)
                    ->where('name', $userTask->name)
                    ->where('date_start', $userTask->date_start)
                    ->get();

                foreach($oldSameTasks as $oldTask) {
                    $oldStart = strtotime($oldTask->date_start . " " . $oldTask->time_start);
                    $oldEnd = strtotime($oldTask->date_end . " " . $oldTask->time_end);
                    $seconds += floor($oldEnd - $oldStart);
                }

                $data = [
                    [
                        'name' => $userTask->name,
                        'description' => $userTask->description,
                        'priority' => $priorityList[$userTask->priority],
                        'seconds' => $seconds,
                        'id' => $id,
                    ]
                ];
            }

            return response()->json([
                'status' => 'success',
                'data' => $data,
            ]);
        }

        $clientId = Auth::user()->getCurrentClientID();
        $taskItems = [];
        if($clientId) {
            $tasks = ClientTask::where('client_id', $clientId)->where('va_id', Auth::user()->id)->get();
            foreach($tasks as $task) {
                $taskItems[] = $this->getTaskData($task);
            }
            $tasks = Task::get();
            foreach($tasks as $task) {
                $taskItems[] = $this->getTaskData($task);
            }
        }
        return response()->json([
            'status' => 'success',
            'data' => $taskItems,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $name = Input::get('name');
        $desc = Input::get('description');

        $clientId = Auth::user()->getCurrentClientID();
        if($clientId) {
            $attendance = Auth::user()->getCurrentAttendance();
            if($attendance) {
                $now = time();
                $date_start = date('Y-m-d', $now);
                $time_start = date('H:i:s', $now);

                $oldSameTasks = UserTask::where('user_id', Auth::user()->id)
                    ->where('client_id', $clientId)
                    ->where('attendance_id', $attendance->id)
                    ->where('name', $name)
                    ->where('date_start', $date_start)
                    ->get();

                $seconds = 0;
                foreach($oldSameTasks as $oldTask) {
                    $start = strtotime($oldTask->date_start . " " . $oldTask->time_start);
                    $end = strtotime($oldTask->date_end . " " . $oldTask->time_end);
                    $seconds += floor($end - $start);
                }

                $task = new UserTask([
                    'user_id' => Auth::user()->id,
                    'client_id' => $clientId,
                    'attendance_id' => $attendance->id,
                    'name' => $name,
                    'description' => $desc,
                    'date_start' => $date_start,
                    'time_start' => $time_start,
                    'status' =>  UserTask::STATUS_STARTED
                ]);
                $task->save();

                return response()->json([
                    'status' => 'success',
                    'data' => [
                        'id' => $task->id,
                        'seconds' => $seconds
                    ]
                ]);
            }

            return response()->json([
                'status' => 'error',
                'msg' => 'No Attendance Found'
            ]);
        }

        return response()->json([
            'status' => 'error',
            'msg' => 'No Client Found'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $task = UserTask::where('id', $id)->first();

        // If usertask is not available assume the first if any.
        //if(!$task) {
        //    $task = Auth::user()->tasks()->whereNull('date_end')->whereNull('time_end')->first();
        //}

        if(!empty($task)) {
            $now = time();
            $date_end = date('Y-m-d', $now);
            $time_end = date('H:i:s', $now);
            
            $task->date_end = $date_end;
            $task->time_end = $time_end;
            $task->save();

            return response()->json([
                'status' => 'success',
            ]);
        }

        return response()->json([
            'status' => 'error',
            'msg' => 'No User Task Found'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function getTaskData($task) {
        $userTask = Auth::user()->tasks()
            ->where('name', $task->name)
            ->whereNull('date_end')
            ->whereNull('time_end')
            ->first();
        $seconds = 0;
        $id = 0;
        if($userTask) {
            $start = strtotime($userTask->date_start . " " . $userTask->time_start);
            $now = time();
            $seconds = floor($now - $start);
            $id = $userTask->id;
        }

        $task->priority = ($task->priority == NULL) ? 2 : $task->priority;
        $priorityList = [
            1 => "High",
            2 => "Normal",
            3 => "Low",
        ];

        return [
            'name' => $task->name,
            'description' => $task->description,
            'priority' => $priorityList[$task->priority],
            'seconds' => $seconds,
            'id' => $id,
        ];
    }
}
