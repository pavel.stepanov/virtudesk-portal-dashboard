<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Auth;
use App\Models\Timesheet;
use DateTime;

class UserTimesheetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $current_day = date("d");
        $current_month = date("m");
        $current_year = date("Y");

        if (($current_day <= 25) && ($current_day >= 11)) {
            $date_start = date($current_year . "-" . $current_month."-11");
            $date_end = date($current_year . "-" . $current_month."-25");

        } else {

            if ($current_day<=31 && $current_day>=26) {
                $date_start = date($current_year . "-" . $current_month."-26");
                $date_ending = new DateTime();
                $date_ending->modify("+1 month");
                $date_end = date($date_ending->format("Y") . "-". $date_ending->format("m") . "-10");
            } else {
                $date_starting = new DateTime();
                $date_starting->modify("-1 month");
                $date_start = date($date_starting->format("Y") . "-". $date_starting->format("m") . "-26");
                $date_end = date($current_year."-".$current_month."-10");

            }

        }

        $timesheets = Auth::user()->timesheets()
        ->whereBetween('schedule_date',[$date_start, $date_end])
        ->get();


        $list = [];
        foreach($timesheets as $timesheet) {
            $list[] = [
                'id' => $timesheet->id,
                'target_date' => $timesheet->schedule_date, //not changing target_date array value to schedule_date so that old client app will not have any errors.
                'client' => $timesheet->client->first_name . " " . $timesheet->client->last_name,
                'total_time' => $timesheet->total_time,
                'total_task_time' => $timesheet->total_task_time,
                'total_idletime' => $timesheet->total_idletime,
                'total_breaktime' => $timesheet->total_breaktime,
                'billable_hours' => $timesheet->billable_hours,
                'status' => $timesheet->status,
            ];
        }

        return response()->json([
            'status' => 'success',
            'data' => $list
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $timestamp = NULL)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
