<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class BrowserHistory extends Model {

    protected $table = "browser_history";

    /**
     * Fillable property.
     *
     * @var array
     */
    protected $fillable = [
        'id', 
        'user_id',
        'data',
        'minutes'
    ];

    public function user()
    {
        return $this->belongsTo("App\Models\User", "user_id");
    }

    public function list()
    {
        return json_decode($this->data);
    }

    public function screenshot()
    {
        return $this->hasOne('App\Models\Screenshot', 'browser_history_id');
    }
}