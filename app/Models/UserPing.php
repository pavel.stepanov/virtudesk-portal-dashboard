<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class UserPing extends Model {

    protected $table = "users_ping";


    public function user()
    {
        return $this->belongsTo("App\Models\User", "user_id");
    }

}
