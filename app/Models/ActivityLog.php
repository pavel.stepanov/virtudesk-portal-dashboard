<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ActivityLog extends Model {

    protected $table = "activity_log";

    /**
     * Fillable property.
     *
     * @var array
     */
    protected $fillable = ['id', 'first_name', 'last_name', 'role', 'log'];

    public static function addLog($log)
    {
        $auth_user = \Auth::user();

        //get first role
        foreach ($auth_user->roles as $r) {
            $role = $r->name;
            break;
        }

        ActivityLog::create([
            'first_name' => $auth_user->first_name,
            'last_name' => $auth_user->last_name,
            'role' => $role,
            'log' => $log
        ]);
    }
}