<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Report extends Model {

    const TYPE_SOD = "SOD";
    const TYPE_EOD = "EOD";

    protected $table = "reports";

    /**
     * Fillable property.
     *
     * @var array
     */
    protected $fillable = [
        'id', 
        'user_id', 
        'type',
        'report'
    ];

    public function user()
    {
        return $this->belongsTo("App\Models\User", "user_id");
    }
}