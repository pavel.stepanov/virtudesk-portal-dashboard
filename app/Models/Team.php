<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\User;

class Team extends Model {

    protected $table = "teams";

    /**
     * Fillable property.
     *
     * @var array
     */
    protected $fillable = ['id', 'name', 'lead_user_id'];

    public function users()
    {
        return $this->belongsToMany("App\Models\User")->withTimestamps();
    }


    public function addUser( $id ) {
    	$this->users()->attach( $id );
    }

    public function removeUser( $id ) {
        $this->users()->detach( $id );
    }

    public static function get_available_va( $id = null ) {

        $team_users_ids = DB::table('team_user')->select('user_id')->where('team_id', $id)->get();

        foreach ($team_users_ids as $key => $value) {
            $team_users_array[] = $value->user_id;
        }

   			return User::whereHas('roles', function ($query) {
				            $query->where('slug', 'va');
				        })
	   					->select( DB::raw( "users.id, CONCAT( first_name, ' ', last_name ) as name" ) )
                           ->whereNotIn( 'users.id',$team_users_array)
                           ->orderBy('name')
	   					->get();
    }

    public static function getAvailableManager($remove_id = null)
    {
        if ($remove_id!=null) {
            $team_leads = Team::where('lead_user_id', "<>", $remove_id)->pluck('lead_user_id')->all();
        } else {
            $team_leads = Team::pluck('lead_user_id')->all();
        }
        return User::HasRole('manager')->whereNotIn('id', $team_leads)->get();
    }

}