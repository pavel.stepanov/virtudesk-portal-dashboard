<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Dispute extends Model {

    protected $table = "dispute";

    /**
     * Fillable property.
     *
     * @var array
     */
    protected $fillable = [
        'id', 
        'user_id',
        'attendance_id',
        'description',
    ];

    public function user()
    {
        return $this->belongsTo("App\Models\User", "user_id");
    }

    public function attendance()
    {
        return $this->belongsTo("App\Models\Attendance", "attendance_id");
    }
}