<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TaskNote extends Model {

    protected $table = "task_notes";
    public $timestamps = false;

    /**
     * Fillable property.
     *
     * @var array
     */
    protected $fillable = ['id', 'task_id', 'user_id','notes', 'created_at'];

    public function user()
    {
        return $this->belongsTo("App\Models\User", "user_id");
    }

    public function task()
    {
        return $this->belongsTo("App\Models\ClientTask", "task_id");
    }

}