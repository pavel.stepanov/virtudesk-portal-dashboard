<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Timesheet extends Model {

    const STATUS_PENDING = "pending";
    const STATUS_APPROVED = "approved";
    const STATUS_DECLINED = "declined";

    //protected $attributes = ['schedule_date'];
    //protected $appends = ['schedule_date'];

    protected $table = "timesheets";

    /**
     * Fillable property.
     *
     * @var array
     */
    protected $fillable = [
        'id', 
        'target_date', 
        'schedule_date', 
        'client_id',
        'user_id',
        'attendance_id',
        'total_time',
        'total_task_time',
        'total_idletime',
        'total_breaktime',
        'billable_hours',
        'status'
    ];

    public function user()
    {
        return $this->belongsTo("App\Models\User", "user_id");
    }

    public function client()
    {
        return $this->belongsTo("App\Models\User", "client_id");
    }

    public function attendance()
    {
        return $this->belongsTo("App\Models\Attendance", "attendance_id");

    }

    public static function convertBillableHours($attendance)
    {
        $minutes = date('i', strtotime($attendance->total_time));
        $hour = date('H', strtotime($attendance->total_time));
        
        $total = 0;

        if ($minutes >= "25" && $minutes <= "29") {
            $total = $hour+0 .".50"; 
        } elseif ($minutes >= "55" && $minutes <= "59") {
            //+1 hour
            $total = ($hour+1).".00";
        } else {
            if ($minutes - ($minutes % 30) == 0) {
                $total = $hour+0 . ".00" ;
            } else {
                $total = $hour+0 . ".50" ;
            }
        }

        //if total exceeds hours needed, always use hours needed
        if ($attendance->hours_needed < $total ) {
            $total = $attendance->hours_needed;
        }

        $paid_break = 0;
        if ($attendance->paid_break!='') {
            $minutes = date('i', strtotime($attendance->paid_break));
            $hour = date('H', strtotime($attendance->paid_break));
            $paid_break = $hour + round($minutes / 60, 2);
        }

        $max_breaktime = 0;
        if ($attendance->max_breaktime!='') {
            $minutes = date('i', strtotime($attendance->max_breaktime));
            $hour = date('H', strtotime($attendance->max_breaktime));
            $max_breaktime = $hour + round($minutes / 60, 2);
        }

        $idle_time = 0;
        if ($attendance->total_idletime!='00:00:00' && $attendance->total_idletime!=null) {
            $minutes = date('i', strtotime($attendance->total_idletime));
            $hour = date('H', strtotime($attendance->total_idletime));
            $idle_time = $hour + round($minutes / 60, 2);

            //if idle time is less than 5 minutes, ignore
            if ( abs($hour) == 0  && abs($minutes) <= 5) {
                $idle_time = 0;
            }
        }

        $total_late = 0;
        if ($attendance->late_time!='00:00:00' && $attendance->late_time!=null) {
            $minutes = date('i', strtotime($attendance->late_time));
            $hour = date('H', strtotime($attendance->late_time));
            $total_late = $hour + round($minutes / 60, 2);

            //if late time is less than 15 minutes, ignore
            if ( abs($hour) == 0  && abs($minutes) < 15) {
                $total_late = 0;
            } else {
                //if more than 15 to 30 minutes
                if ( abs($minutes) >= 15  && abs($minutes) <=30 ) {
                    $minutes = 30;
                } else { //if 31 minutes and more, +1 hour
                    $minutes = 0;
                    $hour = abs($hour) + 1;
                }
                $total_late = $hour + round($minutes / 60, 2);
            }
        }

        $total_overbreak = 0;
        if ($attendance->total_breaktime!='00:00:00' && $attendance->total_breaktime!=null) {
            $minutes = date('i', strtotime($attendance->total_breaktime));
            $hour = date('H', strtotime($attendance->total_breaktime));
            $total_breaktime = $hour + round($minutes / 60, 2);

            if ($total_breaktime > $max_breaktime) {
                $total_overbreak = $total_breaktime - $max_breaktime;
                $hour = floor($total_overbreak);
                $minutes = ($total_overbreak - $hour) * 60;

                //if overbreak time is less than 15 minutes, ignore
                if ( abs($hour) == 0  && abs($minutes) < 15) {
                    $total_overbreak = 0;
                } else {
                    //if more than 15 to 30 minutes
                    if ( abs($minutes) >= 15  && abs($minutes) <=30 ) {
                        $minutes = 30;
                    } else { //if 31 minutes and more, +1 hour
                        $minutes = 0;
                        $hour = abs($hour) + 1;
                    }
                    $total_overbreak = $hour + round($minutes / 60, 2);
                }
            }
        }

        $total = $total + $paid_break - $max_breaktime - $idle_time;
        $total = $total - $total_late;
        $total = $total - $total_overbreak;

        return $total;

    }

}