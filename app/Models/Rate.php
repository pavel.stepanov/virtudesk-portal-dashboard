<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\RateLog;

class Rate extends Model {

    protected $table = "rates";

    /**
     * Fillable property.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'client_id', 'rate_per_hour'];

    public function addLog($array) {
    	RateLog::create($array);
    }

    public function getDefaultRate() {
        $rate = $this->where('user_id', $this->user_id)->where('client_id', 0)->first();
        return $rate->rate_per_hour;
    }

    public function getRatePerHourFormattedAttribute() {
        return number_format( $this->rate_per_hour, 2, '.', '' );
    }

    public function getClientRate($client_id) {
        return $this->where('user_id', $this->user_id)->where('client_id', $client_id)->first();
    }

    public function client() {
        return $this->hasOne('App\Models\User', 'id', 'client_id');
    }

    public function va() {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }
}