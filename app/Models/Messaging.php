<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class Messaging extends Model
{
protected $table = 'messaging';
    protected $fillable = ['to_id', 'from_id', 'message', 'file', 'status', 'reply_to'];
    public $timestamps = false;

    public function replies(){
    	return $this->hasMany(self::class, 'reply_to')->join('users', 'users.id', '=', 'from_id')->select(['messaging.*', 'users.first_name', 'users.last_name']);
    }

    public function sender(){
    	return $this->hasOne('App\Models\User', 'id', 'from_id')->select(['id', 'first_name', 'last_name']);
    }


}
