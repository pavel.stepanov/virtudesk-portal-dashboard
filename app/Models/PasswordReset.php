<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PasswordReset extends Model {

    protected $table = "password_resets";
    public $timestamps = false;

    /**
     * Fillable property.
     *
     * @var array
     */
    protected $fillable = ['email', 'token','created_at'];

}