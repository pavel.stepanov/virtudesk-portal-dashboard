<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Setting extends Model {

    protected $table = "settings";

    /**
     * Fillable property.
     *
     * @var array
     */
    protected $fillable = [
        'id', 
        'slug', 
        'name',
        'value'
    ];

    static public function getValueBySlug($slug) {
        if($setting = Setting::where('slug', $slug)->first()) {
            return $setting->value;
        }

        return null;
    }

    static public function slugify($text) {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }
}