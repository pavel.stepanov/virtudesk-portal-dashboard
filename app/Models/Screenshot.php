<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Screenshot extends Model {

    protected $table = "screenshots";

    /**
     * Fillable property.
     *
     * @var array
     */
    protected $fillable = [
        'id', 
        'user_id', 
        'client_id',
        'process_list_id', 
        'browser_history_id',
        'storage',
        'path',
        'filename',
        'mouse_activity',
        'keyboard_activity'
    ];

    public function user()
    {
        return $this->belongsTo("App\Models\User", "user_id");
    }

    public function client()
    {
        return $this->belongsTo("App\Models\User", "client_id");
    }

    public function processList()
    {
        return $this->belongsTo("App\Models\ProcessList", "process_list_id");
    }

    public function browserHistory()
    {
        return $this->belongsTo("App\Models\BrowserHistory", "browser_history_id");
    }

    public function mouseActivity()
    {
        $mouse_activity_config = config('vatimetracker.mouse_activity');

        $mouse_activity = "Low";
        foreach ($mouse_activity_config as $key => $value) {
            if ($this->mouse_activity >= $value) {
                $mouse_activity = $key;
            }
        }

        return $mouse_activity;
    }

    public function keyboardActivity()
    {
        $keyboard_activity_config = config('vatimetracker.keyboard_activity');

        $keyboard_activity = "Low";
        foreach ($keyboard_activity_config as $key => $value) {
            if ($this->keyboard_activity >= $value) {
                $keyboard_activity = $key;
            }
        }

        return $keyboard_activity;
    }

    public function getURLAttribute() {
        if($this->storage === "s3") {
            $bucket = env('AWS_BUCKET');
            return '//' . $bucket . '.s3.amazonaws.com/' . $this->path  . $this->filename;
        } else {
            return '/' . $this->path . $this->filename;
        }
    }

    public function getThumbnailURLAttribute() {
        if($this->storage === "s3") {
            $bucket = env('AWS_BUCKET') ?? 'virtudesk-crm';
            return '//' . $bucket . '.s3.amazonaws.com/' . $this->path . 'thumb_' . $this->filename;
        } else {
            return '/' . $this->path . 'thumb_' . $this->filename;
        }
    }

}