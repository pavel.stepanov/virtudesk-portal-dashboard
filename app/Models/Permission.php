<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model {

    protected $table = "permissions";

    /**
     * Fillable property.
     *
     * @var array
     */
    protected $fillable = ['name', 'slug', 'description'];
    /**
     * Relation to "Role".
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany("permissions")->withTimestamps();
    }
}