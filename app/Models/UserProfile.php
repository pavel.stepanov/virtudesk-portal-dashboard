<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class UserProfile extends Model {

    protected $table = "users_profile";


    public function user()
    {
        return $this->belongsTo("App\Models\User", "user_id");
    }

}
