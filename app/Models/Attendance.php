<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Attendance extends Model {

    const STATUS_IN = "in";
    const STATUS_OUT = "out";
    const STATUS_IDLE = "idle";
    const STATUS_BREAK = "break";

    const ACCEPTANCE_CONFIRM = "confirm";
    const ACCEPTANCE_DISPUTE = "dispute";

    protected $table = "attendance";

    /**
     * Fillable property.
     *
     * @var array
     */
    protected $fillable = [
        'id', 
        'user_id', 
        'schedule_id',
        'date_in', 
        'date_end', 
        'time_in', 
        'time_end', 
        'status',
        'client_id',
        'schedule_date',
        'start_time',
        'end_time',
        'shift',
        'timezone',
        'day_of_week',
        'year',
        'total_time',
        'total_breaktime',
        'total_idletime',
        'max_breaktime',
        'paid_break',
        'hours_needed',
        'late_reason',
        'late_time'
    ];

    public function user()
    {
        return $this->belongsTo("App\Models\User", "user_id");
    }

    public function client()
    {
        return $this->belongsTo("App\Models\User", "client_id");
    }    

    public function startTimeAMPM() {
        return date("h:i A",strtotime($this->start_time));
    }

    public function endTimeAMPM() {
        return date("h:i A",strtotime($this->end_time));
    }    

    public function getScheduleDetails()
    {
        return $this->startTimeAMPM() . " - " . $this->endTimeAMPM() . ": " . $this->timezone;
    }

    //when using this, be sure that all the fields used here are in the select query of attendance
    public function getRemark()
    {
        $u_timezone = new \DateTime($this->date_in . " " . $this->time_in, new \DateTimeZone($this->user->showTimezone()));
        $u_timezone->setTimezone(new \DateTimeZone($this->client->showTimezone()));
        $c_timezone = new \DateTime($this->schedule_date . ' ' . $this->start_time, new \DateTimeZone($this->client->showTimezone()));
        $interval = $u_timezone->diff($c_timezone);

        $min = round(( strtotime($u_timezone->format('Y-m-d H:i:s')) - strtotime($this->schedule_date . ' ' . $this->start_time)  ) / 60, 2);

        if ($min>0) {
            return "Late - " . $interval->format('%h:%I:%S');
        } else {
            return "On Time";
        }
    }

    public function timeInAMPM()
    {
        return date("h:i A",strtotime($this->time_in));
    }

    public function timeEndAMPM()
    {
        return date("h:i A",strtotime($this->time_end));
    }

    public function dispute()
    {
        return $this->hasOne('App\Models\Dispute', 'attendance_id');
    }

    public function userTasks()
    {
        return $this->hasMany('App\Models\UserTask', 'attendance_id');
    }

    public function getBreakStart() 
    {
        $b = BreakTime::where("attendance_id", $this->id)
        ->where("break_stop", NULL)->first();

        if (empty($b)) {
            return null;
        }

        return $b->break_start;
    }

    public function getIdles()
    {
        $idles = Idle::where("attendance_id", $this->id)
        ->get();

        return $idles;
    }

    public function getBreaks()
    {
        $breaks = BreakTime::where("attendance_id", $this->id)
        ->get();

        return $breaks;
    }

    public function showShift()
    {
        $shift_config = config('vatimetracker.shift');
        foreach ($shift_config as $key => $value) {
            if ($key == $this->shift) {
                return $value;
                break;
            }
        }
    }
}