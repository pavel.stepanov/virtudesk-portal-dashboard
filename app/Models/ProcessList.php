<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ProcessList extends Model {

    protected $table = "process_lists";

    /**
     * Fillable property.
     *
     * @var array
     */
    protected $fillable = [
        'id', 
        'user_id', 
        'data'
    ];

    public function user()
    {
        return $this->belongsTo("App\Models\User", "user_id");
    }

    public function list()
    {
        return explode(',', $this->data);
    }

    public function screenshot()
    {
        return $this->hasOne('App\Models\Screenshot', 'process_list_id');
    }
}