<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;
use App\Broadcaster;
use App\Models\Attendance;
use App\Models\BreakTime;
use App\Models\UserTask;
use App\Models\TimeInOut;
use DateTime;
use DateTimeZone;
use App\Models\TeamUser;

class User extends Authenticatable
{
    use Notifiable;
    use \App\Traits\AuthenticateTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 
        'last_name', 
        'mobile_number', 
        'email', 
        'password', 
        'skype_id', 
        'personal_email', 
        'alternate_contact_number',
        'birthdate',
        'is_verified',
        'verify_token',
        'is_active',
        'address1',
        'address2',
        'avatar',
        'timezone',
        'city',
        'ip_address',
        'company',
        'idle_interval',
        'screenshot_interval',
        'read_terms'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    private $broadcaster = false;

    public function isClockedIn($target_date = null)
    {
        if ($target_date==null) $target_date = date("Y-m-d", strtotime(now()));
        $attendance = Attendance::select(['schedule_date', 'user_id', 'status', 'created_at'])
       // ->where("schedule_date", $target_date)
        ->where("user_id", $this->id)
        ->where('status','in')
        ->orderBy('created_at', "DESC")
        ->first();

        if (empty($attendance)) return false;

        if ($attendance->status == 'out') return false;

        return true;
    }

    public function getCurrentTask()
    {
        $task = UserTask::where('user_id', $this->id)
            ->where('time_end', null)->first();

        if($task) {
            $attendance = Attendance::find($task->attendance_id);
            if($attendance && $attendance->status != Attendance::STATUS_OUT) {
                return $task;
            }
        }

        return null;
    }

    public function stopCurrentTask() {
        $task = UserTask::where('user_id', $this->id)
            ->where('time_end', null)->first();

        if (!empty($task)) {
            $now = time();
            $date_end = date('Y-m-d', $now);
            $time_end = date('H:i:s', $now);
            $task->date_end = $date_end;
            $task->time_end = $time_end;
            $task->save();
        }
    }

    public function broadcast()
    {
        if(!$this->broadcaster) {
            $this->broadcaster = new Broadcaster($this);
        }
        return $this->broadcaster;
    }

    /**
     * Relation to "Role".
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany('App\Models\Role')->withTimestamps();
    }

    public function scopeHasRole($query, $role)
    {
        return $this->whereHas('roles', function ($query) use ($role) {
            $query->where('slug', $role);
        });
    }

    public function scopeHasScheduleDay($query, $day)
    {
        return $this->whereHas('schedules', function ($query) use ($day) {
            $query->where('user_day_of_week', $day);
        });
    }

    public function screenshots()
    {
        return $this->hasMany('App\Models\Screenshot');
    }

    public function screenshotCount($target_date, $client_id = 0)
    {
        if ($client_id == 0) {
            $screenshot_count =  $this->screenshots()->whereDate('created_at', $target_date)
            ->where('user_id', $this->id)
            ->count();
        } else {
            $screenshot_count =  $this->screenshots()->whereDate('created_at', $target_date)
            ->where('user_id', $this->id)
            ->where('client_id', $client_id)
            ->count();
        }
        return $screenshot_count;
    }

    public function rates()
    {
        return $this->hasOne('App\Models\Rate', 'user_id');
    }

    public function client_rates()
    {
        return $this->hasOne('App\Models\ClientRate', 'user_id');
    }

    public function idleTimes()
    {
        return $this->hasMany('App\Models\Idle');
    }

    public function processLists()
    {
        return $this->hasMany('App\Models\ProcessList');
    }

    public function browserHistories()
    {
        return $this->hasMany('App\Models\BrowserHistory');
    }

    public function schedules()
    {
        return $this->hasMany('App\Models\Schedule');
    }
    
    public function getScheduleList($target_date, $day_of_week, $client_id = null)
    {
        $dt = new DateTime($target_date);

        if ($client_id == null) {
            $sched =  $this->schedules()->where('day_of_week', $day_of_week)
            ->where('user_id', $this->id)
            ->get();
            $html = "<div>No Schedule for selected date.</div>";
        } else {
            $sched =  $this->schedules()->where('day_of_week', $day_of_week)
            ->where('client_id', $client_id)
            ->where('user_id', $this->id)
            ->get();
            $html = "<div>This VA has no schedule assigned under you for selected date.</div>";
        }

        if (count($sched)>0) {
            $html = "";
            foreach ($sched as $s) {
                $html .= "<div>" . $s->day_of_week . " " . $s->startTimeAMPM() . " - " . $s->endTimeAMPM() . ": " . $s->timezone . " " . $s->showShift() . "</div>";
            }
        }
        return $html;
    }

    public function getScheduleListVA($target_date, $day_of_week, $client_id = null)
    {
        $dt = new DateTime($target_date);

        if ($client_id == null) {
            $sched =  $this->schedules()->where('user_day_of_week', $day_of_week)
            ->where('user_id', $this->id)
            ->get();
            $html = "<div>No Schedule for selected date.</div>";
        } else {
            $sched =  $this->schedules()->where('user_day_of_week', $day_of_week)
            ->where('client_id', $client_id)
            ->where('user_id', $this->id)
            ->get();
            $html = "<div>This VA has no schedule assigned under you for selected date.</div>";
        }

        if (count($sched)>0) {
            $html = "";
            foreach ($sched as $s) {
                $html .= "<div>" . $s->user_day_of_week . " " . date("h:i A",strtotime($s->user_start_time)) . " - " . date("h:i A",strtotime($s->user_end_time)) . ": " . $s->user_timezone . " " . $s->showShift() . "</div>";
            }
        }
        return $html;
    }

    public function getScheduleOnTime($day_of_week) 
    {

        $s = $this->schedules()->where('user_day_of_week', $day_of_week)
        ->whereRaw( "TIME('". date("H:i:s", strtotime(now())) . "') > AddTime(user_start_time,'00:10:00')")
        ->whereRaw( "TIME('". date("H:i:s", strtotime(now())) . "') < AddTime(user_start_time,'01:00:00')")
        ->first();

        if (!empty($s)) {
            if (!empty($s->client)) { 
                return $s->client->first_name . " " . $s->client->last_name . " - " . $s->user_day_of_week . " " . date("h:i A",strtotime($s->user_start_time)) . " - " . date("h:i A",strtotime($s->user_end_time)) . ": " . $s->user_timezone . " " . $s->showShift();
            } else {
                return "Client deleted from this schedule";
            }
        }
        return "No schedule";
    }

    public function convertToUserTimezone($datetime)
    {
        $user_timezone = new DateTime($datetime, new DateTimeZone("Asia/Manila"));
        $user_timezone->setTimezone(new DateTimeZone($this->showTimezone()));
        return $user_timezone;
    }

    public function attendanceList()
    {
        return $this->hasMany('App\Models\Attendance');
    }

    public function reports()
    {
        return $this->hasMany('App\Models\Report');
    }

    public function getCurrentSchedule()
    {
        $attendance = $this->getCurrentAttendance();

        if (!empty($attendance)) {
            $schedule = $this->schedules()
            ->where('day_of_week', $attendance->day_of_week)
            ->where('start_time', $attendance->start_time)
            ->where('end_time', $attendance->end_time)
            ->first();
        
            if($schedule) {
                return $schedule;
            }
        }
        
        return false;
    }

    public function getCurrentAttendance()
    {
        $attendance = $this->attendanceList()
            ->where('status', '!=', Attendance::STATUS_OUT)
            ->first();

        if($attendance) {
            return $attendance;
        }

        return false;
    }

    public function timeInOutList()
    {
        return $this->hasMany('App\Models\TimeInOut');
    }

    public function getCurrentClientID()
    {
        // $timeInOut = $this->timeInOutList()->whereNull('time_end')->first();
        $timeInOut = $this->timeInOutList()->orderBy('created_at','DESC')->first();
        if($timeInOut) {
            $attendance = Attendance::find($timeInOut->attendance_id);
            if($attendance) {
                return $attendance->client_id;
            }
        }

        return 0;
    }

    public function getCurrentClientName() {
        $client_id = $this->getCurrentClientID();

        if($schedule = $this->getCurrentSchedule()) {
            return $schedule->client->first_name . ' ' . $schedule->client->last_name  ;
        }

        return '';
    }

    public function startIdle()
    {
        $timestamp = time();
        if($attendance = $this->getCurrentAttendance()) {
            if($attendance->status != Attendance::STATUS_IDLE) {
                $attendance->status = Attendance::STATUS_IDLE;
                $attendance->save();

                $this->idleTimes()->save(new Idle([
                    'attendance_id' => $attendance->id,
                    'idle_start' => date("Y-m-d H:i:s")
                ]));
            }
        }
    }

    public function stopIdle(Attendance $attendance = null)
    {
        $now = date('Y-m-d H:i:s', time());
        $attendance = ($attendance == null) ? $this->getCurrentAttendance() : $attendance;
        if($attendance) {
            $idleTimes = $this->idleTimes()->where("attendance_id", $attendance->id)->get();
            $idles = [];
            $totalIdle = 0;
            foreach($idleTimes as $idle) {
                if(empty($idle->idle_stop)) {
                    $idle->idle_stop = $now;
                    $idle->save();
                }
                    $start = date_create($idle->idle_start);
                    $end = date_create($idle->idle_stop);
                    $interval = date_diff($start, $end);
                    $h = intval($interval->format('%h')) * 3600;
                    $i = intval($interval->format('%i')) * 60;
                    $s = intval($interval->format('%s'));
                    $totalIdle += $h + $i + $s;
            }

            $formatted = gmdate("H:i:s", $totalIdle);
            $attendance->total_idletime = $formatted;
            $attendance->save();

            return $attendance;
        }

        return false;
    }

    public function breakTimes()
    {
        return $this->hasMany('App\Models\BreakTime');
    }

    public function startBreak()
    {
        $timestamp = time();
        if($attendance = $this->getCurrentAttendance()) {
            if($attendance->status != Attendance::STATUS_BREAK) {
                $attendance->status = Attendance::STATUS_BREAK;
                $attendance->save();

                $this->breakTimes()->save(new BreakTime([
                    'attendance_id' => $attendance->id,
                    'break_start' => date("Y-m-d H:i:s")
                ]));
            }
        }
    }

    public function stopBreak(Attendance $attendance = null)
    {
        $now = date('Y-m-d H:i:s', time());
        $attendance = ($attendance == null) ? $this->getCurrentAttendance() : $attendance;
        if($attendance) {
            $breakTimes = $this->breakTimes()->where("attendance_id", $attendance->id)->get();
            $breaks = [];

            $totalBreak = 0;
            foreach($breakTimes as $bt) {
                if(empty($bt->break_stop)) {
                    $bt->break_stop = $now;
                    $bt->save();
                }
                    $start = date_create($bt->break_start);
                    $end = date_create($bt->break_stop);
                    $interval = date_diff($start, $end);
                    $h = intval($interval->format('%h')) * 3600;
                    $i = intval($interval->format('%i')) * 60;
                    $s = intval($interval->format('%s'));
                    $totalBreak += $h + $i + $s;
                
            }

            $formatted = gmdate("H:i:s", $totalBreak);
            $attendance->total_breaktime = $formatted;
            $attendance->save();

            return $attendance;
        }

        return false;
    }

    public function userTasks()
    {
        return $this->hasMany('App\Models\UserTask');
    }

    public function stopTask(Attendance $attendance = null)
    {
        $attendance = ($attendance == null) ? $this->getCurrentAttendance() : $attendance;

        if($attendance) {
            $userTasks = $this->userTasks()->where("attendance_id", $attendance->id)->get();
            $tasks = [];
            $now = time();
            $totalTasks = 0;
            foreach($userTasks as $ut) {
                if(empty($ut->time_end)) {
                    $ut->date_end = date('Y-m-d', $now);
                    $ut->time_end = date('H:i:s', $now);
                    $ut->save();
                }

                $start = new DateTime($ut->created_at);
                $end = new DateTime($ut->updated_at);
                $interval = $start->diff($end);
                $h = intval($interval->format('%h')) * 3600;
                $i = intval($interval->format('%i')) * 60;
                $s = intval($interval->format('%s'));
                $totalTasks += $h + $i + $s;
            }
            
            $formatted = gmdate("H:i:s", $totalTasks);
            $attendance->task_total_time = $formatted;
            $attendance->save();

            return $attendance;
        }

        return false;
    }

    public function disputes()
    {
        return $this->hasMany('App\Models\Dispute');
    }

    public function tasks() 
    {
        return $this->hasMany('App\Models\UserTask');
    }

    public function showTimezone()
    {
        $user_timezone = "";
        $timezone_config = config('vatimetracker.timezone');
        foreach ($timezone_config as $key => $value) {
            if ($key == $this->timezone) {
                $user_timezone = $value;
                break;
            }
        }
        return $user_timezone;
    }

    public function hourSummary($target_date) {
        $total_time = $this->attendanceList()
                    ->select(DB::raw('SEC_TO_TIME( SUM( TIME_TO_SEC( `total_time` ) ) ) as time_sum'))
                    ->whereDate('date_end', '>=', $target_date)->first();
        
        if( $total_time ) {
            return $total_time->time_sum;
        }

        return '00:00:00';
    }

    public function teams() {
        return $this->belongsToMany("App\Models\Team");
    }

    public function getFullNameAttribute() {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function timesheets()
    {
        return $this->hasMany('App\Models\Timesheet');
    }

    public function getRateBilling($va_id)
    {
        $client_rate = ClientRate::where("va_id", $va_id)
        ->where('user_id', $this->id)
        ->first();

        if (empty($client_rate)) {
            $client_rate = ClientRate::where("va_id", 0)
            ->where('user_id', $this->id)
            ->first();
        }

        if (empty($client_rate)) {
            return 0;
        }

        return $client_rate->rate_per_hour;
    }

    public function profile()
    {
        return $this->hasOne('App\Models\UserProfile', 'user_id');
    }

    public function pingUpdate()
    {
        return $this->hasOne('App\Models\UserPing', 'user_id');
    }
    
    public function myTeam(){
        return $this->hasMany('App\Models\TeamUser', 'user_id');
    }
    
    public function isActive(){
        return $this->is_active;
    }
}
