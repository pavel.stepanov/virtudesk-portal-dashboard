<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\User;

class RequestConcerns extends Model {

    protected $table = "requests";

    /**
     * Fillable property.
     *
     * @var array
     */
    protected $fillable = [
        'id', 
        'user_id', 
        'updated_by_id', 
        'details', 
        'notes', 
        'type', 
        'status',
        'uploaded_file',
        'overtime'
    ];

    public function user()
    {
        return $this->belongsTo("App\Models\User", "user_id");
    }

    public function teamLead()
    {
        return $this->belongsTo("App\Models\User", "updated_by_id");
    }

    public function showStatus()
    {
        $request_status = config('vatimetracker.request_status');
        foreach ($request_status as $key => $value) {
            if ($key == $this->status) {
                return $value;
                break;
            }
        }
    }

    public function showType()
    {
        $request_type = config('vatimetracker.request_type');
        foreach ($request_type as $key => $value) {
            if ($key == $this->type) {
                return $value;
                break;
            }
        }        
    }
}