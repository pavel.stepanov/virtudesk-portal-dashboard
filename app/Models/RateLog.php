<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class RateLog extends Model {

    protected $table = "rates_log";

    /**
     * Fillable property.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'client_id', 'old_rate_per_hour', 'new_rate_per_hour', 'updated_by'];

}