<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\UserTask;

class UserTask extends Model {

    const STATUS_STARTED = "started";
    const STATUS_ON_GOING = "on going";
    const STATUS_COMPLETED = "completed";

    const TASK_TYPE_GENERIC = "generic";
    const TASK_TYPE_CLIENT = "client";

    protected $table = "user_tasks";

    /**
     * Fillable property.
     *
     * @var array
     */
    protected $fillable = [
        'id', 
        'user_id', 
        'client_id', 
        'attendance_id', 
        'name', 
        'description', 
        'time_start', 
        'time_end', 
        'date_start', 
        'date_end',
        'task_type',
        'status',
    ];

    public function user()
    {
        return $this->belongsTo("App\Models\User", "user_id");
    }

    public function client()
    {
        return $this->belongsTo("App\Models\User", "client_id");
    }
}