<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Idle extends Model {

    protected $table = "idles";

    /**
     * Fillable property.
     *
     * @var array
     */
    protected $fillable = [
        'id', 
        'user_id',
        'attendance_id',
        'idle_start',
        'idle_stop'
    ];

    public function user()
    {
        return $this->belongsTo("App\Models\User", "user_id");
    }

    public function getidleTimeAttribute() {
        $idle_start = new \DateTime($this->idle_start);
        $idle_stop = new \DateTime($this->idle_stop);
        $interval = $idle_stop->diff($idle_start);
        $time_diff = $interval->format('%H:%I:%S');
        return $time_diff;
    }

    public function getIdleStartFormattedAttribute() {
        return date('m-d-Y H:i:s', strtotime($this->idle_start));
    }
}